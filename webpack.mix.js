const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js',
			 'js/admin/app.js')
		.sass('resources/assets/sass/app.scss', 'css/admin/app.css');  

mix.js('resources/assets/js_site/app.js',
			 'js/site.js');  

mix.disableNotifications();
