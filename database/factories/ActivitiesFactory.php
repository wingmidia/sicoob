<?php

$factory->define(App\Models\Activity::class, function (Faker\Generator $faker) {
    return [
				'works'  => $faker->text(200),
				'observation' => $faker->text(300),
				'start_date'  => $faker->dateTimeBetween('-1 days'),
				'end_date'    => $faker->dateTimeBetween('now', '+20 days'),
				'status' 			=> $faker->randomElement(['active', 'waiting', 'progress', 'open', 'complete', 'approved', 'reproved', 'canceled'])
    ];
});