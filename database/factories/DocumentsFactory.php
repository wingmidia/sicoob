<?php

$factory->define(App\Models\Document::class, function (Faker\Generator $faker) {
    return [
        'filename' 			=> $faker->file('./storage/app/content', './storage/app/documents', false),
				'extension' 		=> $faker->randomElement(['zip','doc','pdf']),
				'mimetype' 			=> $faker->mimeType,
				'type' 					=> $faker->randomElement(['folder','archive']),
				'permission' 		=> $faker->randomElement(['private','public']),
				'departaments' 	=> serialize([]),

    ];
});
	
	