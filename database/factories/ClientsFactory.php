<?php

$factory->define(App\Models\Client::class, function (Faker\Generator $faker) {
    return [
        'name'   => $faker->name,  
        'email'  => $faker->freeEmail,  
        'photo'  => $faker->imageUrl,  
        'date_birth'  => $faker->dateTimeBetween('-60 years'),
		'activity'    => $faker->word,  
		'status' 	  => $faker->boolean
    ];
});

$factory->define(App\Models\ClientAddress::class, function (Faker\Generator $faker) {
    return [
        'address1'  => $faker->streetName,  
        'address2'  => '',
        'number'  	=> $faker->numberBetween(100, 200),
		'district'  => 'Centro',
		'state'  	=> $faker->randomElement(['MG', 'SP', 'RJ']),
		'city'  	=> $faker->city,
        'postcode'  => $faker->postcode,
    ];
});

$factory->define(App\Models\ClientEmail::class, function (Faker\Generator $faker) {
    return [
        'email'  => $faker->email
    ];
});

$factory->define(App\Models\ClientPhone::class, function (Faker\Generator $faker) {
    return [
        'phone'  => $faker->phoneNumber
    ];
});