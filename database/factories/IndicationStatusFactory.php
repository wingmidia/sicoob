<?php

$factory->define(App\Models\IndicationStatus::class, function (Faker\Generator $faker) {
    return [
        'name'  => $faker->title
    ];
});