<?php

$factory->define(App\Models\Link::class, function (Faker\Generator $faker) {
    return [
    	  'name'      => $faker->word,
        'logo'      => $faker->imageUrl,
        'link_url'  => $faker->url,
        'clicks'    => 0,
        'status'    => $faker->boolean,
    ];
});