<?php

$factory->define(App\Models\Event::class, function (Faker\Generator $faker) {
    return [
        'title' 			=> $faker->paragraph(1),
        'local'  		    => $faker->streetAddress,
        'content' 			=> $faker->text(300), 
        'start_date' 		=> $faker->dateTime,
		'end_date' 			=> $faker->dateTime,  
		'image_feacture' 	=> $faker->imageUrl,  
		'status' 			=> $faker->boolean
				      
    ];
});