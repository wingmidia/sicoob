<?php

$factory->define(App\Models\Paycheck::class, function (Faker\Generator $faker) {

    return [
    		'month' 		=> $faker->numberBetween(1, 12),
    		'year' 			=> 2016,
				'filename'	=> 'file.pdf',//$faker->file()
        'status'    => $faker->randomElement(['SENDING','OPEN','PENDING']),  
    ];
});