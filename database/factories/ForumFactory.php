<?php

$factory->define(App\Models\ForumCategory::class, function (Faker\Generator $faker) {
    return [
        'name'  	 => $faker->words(5),
        'description'=> $faker->text(15), 
		'icon' 	     => '',
		'sorter'     => $faker->numberBetween(1, 10),  
		'status' 	 => $faker->boolean				      
    ];
});


$factory->define(App\Models\Forum::class, function (Faker\Generator $faker) {
    return [
        'title'      => $faker->text(10),
        'content'    => $faker->text(600), 
        'image_feacture'    => '', 
        'type'       => $faker->randomElement(['topic', 'post']),  
        'status'     => $faker->randomElement(['approved','reproved','waiting_approve','arquived','draft']),  
        
    ];
});

$factory->define(App\Models\ForumComments::class, function (Faker\Generator $faker) {
    return [
        'comments'  => $faker->text(15), 
        'status'    => $faker->boolean                      
    ];
});