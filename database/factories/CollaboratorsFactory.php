<?php

$factory->define(App\Models\Collaborator::class, function (Faker\Generator $faker) {
    return [
        'code' 				=> $faker->numberBetween(1, 999), 
        'name'				=> $faker->name, 
        'photo' 			=> $faker->image(storage_path('app/content'), 200, 200), 
        'phone'  			=> $faker->phoneNumber, 
        'date_birth' 	=>  $faker->dateTimeBetween('-18 years'), 
        'status' 	  	=> $faker->boolean
    ];
});