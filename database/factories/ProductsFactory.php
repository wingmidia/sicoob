<?php

$factory->define(App\Models\Product::class, function (Faker\Generator $faker) {
    return [
    		'title'      			=> $faker->title,
    		'content'  				=> $faker->text(300),
        'image_feacture'  => $faker->imageUrl,
        'status'    		  => $faker->boolean
    ];
});