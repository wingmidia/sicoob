<?php



$factory->define(App\Models\News::class, function (Faker\Generator $faker) {
    return [
        'title' 			=> $faker->paragraph(1),
        'description'  		=> $faker->text(50),
        'content' 			=> $faker->text(300), 
        'start_date' 		=> $faker->dateTimeBetween('-2 days'),
		'end_date' 			=> $faker->dateTimeBetween('-2 days','+30 days'),  
		'image_feacture' 	=> '',  
		'permission' 		=> $faker->randomElement(['public', 'private']),  
		'status' 			=> $faker->boolean
				      
    ];
});