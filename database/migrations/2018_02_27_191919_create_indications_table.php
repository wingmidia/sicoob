<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indications', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('client_id');
            $table->unsignedInteger('collaborator_id');
            $table->unsignedInteger('indication_status_id')
                ->nullable();
            $table->boolean('concluded')
                ->default(0);
            $table->dateTime('display_at');
            $table->timestamps();
            $table->foreign('client_id')
                ->references('id')
                ->on('clients');
            $table->foreign('collaborator_id')
                ->references('id')
                ->on('collaborators');
            $table->foreign('indication_status_id')
                ->references('id')
                ->on('indication_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('indications');
    }
}
