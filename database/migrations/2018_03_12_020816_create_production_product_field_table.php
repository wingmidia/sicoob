<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionProductFieldTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('production_product_field', function (Blueprint $table) {
            $table->unsignedInteger('production_id');
            $table->unsignedInteger('product_fields_id');
            $table->longText('value');
            $table->foreign('production_id')
                ->references('id')
                ->on('productions')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('product_fields_id')
                ->references('id')
                ->on('product_fields')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('production_product_field');
    }
}
