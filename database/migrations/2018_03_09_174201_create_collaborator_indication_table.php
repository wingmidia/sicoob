<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollaboratorIndicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collaborator_indication', function (Blueprint $table) {
            $table->unsignedInteger('indication_id');
            $table->unsignedInteger('collaborator_id');
            $table->foreign('indication_id')
                ->references('id')
                ->on('indications')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('collaborator_id')
                ->references('id')
                ->on('collaborators')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collaborator_indication');
    }
}
