<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDepartmentsNew extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('departaments', function (Blueprint $table) {
            $table->string('description');
            $table->string('phone');
            $table->string('address');
            $table->string('district');
            $table->string('cnpj');
            $table->string('cep');
            $table->string('email');
            $table->string('photo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('departaments', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('phone');
            $table->dropColumn('address');
            $table->dropColumn('district');
            $table->dropColumn('cnpj');
            $table->dropColumn('cep');
            $table->dropColumn('email');
            $table->dropColumn('photo');
        });
    }
}
