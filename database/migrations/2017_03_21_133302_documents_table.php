<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id');
            $table->unsignedInteger('collaborator_id')
                  ->nullable();
            $table->string('filename');
            $table->string('extension');
            $table->string('mimetype');
            $table->enum('type', ['folder', 'archive']);
            $table->enum('permission', ['private', 'public']);
            $table->longText('departaments');
            $table->timestamps();  

            $table->foreign('collaborator_id')
                  ->references('id')->on('collaborators')
                  ->onDelete('set null');          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}

