<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsPhotosTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('news_photos', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('new_id');
            $table->string('image');

            $table->foreign('new_id')
                  ->references('id')->on('news')
                  ->onDelete('cascade');    
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('news_photos');
	}

}
