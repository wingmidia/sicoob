<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateQuizTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quizzes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->timestamp('start_date')
                    ->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('end_date')
                    ->nullable();
            $table->text('description')
                    ->nullable();
            $table->integer('time')
                    ->nullable();
            $table->boolean('active')
                ->default(0);

            $table->timestamps();
        });

        Schema::create('quiz_asks', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title');

            $table->integer('weight')
                ->default(1);

            $table->boolean('active')
                ->default(0);

            $table->unsignedInteger('quiz_id');

            $table->foreign('quiz_id')
                ->references('id')
                ->on('quizzes')
                ->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('quiz_results', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('right_answers')
                ->default(0);
            $table->integer('wrong_answers')
                ->default(0);
            $table->timestamp('started_at')
                  ->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('ended_at')
                  ->nullable();
            $table->boolean('terminated')
                    ->default(0);
            $table->unsignedInteger('quiz_id');
            $table->unsignedInteger('collaborator_id');

            $table->foreign('quiz_id')
                ->references('id')
                ->on('quizzes')
                ->onDelete('cascade');

            $table->foreign('collaborator_id')
                ->references('id')
                ->on('collaborators')
                ->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('quiz_ask_alternatives', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title');
            $table->boolean('correct')
                ->default(0);
            $table->boolean('active')
                ->default(0);

            $table->unsignedInteger('quiz_ask_id');

            $table->foreign('quiz_ask_id')
                ->references('id')
                ->on('quiz_asks')
                ->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('quiz_ask_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title');
            $table->unsignedInteger('quiz_ask_id');
            $table->unsignedInteger('quiz_ask_alternative_id');
            $table->unsignedInteger('quiz_result_id');

            $table->foreign('quiz_ask_id')
                ->references('id')
                ->on('quiz_asks')
                ->onDelete('cascade');

            $table->foreign('quiz_ask_alternative_id')
                ->references('id')
                ->on('quiz_ask_alternatives')
                ->onDelete('cascade');

            $table->foreign('quiz_result_id')
                ->references('id')
                ->on('quiz_results')
                ->onDelete('cascade');

            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quizzes');
        Schema::dropIfExists('quiz_asks');
        Schema::dropIfExists('quiz_results');
        Schema::dropIfExists('quiz_ask_alternatives');
        Schema::dropIfExists('quiz_ask_answers');
    }
}
