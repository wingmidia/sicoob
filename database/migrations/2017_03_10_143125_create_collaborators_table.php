<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollaboratorsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('collaborators', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('code', 200); // matrícula
            $table->string('name', 200);
            $table->string('photo', 255)
            	  ->nullable();
            $table->string('phone', 30)
            	  ->nullable();
            $table->date('date_birth')
            	  ->nullable();
            $table->boolean('status');
            $table->timestamps();
		});

		Schema::create('collaborator_departaments', function(Blueprint $table) {
            $table->unsignedInteger('collaborator_id');
            $table->unsignedInteger('departament_id');
		});

        Schema::table('collaborators', function (Blueprint $table) {
            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');
        });

        Schema::table('collaborator_departaments', function (Blueprint $table) {
            $table->foreign('collaborator_id')
                  ->references('id')->on('collaborators')
                  ->onDelete('cascade');
        });

        Schema::table('collaborator_departaments', function (Blueprint $table) {
            $table->foreign('departament_id')
                  ->references('id')->on('departaments')
                  ->onDelete('cascade');
        });

        Schema::table('activities', function (Blueprint $table) {
            $table->foreign('collaborator_id')
                  ->references('id')->on('collaborators')
                  ->onUpdate('SET NULL')
                  ->onDelete('SET NULL');
        });

        Schema::table('paychecks', function (Blueprint $table) {
            $table->foreign('collaborator_id')
                  ->references('id')->on('collaborators')
                  ->onDelete('cascade');
        });                  
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('collaborators', function (Blueprint $table) {
            $table->dropForeign('forum_comments_collaborator_id_foreign');
            $table->dropForeign('forum_comments_topic_id_foreign');
        });
		Schema::dropIfExists('collaborators');

		Schema::dropIfExists('collaborator_departaments');
	}

}