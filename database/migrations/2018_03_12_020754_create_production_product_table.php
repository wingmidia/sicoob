<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('production_product', function (Blueprint $table) {
            $table->unsignedInteger('production_id');
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('indication_id')
                ->nullable();
            $table->decimal('value', 9,2);
            $table->date('sold_at');
            $table->foreign('production_id')
                ->references('id')
                ->on('productions')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('indication_id')
                ->references('id')
                ->on('indications');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('production_product');
    }
}
