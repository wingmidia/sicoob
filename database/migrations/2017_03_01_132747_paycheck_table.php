<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PaycheckTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paychecks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('collaborator_id');
            $table->unsignedInteger('month');
            $table->unsignedInteger('year');
            $table->string('filename', 100);
            $table->enum('status', ['SENDING', 'OPEN', 'PENDING', 'COMPLETE']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paychecks');
    }
}
