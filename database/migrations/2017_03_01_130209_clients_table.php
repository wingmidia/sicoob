<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('address_id')
                  ->nullable(); // endereço default
            $table->unsignedInteger('departament_id')
                  ->nullable(); // departamento
            $table->string('name');
            $table->string('email');
            $table->string('photo');
            $table->date('date_birth');
            $table->string('activity'); // função
            $table->boolean('status');
            $table->timestamps();
        });


        Schema::create('client_address', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('client_id');
            $table->string('address1');// endereço
            $table->string('address2'); // complemento
            $table->string('district'); // bairro
            $table->string('number'); // número
            $table->string('state'); // estado
            $table->string('city'); // cidade
            $table->string('postcode'); // cep
        });

        Schema::create('client_emails', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('client_id');
            $table->string('email');
        });

        Schema::create('client_phones', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('client_id');
            $table->string('phone');
        });

        // add foreign
        Schema::table('clients', function (Blueprint $table) {
            $table->foreign('address_id')
                  ->references('id')->on('client_address')
                  ->onDelete('no action');

            $table->foreign('departament_id')
                  ->references('id')->on('departaments')
                  ->onDelete('no action');
                  
        });

        Schema::table('client_address', function (Blueprint $table) {
            $table->foreign('client_id')
                  ->references('id')->on('clients')
                  ->onDelete('cascade');
        });

        Schema::table('client_emails', function (Blueprint $table) {
            $table->foreign('client_id')
                  ->references('id')->on('clients')
                  ->onDelete('cascade');
        });
        
        Schema::table('client_phones', function (Blueprint $table) {
            $table->foreign('client_id')
                  ->references('id')->on('clients')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
        Schema::dropIfExists('client_address');
        Schema::dropIfExists('client_emails');
        Schema::dropIfExists('client_phones');
    }
}
