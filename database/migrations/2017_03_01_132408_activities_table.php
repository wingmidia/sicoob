<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('collaborator_id')
                  ->nullable();
            $table->unsignedInteger('client_id')
                  ->nullable();;
            $table->unsignedInteger('product_id')
                  ->nullable();
            $table->text('works');
            $table->longText('observation');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->enum('status', ['active', 'waiting', 'progress', 'open', 'complete', 'approved', 'reproved', 'canceled']);
            $table->timestamps();

            $table->foreign('client_id')
                  ->references('id')->on('clients')
                  ->onDelete('SET NULL')
                  ->onUpdate('SET NULL');

            $table->foreign('product_id')
                  ->references('id')->on('products')
                  ->onDelete('SET NULL')
                  ->onUpdate('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities');
    }
}
