<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function (Blueprint $table) {
            
            $table->dropForeign('clients_address_id_foreign');
            $table->dropColumn('address_id');
            $table->string('photo', 100)->nullable()->change();
            $table->date('date_birth')->nullable()->change();
            $table->date('activity')->nullable()->change();

            $table->unsignedInteger('collaborator_id')
                  ->after('departament_id');

            $table->foreign('collaborator_id')
                  ->references('id')->on('collaborators')
                  ->onDelete('cascade');
            

            $table->string('state')
                  ->nullable()
                  ->after('photo');

            $table->string('city')
                  ->nullable()
                  ->after('photo');

            $table->string('complement')
                  ->nullable()
                  ->after('photo');

            $table->integer('number')
                  ->nullable()
                  ->after('photo');

            $table->string('district', 150)
                  ->nullable()
                  ->after('photo');

            $table->string('address')
                  ->nullable()
                  ->after('photo');

            $table->string('postcode')
                  ->nullable()
                  ->after('photo');
           

            
            $table->string('cellphone', 40)
                  ->nullable()
                  ->after('name');

            $table->string('phone2', 40)
                  ->nullable()
                  ->after('name');

            $table->string('phone1', 40)
                  ->nullable()
                  ->after('name');

            $table->string('cnpj')
                  ->nullable()
                  ->after('name');

            $table->string('cpf')
                  ->nullable()
                  ->after('name');

            $table->string('company_name')
                  ->nullable()
                  ->after('name');

            $table->enum('type_profile', ['J', 'F'])
                  ->default('J')
                  ->after('departament_id');   
        });
        
        Schema::dropIfExists('client_address');
        Schema::dropIfExists('client_emails');
        Schema::dropIfExists('client_phones');


        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
