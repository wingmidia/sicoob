<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('documents');

        Schema::create('documents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('filename');
            $table->string('original_name');
            $table->string('extension');
            $table->string('mimetype');
            $table->boolean('status')
                  ->default(1);
            $table->timestamps();              
        });

        Schema::create('documents_departaments', function (Blueprint $table) {
            $table->unsignedInteger('document_id');
            $table->unsignedInteger('departament_id');

            $table->foreign('document_id')
                  ->references('id')->on('documents')
                  ->onDelete('cascade');

            $table->foreign('departament_id')
                  ->references('id')->on('departaments')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
