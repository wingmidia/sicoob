<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsPhotosTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('events_photos', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('event_id');
            $table->string('image');

            $table->foreign('event_id')
                  ->references('id')->on('events')
                  ->onDelete('cascade');    
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('events_photos');
	}

}
