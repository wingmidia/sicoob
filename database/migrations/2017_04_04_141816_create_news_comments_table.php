<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsCommentsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('news_comments', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('replay_id');
            $table->unsignedInteger('new_id');
            $table->unsignedInteger('collaborator_id')
            		  ->nullable();
            $table->longText('comments');
            $table->boolean('status')
            			->default(0);
            $table->timestamps();

            $table->foreign('new_id')
                  ->references('id')->on('news')
                  ->onDelete('cascade'); 
            $table->foreign('collaborator_id')
                  ->references('id')->on('collaborators')
                  ->onDelete('set null');  
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('news_comments');
	}

}
