<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->text('description');
            $table->longText('content');
            $table->dateTime('start_date')
                  ->nullable();
            $table->dateTime('end_date')
                  ->nullable();
            $table->string('image_feacture')
                  ->nullable();
            $table->enum('permission', ['public', 'private']);
            $table->boolean('status');
            $table->timestamps();
        });

        Schema::create('news_departaments', function (Blueprint $table) {
            $table->unsignedInteger('new_id');
            $table->unsignedInteger('departament_id');
        });

        /*Schema::create('news_permissions', function (Blueprint $table) {
            $table->integer('new_id');
            $table->integer('permission_id');
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
        Schema::dropIfExists('news_departaments');
        //Schema::dropIfExists('news_permissions');
    }
}
