<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Models\Activity;
use App\Models\Product;
use App\Models\Departament;
use App\Models\Client;
use App\Models\Link;
use App\Models\Paycheck;
use App\Models\News;
use App\Models\Event;
use App\Models\Collaborator;
use App\Models\IndicationStatus;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call(PermissionsTableSeeder::class);
        //$this->call(IndicationStatusSeeder::class);

        if( ! IndicationStatus::where('id', 9000)->count())
            factory(IndicationStatus::class)->create([
                'id' => 9000,
                'name' => 'Cancelada',
                'state' => 1,
                'status' => 1
            ]);

        if( ! IndicationStatus::where('id', 9001)->count())
            factory(IndicationStatus::class)->create([
                'id' => 9001,
                'name' => 'Prospectada',
                'state' => 1,
                'status' => 1
            ]);

        //$this->call(RolesPermissions::class);
        //$this->call(ForumSeeder::class);
    	// usuário admin
    	//$master = factory(User::class, 1)->create(['email' => 'admin@admin.com']);
    	// create links
    	//factory(Link::class, 10)->create();
    	// create events
    	//factory(Event::class, 10)->create();

    	/*$departaments = [
    		['name' => 'Marketing'],
    		['name' => 'TI'],
    		['name' => 'RH'],
    		['name' => 'Carteira'],
    		['name' => 'Cadastro'],
    		['name' => 'Cobrança'],
    		['name' => 'CI'],
    		['name' => 'Diretoria'],
            ['name' => 'Geral']
    	];

    	foreach($departaments as $departament){
				factory(Departament::class)
							->create($departament);
    	};*/
    	/*unset($departaments); // remove variable

    	$departaments = Departament::all();

    	$users   = factory(User::class, 10)->create();
    	$clients = factory(Client::class, 5)->create();

	    $products = factory(Product::class, 10)->create();

        foreach ($users as $user) {
            factory(Collaborator::class, 1)->create([
                    'user_id' => $user->id, 
                    'name' => $user->name,
                    'status' => $user->status,
                ]);
        }

        $collaborators = Collaborator::get();

    	foreach($products as $product){
				factory(Activity::class, 2)->create([
        										'collaborator_id' => $collaborators->random()->id, 
        										'product_id'  => $product->id,
        										'client_id'   => $clients->random()->id 
        									]);
    	}

		foreach($users as $user){
    		factory(Paycheck::class, 2)
						->create(['collaborator_id' => $collaborators->random()->id]);
    	}

        // create news
        $news =  factory(News::class, 30)->create();*/
    }
}
