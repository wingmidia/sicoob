<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;
use App\Models\Role;
use App\User;
use Zizaco\Entrust\EntrustFacade as Entrust;

class RolesPermissions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    /* 
    * Diretoria - Acessa admin. Visualiza todas as demandas de menu Atividades 
    * Gerentes - Cadastra Atividades, Clientes
    * Marketing - Acessa admin. Cadastra noticias, eventos.
    * TI - Acesso master
    * RH - Acesso ao cadastro de Contra-cheques*/
      
      Permission::where('id', '!=', 0)->delete();
      Role::where('id', '!=', 0)->delete();

      $roles = [
          ['display_name' => 'Administrador', 'name' => 'admin'],
          ['display_name' => 'TI'],
          ['display_name' => 'Diretoria'],
          ['display_name' => 'Gerente'],
          ['display_name' => 'Marketing'],
          ['display_name' => 'RH'],
      ];   

      foreach($roles as $role){
        factory(Role::class)
                ->create($role);
      };

      $permissions = [
          ['display_name' => 'Atividades', 'name' => 'activity-owner'],
          ['display_name' => 'Atividades (admin)', 'name' => 'activity-admin'],

          ['display_name' => 'Clientes', 'name' => 'client-owner'],
          ['display_name' => 'Clientes (admin)', 'name' => 'client-admin'],
      ]; 
			
      foreach($permissions as $permission){
        factory(Permission::class)
                ->create($permission);
      }; 

      
      $roles = Role::where('name', 'admin')->first(); // to admin
      $permissionsAll = Permission::get()->pluck('id');
      $roles->savePermissions($permissionsAll->all());

      $rolesTi = Role::where('name', 'ti')->first(); // to admin
      $rolesTi->savePermissions($permissionsAll->all());

      $rolesDiretoria = Role::where('name', 'diretoria')->first(); // to diretoria
      $permissionsDiretoria = Permission::whereIn('name', ['activity-admin', 'client-admin'])->get()->pluck('id');
      $rolesDiretoria->savePermissions($permissionsDiretoria->all());

      $rolesDiretoria = Role::where('name', 'gerente')->first(); // to diretoria
      $permissionsDiretoria = Permission::whereIn('name', ['activity-owner', 'client-owner'])->get()->pluck('id');
      $rolesDiretoria->savePermissions($permissionsDiretoria->all());

      


    }
}
