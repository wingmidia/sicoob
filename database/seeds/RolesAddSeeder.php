<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;
use App\Models\Role;
use App\User;
use Zizaco\Entrust\EntrustFacade as Entrust;

class RolesAddSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Role::where('name', '=', 'colaborador')->delete();

      $role = ['display_name' => 'Colaborador'];   

      factory(Role::class)
                ->create($role);
    }
}
