<?php

use Illuminate\Database\Seeder;
use App\Models\Forum;
use App\Models\ForumCategory;
use App\Models\ForumComments;
use Carbon\Carbon;

class ForumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
        	['name' => 'Financeiro', 'description' => 'Dúvidas e assuntos voltados ao financeiro.', 'sorter' => 1, 'icon' => '', 'status' => 1, 'created_at' => Carbon::now()],
        	['name' => 'Jurídico', 'description' => 'Dúvidas e assuntos voltados ao juriídico.', 'sorter' => 2, 'icon' => '', 'status' => 1, 'created_at' => Carbon::now()],
        	['name' => 'Tutoriais', 'description' => 'Resumo de conteúdo de apreendizado interno.', 'sorter' => 3, 'icon' => '', 'status' => 1, 'created_at' => Carbon::now()],
        	['name' => 'Fora de Tópico', 'description' => 'Assuntos Gerais', 'sorter' => 4, 'icon' => '', 'status' => 1, 'created_at' => Carbon::now()],

        	['name' => 'Processo Seletivo Interno', 'description' => 'Divulgação de oportunidades.', 'sorter' => 5, 'icon' => '', 'status' => 1, 'created_at' => Carbon::now()],
        	['name' => 'Anúncios', 'description' => 'Divulgação de conteúdo importante.', 'sorter' => 6, 'icon' => '', 'status' => 1, 'created_at' => Carbon::now()],
        ];

        // cria categorias
        DB::table('forum_categories')->insert($categories);
        
        $categories = ForumCategory::get();
				
        foreach($categories as $category)
        {
            // cria tópicos
            factory(Forum::class, rand(2, 5))->create(['category_id' => $category->id, 'type' => 'topic', 'status' => 'approved']);
        }

        $topics = Forum::where('type', 'topic')->get();

        foreach($topics as $topic)
        {       	
			// cria tópicos posts
			factory(Forum::class, rand(2, 5))->create(['category_id' => $topic->category_id, 'topic_id' => $topic->id, 'type' => 'post']);
        }
        
    }
}
