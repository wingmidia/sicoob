<?php

use Illuminate\Database\Seeder;
use App\Models\Collaborator;
use App\Models\Document;
use App\User;

class DocumentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		  	$users   = factory(User::class, 5)->create();

	      foreach ($users as $user) {
	          factory(Collaborator::class, 1)->create([
	                  'user_id' => $user->id, 
	                  'name' 		=> $user->name,
	                  'status'  => $user->status,
	              ]);
	      }

	      $collaborators = Collaborator::get();

	      $parent_id = 0;
        foreach ($collaborators as $collaborator) {
        	$folder = factory(Document::class)->create([
        																					'parent_id' 			=> $parent_id, 
        																					'collaborator_id' => $collaborator->id, 
        																					'filename' 				=> 'Folder '.rand(1,999),
        																					'type' 						=> 'folder'
        																				]);

        	factory(Document::class, 2)->create([
        																					'parent_id' 			=> $parent_id, 
        																					'collaborator_id' => $collaborator->id, 
        																					'type' 						=> 'archive'
        																				]);
        	$parent_id = $folder->id;
        }
        
    }
}
