<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;
use App\Models\Role;
use App\User;
use Zizaco\Entrust\EntrustFacade as Entrust;
use App\Models\Config;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Config::create([
        'name' => 'Exibir Rankeamento',
        'value' => 'off',
        'type' => 'checkbox',
        'key' => 'rank'
      ]);
      Config::create(['name' => 'SMTP - Driver','value' => 'smtp','type' => 'text','key' => 'driverSMTP']);
      Config::create(['name' => 'SMTP - Servidor','value' => 'netmail.crediminas.com.br','type' => 'text','key' => 'hostSMTP']);
      Config::create(['name' => 'SMTP - Porta','value' => '587','type' => 'text','key' => 'portSMTP']);
      Config::create(['name' => 'SMTP - Email de quem esta enviando','value' => 'acelera@sicoobcredipatos.com.br','type' => 'text','key' => 'addressSMTP']);
      Config::create(['name' => 'SMTP - Nome de quem esta enviando','value' => 'Acelera Credipatos','type' => 'text','key' => 'nameSMTP']);
      Config::create(['name' => 'SMTP - Email','value' => 'takio.moreira@sicoobcredipatos.com.br','type' => 'text','key' => 'usernameSMTP']);
      Config::create(['name' => 'SMTP - Senha','value' => 'Ftpwidc22*','type' => 'text','key' => 'passwordSMTP']);
      Config::create(['name' => 'SMTP - Criptografia','value' => 'tls','type' => 'text','key' => 'encryptionSMTP']);

      if( ! Permission::where('name', 'custom-config')->count() ){

        $permissions = [
          ['display_name' => 'Configurações do Sistema', 'name' => 'custom-config']
        ];

        foreach($permissions as $permission){
          factory(Permission::class)->create($permission);
        };

      }

      $roles = Role::where('name', 'admin')->first(); // to admin
      $permissionsAll = Permission::get()->pluck('id');
      $roles->savePermissions($permissionsAll->all());

    }
}
