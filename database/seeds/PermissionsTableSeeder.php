<?php

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Add Perfil de usuario
        if( ! Permission::where('name', 'roles')->count() ){

            $permissions = [
                ['display_name' => 'PERFIL DE USUÁRIO - Tudo', 'name' => 'roles']
            ];

            foreach($permissions as $permission){

                factory(Permission::class)->create($permission);

            };

        }

        // Add Produto
        if( ! Permission::where('name', 'products')->count() ){

            $permissions = [
                ['display_name' => 'PRODUTOS - Tudo', 'name' => 'products']
            ];

            foreach($permissions as $permission){

                factory(Permission::class)->create($permission);

            };

        }

        // Add Indicacao
        if( ! Permission::where('name', 'indications')->count() ){

            $permissions = [
                ['display_name' => 'INDICAÇÃO STATUS - Tudo', 'name' => 'indication-status'],
                ['display_name' => 'INDICAÇÃO - Visualizar', 'name' => 'indications'],
                ['display_name' => 'INDICAÇÃO - Criar', 'name' => 'indications-create'],
                ['display_name' => 'INDICAÇÃO - Atualizar', 'name' => 'indications-update'],
                ['display_name' => 'INDICAÇÃO - Excluir', 'name' => 'indications-delete'],
                ['display_name' => 'INDICAÇÃO - Prospecção', 'name' => 'indications-prospection'],

            ];

            foreach($permissions as $permission){

                factory(Permission::class)->create($permission);

            };

        }

        // Add Producao
        if( ! Permission::where('name', 'productions')->count() ){

            $permissions = [
                ['display_name' => 'PRODUÇÃO - Visualizar', 'name' => 'productions'],
                ['display_name' => 'PRODUÇÃO - Criar', 'name' => 'productions-create'],
                ['display_name' => 'PRODUÇÃO - Atualizar', 'name' => 'productions-update'],
                ['display_name' => 'PRODUÇÃO - Excluir', 'name' => 'productions-delete'],
                ['display_name' => 'PRODUÇÃO - Exportar', 'name' => 'productions-export'],
                ['display_name' => 'PRODUÇÃO - Gerenciar', 'name' => 'productions-manager'],

            ];

            foreach($permissions as $permission){

                factory(Permission::class)->create($permission);

            };

        }

        // Add Departamento
        if( ! Permission::where('name', 'departaments')->count() ){

            $permissions = [
                ['display_name' => 'DEPARTAMENTOS - Tudo', 'name' => 'departaments']
            ];

            foreach($permissions as $permission){

                factory(Permission::class)->create($permission);

            };

        }

        // Add Quiz
        if( ! Permission::where('name', 'quiz')->count() ){

            Permission::whereIn('name', ['quizzes', 'quiz-create', 'quiz-edit', 'quiz-delete', 'quizzes-export', 'quiz-do'])
                ->delete();

            $permissions = [
                ['display_name' => 'QUIZZES - Vizualizar', 'name' => 'quiz'],
                ['display_name' => 'QUIZZES - Responder', 'name' => 'quiz-answer'],
                ['display_name' => 'QUIZZES - Exportar', 'name' => 'quiz-export'],
                ['display_name' => 'QUIZZES - Gerenciar', 'name' => 'quiz-manager'],
            ];

            foreach($permissions as $permission){

                factory(Permission::class)->create($permission);

            };

        }

        // Add Campanhas
        if( ! Permission::where('name', 'campaigns')->count() ){

            $permissions = [
                ['display_name' => 'CAMPANHAS - Vizualizar', 'name' => 'campaigns'],
                ['display_name' => 'CAMPANHAS - Criar', 'name' => 'campaign-create'],
                ['display_name' => 'CAMPANHAS - Editar', 'name' => 'campaign-edit'],
                ['display_name' => 'CAMPANHAS - Excluir', 'name' => 'campaign-delete'],
                ['display_name' => 'CAMPANHAS - Exportar', 'name' => 'campaign-export']
            ];

            foreach($permissions as $permission){

                factory(Permission::class)->create($permission);

            };

        }



        $roles = Role::where('name', 'admin')->first(); // to admin
        $permissionsAll = Permission::get()->pluck('id');
        $roles->savePermissions($permissionsAll->all());

        //$rolesTi = Role::where('name', 'ti')->first(); // to ti
        //$rolesTi->savePermissions($permissionsAll->all());

    }
}
