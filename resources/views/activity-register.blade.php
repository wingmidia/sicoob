@extends('layouts.sicoob.default')

@section('arquivos_estilos')
<link rel="stylesheet" href="/css/jquery-ui.css">
@endsection

@section('arquivos_scripts')
<script src="/js/jquery.meio.mask.min.js"></script>
  <script src="/js/jquery-ui.js"></script>
@endsection

@section('blocos_script')
// Calendario para os campos de data
      $( ".date-picker" ).datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
        dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
        monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez']
      });

      // Máscaras para campos dos formulários
      $('.fmtData').setMask('date');
      $('.fmtHora').setMask('time');
      


@endsection

@section('content')
  <div class="fundo-branco sombra-caixa ctn-atividades">
    <h1 class="titulo-janelas">
      <i class="icomoon icon-cog-outline"></i>
      <div>ATIVIDADES</div>
    </h1>
    <form class="form-horizontal frm-atividade" action="{{ route('activity.store') }}" method="POST" name="form-activity">
      {{ csrf_field() }}
      <fieldset>
        <div class="form-group">
          <label for="produto" class="col-sm-1 control-label">Produto:</label>
          <div class="col-sm-5  {{ $errors->has('product_id')? 'has-error' : null }}">
            <select name="product_id" id="product_id" class="form-control">
              <option value="">Selecione</option>
              @foreach(getProducts() as $product)
              <option value="{{$product->id}}" {{ $product->id==old('product_id') ? 'selected' : null }}>{{$product->title}}</option>
              @endforeach
            </select>
            
            @if($errors->has('product_id'))
            <span class="help-block">{{ $errors->first('product_id') }}</span>
            @endif
          </div>

          <label for="data-inicial-atividade" class="col-sm-1 control-label">Data Inicial:</label>
          <div class="col-sm-2 {{ $errors->has('date_start')? 'has-error' : null }}">
            <div class="input-group disabled">
              <input type="text" name="date_start" id="data-inicial-atividade" class="date-picker form-control" value="{{old('date_start')}}" size="8" />
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
            </div>
            @if($errors->has('date_start'))
            <span class="help-block">{{ $errors->first('date_start') }}</span>
            @endif
          </div>
          <label for="data-final-atividade" class="col-sm-1 control-label">Data Final:</label>
          <div class="col-sm-2 {{ $errors->has('date_end')? 'has-error' : null }}">
            <div class="input-group disabled">
              <input type="text" name="date_end" id="data-final-atividade" class="date-picker form-control" value="{{old('date_end')}}" size="8" />
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
            </div>
            @if($errors->has('date_end'))
            <span class="help-block">{{ $errors->first('date_end') }}</span>
            @endif
          </div>
        </div>

        <div class="form-group">
          <label for="produto" class="col-sm-1 control-label">Cliente:</label>
          <div class="col-sm-5 {{ $errors->has('client_id')? 'has-error' : null }}">
            <div class="input-group disabled">
              <select name="client_id" id="client_id" class="form-control">
                <option value="">Selecione</option>
                @foreach(getClients() as $client)
                <option value="{{$client->id}}">{{$client->name}}</option>
                @endforeach
              </select>
              <a href="javascript:void(0)" onclick="SximoModal('{{{ route('clientes.create') }}}', 'Cadastro de Clientes')" title="Adicionar Cliente" class="input-group-addon">
                <i class="fa fa-plus"></i>                  
              </a>
            </div>
            @if($errors->has('client_id'))
            <span class="help-block">{{ $errors->first('client_id') }}</span>
            @endif
          </div>

          <label for="hora-inicial-atividade" class="col-sm-1 control-label">Hora Inicial:</label>
          <div class="col-sm-2 {{ $errors->has('time_start')? 'has-error' : null }}">
            <input type="text" name="time_start" id="hora-inicial-atividade" class="form-control fmtHora" value="{{old('time_start')}}" size="14" />
            @if($errors->has('time_start'))
            <span class="help-block">{{ $errors->first('time_start') }}</span>
            @endif
          </div>
          <label for="hora-final-atividade" class="col-sm-1 control-label">Hora Final:</label>
          <div class="col-sm-2 {{ $errors->has('time_end')? 'has-error' : null }}">
            <input type="text" name="time_end" id="hora-final-atividade" class="form-control fmtHora"  value="{{old('time_end')}}" size="14" />
            @if($errors->has('time_end'))
            <span class="help-block">{{ $errors->first('time_end') }}</span>
            @endif
          </div>
        </div>
      </fieldset>

      <div class="form-group">
        <label for="descricao-atividade" class="col-sm-1 control-label">Atividade:</label>
        <div class="col-sm-11 {{ $errors->has('works')? 'has-error' : null }}">
          <textarea name="works" id="descricao-atividade" rows="6" class="form-control">{{old('works')}}</textarea>
          @if($errors->has('works'))
          <span class="help-block">{{ $errors->first('works') }}</span>
          @endif
        </div>
      </div>

      <div class="form-group">
        <label for="observacoes-atividade" class="col-sm-1 control-label">Observações:</label>
        <div class="col-sm-11 {{ $errors->has('observation')? 'has-error' : null }}">
          <textarea name="observation" id="observacoes-atividade" rows="6" class="form-control">{{old('observation')}}</textarea>
          @if($errors->has('observation'))
          <span class="help-block">{{ $errors->first('observation') }}</span>
          @endif
        </div>
      </div>

      <div class="form-group">
        <label for="status-atividade" class="col-sm-1 control-label">Status:</label>
        <div class="col-sm-6 col-md-2 {{ $errors->has('status')? 'has-error' : null }}">
          <select name="status" id="status-atividade" class="form-control">
            <option value="">Selecione: </option>
            @foreach(statusActivity() as $key => $name)
            <option value="{{$key}}" {{ $key == request()->get('status', null) ? 'selected' : null }}>{{$name}}</option>
            @endforeach
          </select>
          @if($errors->has('status'))
          <span class="help-block">{{ $errors->first('status') }}</span>
          @endif
        </div>

        <div class="col-sm-5 col-md-9">
          <button type="submit" class="btn transicao pull-right">
            SALVAR
          </button>

          <a href="{{ route('activity.index') }}" class="btn btn-default pull-right" style="color: #FFF; background-color: #CCC; border-color: #ccc; margin-right: 5px;">
            CANCELAR
          </a>
        </div>
      </div>
    </form>
  </div>
@endsection