@extends('layouts.sicoob.default')

@section('content')
  <div class="fundo-branco sombra-caixa ctn-eventos">
    <h1 class="titulo-janelas">
      <i class="icomoon icon-calendar"></i>
      <div>ANIVERSARIANTES DO MÊS</div>
    </h1>

    <div class="conteudo-interno borda-topo-cinza">
      <div class="row">
        <div class="col-sm-8">
          <div class="conteudo">
            @foreach($birth_month as $birth)
            <div class="row">
                <div class="col-sm-2 text-center">
                  <img src="{{ route('imagecache', ['template' => 'profile', 'filename' => $birth->photo]) }}" 
                       alt="{{$birth->name}}" 
                       title="{{$birth->name}}" 
                       class="img-responsive img-circle borda-branca" />
                </div>
                <div class="col-sm-7">
                  {{$birth->name}}
                </div>

                <div class="col-sm-3">
                  {{$birth->date_birth->format('d/m')}}
                </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection