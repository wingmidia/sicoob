@extends('intranet.layouts.master')

@section('title', $controllers->title)

@push('page.title')

    <div class="row">
        <div class="col-md-8">
            <h1 class="page-header text-overflow"><i class="{{ $controllers->icon }}"></i> Ranking de indicações</h1>
        </div>
        <div class="col-md-4 text-right">
            <a href="{{ route('intranet.campaign.ranking') }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Voltar</a>
        </div>
    </div>


@endpush

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-heading">
                    <h1 class="panel-title text-overflow title-sec"><i class="fa fa-bullhorn"></i> {{$campaign->name}}</h1>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="painel-filtros" class="panel panel-default" style="display:none;">
                                <div class="panel-heading">
                                    <h5 class="panel-title">
                                        <i class="fa fa-filter"></i>FILTROS
                                    </h5>
                                </div>
                                <div class="panel-body">
                                    <form id="filter-form">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="filtro-campanha">AGÊNCIA</label>
                                                    <div class="clearfix"></div>
                                                    <select id="filtro-agencia" name="departaments[]" multiple style="width: 100%" aria-controls="tabela-ranking">
                                                        @foreach($departaments as $departament)
                                                            <option value="{{$departament->id}}">{{$departament->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="filtro-campanha">CATEGORIA</label>
                                                    <div class="clearfix"></div>
                                                    <select id="filtro-produto" name="categories[]" multiple style="width: 100%" aria-controls="tabela-ranking">
                                                        @foreach($categories as $cat)
                                                            <option value="{{$cat->id}}">{{$cat->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="panel-footer">
                                    <button id="apply-filtro" class="btn btn-info">
                                        <i class="fa fa-search"></i> Filtrar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            @if($exibition)
                                {!! $datatable->table(['id'=>'tabela-ranking', 'class' => 'table table-striped table-hover']) !!}
                            @else
                                <h1>Rankeamento de Indicação Ocultado Para Apuração!</h1>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('styles')
    {!! Html::style('intranet/node_modules/datatables.net-bs/css/dataTables.bootstrap.css') !!}
    {!! Html::style('intranet/node_modules/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') !!}
    {{ Html::style('intranet/node_modules/select2/dist/css/select2.min.css') }}
    <style>
        .act-mar:not(:first-child){
            margin-left: 10px;
        }
    </style>
@endpush

@push('scripts')

    {!! Html::script('intranet/node_modules/datatables.net/js/jquery.dataTables.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-bs/js/dataTables.bootstrap.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons/js/dataTables.buttons.min.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons/js/buttons.colVis.min.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons/js/buttons.html5.min.js') !!}
    {!! Html::script('intranet/node_modules/jszip/dist/jszip.min.js') !!}
    {{ Html::script('intranet/node_modules/select2/dist/js/select2.min.js') }}
    {!! Html::script('intranet/node_modules/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') !!}
    <script>
        function renderTable(d) {
            console.log('render', d);
            d.filters =  $('#filter-form').serialize();
        }
    </script>
    {!! $datatable->scripts() !!}


    <script>


        $(document).ready(function () {

            $('.filter-btn').parent().parent().on('click', function () {
                $('#painel-filtros').toggle('low');
            });

            $('#apply-filtro').on('click', function () {
                // console.log($('#tabela-ranking').DataTable().ajax.params());
                $('#tabela-ranking').DataTable().ajax.reload();
            });


            $('#painel-filtros').hide();

            $('#delete-btn').on('click', function () {
                var url = $(this).attr('data-link');
                $('.delete-modal .delete-link').attr('href', url);
                $('#deleteModal').modal('show');
            });

            $('#filtro-campanha').select2({
                placeholder: 'Selecione um produto'
            });
            $('#filtro-agencia').select2({
                placeholder: 'Selecione uma agência'
            });
            $('#filtro-produto').select2({
                placeholder: 'Selecione um produto'
            });
            $('#filtro-status').select2({
                placeholder: 'Selecione um produto'
            });

        });

        let html;

        function datatablesFormatActions(data, type, full){
            console.log(full);
            html = '<div class="text-center">'
            @if(isset($controllers->actions->edit))
                @permission($controllers->actions->edit->permission)
                    html += '<a title="Editar campanha" href="{{ route($controllers->actions->edit->route) }}/'+full['id']+'" class="btn btn-icon-toggle btn-sm btn-info act-mar add-tooltip"><i class="fa fa-pencil"></i></a>';
                @endpermission
            @endif

            @if(isset($controllers->actions->delete))
                @permission($controllers->actions->delete->permission)
                html += '<a id="delete-btn" data-link="{{ route($controllers->actions->delete->route) }}/'+full['id']+'" class="btn btn-icon-toggle btn-sm btn-danger act-mar"><i class="fa fa-trash"></i></a>';
                @endpermission
            @endif

                html += '</div>';
            return html;
        }

        function datatablesFormatStrong(data){

            html = '<strong>'+data+'</strong>';
            return html;

        }
        var trophs = [
            {color: '#ecca6b', title: 'OURO'},
            {color: '#afafaf', title: 'PRATA'},
            {color: '#cda470', title: 'BRONZE'}
        ];
        function datatablesIndexRanking(meta){
            var concat = '';
            var pageInfo = $('#tabela-ranking').DataTable().page.info();
            var index = meta.row + pageInfo.page*pageInfo.length;

            if(index+1<=trophs.length){
                concat = ` <i class="fa fa-trophy" style="color: ${trophs[index].color}" title="${trophs[index].title}"></i>`;
            }

            return `<h5 style="text-align: center">${index+1}${concat}</h5>`;
        }

        function datatablesFormatCampaignStatus(data) {
            return  (data == 1) ? '<i class="fa fa-circle text-success" title="Ativo"></i> Ativo' : '<i class="fa fa-circle text-danger" title="Inativo"></i> Inativo';
        }

    </script>
@endpush