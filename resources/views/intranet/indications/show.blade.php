@extends('intranet.layouts.master')

@section('title', $controllers->title)

@push('page.title')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="row">
                <div class="col-md-8">
                    <h1 class="page-header text-overflow"><i class="{{ $controllers->icon }}"></i> {{ $controllers->title }} | <small><i class="fa fa-info-circle"></i> Visualizando</small></h1>
                </div>
                <div class="col-md-4 text-right">
                    <a href="{{ route($controllers->route) }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Voltar</a>
                    @if(!empty($item->id))
                        @permission($controllers->actions->add->permission)
                        <a href="{{ route($controllers->actions->add->route) }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Adicionar</a>
                        @endpermission
                    @endif
                </div>
            </div>
        </div>
    </div>

@endpush

@section('content')

    
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="row">
                    <div class="col-md-8">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Dados do cliente</h3>
                            </div>
                            <table class="table table-hover">
                                <tr>
                                    <th width="20%">Tipo</th>
                                    <td>{{ $item->client->getTypeProfileName() }}</td>
                                </tr>
                                @if($item->client->cpf)
                                    <tr>
                                        <th>CPF</th>
                                        <td>{{ $item->client->cpf }}</td>
                                    </tr>
                                @endif
                                @if($item->client->cnpj)
                                    <tr>
                                        <th>CNPJ</th>
                                        <td>{{ $item->client->cnpj }}</td>
                                    </tr>
                                @endif
                                <tr>
                                    <th>Nome</th>
                                    <td>{{ $item->client->name }}</td>
                                </tr>
                                @if($item->client->cnpj)
                                    <tr>
                                        <th>Razão Social</th>
                                        <td>{{ $item->client->company_name }}</td>
                                    </tr>
                                @endif
                            </table>
                        </div>
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Dados de contato</h3>
                            </div>
                            <table class="table table-hover">
                                @if($item->client->email)
                                    <tr>
                                        <th width="20%">E-mail</th>
                                        <td>{{ $item->client->email }}</td>
                                    </tr>
                                @endif
                                <tr>
                                    <th>Celular/Whatsapp</th>
                                    <td>{{ $item->client->cellphone }}</td>
                                </tr>
                                @if($item->client->phone1)
                                    <tr>
                                        <th>Telefone</th>
                                        <td>{{ $item->client->phone1 }}</td>
                                    </tr>
                                @endif
                                @if($item->client->phone2)
                                    <tr>
                                        <th>Telefone Alternativo</th>
                                        <td>{{ $item->client->phone2 }}</td>
                                    </tr>
                                @endif
                            </table>
                        </div>
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Produtos</h3>
                            </div>
                            <table class="table">
                                @foreach($item->products as $product)
                                    <tr @if($product->pivot->concluded) class="success" @endif>
                                        <th>
                                            <i class="fa fa-angle-double-right"></i> {{ $product->title }}
                                            @if($product->pivot->concluded) <br> <span class="label label-success">Indicação convertida em venda</span> @endif
                                        </th>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Resumo da indicação</h3>
                            </div>
                            <table class="table table-hover">
                                <tr>
                                    <th width="20%">Colaborador</th>
                                    <td>{{ $item->collaborator->name }}</td>
                                </tr>
                                <tr>
                                    <th>Criada em</th>
                                    <td>{{ $item->created_at->format('d/m/Y') }}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Resumo da prospecção</h3>
                            </div>
                            <table class="table table-hover">
                                <tr>
                                    <th width="20%">Colaborador</th>
                                    <td>
                                        @foreach($item->prospections as $prospection)
                                            {{ $prospection->name }}
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <th>Atualizada em</th>
                                    <td>{{ ($item->created_at != $item->updated_at) ? $item->updated_at->format('d/m/Y') : '--' }}</td>
                                </tr>
                            </table>
                        </div>
                        @if(!isset($item->status) && !$item->concluded && $item->collaborator_id == Auth::user()->collaborator->id)
                        <div class="text-right">
                            <a href="{{ route($controllers->actions->delete->route, $item->id) }}" class="btn btn-danger btn-sm mar-btm"><i class="fa fa-trash"></i> Remover indicação</a>
                        </div>
                        @endif
                    </div>
                    <div class="col-md-4">
                        @if($can_prospect)
                        <form id="form-indication" action="{{ route($controllers->actions->update->route, $item->id) }}" method="post">
                            {{ method_field('PUT') }}
                            {{ csrf_field() }}
                            <div class="panel panel-bordered panel-{{ (isset($item->status)) ? ($item->status->state) ? 'success' : 'danger' : 'warning' }}">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Prospectar</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="form-indication-indication_status_id" class="control-label">Status</label>
                                        <select name="indication_status_id" id="form-indication-indication_status_id" class="form-control">
                                            <option value="" style="color: red">PENDENTE</option>
                                            @foreach($indication_status as $status)
                                                <option value="{{ $status->id }}" @if($item->indication_status_id == $status->id) selected @endif style="color: {{ ($status->state) ? 'green' : 'red' }}" >{{ strtoupper($status->name) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="" class="control-label">Reagendar contato</label>
                                        <input type="datetime-local" name="display_at" @if($item->display_at != $item->created_at) value="{{ $item->display_at->format('Y-m-d\TH:i') }}" @endif class="form-control">
                                    </div>
                                    <button class="btn btn-primary btn-lg btn-block" type="submit"><i class="fa fa-save"></i> Salvar</button>
                                </div>
                            </div>
                        </form>

                        <form id="form-indication" action="{{ route('intranet.indications.transfer-propection') }}" method="post">
                            {{ method_field('POST') }}
                            {{ csrf_field() }}
                            <div class="panel panel-bordered panel-success" style="border-color: #0B3535;">
                                <div class="panel-heading primary" style="background-color: #0B3535;" >
                                    <h3 class="panel-title">Transferir Prospecção</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="form-indication-indication_employer_id" class="control-label">Colaborador</label>
                                        <select name="indication_employer_id" id="form-indication-indication_employer_id" class="form-control">
                                            <option value="-1" style="color: green">Selecione...</option>
                                            @foreach($employers as $key_id => $emp_name)
                                                <option value="{{$key_id}}" style="color: green" >{{ $emp_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <input type="hidden" value="{{$item->id}}" name="item_id" />
                                    <button class="btn btn-primary btn-lg btn-block" type="submit"><i class="fa fa-save"></i> Transferir</button>
                                </div>
                            </div>
                        </form>
                        @else
                            <div class="panel panel-colorful panel-{{ (isset($item->status)) ? ($item->status->state) ? 'success' : 'danger' : 'warning' }}">
                                <div class="panel-body text-center">
                                    <i class="fa fa-4x {{ (isset($item->status)) ? ($item->status->state) ? 'fa-check-circle' : 'fa-exclamation-triangle' : 'fa-exclamation-circle' }}"></i>
                                    <h4 class="text-light">
                                        {{ (isset($item->status)) ? ($item->status->state) ? 'INDICAÇÃO PROSPECTADA' : 'INDICAÇÃO SUSPENSA NA PROSPECÇÃO' : 'INDICAÇÃO ENVIADA PARA PROSPEÇÃO' }}
                                    </h4>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    

@endsection

@push('styles')
@endpush

@push('scripts')
@endpush