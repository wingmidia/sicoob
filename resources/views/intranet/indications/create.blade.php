@extends('intranet.layouts.master')

@section('title', $controllers->title)

@push('page.title')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <div class="row">
                <div class="col-md-8">
                    <h1 class="page-header text-overflow"><i class="{{ $controllers->icon }}"></i> {{ $controllers->title }} | <small><i class="fa fa-plus-circle"></i> Adicionando nova indicação</small></h1>
                </div>
                <div class="col-md-4 text-right">
                    <a href="{{ route($controllers->route) }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Voltar</a>
                </div>
            </div>

        </div>
    </div>

@endpush

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <div class="panel panel-warning">

                <!-- Circular Form Wizard -->
                <!--===================================================-->
                <div id="wizard-indication">
                    <div class="wz-heading pad-ver bord-btm">

                        <!--Progress bar-->
                        <div class="progress progress-xs progress-light-base">
                            <div class="progress-bar progress-bar-warning"></div>
                        </div>

                        <!--Nav-->
                        <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                        <ul class="wz-nav-off">
                            <li class="col-xs-3">
                                <a data-toggle="tab" href="#wizard-indication-tab1" title="Tipo de cliente" class="add-tooltip">
                                    <div class="icon-wrap icon-wrap-sm bg-warning">
                                        <i class="wz-icon fa fa-user fa-2x icon-lg"></i>
                                        <i class="wz-icon-done fa fa-check-circle fa-2x icon-lg"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="col-xs-3">
                                <a data-toggle="tab" href="#wizard-indication-tab2" title="Dados do cliente" class="add-tooltip">
                                    <div class="icon-wrap icon-wrap-sm bg-warning">
                                        <i class="wz-icon fa fa-vcard fa-2x icon-lg"></i>
                                        <i class="wz-icon-done fa fa-check-circle fa-2x icon-lg"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="col-xs-3">
                                <a data-toggle="tab" href="#wizard-indication-tab3" title="Produtos" class="add-tooltip">
                                    <div class="icon-wrap icon-wrap-sm bg-warning">
                                        <i class="wz-icon fa fa-briefcase fa-2x icon-lg"></i>
                                        <i class="wz-icon-done fa fa-check-circle fa-2x icon-lg"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="col-xs-3">
                                <a data-toggle="tab" href="#wizard-indication-tab4" title="Finalizar" class="add-tooltip">
                                    <div class="icon-wrap icon-wrap-sm bg-warning">
                                        <i class="wz-icon fa fa-thumbs-up fa-2x icon-lg"></i>
                                        <i class="wz-icon-done fa fa-thumbs-up fa-2x icon-lg"></i>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <!--Form-->
                    <form id="form-indication" action="{{ route($controllers->actions->store->route) }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="client_id" value="{{ old('client_id') }}">

                        <div class="panel-body">
                            <div class="tab-content">

                                <!--First tab-->
                                <div id="wizard-indication-tab1" class="tab-pane fade">

                                    <div class="mar-btm text-center">
                                        <h4>Nova indicação</h4>
                                        <p>Insira as informações da sua indicação. Fique atento para preencher os dados corretamente.</p>
                                    </div>

                                    <hr>

                                    <fieldset>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="form-indication-type_profile">Tipo</label>
                                                    <div class="radio">
                                                        <input id="form-indication-type_profile-1" class="magic-radio" type="radio" name="type_profile" value="F" @if(old('type_profile') != 'J')checked="checked"@endif>
                                                        <label for="form-indication-type_profile-1">Pessoa Física</label>

                                                        <input id="form-indication-type_profile-2" class="magic-radio" type="radio" name="type_profile" value="J" @if(old('type_profile') == 'J')checked="checked"@endif>
                                                        <label for="form-indication-type_profile-2">Pessoa Jurídica</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" id="form-indication-box-cpf">
                                                    <label for="form-indication-cpf" class="control-label">CPF</label>
                                                    <input id="form-indication-cpf" type="text" class="form-control input-lg mask-cpf" name="cpf" value="{{ old('cpf') }}" autofocus>
                                                </div>
                                                <div class="form-group" id="form-indication-box-cnpj" style="display: none">
                                                    <label for="form-indication-cnpj" class="control-label">CNPJ</label>
                                                    <input id="form-indication-cnpj" type="text" class="form-control input-lg mask-cnpj" name="cnpj" value="{{ old('cnpj') }}">
                                                </div>
                                            </div>
                                        </div>

                                    </fieldset>

                                </div>

                                <!--Second tab-->
                                <div id="wizard-indication-tab2" class="tab-pane fade">
                                    <fieldset>
                                        <legend>Complete os dados do cliente</legend>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label for="form-indication-name" class="control-label">Nome</label>
                                                    <input id="form-indication-name" type="text" class="form-control" name="name" value="{{ old('name') }}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="form-indication-date_birth" class="control-label">Data de nascimento</label>
                                                    <input id="form-indication-date_birth" type="text" class="form-control mask-date" name="date_birth" value="{{ old('date_birth') }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group" id="form-indication-box-company_name" style="display: none;">
                                                    <label for="form-indication-company_name" class="control-label">Razão Social</label>
                                                    <input id="form-indication-company_name" type="text" class="form-control" name="company_name" value="{{ old('company_name') }}">
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <legend>Dados de contato</legend>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="form-indication-email" class="control-label">E-mail</label>
                                                    <input id="form-indication-email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="form-indication-cellphone" class="control-label">Celular/Whatsapp</label>
                                                    <input id="form-indication-cellphone" type="text" class="form-control mask-phone" name="cellphone" value="{{ old('cellphone') }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="form-indication-phone1" class="control-label">Telefone</label>
                                                    <input id="form-indication-phone1" type="text" class="form-control mask-phone" name="phone1" value="{{ old('phone1') }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="form-indication-phone2" class="control-label">Telefone alternativo</label>
                                                    <input id="form-indication-phone2" type="text" class="form-control mask-phone" name="phone2" value="{{ old('phone2') }}">
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    {{--<fieldset>
                                        <legend>Endereço do cliente</legend>
                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    <label for="form-indication-address" class="control-label">Endereço</label>
                                                    <input id="form-indication-address" type="text" class="form-control" name="address" value="{{ old('address') }}">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="form-indication-number" class="control-label">Número</label>
                                                    <input id="form-indication-number" type="text" class="form-control" name="number" value="{{ old('number') }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="form-indication-complement" class="control-label">Complemento</label>
                                                    <input id="form-indication-complement" type="text" class="form-control" name="complement" value="{{ old('complement') }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="form-indication-district" class="control-label">Bairro</label>
                                                    <input id="form-indication-district" type="text" class="form-control" name="district" value="{{ old('district') }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="form-indication-city" class="control-label">Cidade</label>
                                                    <input id="form-indication-city" type="text" class="form-control" name="city" value="{{ old('city') }}">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="form-indication-state" class="control-label">Estado</label>
                                                    {!! Form::select('state', array_merge([''=> 'Selecione um estado'],getStates()), old('state'), ['id' => 'form-indication-state', 'class' => 'form-control']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="form-indication-postcode" class="control-label">CEP</label>
                                                    <input id="form-indication-postcode" type="text" class="form-control mask-cep" name="postcode" value="{{ old('postcode') }}">
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>--}}
                                </div>

                                <!--Third tab-->
                                <div id="wizard-indication-tab3" class="tab-pane">
                                    <fieldset>
                                        <legend>Escolha os produtos</legend>
                                        <div class="form-group">
                                            <div id="alert-indication-products-message"></div>
                                            <span id="alert-indication-products-icon"></span>
                                            <div class="clearfix"></div>
                                            <br>
                                            <div class="row">
                                                @foreach($products as $product)
                                                <div class="col-md-4">
                                                    <div class="checkbox text-center">
                                                        <label for="form-indication-products-{{ $product->id }}" id="form-indication-products-label-{{ $product->id }}" class="panel pad-no form-indication-products-labels" style="width: 100%">

                                                            <div class="panel-body">
                                                                <input id="form-indication-products-{{ $product->id }}" name="products[]" value="{{ $product->id }}" data-title="{{ $product->title }}" class="magic-checkbox form-indication-products-inputs" type="checkbox">
                                                                <label for="form-indication-products-{{ $product->id }}"></label>
                                                            </div>

                                                            <div class="panel-footer">
                                                                <strong>{{ $product->title }}</strong>
                                                            </div>

                                                        </label>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>


                                        </div>
                                    </fieldset>
                                </div>

                                <!--Fourth tab-->
                                <div id="wizard-indication-tab4" class="tab-pane fade">
                                    <div class="mar-btm text-center">
                                        <h4>Concluir indicação</h4>
                                        <p>Confira os dados da sua indicação e clique no botão finalizar.</p>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <fieldset>
                                                <legend>Resumo do indicação</legend>
                                                <div class="panel panel-primary">
                                                    <table class="table table-bordered">
                                                        <tr>
                                                            <th>Nome</th>
                                                            <td id="form-indication-box-confirmation-name"></td>
                                                        </tr>
                                                        <tr id="form-indication-box-confirmation-box-cpf">
                                                            <th>CPF</th>
                                                            <td id="form-indication-box-confirmation-cpf"></td>
                                                        </tr>
                                                        <tr id="form-indication-box-confirmation-box-cnpj" style="display: none">
                                                            <th>CNPJ</th>
                                                            <td id="form-indication-box-confirmation-cnpj"></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Celular/Whatsapp</th>
                                                            <td id="form-indication-box-confirmation-cellphone"></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Produtos</th>
                                                            <td id="form-indication-box-confirmation-products"></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <div class="col-md-6">
                                            <fieldset>
                                                <legend>Responsável pela prospecção</legend>
                                                <div class="form-group">
                                                    <div class="radio">
                                                        <input id="form-indication-collection" class="magic-checkbox" type="checkbox" name="collection" value="1">
                                                        <label for="form-indication-collection">Angariação</label>
                                                        <span class="help-block">Marque essa opção caso queria fazer a prospeção da indicação.</span>
                                                    </div>
                                                </div>
                                                <div class="form-group" id="form-indication-box-employer_id">
                                                    <hr>
                                                    <label for="form-indication-employer_id" class="control-label">Colaboradores</label>
                                                    {!! Form::select('employer_id', $employers->prepend('Escolha...','-1'), null, ['id'=>'form-indication-employer_id', 'class'=>'form-control']) !!}
                                                    <span class="help-block">A indicação será prospectada para o colaborador escolhido.</span>
                                                </div>
                                                <div class="form-group" id="form-indication-box-departament_id">
                                                    <hr>
                                                    <label for="form-indication-departament_id" class="control-label">Colaboradores da PA</label>
                                                    {!! Form::select('departament_id', $departaments->prepend('Escolha...','-1'), null, ['id'=>'form-indication-departament_id', 'class'=>'form-control']) !!}
                                                    <span class="help-block">A indicação será prospectada por um colaborador da PA escolhida.</span>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!--Footer button-->
                        <div class="panel-footer footer-fixed text-right bord-top bg-gray">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <div class="box-inline">
                                        <button type="button" class="previous btn btn-default btn-lg"><i class="fa fa-angle-left"></i> Anterior</button>
                                        <button type="button" class="next btn btn-primary btn-lg">Avançar <i class="fa fa-angle-right"></i></button>
                                        <button type="submit" class="finish btn btn-primary btn-lg" formnovalidate>Finalizar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!--===================================================-->
                <!-- End Circular Form Wizard -->

            </div>
        </div>
    </div>

@endsection

@push('styles')
    {{ Html::style('intranet/plugins/bootstrap-toggle/css/bootstrap-toggle.min.css') }}
    {{ Html::style('intranet/plugins/bootstrap-validator/bootstrapValidator.min.css') }}
@endpush

@push('scripts')
    {{ Html::script('intranet/plugins/bootstrap-toggle/js/bootstrap-toggle.min.js') }}
    {{ Html::script('intranet/plugins/bootstrap-validator/bootstrapValidator2.min.js') }}
    {{ Html::script('intranet/node_modules/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}
    {{ Html::script('intranet/node_modules/jquery-mask-plugin/dist/jquery.mask.min.js') }}
    {{ Html::script('intranet/node_modules/moment/min/moment.min.js') }}
    {{ Html::script('intranet/node_modules/moment/locale/pt-br.js') }}

    <script>

        $('#wizard-indication').bootstrapWizard({
            tabClass		    : 'wz-steps',
            nextSelector	    : '.next',
            previousSelector    : '.previous',
            onTabClick: function(tab, navigation, index) {
                return false;
            },
            onInit : function(){
                $('#wizard-indication').find('.finish').hide().prop('disabled', true);
                $('#wizard-indication').find('.finish').hide();
            },
            onTabShow: function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index+1;
                var $percent = (index/$total) * 100;
                var margin = (100/$total)/2;
                $('#wizard-indication').find('.progress-bar').css({width:$percent+'%', 'margin': 0 + 'px ' + margin + '%'});

                navigation.find('li:eq('+index+') a').trigger('focus');

                // If it's the last tab then hide the last button and show the finish instead
                if($current >= $total) {
                    $('#wizard-indication').find('.next').hide();
                    $('#wizard-indication').find('.finish').show();
                    $('#wizard-indication').find('.finish').attr('disabled', false);
                } else {
                    $('#wizard-indication').find('.next').show();
                    $('#wizard-indication').find('.finish').hide().prop('disabled', true);
                    $('#wizard-indication').find('.finish').hide();
                }
            },
            onNext: function(tab, navigation, index){

                isValid = null;

                $('#form-indication').formValidation('validate');

                if(isValid === false)
                    return false;

                // verifica se cliente existe
                if(index === 1){

                    let type_profile = $('input[name="type_profile"]:checked').val();

                    if( type_profile == 'F')
                        $('input[name="cnpj"]').val('');
                    else
                        $('input[name="cpf"]').val('');

                    $('input[name="client_id"]').val('');

                    $.post("{{ route($controllers->actions->client->verify->route) }}", {
                        type_profile: type_profile,
                        cpf: (type_profile == 'F') ? $('input[name="cpf"]').val() : '',
                        cnpj: (type_profile == 'J') ? $('input[name="cnpj"]').val() : ''
                    }).done(function( data ) {

                        if(data.id && (client_id === null || client_id !== data.id)) {

                            client_id = null;

                            if (data.id){
                                client_id = data.id;
                                $('input[name="client_id"]').val(data.id);
                            }

                            $('input[name="name"]').val(data.name);
                            $('input[name="company_name"]').val(data.company_name);
                            $('input[name="phone1"]').val(data.phone1);
                            $('input[name="phone2"]').val(data.phone2);
                            $('input[name="cellphone"]').val(data.cellphone);
                            $('input[name="email"]').val(data.email);
                            /*$('input[name="postcode"]').empty().val(data.postcode);
                            $('input[name="address"]').empty().val(data.address);
                            $('input[name="district"]').empty().val(data.district);
                            $('input[name="complement"]').empty().val(data.complement);
                            $('input[name="city"]').empty().val(data.city);
                            $('select[name="state"]').empty().val(data.state);*/

                            $('input[name="date_birth"]').val('');
                            if(data.date_birth)
                                $('input[name="date_birth"]').val(moment(data.date_birth).format('DD/MM/YYYY'));

                            setMask();

                        }

                    });

                }

                // verifica produtos ja indicados para o cliente
                if(index === 2){

                    if($('input[name="client_id"]').val()) {

                        $.post("{{ route($controllers->actions->client->product_options->route) }}", {
                            client_id: $('input[name="client_id"]').val()
                        }).done(function (data) {

                            $('.form-indication-products-inputs').attr('disabled', false);
                            $('.form-indication-products-labels').css('opacity', 1);

                            $.each( data, function( key, value ) {
                                $('#form-indication-products-'+value.id).attr('disabled', true);
                                $('#form-indication-products-label-'+value.id).css('opacity', 0.5);
                            });

                        });

                    }

                }

                // dados para verificacao
                if(index === 3){

                    if($('input[name="products[]"]:checked').length < 1) {
                        $.niftyNoty({
                            type: 'danger',
                            container : '#wizard-indication-tab3',
                            title : 'Ops!!',
                            message : 'Selecione no mínimo 1 produto para a sua indicação',
                            timer: 2000,
                        });
                        return false;
                    }

                    $('#form-indication-box-confirmation-name').text($('input[name="name"]').val());
                    $('#form-indication-box-confirmation-cnpj').text($('input[name="cnpj"]').val());
                    $('#form-indication-box-confirmation-cpf').text($('input[name="cpf"]').val());
                    $('#form-indication-box-confirmation-cellphone').text($('input[name="cellphone"]').val());

                    var html = '<ul>';

                    $('input[name="products[]"]:checked').each(function() {
                        html += '<li>'+$(this).data('title')+'</li>';
                    });

                    html += '</ul>';

                    $('#form-indication-box-confirmation-products').html(html);

                }

            }
        });

        $('input[name="type_profile"]').on('change', function(){

            $('#form-indication-box-cpf, #form-indication-box-cnpj, #form-indication-box-company_name, #form-indication-box-confirmation-box-cpf, #form-indication-box-confirmation-box-cnpj').toggle();

        });

        $('input[name="collection"]').on('change', function(){

            $('#form-indication-box-employer_id').toggle();
            $('#form-indication-box-departament_id').toggle();

        });

        $('select[name="employer_id"]').on('change', function(){
            
            $('select[name="departament_id"]').val("-1");

        });

        $('select[name="departament_id"]').on('change', function(){

            $('select[name="employer_id"]').val("-1");

        });

        var isValid;
        var client_id = null;

        $('#form-indication').on('init.field.fv', function(e, data) {
            // data.field   --> The field name
            // data.element --> The field element
            if (data.field === 'products[]') {
                var $icon = data.element.data('fv.icon');
                $icon.appendTo('#alert-indication-products-icon');
            }
        }).formValidation({
            message: 'Este valor não é valido',
            feedbackIcons: {
                valid: 'fa fa-check-circle fa-lg text-success',
                invalid: 'fa fa-times-circle fa-lg',
                validating: 'fa fa-refresh'
            },
            fields: {
                cpf: {
                    validators: {
                        notEmpty: {
                            message: 'O CPF é obrigatório.'
                        },
                        stringLength: {
                            min: 14,
                            max: 14,
                            message: 'Formato de CPF inválido.'
                        }
                    }
                },
                cnpj: {
                    validators: {
                        notEmpty: {
                            message: 'O CNPJ é obrigatório.'
                        },
                        stringLength: {
                            min: 18,
                            max: 18,
                            message: 'Formato de CNPJ inválido.'
                        }
                    }
                },
                name: {
                    validators: {
                        notEmpty: {
                            message: 'O Nome do cliente é obrigatório.'
                        }
                    }
                },
                cellphone: {
                    validators: {
                        notEmpty: {
                            message: 'O Telefone é obrigatório.'
                        }
                    }
                },
                'products[]': {
                    err: '#alert-indication-products-message',
                    validators: {
                        notEmpty: {
                            message: 'O Produto é obrigatório.'
                        }
                    }
                },
                departament_id: {
                    validators: {
                        notEmpty: {
                            message: 'Escolha uma PA ou marque a opção Angariação.'
                        }
                    }
                },
            }
        }).on('success.field.fv', function(e, data) {
            // $(e.target)  --> The field element
            // data.bv      --> The BootstrapValidator instance
            // data.field   --> The field name
            // data.element --> The field element

            var $parent = data.element.parents('.form-group');

            // Remove the has-success class
            $parent.removeClass('has-success');

            // Hide the success icon
            //$parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        }).on('err.field.fv', function(e) {
            isValid = false;
        });

        var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            },
            placeholder: "(__) ____-____",
            selectOnFocus: true
        };

        function setMask(){
            $('.mask-date').mask('00/00/0000', {placeholder: "__/__/____", selectOnFocus: true});
            $('.mask-cep').mask('00000-000', {placeholder: "_____-___", selectOnFocus: true});
            $('.mask-phone').mask(SPMaskBehavior, spOptions);
            $('.mask-cpf').mask('000.000.000-00', {placeholder: "___.___.___-__", reverse: true, selectOnFocus: true});
            $('.mask-cnpj').mask('00.000.000/0000-00', {placeholder: "__.___.___/____-__", reverse: true, selectOnFocus: true});
        }

        setMask();

    </script>

@endpush