@extends('intranet.layouts.master')

@section('title', $controllers->title)

@push('page.title')

    <div class="row">
        <div class="col-md-8">
            <h1 class="page-header text-overflow"><i class="{{ $controllers->icon }}"></i> Indicações e Prospecção</h1>
        </div>
        <div class="col-md-4 text-right">
            @permission($controllers->actions->add->permission)
            <a href="{{ route($controllers->actions->add->route) }}" class="btn btn-primary btn-lg"><i class="fa fa-plus-circle"></i> Adicionar</a>
            @endpermission
        </div>
    </div>

@endpush

@section('content')

    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-warning panel-colorful">
                <div class="pad-all">
                    <p class="text-lg text-semibold">
                        <i class="demo-pli-data-storage icon-fw"></i> Indicações realizadas
                        <a href="{{ route($controllers->actions->byme->route) }}" class="pull-right btn btn-xs btn-default">Visualizar Todas</a>
                    </p>
                    <p class="mar-no">
                        <span class="pull-right text-bold">{{ $estatistica->total->geral }}</span> Últimos 12 meses
                    </p>
                    <p class="mar-no">
                        <span class="pull-right text-bold">{{ $estatistica->total->mes_anterior }}</span> Mês anterior
                    </p>
                    <p class="mar-no">
                        <span class="pull-right text-bold">{{ $estatistica->total->mes_atual }}</span> Mês atual
                    </p>
                </div>
                <div class="pad-top text-center">
                    <!--Placeholder-->
                    <div id="sparkline-indication-total" class="sparklines-full-content"></div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-mint panel-colorful">
                <div class="pad-all">
                    <p class="text-lg text-semibold"><i class="demo-pli-data-storage icon-fw"></i> Indicações prospectadas</p>
                    <p class="mar-no">
                        <span class="pull-right text-bold">{{ $estatistica->prospectadas->geral }}</span> Últimos 12 meses
                    </p>
                    <p class="mar-no">
                        <span class="pull-right text-bold">{{ $estatistica->prospectadas->mes_anterior }}</span> Mês anterior
                    </p>
                    <p class="mar-no">
                        <span class="pull-right text-bold">{{ $estatistica->prospectadas->mes_atual }}</span> Mês atual
                    </p>
                </div>
                <div class="pad-top text-center">
                    <!--Placeholder-->
                    <div id="sparkline-indication-prospectadas" class="sparklines-full-content"></div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-success panel-colorful">
                <div class="pad-all">
                    <p class="text-lg text-semibold"><i class="demo-pli-data-storage icon-fw"></i> Indicações efetivadas</p>
                    <p class="mar-no">
                        <span class="pull-right text-bold">{{ $estatistica->convertidas->geral }}</span> Últimos 12 meses
                    </p>
                    <p class="mar-no">
                        <span class="pull-right text-bold">{{ $estatistica->convertidas->mes_anterior }}</span> Mês anterior
                    </p>
                    <p class="mar-no">
                        <span class="pull-right text-bold">{{ $estatistica->convertidas->mes_atual }}</span> Mês atual
                    </p>
                </div>
                <div class="pad-top text-center">
                    <!--Placeholder-->
                    <div id="sparkline-indication-convertidas" class="sparklines-full-content"></div>
                </div>
            </div>
        </div>
    </div>

    <div id="page-title">
        <div class="row">
            <div class="col-md-12">
                <h1 class="page-header text-overflow"><i class="fa fa-address-card"></i> Lista de indicações para prospecção</h1>
            </div>
        </div>
        <div class="mar-btm"></div>
    </div>

    <div class="panel">
        {!! $datatable->table(['class' => 'table table-striped table-hover']) !!}
    </div>

@endsection

@push('styles')
    {!! Html::style('intranet/node_modules/datatables.net-bs/css/dataTables.bootstrap.css') !!}
    {!! Html::style('intranet/node_modules/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') !!}
@endpush

@push('scripts')

    {!! Html::script('intranet/node_modules/datatables.net/js/jquery.dataTables.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-bs/js/dataTables.bootstrap.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons/js/dataTables.buttons.min.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons/js/buttons.colVis.min.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons/js/buttons.html5.min.js') !!}
    {!! Html::script('intranet/node_modules/jszip/dist/jszip.min.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') !!}

    {!! $datatable->scripts() !!}

    <script>

        let html;

        function datatablesFormatStrong(data){

            html = '<strong>'+data+'</strong>';
            return html;

        }

        function datatablesFormatIndicationStatus(data, type, full){

            if(data !== null) {

                html = '<div title="'+ data +'" class="label label-table label-' + ((full['status_state']) ? 'success' : 'danger') + '">' + data + '</div>';
                return html;

            } else
                return '<div class="label label-table label-warning">Pendente</div>';

        }

        function datatablesFormatActionShow(data){

            return '<div class="text-center"><a href="'+data+'" class="btn btn-icon-toggle btn-sm btn-mint"><i class="fa fa-info-circle"></i></a></div>';
        }

        function datatablesFormatIndicationDisplayAt(data, type, full){

            if(data != full['created_at']){

                return '<strong>'+data+'</strong>';

            } else {
                return '';
            }

        }

    </script>
@endpush