@extends('intranet.layouts.master')

@section('title', $controllers->title)

@push('page.title')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="row">
                <div class="col-md-8">
                    <h1 class="page-header text-overflow"><i class="{{ $controllers->icon }}"></i> {{ $controllers->title }} | <small><i class="fa fa-pencil"></i> Editando: </small></h1>
                </div>
                <div class="col-md-4 text-right">
                    <a href="{{ route($controllers->route) }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Voltar</a>
                </div>
            </div>
        </div>
    </div>

@endpush

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form action="{{ route($controllers->actions->update->route) }}" method="post">
                {{ csrf_field() }}
                <div class="tab-base">

                    <!--Nav Tabs-->
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tabs-box-dados">Configurações Gerais</a></li>
                    </ul>

                    <!--Tabs Content-->
                    <div class="tab-content">
                        <div id="tabs-box-dados" class="tab-pane fade in active">
                            @foreach($config as $c)
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label">{{$c->name}}</label>
                                            <div class="clearfix"></div>
                                            @if($c->type == 'checkbox')
                                                <input type="checkbox" class="form-control" name="config[{{$c->id}}]" value="1" data-toggle="toggle" data-on="Exibir" data-off="Não Exibir" data-size="small" data-onstyle="success" data-offstyle="danger" data-width="100" @if($c->value == 'on') checked  @endif >
                                            @elseif($c->type == 'text')
                                                <input type="text" class="form-control" name="config[{{$c->id}}]" value="{{$c->value}}">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="tab-footer text-right">
                            <button class="btn btn-primary btn-lg" type="submit"><i class="fa fa-save"></i> Salvar</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@push('styles')
    {{ Html::style('intranet/plugins/bootstrap-toggle/css/bootstrap-toggle.min.css') }}
@endpush

@push('scripts')
    {{ Html::script('intranet/plugins/bootstrap-toggle/js/bootstrap-toggle.min.js') }}
@endpush