@extends('intranet.layouts.master')

@section('title', $controllers->title)

@push('page.title')

    <div class="row">
        <div class="col-md-8">
            <h1 class="page-header text-overflow"><i class="{{ $controllers->icon }}"></i> {{ $controllers->title }}</h1>
        </div>
        <div class="col-md-4 text-right">
            <a href="javascript:void(0)" onclick="window.history.back();" class="btn btn-default"><i class="fa fa-angle-left"></i> Voltar</a>
            @permission($controllers->actions->add->permission)
            <a href="{{ route($controllers->actions->add->route) }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Adicionar</a>
            @endpermission
        </div>
    </div>

@endpush

@section('content')

    <div class="panel">
        {!! $datatable->table(['class' => 'table table-striped table-hover']) !!}
    </div>

@endsection

@push('styles')
    {!! Html::style('intranet/node_modules/datatables.net-bs/css/dataTables.bootstrap.css') !!}
    {!! Html::style('intranet/node_modules/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') !!}
@endpush

@push('scripts')

    {!! Html::script('intranet/node_modules/datatables.net/js/jquery.dataTables.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-bs/js/dataTables.bootstrap.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons/js/dataTables.buttons.min.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons/js/buttons.colVis.min.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons/js/buttons.html5.min.js') !!}
    {!! Html::script('intranet/node_modules/jszip/dist/jszip.min.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') !!}

    {!! $datatable->scripts() !!}

    <script>

        let html;

        function datatablesFormatStrong(data){

            html = '<strong>'+data+'</strong>';
            return html;

        }

        function datatablesCategory(data,categories){

            html = categories[data];

            return html;
        }

        function datatablesFormatStatus(data){

            html =  '<div class="text-center">';
            html += (data == 1) ? '<i class="fa fa-circle text-success" title="Ativo"></i> Ativo' : '<i class="fa fa-circle text-danger" title="Inativo"></i> Inativo';
            html += '</div>';

            return html;
        }

        function datatablesFormatActive(data){

            html =  '<div class="text-center">';
            html += (data == 1) ? '<i class="fa fa-circle text-success" title="Sim"></i> Sim' : '<i class="fa fa-circle text-danger" title="Não"></i> Não';
            html += '</div>';

            return html;
        }

        function datatablesFormatActions(data, type, full){

            html = '<div class="text-center">';

            @if(isset($controllers->actions->edit))
            @permission($controllers->actions->edit->permission)
            html += '<a href="{{ route($controllers->actions->edit->route) }}/'+full['id']+'" class="btn btn-icon-toggle btn-sm btn-info"><i class="fa fa-pencil"></i></a>';
            @endpermission
            @endif

            @if(isset($controllers->actions->delete))
            @permission($controllers->actions->delete->permission)
            html += '<a href="{{ route($controllers->actions->delete->route) }}/'+full['id']+'" class="btn btn-icon-toggle btn-sm btn-danger mar-lft"><i class="fa fa-trash"></i></a>';
            @endpermission
            @endif

            html += '</div>';

            return html;
        }

        function datatablesFormatIndicationStatus(data, type, full){

            if(data !== null) {

                html = '<div class="label label-table label-' + (full['status_status']) ? 'success' : 'danger' + '">' + data + '</div>';
                return html;

            } else
                return '<div class="label label-table label-warning">Pendente</div>';

        }

        function datatablesFormatActionShow(data){

            return '<div class="text-center"><a href="'+data+'" class="btn btn-icon-toggle btn-sm btn-mint"><i class="fa fa-info-circle"></i></a></div>';
        }

        function datatablesFormatProductionStatus(data, type, full){

            if(data !== null) {

                console.log(data);

                switch (data){

                    case '0' :
                        html = '<div class="label label-table label-danger">Cancelada</div>';
                        break;

                    case '1' :
                        html = '<div class="label label-table label-success">Confirmada</div>';
                        break;

                    case '2' :
                        html = '<div class="label label-table label-purple">Em contestação</div>';
                        break;

                    case '3' :
                        html = '<div class="label label-table label-warning">Aguardando confirmação</div>';
                        break;

                    default:
                        html = '';

                }

                return html;

            } else
                return '';

        }

    </script>

@endpush