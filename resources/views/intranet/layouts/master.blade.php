<!doctype html>
<html lang="pt_BR">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="apple-touch-icon" sizes="76x76" href="{{ url('/intranet/assets/icons/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ url('/intranet/assets/icons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('/intranet/assets/icons/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ url('/intranet/assets/icons/site.webmanifest') }}">
    <link rel="mask-icon" href="{{ url('/intranet/assets/icons/safari-pinned-tab.svg') }}" color="#0B3535">
    <meta name="msapplication-TileColor" content="#0b3535">
    <meta name="theme-color" content="#0b3535">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>SICOOB Credipatos - @yield('title')</title>
    <meta name="description" content="@yield('description')">
    <meta name="author" content="Samuel Edson">

    @stack('styles')

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>

    {{ Html::style('intranet/node_modules/bootstrap/dist/css/bootstrap.min.css') }}
    {{ Html::style('intranet/assets/css/nifty.min.css') }}
    {{ Html::style('intranet/assets/css/theme-navy.css') }}

    {{ Html::style('intranet/node_modules/font-awesome/css/font-awesome.min.css') }}

    {{ Html::style('intranet/assets/css/app.css') }}

</head>

<body>

<div id="wait">
    <img src="{{ url('intranet/assets/img/page-loader.gif') }}">
</div>

<div id="container" class="effect aside-float aside-bright mainnav-fixed mainnav-sm navbar-fixed">

    @include('intranet.layouts.inc.navbar')

    <div class="boxed">

        <!--CONTENT CONTAINER-->
        <!--===================================================-->
        <div id="content-container">

            <div id="page-head">

                <div id="page-title">

                    @stack('page.title')

                </div>

                @if (session('alert'))
                    <div class="alert alert-{!! session('alert.status') !!} alert-dismissible mar-all">
                        <button class="close" data-dismiss="alert"><i class="fa fa-times-circle"></i></button>
                        {{ session('alert.message') }}
                    </div>
                @endif

                @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible mar-all">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

            </div>

            <div id="page-content">

                @yield('content')

            </div>



        </div>
        <!--===================================================-->
        <!--END CONTENT CONTAINER-->



        @include('intranet.layouts.inc.aside')

        @include('intranet.layouts.inc.navigation')

    </div>



    @include('intranet.layouts.inc.footer')


    <!-- SCROLL PAGE BUTTON -->
    <!--===================================================-->
    <button class="scroll-top btn">
        <i class="pci-chevron chevron-up"></i>
    </button>
    <!--===================================================-->



</div>

    {{ Html::script('intranet/node_modules/jquery/dist/jquery.min.js') }}
    {{ Html::script('intranet/node_modules/bootstrap/dist/js/bootstrap.min.js') }}
    {{ Html::script('intranet/assets/js/nifty.min.js') }}

    <script>

        function wait(o){
            o ? $('#wait').show() : $('#wait').hide();
        }

        wait(true);

        $(document).ready(function () {
            wait(false);
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).ajaxStart(function () {
            wait(true);
        });

        $(document).ajaxComplete(function () {
            wait(false);
        });

    </script>

    @stack('scripts')

</body>
</html>
