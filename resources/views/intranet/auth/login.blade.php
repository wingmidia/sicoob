<!doctype html>
<html lang="pt_BR">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="apple-touch-icon" sizes="76x76" href="{{ url('/intranet/assets/icons/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ url('/intranet/assets/icons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('/intranet/assets/icons/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ url('/intranet/assets/icons/site.webmanifest') }}">
    <link rel="mask-icon" href="{{ url('/intranet/assets/icons/safari-pinned-tab.svg') }}" color="#0B3535">
    <meta name="msapplication-TileColor" content="#0b3535">
    <meta name="theme-color" content="#0b3535">

    <title>SICOOB Credipatos - Login</title>
    <meta name="description" content="Faça login na intranet da SICOOB Credipatos">
    <meta name="author" content="Samuel Edson">

    {{ Html::style('intranet/node_modules/bootstrap/dist/css/bootstrap.min.css') }}
    {{ Html::style('intranet/assets/css/app.css') }}

</head>

<body>

<main role="main" class="container">

    <div class="panel panel-success" style="max-width: 300px; margin: 0 auto; margin-top: 5%">
        <div class="panel-heading text-center">
            <img src="{{ url('/intranet/assets/img/logo-sicoob-credipatos.png') }}" alt="Multi CRM" style="vertical-align: bottom">
        </div>
        <div class="panel-body">
            <form action="{{ route('intranet.auth.login') }}" method="post">
                {{ csrf_field() }}
                @if (session('status'))
                    <div class="alert alert-danger">
                        {{ session('status') }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        Não foi possível efetuar login!
                    </div>
                @endif
                <div class="form-group">
                    <label for="input_email">Email</label>
                    <input type="email" name="email" maxlength="15" value="{{ old('email') }}" class="form-control" id="input_email" placeholder="Digite seu email">
                </div>
                <div class="form-group">
                    <label for="input_password">Senha</label>
                    <input type="password" name="password" maxlength="25" class="form-control" id="input_password" placeholder="Senha">
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-block btn-success">Entrar</button>
                </div>
            </form>
        </div>
    </div>

</main>

</body>
</html>
