@extends('intranet.layouts.master')

@section('title', $controllers->title)

@push('page.title')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="row">
                <div class="col-md-8">
                    <h1 class="page-header text-overflow"><i class="{{ $controllers->icon }}"></i> {{ $controllers->title }} | <small>{!! (!empty($item->id)) ? '<i class="fa fa-pencil"></i> Editando perfil: ' . $item->display_name : '<i class="fa fa-plus-circle"></i> Adicionando novo perfil' !!}</small></h1>
                </div>
                <div class="col-md-4 text-right">
                    <a href="{{ route($controllers->route) }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Voltar</a>
                </div>
            </div>
        </div>
    </div>

@endpush

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form action="{{ route('intranet.roles.store') }}" method="post">
                {{ csrf_field() }}
                @if(isset($item->id))
                    <input type="hidden" name="id" value="{{ old('id', $item->id) }}">
                @endif
                <div class="tab-base">

                    <!--Nav Tabs-->
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tabs-box-dados">Dados básicos</a></li>
                        <li><a data-toggle="tab" href="#tabs-box-permissao">Permissões</a></li>
                    </ul>

                    <!--Tabs Content-->
                    <div class="tab-content">
                        <div id="tabs-box-dados" class="tab-pane fade in active">
                            <fieldset>
                                <legend>Insira os dados básicos</legend>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Título</label>
                                            <input type="text" class="form-control" name="display_name" value="{{ old('display_name', $item->display_name) }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Apelido</label>
                                            <input type="text" class="form-control" name="name" value="{{ old('name', $item->name) }}" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label">Descrição</label>
                                            <textarea name="description" class="form-control">{{ old('description', $item->description) }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div id="tabs-box-permissao" class="tab-pane fade">
                            <fieldset>
                                <legend>Escolha as permissões para o perfil</legend>
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-hover table-striped">
                                            <thead>
                                            <tr>
                                                <th>Nome</th>
                                                <th width="10%" class="text-center">Status</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($permissions as $permission)
                                                <tr>
                                                    <td>
                                                        {{ $permission->display_name }}
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="permissions[]" value="{{ $permission->id }}" data-toggle="toggle" data-on="Ativo" data-off="Inativo" data-size="mini" data-onstyle="success" data-offstyle="danger" data-width="80" @if($item->hasPermission($permission->name)) checked @endif>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <div class="tab-footer text-right">
                            <button class="btn btn-primary btn-lg" type="submit"><i class="fa fa-save"></i> Salvar</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@push('styles')
    {{ Html::style('intranet/plugins/bootstrap-toggle/css/bootstrap-toggle.min.css') }}
@endpush

@push('scripts')
    {{ Html::script('intranet/plugins/bootstrap-toggle/js/bootstrap-toggle.min.js') }}
@endpush