@extends('intranet.layouts.master')

@section('title', $controllers->title)

@push('page.title')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="row">
                <div class="col-md-8">
                    <h1 class="page-header text-overflow"><i class="{{ $controllers->icon }}"></i> {{ $controllers->title }} | <small>{!! (!empty($item->id)) ? '<i class="fa fa-pencil"></i> Editando: ' . $item->name : '<i class="fa fa-plus-circle"></i> Adicionando novo' !!}</small></h1>
                </div>
                <div class="col-md-4 text-right">
                    <a href="{{ route($controllers->route) }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Voltar</a>
                    @if(!empty($item->id))
                        <a href="{{ route($controllers->actions->add->route) }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Adicionar</a>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endpush

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form action="{{ route($controllers->actions->store->route) }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                @if(isset($item->id))
                    <input type="hidden" name="id" value="{{ old('id', $item->id) }}">
                @endif
                <div class="tab-base">

                    <!--Nav Tabs-->
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tabs-box-dados">Dados básicos</a></li>
                    </ul>

                    <!--Tabs Content-->
                    <div class="tab-content">
                        <div id="tabs-box-dados" class="tab-pane fade in active">
                            <div class="row">
                                <div class="col-md-8">
                                    <fieldset>
                                        <legend>Insira os dados básicos</legend>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Nome</label>
                                                    <input type="text" class="form-control" name="name" value="{{ old('name', $item->name) }}">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Descrição</label>
                                                    <input type="text" class="form-control" name="description" value="{{ old('description', $item->description) }}">
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset>
                                        <legend>Parâmetros</legend>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="control-label">Colaboradores podem prospectar?</label>
                                                    <div class="clearfix"></div>
                                                    <input type="checkbox" class="form-control" name="prospection" value="1" data-toggle="toggle" data-on="Sim" data-off="Não" data-size="small" data-onstyle="success" data-offstyle="danger" data-width="80" @if(old('prospection', $item->prospection)) checked @endif>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Endereço</label>
                                        <input type="text" class="form-control" name="address" value="{{ old('address', $item->address) }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Bairro</label>
                                        <input type="text" class="form-control" name="district" value="{{ old('district', $item->district) }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Cep</label>
                                        <input type="text" class="form-control" name="cep" value="{{ old('cep', $item->cep) }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">CNPJ</label>
                                        <input type="text" class="form-control" name="cnpj" value="{{ old('cnpj', $item->cnpj) }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Email</label>
                                        <input type="text" class="form-control" name="email" value="{{ old('email', $item->email) }}">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="control-label">Foto</label>
                                        <input type="file" class="form-control" name="photo">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-footer text-right">
                            <button class="btn btn-primary btn-lg" type="submit"><i class="fa fa-save"></i> Salvar</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@push('styles')
    {{ Html::style('intranet/plugins/bootstrap-toggle/css/bootstrap-toggle.min.css') }}
@endpush

@push('scripts')
    {{ Html::script('intranet/plugins/bootstrap-toggle/js/bootstrap-toggle.min.js') }}
@endpush