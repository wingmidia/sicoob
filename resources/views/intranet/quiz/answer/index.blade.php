@extends('intranet.layouts.master')

@section('title', $controllers->title)

@push('page.title')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="row">
            <div class="col-md-8">
                <h1 class="page-header text-overflow"><i class="{{ $controllers->icon }}"></i> {{ $controllers->title }} | <small><i class="fa fa-pencil"></i> Respondendo quiz</small></h1>
            </div>
            <div class="col-md-4 text-right">
                <a href="{{ route($controllers->route) }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Voltar</a>
            </div>
        </div>
    </div>
</div>
@endpush

@section('content')
    <form action="{{route($controllers->actions->do_awnser->route)}}" id="form-answer" method="POST">
        @if(!$visitor)
        {{ csrf_field() }}
        @endif
        <div class="col-md-8 col-md-offset-2" style="margin-bottom: 60px">
            <input type="hidden" name="quiz_id" value="{{$quiz->id}}">

            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8">
                            <h2 class="quiz-title"></h2>
                            <p class="quiz-description"></p>
                        </div>
                        <div class="col-md-4 quiz-timer-container">
                            <h3 class="quiz-timer text-right"><strong></strong></h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="asks-container">

            </div>

            @if(!$visitor)
                <div class="panel-footer footer-fixed text-right bord-top bg-gray">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="box-inline">
                                <button type="button" class="previous btn btn-default btn-lg"><i class="fa fa-angle-left"></i> Voltar</button>
                                <button type="submit" class="finish btn btn-primary btn-lg">Finalizar</button>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </form>
@endsection

@push('scripts')
    {{ Html::script('intranet/node_modules/moment/min/moment.min.js') }}
    {{ Html::script('intranet/node_modules/moment/locale/pt-br.js') }}

    <script type="text/javascript">
        $(document).ready(function () {
            var quiz = {};
            var result = {};
            var visitor = false;

            @if(!empty($quiz))
                quiz = {!! json_encode($quiz) !!};
            @endif
            @if(!empty($resultFind))
                result = {!! json_encode($resultFind) !!};
            @endif
            @if(!empty($visitor))
                visitor = {!! $visitor !!};
            @endif


            $('.quiz-title').append('<strong>'+quiz.name+'</strong>');
            $('.quiz-description').text(quiz.description);
            var route = "{{   route($controllers->route) }}";

            if(quiz.time && !visitor){
                var timerContainer = $('.quiz-timer-container .quiz-timer strong');

                var toTime = moment(result.started_at).add(quiz.time, 'minutes');

                var timeNow = moment().utc();

                var timeUp = moment(toTime - timeNow);

                var diffSeconds = timeUp.get('minutes')*60 + timeUp.get('seconds');

                if(diffSeconds > quiz.time*60){
                    diffSeconds = quiz.time*60;
                    timeUp.set('minutes', quiz.time).set('seconds', 0);
                }


                timerContainer.text(timeUp.format('mm:ss'));

                var timer = setInterval(function() {
                    // var now = moment().utc();
                    diffSeconds -= 1;
                    timeUp.subtract(1, 'seconds');
                    timerContainer.text(timeUp.format('mm:ss'));

                    if(diffSeconds<0){
                        alert('Tempo de resposta do quiz terminou');
                        window.location.href = route;
                        clearInterval(timer);
                    }

                }, 1000);
            }
            if(quiz.asks) {
                quiz.asks.forEach(function (ask, i) {
                    if (ask.active) {
                        var askTemplate = $($('#askTemplate').html());
                        askTemplate.find('.ask-title').text(ask.title);
                        if (ask.alternatives) {
                            ask.alternatives.forEach(function (alternative, idx) {
                                if(alternative.active) {
                                    var alternativeTemplate = $($("#alternativeTemplate").html());
                                    alternativeTemplate.find('.alternative-title').text(alternative.title);
                                    alternativeTemplate.find('.alternative-value').attr('name', 'asks[' + ask.id + '][alternatives][' + alternative.id + '][value]');
                                    askTemplate.find('.ask-alternatives').append(alternativeTemplate);
                                }
                            });
                        }
                        $('.asks-container').append(askTemplate);
                    }
                });
            }
            if(result.answers){
                result.answers.forEach(function (answer) {
                    var name = `asks[${answer.quiz_ask_id}][alternatives][${answer.quiz_ask_alternative_id}]`;
                    var altName = name+"[value]";
                    var element = $('.alternative-value[name="'+altName+'"]');
                    element.attr('checked', 'checked');
                    var altIdName = name + '[answer_id]';
                    var idElm = element.parent().parent().find('.alternative-asnwer');
                    idElm.val(answer.id);
                    idElm.attr('name', altIdName);
                    element.on('change', function () {
                        console.log('che', this.checked);
                        if(!this.checked) {
                            idElm.removeAttr('name')
                        }else{
                            idElm.attr('name', altIdName);
                        }
                    });
                });
            }
        });

    </script>


    <script type="text/template" id="askTemplate">
        <div class="row quiz-ask">
            <div class="col-sm-12">
                <div class="panel panel-info">
                    <div class="panel-body">
                        <h3 class="ask-title"></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12 ask-alternatives">

                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </script>
    <script type="text/template" id="alternativeTemplate">
        <div class="form-group">
            <div class="checkbox">
                <input type="hidden" class="alternative-asnwer">
                <label><input type="checkbox" class="alternative-value" value="1"><span class="alternative-title"></span></label>
            </div>
        </div>
    </script>
@endpush