@extends('intranet.layouts.master')

@section('title', $controllers->title)


@push('page.title')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <div class="row">
                <div class="col-md-8">
                    <h1 class="page-header text-overflow"><i class="{{ $controllers->icon }}"></i> {{ $controllers->title }} | <small><i class="fa fa-plus-circle"></i> Adicionando quiz</small></h1>
                </div>
                <div class="col-md-4 text-right">
                    <a href="{{ route($controllers->route) }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Voltar</a>
                </div>
            </div>

        </div>
    </div>

@endpush

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-info">
            <div id="wizard-quiz">
                <div class="wz-heading pad-ver bord-btm">
                    <!--Progress bar-->
                    <div class="progress progress-xs progress-light-base">
                        <div class="progress-bar progress-bar-info"></div>
                    </div>
                    <ul class="wz-nav-off">
                        <li class="col-xs-4">
                            <a data-toggle="tab" href="#wizard-quiz-tab1" title="Dados do quiz" class="add-tooltip">
                                <div class="icon-wrap icon-wrap-sm bg-info">
                                    <i class="wz-icon fa fa-list-ul fa-2x icon-lg"></i>
                                    <i class="wz-icon-done fa fa-check-circle fa-2x icon-lg"></i>
                                </div>
                            </a>
                        </li>
                        <li class="col-xs-4">
                            <a data-toggle="tab" href="#wizard-quiz-tab2" title="Perguntas do quiz" class="add-tooltip">
                                <div class="icon-wrap icon-wrap-sm bg-info">
                                    <i class="wz-icon fa fa-info fa-2x icon-lg"></i>
                                    <i class="wz-icon-done fa fa-check-circle fa-2x icon-lg"></i>
                                </div>
                            </a>
                        </li>
                        <li class="col-xs-4">
                            <a data-toggle="tab" href="#wizard-quiz-tab3" title="Finalizar" class="add-tooltip">
                                <div class="icon-wrap icon-wrap-sm bg-info">
                                    <i class="wz-icon fa fa-thumbs-up fa-2x icon-lg"></i>
                                    <i class="wz-icon-done fa fa-check-circle fa-2x icon-lg"></i>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>

                <!--form-->
                <form id="form-quiz" action="{{route($controllers->actions->store->route)}}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ old('id') }}">
                    <div class="panel-body">
                        <div class="tab-content">
                            <div id="wizard-quiz-tab1" class="tab-pane fade">
                                <div class="text-center">
                                    <h4>Novo quiz</h4>
                                    <p>Insira as informações do quiz. Fique atento para preencher os dados corretamente.</p>
                                </div>
                                <hr>
                                <fieldset>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="form-quiz-name">Nome</label>
                                                <input type="text" id="form-quiz-name" class="form-control" name="name" value="{{old('name')}}" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="form-quiz-description">Descrição</label>
                                                <textarea name="description" id="form-quiz-description" class="form-control" value="{{old('description')}}" cols="30" rows="3"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="form-quiz-start_date">Data inicial</label>
                                                <input id="form-quiz-start_date" type="text" class="form-control mask-date" name="start_date" value="{{ old('start_date') }}">
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="form-quiz-end_date">Data final</label>
                                                <input id="form-quiz-end_date" type="text" class="form-control mask-date" name="end_date" value="{{ old('end_date') }}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="form-quiz-time">Tempo(minutos)</label>
                                                <input type="number" id="form-quiz-time" class="form-control" name="time" min="0"  value="{{old('time')}}">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <div class="radio">
                                                <label><input type="radio" name="active" value="1" @if(old('active') OR empty(old('active'))) checked @endif>Ativo</label>
                                            </div>
                                            <div class="radio">
                                                <label><input type="radio" name="active" value="0" @if(!old('active') && !empty(old('active'))) checked @endif>Inativo</label>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div id="wizard-quiz-tab2" class="tab-pane" >
                                <div class="text-center">
                                    <h4>Perguntas do quiz</h4>
                                </div>
                                <hr>
                                <div class="panels-asks">
                                    <div class="row pad-ver">
                                        <div class="col-sm-12">
                                            <button id="btn-add-ask" class="btn btn-primary pull-right"><i class="fa fa-plus"></i>&nbsp;Adicionar pergunta</button>
                                        </div>
                                    </div>
                                    @if(!empty(old('asks')))
                                        @foreach(old('asks') as $key => $ask)
                                            <div class="row quiz-ask-one">
                                                <div class="col-sm-12">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h3 class="panel-title"></h3>
                                                        </div>
                                                        <div class="panel-body">
                                                            <fieldset>
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <label>Título</label>
                                                                            <input type="text" name="asks[{{$key}}].title" class="form-control ask-title" value="{{$ask->title}}" required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-3">
                                                                        <div class="form-group">
                                                                            <label >Peso</label>
                                                                            <input type="number" name="asks[{{$key}}].weight" class="form-control ask-weight" name="weight" min="1" value="{{$ask->weight}}">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-6 col-sm-offset-3">
                                                                        <div class="radio">
                                                                            <label><input type="radio" name="asks[{{$key}}].active" value="1" class="ask-active" @if($ask->active)checked @endif>Ativo</label>
                                                                        </div>
                                                                        <div class="radio">
                                                                            <label><input type="radio" name="asks[{{$key}}].active" value="0" class="ask-active" @if(!$ask->active)checked @endif>Inativo</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <table class="ask-alternative-table table table-default">
                                                                            <thead>
                                                                            <tr>
                                                                                <th colspan="3">
                                                                                    <h4 class="text-center">Alternativas</h4>
                                                                                </th>
                                                                                <th><button class="ask-add-alternative btn btn-success"><i class="fa fa-plus"></i>&nbsp;Adicionar Alternativa</button></th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>

                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <div id="wizard-quiz-tab3" class="tab-panel fade">
                                <div class="text-center">
                                    <h4>Concluir quiz</h4>
                                    <p>Confira os dados do quiz e clique no botão finalizar.</p>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <fieldset>
                                            <legend class="pad-ver">Resumo do quiz</legend>
                                            <div class="panel panel-primary">
                                                <table id="tab-desc-quiz" class="table table-bordered">
                                                    <tbody>
                                                        <tr class="quiz-tab-name">
                                                            <th>Nome</th>
                                                            <td></td>
                                                        </tr>
                                                        <tr class="quiz-tab-description">
                                                            <th>Descrição</th>
                                                            <td></td>
                                                        </tr>
                                                        <tr class="quiz-tab-start_date">
                                                            <th>Data inicial</th>
                                                            <td></td>
                                                        </tr>
                                                        <tr class="quiz-tab-end_date">
                                                            <th>Data final</th>
                                                            <td></td>
                                                        </tr>
                                                        <tr class="quiz-tab-time">
                                                            <th>Tempo</th>
                                                            <td></td>
                                                        </tr>
                                                        <tr class="quiz-tab-active">
                                                            <th>Ativo</th>
                                                            <td></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <legend class="pad-ver">Perguntas</legend>
                                            <div class="ask-resume-panels">

                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer footer-fixed text-right bord-top bg-gray">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="box-inline">
                                    <button type="button" class="previous btn btn-default btn-lg"><i class="fa fa-angle-left"></i> Anterior</button>
                                    <button type="button" class="next btn btn-primary btn-lg">Avançar <i class="fa fa-angle-right"></i></button>
                                    <button type="submit" class="finish btn btn-primary btn-lg" formnovalidate>Finalizar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection

@push('styles')
    {{ Html::style('intranet/plugins/bootstrap-validator/bootstrapValidator.min.css') }}
    {{ Html::style('intranet/plugins/bootstrap-toggle/css/bootstrap-toggle.min.css') }}
@endpush

@push('scripts')
    {{ Html::script('intranet/plugins/bootstrap-toggle/js/bootstrap-toggle.min.js') }}
    {{ Html::script('intranet/plugins/bootstrap-validator/bootstrapValidator2.min.js') }}
    {{ Html::script('intranet/node_modules/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}
    {{ Html::script('intranet/node_modules/jquery-mask-plugin/dist/jquery.mask.min.js') }}
    {{ Html::script('intranet/node_modules/moment/min/moment.min.js') }}
    {{ Html::script('intranet/node_modules/moment/locale/pt-br.js') }}

    <script>
        var wizardSelector = '#wizard-quiz';
        console.log('bot');


        var quiz = {
            asks: []
        };


        $(wizardSelector).bootstrapWizard({
            tabClass: 'wz-steps',
            nextSelector: '.next',
            previousSelector: '.previous',
            onTabClick: function(tab, navigation, index) {
                return false;
            },
            onInit : function(){
                $(wizardSelector).find('.finish').hide().prop('disabled', true);
                $(wizardSelector).find('.finish').hide();
            },
            onTabShow: function(tab, navigation, index) {

                var $total = navigation.find('li').length;
                var $current = index+1;
                var $percent = (index/$total) * 100;
                var margin = (100/$total)/2;

                $(wizardSelector).find('.progress-bar').css({width:$percent+'%', 'margin': 0 + 'px ' + margin + '%'});

                navigation.find('li:eq('+index+') a').trigger('focus');
                // If it's the last tab then hide the last button and show the finish instead
                if($current >= $total) {
                    $(wizardSelector).find('.next').hide();
                    $(wizardSelector).find('.finish').show();
                    $(wizardSelector).find('.finish').attr('disabled', false);
                } else {
                    $(wizardSelector).find('.next').show();
                    $(wizardSelector).find('.finish').hide().prop('disabled', true);
                    $(wizardSelector).find('.finish').hide();
                }

            },
            onNext: function(tab, navigation, index){
                isValid = null;

                $('#form-quiz').formValidation('validate');

                if(isValid===false) return  false;

                console.log('nav '+index, navigation);

                if(index==1){
                    if(quiz.asks.length==0)
                        appendAsk();
                    quiz.name = $('[name="name"]').val();
                    quiz.description = $('[name="description"]').val();
                    quiz.start_date = $('[name="start_date"]').val();
                    quiz.end_date = $('[name="end_date"]').val();
                    quiz.time = $('[name="time"]').val();
                    quiz.active = $('[name="active"]:checked').val();

                }

                if(index==2){
                    var empty = false;
                    var titles = $('.ask-title');
                    titles.each(function () {
                       if($(this).val()=="") empty = true;
                       this.reportValidity('Titulo obrigatório');
                    });
                    if(empty) return false;
                    var asksContent = $('.panels-asks .quiz-ask-one');
                    var quizAsks = [];
                    asksContent.each(function () {
                       var oneAsk = {
                           alternatives: []
                       };
                        oneAsk.title = $(this).find('.ask-title').val();
                        oneAsk.weight = $(this).find('.ask-weight').val();
                        oneAsk.active = $(this).find('.ask-active:checked').val();

                        $(this).find('.ask-alternative-table .ask-alternative-item').each(function () {
                            var alternative = {};
                            alternative.title = $(this).find('.ask-alternative-title').val();
                            alternative.correct = $(this).find('.ask-alternative-correct:checked').val();
                            alternative.active = $(this).find('.ask-alternative-active:checked').val();
                            oneAsk.alternatives.push(alternative);
                        });
                        quizAsks.push(oneAsk);
                    });
                    quiz.asks = quizAsks;

                    $('#tab-desc-quiz')
                        .find('.quiz-tab-name td')
                        .html(quiz.name);

                    $('#tab-desc-quiz')
                        .find('.quiz-tab-description td')
                        .html(quiz.description);

                    $('#tab-desc-quiz')
                        .find('.quiz-tab-start_date td')
                        .html(quiz.start_date);

                    $('#tab-desc-quiz')
                        .find('.quiz-tab-end_date td')
                        .html(quiz.end_date);

                    $('#tab-desc-time')
                        .find('.quiz-tab-time td')
                        .html(quiz.time?quiz.time:'0');

                    $('#tab-desc-quiz')
                        .find('.quiz-tab-active td')
                        .html(quiz.active?'SIM':'NÃO');

                    $('.ask-resume-panels').html('');

                    quiz.asks.forEach(function (thisAsk, index) {
                        var askTemplate = $($('#askResumeTemplate').html());
                        askTemplate.find('.panel-title').html('Pergunta '+(index+1));
                        askTemplate.find('.ask-tab-title td').html(thisAsk.title);
                        askTemplate.find('.ask-tab-weight td').html(thisAsk.weight);
                        askTemplate.find('.ask-tab-active td').html(thisAsk.active==1?'SIM':'NÃO');
                        $('.ask-resume-panels').append(askTemplate);
                    });
                    console.log($('#form-quiz').serialize());
                }

                console.log(quiz);
            }
        });

        var isValid;


        $('#form-quiz').on('init.field.fv', function(e, data) {
            // data.field   --> The field name
            // data.element --> The field element

        }).formValidation({
            message: 'Este valor não é valido',
            feedbackIcons: {
                valid: 'fa fa-check-circle fa-lg text-success',
                invalid: 'fa fa-times-circle fa-lg',
                validating: 'fa fa-refresh'
            },
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'O nome é obrigatório.'
                        },
                        stringLength: {
                            min: 3,
                            message: 'Nome inválido.'
                        }
                    }
                },
                start_date: {
                    validators: {
                        notEmpty: {
                            message: 'A data inicial é obrigatória.'
                        },
                        stringLength: {
                            min: 10,
                            max: 10,
                            message: 'Data inválida.'
                        }
                    }
                },
                end_date: {
                    validators: {
                        notEmpty: {
                            message: 'A data final é obrigatória.'
                        },
                        stringLength: {
                            min: 10,
                            max: 10,
                            message: 'Data inválida.'
                        }
                    }
                }
            }
        }).on('success.field.fv', function(e, data) {
            // $(e.target)  --> The field element
            // data.bv      --> The BootstrapValidator instance
            // data.field   --> The field name
            // data.element --> The field element

            var $parent = data.element.parents('.form-group');

            // Remove the has-success class
            $parent.removeClass('has-success');

            // Hide the success icon
            //$parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        }).on('err.field.fv', function(e) {
            isValid = false;
        });

        $('#btn-add-ask').on('click', function () {
            appendAsk();
        });

        $(document).on('click', '.ask-add-alternative', function () {
            var askContainer = $(this).closest('.quiz-ask-one');

            let askName = askContainer.find('.ask-title').attr('name')
                .replace('asks[','')
                .replace('][title]', '');

            var askIndex = parseInt(askName);

            appendAlternative(askIndex, askContainer);
        });


        function appendAsk() {
            var index = quiz.asks.length;
            console.log('ask: '+index, quiz);
            var newAsk = $($('#askTemplate').html());
            newAsk.find('.panel-title').html('Pergunta '+(index+1));
            newAsk.find('.ask-title').attr('name', 'asks['+index+'][title]');
            newAsk.find('.ask-weight').attr('name', 'asks['+index+'][weight"]');
            newAsk.find('.ask-active').attr('name', 'asks['+index+'][active]');
            quiz.asks.push({
                alternatives: []
            });
            appendAlternative(index, newAsk);
            $('.panels-asks').append(newAsk);
        }

        function appendAlternative(askIndex, askContainer) {


            var currentAsk = quiz.asks[askIndex];
            console.log('cur', currentAsk);
            if(!currentAsk.alternatives)
                currentAsk.alternatives = [];

            var alternativeAsk = currentAsk.alternatives.length;

            var alternativeTemplate = $($('#askAlternativeTemplate').html());

            alternativeTemplate.find('.ask-alternative-index').text(alternativeAsk+1);
            alternativeTemplate.find('.ask-alternative-title').attr('name', 'asks['+askIndex+'][alternatives]['+alternativeAsk+'][title]');
            alternativeTemplate.find('.ask-alternative-correct').attr('name', 'asks['+askIndex+'][alternatives]['+alternativeAsk+'][correct]');
            alternativeTemplate.find('.ask-alternative-active').attr('name', 'asks['+askIndex+'][alternatives]['+alternativeAsk+'][active]');

            askContainer.find('.ask-alternative-table tbody').append(alternativeTemplate);
            quiz.asks[askIndex].alternatives.push({
                active: 1
            });
        }

        function setMask(){
            $('.mask-date').mask('00/00/0000', {placeholder: "__/__/____", selectOnFocus: true});
            $('.mask-cep').mask('00000-000', {placeholder: "_____-___", selectOnFocus: true});

            $('.mask-cpf').mask('000.000.000-00', {placeholder: "___.___.___-__", reverse: true, selectOnFocus: true});
            $('.mask-cnpj').mask('00.000.000/0000-00', {placeholder: "__.___.___/____-__", reverse: true, selectOnFocus: true});
        }
        setMask();
    </script>

    <script id="askTemplate" type="text/template">
        <div class="row quiz-ask-one">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"></h3>
                    </div>
                    <div class="panel-body">
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Título</label>
                                        <input type="text" name="asks[index].title" class="form-control ask-title" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label >Peso</label>
                                        <input type="number" name="asks[index].weight" class="form-control ask-weight" min="1" value="1">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-sm-offset-3">
                                    <div class="radio">
                                        <label><input type="radio" name="asks[index].active" value="1" class="ask-active" checked>Ativo</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="asks[index].active" value="0" class="ask-active">Inativo</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <table class="ask-alternative-table table table-default">
                                        <thead>
                                            <tr>
                                                <th colspan="3">
                                                    <h4 class="text-center">Alternativas</h4>
                                                </th>
                                                <th><button class="ask-add-alternative btn btn-success"><i class="fa fa-plus"></i>&nbsp;Adicionar Alternativa</button></th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>

            </div>
        </div>
    </script>
    <script id="askResumeTemplate" type="text/template">
        <div class="row quiz-ask-one">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"></h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered ask-tab-desc">
                            <tbody>
                                <tr class="ask-tab-title">
                                    <th>Título</th>
                                    <td></td>
                                </tr>
                                <tr class="ask-tab-weight">
                                    <th>Peso</th>
                                    <td></td>
                                </tr>
                                <tr class="ask-tab-active">
                                    <th>Ativo</th>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </script>
    <script id="askAlternativeTemplate" type="text/template">
        <tr class="ask-alternative-item">
            <th>
                #<span class="ask-alternative-index"></span>
            </th>
            <td><input type="text" class="form-control ask-alternative-title" placeholder="Título"></td>
            <td>
                <div class="form-group">
                    <label><input type="radio" value="1" class="ask-alternative-correct" checked>Correta</label>
                    <label><input type="radio" value="0" class="ask-alternative-correct">Incorreta</label>
                </div>
            </td>
            <td>
                <label><input type="radio" value="1" class="ask-alternative-active" checked>Ativo</label>
                <label><input type="radio" value="0" class="ask-alternative-active">Inativo</label>
            </td>
        </tr>
    </script>
@endpush