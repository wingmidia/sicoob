@extends('intranet.layouts.master')


@section('title', $controllers->title)

@push('page.title')

    <div class="row">
        <div class="col-md-8">
            <h1 class="page-header text-overflow"><i class="{{ $controllers->icon }}"></i> Quizzes</h1>
        </div>
        <div class="col-md-4 text-right">
            @permission($controllers->actions->manager->permission)
            <a href="{{ route($controllers->actions->manager->route) }}" class="btn btn-info btn-lg"><i class="fa fa-pencil-square"></i> Gerenciar</a>
            @endpermission
        </div>
    </div>

@endpush


@section('content')

    <div class="panel">
        {!! $datatable->table(['class' => 'table table-striped table-hover']) !!}
    </div>

@endsection

@push('styles')
    {!! Html::style('intranet/node_modules/datatables.net-bs/css/dataTables.bootstrap.css') !!}
    {!! Html::style('intranet/node_modules/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') !!}
    <style>

    </style>
@endpush

@push('scripts')

    {!! Html::script('intranet/node_modules/datatables.net/js/jquery.dataTables.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-bs/js/dataTables.bootstrap.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons/js/dataTables.buttons.min.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons/js/buttons.colVis.min.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons/js/buttons.html5.min.js') !!}
    {!! Html::script('intranet/node_modules/jszip/dist/jszip.min.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') !!}

    {!! $datatable->scripts() !!}

    <script>

        let html;

        function datatablesFormatStrong(data){

            html = '<strong>'+data+'</strong>';
            return html;

        }

        function datatablesFormatQuizStatus(data) {
            return  (data == 1) ? '<i class="fa fa-circle text-success" title="Ativo"></i> Ativo' : '<i class="fa fa-circle text-danger" title="Inativo"></i> Inativo';
        }

        function datatablesFormatActions(data, type, full){
            console.log(full);
            html = '<div class="text-center">';

            /*html += '<a href="{{ route($controllers->actions->edit->route) }}/'+full['id']+'" class="btn btn-icon-toggle btn-sm btn-mint"><i class="fa fa-info-circle"></i></a>';*/


            @if(isset($controllers->actions->answer))
                @permission($controllers->actions->do_awnser->permission)
                if(!full['terminated'])
                    html += '<a href="{{ route($controllers->actions->answer->route) }}/'+full['id']+'" title="Responder quiz" class="btn btn-icon-toggle btn-sm btn-success mar-lft add-tooltip">Responder</a>';
                @endpermission
            @endif

            @if(isset($controllers->actions->answer))
                if(full['terminated'])
                    html += '<span class="label label-dark">FINALIZADO</span>';
                    //html += '<a href="{{ route($controllers->actions->answer->self, $collaboratorId) }}/'+full['id']+'" title="Ver resultado" class="btn btn-icon-toggle btn-sm btn-info mar-lft add-tooltip">Ver Respostas</a>';
            @endif

             html += '</div>';
             return html;
        }
    </script>
@endpush