@extends('intranet.layouts.master')

@section('title', $controllers->title)


@push('page.title')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <div class="row">
                <div class="col-md-8">
                    @if(empty($finded))
                    <h1 class="page-header text-overflow"><i class="{{ $controllers->icon }}"></i> {{ $controllers->title }} | <small><i class="fa fa-plus-circle"></i> Adicionando quiz</small></h1>
                    @else
                    <h1 class="page-header text-overflow"><i class="{{ $controllers->icon }}"></i> {{ $controllers->title }} | <small><i class="fa fa-plus-circle"></i> Editando quiz</small></h1>
                    @endif

                </div>
                <div class="col-md-4 text-right">
                    <a href="{{ route($controllers->actions->manager->route) }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Voltar</a>
                </div>
            </div>

        </div>
    </div>

@endpush

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-info">
            <div id="wizard-quiz">
                <div class="wz-heading pad-ver bord-btm">
                    <!--Progress bar-->
                    <div class="progress progress-xs progress-light-base">
                        <div class="progress-bar progress-bar-info"></div>
                    </div>
                    <ul class="wz-nav-off">
                        <li class="col-xs-4">
                            <a data-toggle="tab" href="#wizard-quiz-tab1" title="Dados do quiz" class="add-tooltip">
                                <div class="icon-wrap icon-wrap-sm bg-info">
                                    <i class="wz-icon fa fa-list-ul fa-2x icon-lg"></i>
                                    <i class="wz-icon-done fa fa-check-circle fa-2x icon-lg"></i>
                                </div>
                            </a>
                        </li>
                        <li class="col-xs-4">
                            <a data-toggle="tab" href="#wizard-quiz-tab2" title="Perguntas do quiz" class="add-tooltip">
                                <div class="icon-wrap icon-wrap-sm bg-info">
                                    <i class="wz-icon fa fa-info fa-2x icon-lg"></i>
                                    <i class="wz-icon-done fa fa-check-circle fa-2x icon-lg"></i>
                                </div>
                            </a>
                        </li>
                        <li class="col-xs-4">
                            <a data-toggle="tab" href="#wizard-quiz-tab3" title="Finalizar" class="add-tooltip">
                                <div class="icon-wrap icon-wrap-sm bg-info">
                                    <i class="wz-icon fa fa-thumbs-up fa-2x icon-lg"></i>
                                    <i class="wz-icon-done fa fa-check-circle fa-2x icon-lg"></i>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>

                <!--form-->
                <form id="form-quiz" action="{{route($controllers->actions->store->route)}}" method="POST">
                    {{ csrf_field() }}
                    <div class="panel-body">
                        <div class="tab-content">
                            <div id="wizard-quiz-tab1" class="tab-pane fade">
                                <div class="text-center">
                                    <h4>Novo quiz</h4>
                                    <p>Insira as informações do quiz. Fique atento para preencher os dados corretamente.</p>
                                </div>
                                <hr>
                                <fieldset class="quiz-info">
                                    <input type="hidden" name="id" class="quiz_id">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="form-quiz-name">Nome</label>
                                                <input type="text" id="form-quiz-name" class="form-control" name="name"  required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="form-quiz-description">Descrição</label>
                                                <textarea name="description" id="form-quiz-description" class="form-control" cols="30" rows="3"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="form-quiz-start_date">Data inicial</label>
                                                <input id="form-quiz-start_date" type="text" class="form-control mask-date" name="start_date" >
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="form-quiz-end_date">Data final</label>
                                                <input id="form-quiz-end_date" type="text" class="form-control mask-date" name="end_date">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="form-quiz-time">Tempo(minutos)</label>
                                                <input type="number" id="form-quiz-time" class="form-control" name="time" min="0">
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <label class="control-label">STATUS</label>
                                            <div class="clearfix"></div>
                                            <input type="checkbox" class="form-control form-quiz-active" name="active" value="1" data-toggle="toggle" data-on="Ativo" data-off="Inativo" data-size="small" data-onstyle="success" data-offstyle="danger" data-width="80" checked >
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div id="wizard-quiz-tab2" class="tab-pane fade" >
                                <div class="text-center">
                                    <h4>Perguntas do quiz</h4>
                                </div>
                                <hr>
                                <div class="panels-asks">
                                    <div class="row pad-ver panel-asks-menu">
                                        <div class="col-sm-12">
                                            <button id="btn-add-ask" class="btn btn-mint"><i class="fa fa-plus"></i>&nbsp;Adicionar pergunta</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="wizard-quiz-tab3" class="tab-pane fade">
                                <div class="text-center">
                                    <h4>Concluir quiz</h4>
                                    <p>Confira os dados do quiz e clique no botão finalizar.</p>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <fieldset>
                                            <legend class="pad-ver">Resumo do quiz</legend>
                                            <div class="panel panel-primary">
                                                <table id="tab-desc-quiz" class="table table-bordered">
                                                    <tbody>
                                                    <tr class="quiz-tab-name">
                                                        <th>Nome</th>
                                                        <td></td>
                                                    </tr>
                                                    <tr class="quiz-tab-description">
                                                        <th>Descrição</th>
                                                        <td></td>
                                                    </tr>
                                                    <tr class="quiz-tab-start_date">
                                                        <th>Data inicial</th>
                                                        <td></td>
                                                    </tr>
                                                    <tr class="quiz-tab-end_date">
                                                        <th>Data final</th>
                                                        <td></td>
                                                    </tr>
                                                    <tr class="quiz-tab-time">
                                                        <th>Tempo</th>
                                                        <td></td>
                                                    </tr>
                                                    <tr class="quiz-tab-active">
                                                        <th>Ativo</th>
                                                        <td></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <legend class="pad-ver">Perguntas</legend>
                                            <div class="ask-resume-panels">

                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer footer-fixed text-right bord-top bg-gray">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="box-inline">
                                    <button type="button" class="previous btn btn-default btn-lg"><i class="fa fa-angle-left"></i> Anterior</button>
                                    <button type="button" class="next btn btn-primary btn-lg">Avançar <i class="fa fa-angle-right"></i></button>
                                    <button type="submit" class="finish btn btn-primary btn-lg" formnovalidate>Finalizar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection

@push('styles')
    {{ Html::style('intranet/plugins/bootstrap-validator/bootstrapValidator.min.css') }}
    {{ Html::style('intranet/plugins/bootstrap-toggle/css/bootstrap-toggle.min.css') }}
    <style>
        .btn-remove-quiz{
            margin-top: 5px;
        }
    </style>
@endpush

@push('scripts')
    {{ Html::script('intranet/plugins/bootstrap-toggle/js/bootstrap-toggle.min.js') }}
    {{ Html::script('intranet/plugins/bootstrap-validator/bootstrapValidator2.min.js') }}
    {{ Html::script('intranet/node_modules/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}
    {{ Html::script('intranet/node_modules/jquery-mask-plugin/dist/jquery.mask.min.js') }}
    {{ Html::script('intranet/node_modules/moment/min/moment.min.js') }}
    {{ Html::script('intranet/node_modules/moment/locale/pt-br.js') }}

    <script id="askTemplate" type="text/template">
        <div class="row quiz-ask-one">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><button type="button" class='btn btn-danger btn-xs add-tooltip pull-right btn-remove-quiz' title="Remover pergunta"><i class="fa fa-trash"></i></button></h3>
                    </div>
                    <div class="panel-body">
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-12">
                                    <input type="hidden" class="ask-id">
                                    <div class="form-group">
                                        <label>Título</label>
                                        <textarea name="asks[index].title" class="form-control ask-title" rows="2" oninvalid="setCustomValidity('Título obrigatório')" required></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label >Peso</label>
                                        <input type="number" name="asks[index].weight" class="form-control ask-weight" min="1" value="1">
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <label>STATUS</label>
                                        <div class="clearfix"></div>
                                        <input type="checkbox" class="form-control ask-active" name="asks[index].active" value="1" data-toggle="toggle" data-on="Ativo" data-off="Inativo" data-size="small" data-onstyle="success" data-offstyle="danger" data-width="80" checked >
                                    </div>
                                </div>
                            </div>
                            <div class="row pad-ver">
                                <div class="col-md-12">
                                    <button class="ask-add-alternative btn btn-mint"><i class="fa fa-plus"></i>&nbsp;Adicionar Alternativa</button>
                                </div>
                            </div>
                            <fieldset>
                                <legend>Alternativas</legend>
                                <div class="row">
                                    <div class="col-sm-12 ask-alternatives-container">

                                    </div>
                                </div>
                            </fieldset>
                        </fieldset>
                    </div>
                </div>

            </div>
        </div>
    </script>
    <script id="askAlternativeTemplate" type="text/template">
        <div class="well ask-alternative-one">
            <div class="form-group">
                <label>TÍTULO</label>
                <textarea class="form-control ask-alternative-title" rows="2" oninvalid="setCustomValidity('Título obrigatório')" placeholder="Título" required></textarea>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>STATUS</label>
                        <div class="clearfix"></div>
                        <input type="checkbox" class="ask-alternative-active" name="teste" value="1" data-toggle="toggle" data-on="Ativo" data-off="Inativo" data-size="small" data-onstyle="success" data-offstyle="danger" data-width="80" checked >
                        <input type="hidden" class="ask-alternative-id">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>CORRETO</label>
                        <div class="clearfix"></div>
                        <input type="checkbox" class="ask-alternative-correct" name="teste" value="1" data-toggle="toggle" data-on="Sim" data-off="Não" data-size="small" data-onstyle="success" data-offstyle="danger" data-width="80" checked >
                    </div>
                </div>
                <div class="col-md-6">
                    <button type="button" class='btn btn-danger btn-xs add-tooltip pull-right btn-remove-alternative' title="Remover alternativa"><i class="fa fa-trash"></i></button>
                </div>
            </div>
        </div>
    </script>
    <script id="askResumeTemplate" type="text/template">
        <div class="row quiz-ask-resume-one">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"></h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered ask-tab-desc">
                            <tbody>
                            <tr class="ask-tab-title">
                                <th>Título</th>
                                <td></td>
                            </tr>
                            <tr class="ask-tab-weight">
                                <th>Peso</th>
                                <td></td>
                            </tr>
                            <tr class="ask-tab-active">
                                <th>Ativo</th>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </script>

    <script>
        var wizardSelector = '#wizard-quiz';
        var quiz = {
            asks: [{
                weight: 1,
                active: 1,
                alternatives: [{
                    active: 1,
                    correct: 1
                }]
            }]
        };
        @if(!empty($finded))
            quiz = {!! json_encode($finded) !!};
        @endif

        console.log('cuiz', JSON.parse(JSON.stringify(quiz)));

        if(quiz.start_date && quiz.end_date){
            quiz.start_date = moment(quiz.start_date).format('DD/MM/YYYY');
            quiz.end_date = moment(quiz.end_date).format('DD/MM/YYYY');
        }


        $(wizardSelector).bootstrapWizard({
            tabClass: 'wz-steps',
            nextSelector: '.next',
            previousSelector: '.previous',
            onTabClick: function(tab, navigation, index) {
                return true;
            },
            onInit : function(){
                $(wizardSelector).find('.finish').hide().prop('disabled', true);
                $(wizardSelector).find('.finish').hide();
                console.log($($('#askTemplate').html()), $('#wizard-quiz-tab2 > *'));

                bind();

            },
            onTabShow: function(tab, navigation, index) {
                console.log('shown');
                var $total = navigation.find('li').length;
                var $current = index+1;
                var $percent = (index/$total) * 100;
                var margin = (100/$total)/2;

                $(wizardSelector).find('.progress-bar').css({width:$percent+'%', 'margin': 0 + 'px ' + margin + '%'});

                navigation.find('li:eq('+index+') a').trigger('focus');
                // If it's the last tab then hide the last button and show the finish instead
                if($current >= $total) {
                    $(wizardSelector).find('.next').hide();
                    $(wizardSelector).find('.finish').show();
                    $(wizardSelector).find('.finish').attr('disabled', false);
                } else {
                    $(wizardSelector).find('.next').show();
                    $(wizardSelector).find('.finish').hide().prop('disabled', true);
                    $(wizardSelector).find('.finish').hide();
                }
                reBind();


                if(index==2){

                    var empty = false;
                    var titles = $('.panels-asks .ask-title');
                    var alterTitle = $('.panels-asks .ask-alternative-title');

                    if(!quiz.asks) return false;

                    titles.each(function () {
                        if($(this).val()=="") empty = true;
                    });
                    alterTitle.each(function () {
                        if($(this).val()=="") empty = true;
                    });
                    if(empty) return false;


                    $('#tab-desc-quiz')
                        .find('.quiz-tab-name td')
                        .html(quiz.name);

                    $('#tab-desc-quiz')
                        .find('.quiz-tab-description td')
                        .html(quiz.description);

                    $('#tab-desc-quiz')
                        .find('.quiz-tab-start_date td')
                        .html(quiz.start_date);

                    $('#tab-desc-quiz')
                        .find('.quiz-tab-end_date td')
                        .html(quiz.end_date);

                    $('#tab-desc-quiz')
                        .find('.quiz-tab-time td')
                        .html(quiz.time?quiz.time:'0');

                    $('#tab-desc-quiz')
                        .find('.quiz-tab-active td')
                        .html(quiz.active?'SIM':'NÃO');

                    $('.ask-resume-panels').html('');

                    quiz.asks.forEach(function (thisAsk, index) {
                        var askResumeTemplate = $($('#askResumeTemplate').html());
                        askResumeTemplate.find('.panel-title').html('Pergunta '+(index+1));

                        askResumeTemplate.find('.ask-tab-title td').html(thisAsk.title);
                        askResumeTemplate.find('.ask-tab-weight td').html(thisAsk.weight);
                        askResumeTemplate.find('.ask-tab-active td').html(thisAsk.active==1?'SIM':'NÃO');
                        $('#wizard-quiz-tab3 .ask-resume-panels').append(askResumeTemplate);
                    });
                }

            },
            onNext: function(tab, navigation, index){
                isValid = null;

                $('#form-quiz').formValidation('validate');

                if(isValid===false) return  false;

                if(index==1){
                    if(quiz.asks.length==0) {
                        addAsk();
                    }
                }


            }
        });

        var isValid;

        $('#form-quiz').on('init.field.fv', function(e, data) {
            // data.field   --> The field name
            // data.element --> The field element

        }).formValidation({
            message: 'Este valor não é valido',
            feedbackIcons: {
                valid: 'fa fa-check-circle fa-lg text-success',
                invalid: 'fa fa-times-circle fa-lg',
                validating: 'fa fa-refresh'
            },
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: 'O nome é obrigatório.'
                        },
                        stringLength: {
                            min: 3,
                            message: 'Nome inválido.'
                        }
                    }
                },
                start_date: {
                    validators: {
                        notEmpty: {
                            message: 'A data inicial é obrigatória.'
                        },
                        stringLength: {
                            min: 10,
                            max: 10,
                            message: 'Data inválida.'
                        }
                    }
                },
                end_date: {
                    validators: {
                        notEmpty: {
                            message: 'A data final é obrigatória.'
                        },
                        stringLength: {
                            min: 10,
                            max: 10,
                            message: 'Data inválida.'
                        }
                    }
                }
            }
        }).on('success.field.fv', function(e, data) {
            // $(e.target)  --> The field element
            // data.bv      --> The BootstrapValidator instance
            // data.field   --> The field name
            // data.element --> The field element

            var $parent = data.element.parents('.form-group');

            // Remove the has-success class
            $parent.removeClass('has-success');

            // Hide the success icon
            //$parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        }).on('err.field.fv', function(e) {
            isValid = false;
        });

        $(document).on('click', '.ask-add-alternative', function () {
            var askContainer = $(this).closest('.quiz-ask-one');

            let askName = askContainer.find('.ask-title').attr('name')
                .replace('asks[','')
                .replace('][title]', '');
            var askIndex = parseInt(askName?askName:'0');
            reBind();
            quiz.asks[askIndex].alternatives.push({
                correct: 1,
                active: 1
            });
            bind();
        });

        $('#btn-add-ask').on('click', function () {
            addAsk();
        });
        function addAsk() {
            reBind();
            quiz.asks.push({
                weight: 1,
                active: 1,
                alternatives: [{
                    correct: 1,
                    active: 1
                }]
            });
            bind();
        }
        function reBind() {
            console.log('rebind');
            if($('#quiz_id[name="id"]').length>0)
                quiz.id = $('#quiz_id[name="id"]').val();
            if($('[name="name"]').length>0)
                quiz.name = $('[name="name"]').val();
            if($('#form-quiz-description[name="description"]').length>0)
                quiz.description = $('#form-quiz-description[name="description"]').val();
            if($('[name="start_date"]').length>0)
                quiz.start_date = $('[name="start_date"]').val();
            if($('[name="end_date"]').length>0)
                quiz.end_date = $('[name="end_date"]').val();
            if($('[name="time"]').length>0)
                quiz.time = $('[name="time"]').val();
            if($('[name="active"]').length>0)
                quiz.active = $('[name="active"]').prop('checked');
            var quizAsks = [];
            $('.quiz-ask-one').each(function (i) {
                var oneAsk = {
                    alternatives: []
                };
                if($(this).find('.ask-id').length > 0)
                    oneAsk.id = $(this).find('.ask-id').val();
                if($(this).find('.ask-title').length > 0)
                    oneAsk.title = $(this).find('.ask-title').val();
                if($(this).find('.ask-weight').length > 0)
                    oneAsk.weight = $(this).find('.ask-weight').val();
                if($(this).find('.ask-active').length > 0)
                    oneAsk.active = $(this).find('.ask-active').prop('checked');

                $(this).find('.ask-alternatives-container .ask-alternative-one').each(function () {
                    var oneAlter = {};
                    if($(this).find('.ask-alternative-id').length > 0)
                        oneAlter.id = $(this).find('.ask-alternative-id').val();
                    if($(this).find('.ask-alternative-title').length > 0)
                        oneAlter.title = $(this).find('.ask-alternative-title').val();
                    if($(this).find('.ask-alternative-correct').length > 0)
                        oneAlter.correct = $(this).find('.ask-alternative-correct').prop('checked');
                    if($(this).find('.ask-alternative-active').length > 0)
                        oneAlter.active = $(this).find('.ask-alternative-active').prop('checked');
                    oneAsk.alternatives.push(oneAlter);
                });
                quizAsks.push(oneAsk);
            });

            quiz.asks = quizAsks;
            console.log('_rebind');

        }

        function setMask(){
            $('.mask-date').mask('00/00/0000', {placeholder: "__/__/____", selectOnFocus: true});
            $('.mask-cep').mask('00000-000', {placeholder: "_____-___", selectOnFocus: true});

            $('.mask-cpf').mask('000.000.000-00', {placeholder: "___.___.___-__", reverse: true, selectOnFocus: true});
            $('.mask-cnpj').mask('00.000.000/0000-00', {placeholder: "__.___.___/____-__", reverse: true, selectOnFocus: true});
        }

        function Attr(name, value) {
            this.name = name;
            this.value = value;
        }

        function bind(){
            console.log('bind');
            for(var quizinfo in quiz){
                var curVal = quiz[quizinfo];
                if(Array.isArray(curVal)){
                    $('.panels-asks > div:not(.panel-asks-menu)').remove();
                    curVal.forEach(function(curItem, i){
                        var askTemplate = $($('#askTemplate').html());
                        askTemplate.attr('id', 'quiz-ask-'+i);
                        askTemplate.find('.panel-title').prepend(document.createTextNode('Pergunta '+(i+1)));
                        addAttrs(askTemplate.find('.ask-id'), new Attr('name', 'asks['+i+'][id]'), curItem.id);
                        addAttrs(askTemplate.find('.ask-title'), new Attr('name', 'asks['+i+'][title]'), curItem.title);
                        addAttrs(askTemplate.find('.ask-weight'), new Attr('name', 'asks['+i+'][weight]'), curItem.weight);
                        addAttrs(askTemplate.find('.ask-active'), new Attr('name', 'asks['+i+'][active]'), curItem.active);
                        var askAlternatives = curItem.alternatives;
                        if(askAlternatives){
                            askAlternatives.forEach(function (alter, idx) {
                                var alternativeTemplate = $($('#askAlternativeTemplate').html());
                                alternativeTemplate.attr('id', 'quiz-ask-'+i+'-alternative-'+idx);
                                alternativeTemplate.find('.ask-alternative-index').text(idx+1);
                                addAttrs(alternativeTemplate.find('.ask-alternative-id'), new Attr('name', 'asks['+i+'][alternatives]['+idx+'][id]'), alter.id);
                                addAttrs(alternativeTemplate.find('.ask-alternative-title'), new Attr('name', 'asks['+i+'][alternatives]['+idx+'][title]'), alter.title);
                                addAttrs(alternativeTemplate.find('.ask-alternative-correct'), new Attr('name', 'asks['+i+'][alternatives]['+idx+'][correct]'), alter.correct);
                                addAttrs(alternativeTemplate.find('.ask-alternative-active'), new Attr('name', 'asks['+i+'][alternatives]['+idx+'][active]'), alter.active);
                                askTemplate.find('.ask-alternatives-container').append(alternativeTemplate);
                            });
                        }
                        var askContainer = $('.panels-asks').append(askTemplate);
                    });

                }else{
                    var elem = $('.quiz-info input[name="'+quizinfo+'"], .quiz-info textarea[name="'+quizinfo+'"]');
                    if(elem.attr('type')=='checkbox'){
                        elem.each(function(){
                            if(curVal) $(this).bootstrapToggle('on');
                            else $(this).bootstrapToggle('off')
                        });
                    }else{
                        elem.val(curVal);
                    }
                }
            }
            console.log('_bind');
        }

        window.bind = bind;

        $(document).on('click', '.btn-remove-quiz', function () {
            if(quiz.asks.length>1) {
                var askPanel = $(this).parent().parent().parent().parent().parent();
                var indice = askPanel.attr('id').replace('quiz-ask-', '');
                reBind();
                quiz.asks.splice(indice, 1);
                bind();
            }
        });

        $(document).on('click', '.btn-remove-alternative', function () {
            var alternativePanel = $(this).parent().parent().parent();
            var indice = alternativePanel.attr('id').replace('quiz-ask-', '').replace('alternative-', '');
            indice = indice.split('-');
            if(quiz.asks[indice[0]].alternatives.length > 1) {
                reBind();
                quiz.asks[indice[0]].alternatives.splice(indice[1], 1);
                bind();
            }
        });

        function addAttrs(element, attr, val) {
            element.attr(attr.name, attr.value);
            if(element.attr('type')=='checkbox'){
                element.each(function () {
                    if(val) $(this).bootstrapToggle('on');
                    else $(this).bootstrapToggle('off');
                })
            }else element.val(val);
        }

        setMask();
    </script>


@endpush