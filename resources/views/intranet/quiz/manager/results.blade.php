@extends('intranet.layouts.master')


@section('title', $controllers->title)

@push('page.title')

    <div class="row">
        <div class="col-md-8">
            <h1 class="page-header text-overflow"><i class="{{ $controllers->icon }}"></i> Resultado do quiz: <strong>{{$quiz->name}}</strong></h1>
        </div>
        <div class="col-md-4 text-right">
            <a href="{{ route($controllers->actions->manager->route) }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Voltar</a>
        </div>
    </div>

@endpush


@section('content')

    <div class="panel">
        {!! $datatable->table(['class' => 'table table-striped table-hover']) !!}
    </div>

@endsection

@push('styles')
    {!! Html::style('intranet/node_modules/datatables.net-bs/css/dataTables.bootstrap.css') !!}
    {!! Html::style('intranet/node_modules/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') !!}
    <style>

    </style>
@endpush

@push('scripts')

    {!! Html::script('intranet/node_modules/datatables.net/js/jquery.dataTables.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-bs/js/dataTables.bootstrap.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons/js/dataTables.buttons.min.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons/js/buttons.colVis.min.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons/js/buttons.html5.min.js') !!}
    {!! Html::script('intranet/node_modules/jszip/dist/jszip.min.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') !!}

    {!! $datatable->scripts() !!}

    <script>

        let html;

        function datatablesFormatStrong(data){

            html = '<strong>'+data+'</strong>';
            return html;

        }

        function datatablesFormatResultStatus(data) {
            if(data)
                return '<span class="label label-dark">FINALIZADO</span>';
            else return '<span class="label label-warning">ABERTO</span>';
        }

        function datatablesFormatActions(data, type, full){

            html = '<div class="text-center">';

            /*html += '<a href="{{ route($controllers->actions->edit->route) }}/'+full['id']+'" class="btn btn-icon-toggle btn-sm btn-mint"><i class="fa fa-info-circle"></i></a>';*/

            @if(isset($controllers->actions->answer))
                @permission($controllers->actions->answer->permission)
                html += '<a href="{{ route($controllers->actions->answer->see, $quiz->id) }}/'+full['collaborator_id']+'" title="Ver resultado" class="btn btn-sm btn-info">RESULTADO</a>';
                @endpermission
            @endif

             html += '</div>';
             return html;
        }
    </script>
@endpush