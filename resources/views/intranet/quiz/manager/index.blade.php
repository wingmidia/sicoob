@extends('intranet.layouts.master')


@section('title', $controllers->title)

@push('page.title')

    <div class="row">
        <div class="col-md-8">
            <h1 class="page-header text-overflow"><i class="{{ $controllers->icon }}"></i> Quizzes</h1>
        </div>
        <div class="col-md-4 text-right">
            @permission($controllers->actions->add->permission)
            <a href="{{ route($controllers->actions->add->route) }}" class="btn btn-primary btn-lg"><i class="fa fa-plus-circle"></i> Adicionar</a>
            @endpermission
        </div>
    </div>

@endpush


@section('content')

    <div class="panel">
        {!! $datatable->table(['class' => 'table table-striped table-hover']) !!}
    </div>
    <div id="deleteModal" class="modal fade delete-modal" tabindex="-1" role="dialog" aria-labelledby="deleteModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">VOCÊ REALMENTE DESEJA EXCLUIR O QUIZ?</h3>
                </div>
                <div class="modal-body">
                    <a class="btn btn-primary delete-link">Sim</a>
                    <button class="btn btn-danger" data-dismiss="modal">Não</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    {!! Html::style('intranet/node_modules/datatables.net-bs/css/dataTables.bootstrap.css') !!}
    {!! Html::style('intranet/node_modules/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') !!}
    <style>
        .act-mar:not(:first-child){
            margin-left: 10px;
        }
    </style>
@endpush

@push('scripts')

    {!! Html::script('intranet/node_modules/datatables.net/js/jquery.dataTables.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-bs/js/dataTables.bootstrap.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons/js/dataTables.buttons.min.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons/js/buttons.colVis.min.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons/js/buttons.html5.min.js') !!}
    {!! Html::script('intranet/node_modules/jszip/dist/jszip.min.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') !!}

    {!! $datatable->scripts() !!}

    <script>

        let html;
        $(document).ready(function () {
            $(document).on('click', '.delete-btn', function () {
                var url = $(this).attr('data-link');
                $('.delete-modal .delete-link').attr('href', url);
                $('#deleteModal').modal('show');
            });
        });

        function datatablesFormatStrong(data){

            html = '<strong>'+data+'</strong>';
            return html;

        }

        function datatablesFormatQuizStatus(data) {
            return  (data == 1) ? '<i class="fa fa-circle text-success" title="Ativo"></i> Ativo' : '<i class="fa fa-circle text-danger" title="Inativo"></i> Inativo';
        }

        function datatablesFormatActions(data, type, full){

            html = '<div class="text-center">';

            /*html += '<a href="{{ route($controllers->actions->edit->route) }}/'+full['id']+'" class="btn btn-icon-toggle btn-sm btn-mint"><i class="fa fa-info-circle"></i></a>';*/


            @if(isset($controllers->actions->result))
                @permission($controllers->actions->result->permission)
                console.log(full);
                html += '<a href="{{ route($controllers->actions->result->route) }}/'+full['id']+'" title="Ver resultados" class="btn btn-icon-toggle btn-sm btn-mint act-mar add-tooltip">RESPOSTAS</a>';
                @endpermission
            @endif
            @if(isset($controllers->actions->edit))
            @permission($controllers->actions->edit->permission)
                html += '<a href="{{ route($controllers->actions->edit->route) }}/'+full['id']+'" title="Editar quiz" class="btn btn-icon-toggle btn-sm btn-info act-mar add-tooltip"><i class="fa fa-pencil"></i></a>';
            @endpermission
            @endif
             @if(isset($controllers->actions->delete))
                @permission($controllers->actions->delete->permission)
                html += '<a data-link="{{ route($controllers->actions->delete->route) }}/'+full['id']+'" class="delete-btn btn btn-icon-toggle btn-sm btn-danger act-mar"><i class="fa fa-trash"></i></a>';
                @endpermission
             @endif

             html += '</div>';
             return html;
        }
    </script>
@endpush