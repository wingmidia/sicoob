@extends('intranet.layouts.master')

@section('title', $controllers->title)

@push('page.title')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="row">
            <div class="col-md-8">
                @if(empty($resultFind))
                <h1 class="page-header text-overflow"><i class="{{ $controllers->icon }}"></i> {{ $controllers->title }} | <small><i class="fa fa-pencil"></i> Respondendo quiz</small></h1>
                @else
                <h1 class="page-header text-overflow"><i class="{{ $controllers->icon }}"></i> {{ $controllers->title }} | <small><i class="fa fa-pencil"></i> Visualizando resultado</small></h1>
                @endif
            </div>
            <div class="col-md-4 text-right">
                @if($visitor)
                <a href="{{ route($controllers->actions->result->route, $quiz->id) }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Voltar</a>
                @else
                <a href="{{ route($controllers->route) }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Voltar</a>
                @endif
            </div>
        </div>
    </div>
</div>
@endpush

@section('content')
    <form action="{{route($controllers->actions->do_awnser->route)}}" id="form-answer" method="POST">
        @if(false)
        {{ csrf_field() }}
        @endif
        <div class="col-md-8 col-md-offset-2" style="margin-bottom: 40px">
            <input type="hidden" name="quiz_id" value="{{$quiz->id}}">

            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8">
                            <h2 class="quiz-title"></h2>
                            <p class="quiz-description"></p>
                        </div>
                        <div class="col-md-4 quiz-timer-container">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th>Pontuação</th>
                                    <th>Duração</th>
                                </tr>
                                <tr>
                                    <td class="quiz-score"><strong></strong></td>
                                    <td class="quiz-duration"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="asks-container">

            </div>

            @if(false)
                <div class="panel-footer footer-fixed text-right bord-top bg-gray">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="box-inline">
                                <button type="button" class="previous btn btn-default btn-lg"><i class="fa fa-angle-left"></i> Voltar</button>
                                <button type="submit" class="finish btn btn-primary btn-lg">Finalizar</button>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </form>
@endsection
@push('styles')
    <style>
        .group-alternative{
            padding: 7px 10px;
        }
        .quiz-ask{
            position: relative;
        }
        .ask-weight{
            position: absolute;
            right: 10%;
            top: 2%;
        }
    </style>
@endpush

@push('scripts')
    {{ Html::script('intranet/node_modules/moment/min/moment.min.js') }}
    {{ Html::script('intranet/node_modules/moment/locale/pt-br.js') }}

    <script type="text/javascript">
        $(document).ready(function () {
            var quiz = {};
            var result = {};
            var visitor = false;
            var score = '0%';

            @if(!empty($quiz))
                quiz = {!! json_encode($quiz) !!};
            @endif
            @if(!empty($resultFind))
                result = {!! json_encode($resultFind) !!};
            @endif
            @if(!empty($visitor))
                visitor = {!! $visitor !!};
            @endif

            @if(!empty($total))
                var numberScore = {!! $total !!};
                score = numberScore*100;
                score = score.toFixed(2);
                score = score.toLocaleString('pt-BR')+'%';
            @endif


            $('.quiz-title').append('<strong>'+quiz.name+'</strong>');
            $('.quiz-description').text(quiz.description);
            $('.quiz-score strong').text(score);

            var route = "{{   route($controllers->route) }}";

            console.log(result);

            if(quiz.asks) {
                quiz.asks.forEach(function (ask, i) {
                    if (ask.active) {
                        var askTemplate = $($('#askTemplate').html());
                        askTemplate.find('.ask-weight').text('Peso: '+ask.weight);
                        askTemplate.find('.ask-title').text(ask.title);
                        if (ask.alternatives) {
                            var qtdCorrects = 0;

                            ask.alternatives.forEach(function (alter) {
                                if(alter.correct) qtdCorrects++;
                            });

                            ask.alternatives.forEach(function (alternative, idx) {
                                if(alternative.active) {
                                    var alternativeTemplate = $($("#alternativeTemplate").html());
                                    alternativeTemplate.find('.alternative-title').text(alternative.title);
                                    alternativeTemplate.find('.alternative-value').attr('name', 'asks[' + ask.id + '][alternatives][' + alternative.id + '][value]');
                                    if(result.terminated) alternativeTemplate.find('.alternative-value').attr('dt-correct', alternative.correct);
                                    if(result.terminated) alternativeTemplate.find('.alternative-value').attr('disabled', 'disabled');
                                    askTemplate.find('.ask-alternatives').append(alternativeTemplate);
                                }
                            });
                        }
                        $('.asks-container').append(askTemplate);
                    }
                });
            }

            if(result.answers){
                if(result.terminated) {

                    var now  = result.ended_at;
                    var then = result.started_at;

                    console.log(now, then);

                    var ms = moment(now).diff(moment(then));
                    var d = moment.duration(ms);
                    var s = [];

                    var hora = Math.floor(d.asHours());
                    var minutos = moment.utc(ms).format("mm");

                    if(hora > 0)
                        s.push(hora + ((hora > 1) ? ' horas' : ' hora'));

                    if(minutos > 0)
                        s.push(minutos + ((minutos > 1) ? ' minutos' : ' minuto'));

                    if(moment.utc(ms).format("ss") > 0)
                        s.push(moment.utc(ms).format("ss") + ' segundos');

                    console.log(s);

                    $('.quiz-duration').text(s.join(' e '));

                    /*var timeInicial = moment(result.started_at);
                    var timeFinal = moment(result.ended_at);
                    $('.quiz-duration').text(timeFinal.diff(timeInicial, 'minutes'));*/
                }else $('.quiz-duration').text('QUIZ NÃO FINALIZADO');

                result.answers.forEach(function (answer) {
                    var name = `asks[${answer.quiz_ask_id}][alternatives][${answer.quiz_ask_alternative_id}]`;
                    var altName = name+"[value]";
                    var element = $('.alternative-value[name="'+altName+'"]');
                    element.attr('checked', 'checked');

                    if(element.attr('dt-correct')=='1'){
                        element.parent().parent().parent().addClass('bg-success');
                    }else{
                        element.parent().parent().parent().addClass('bg-danger');
                    }

                    var altIdName = name + '[answer_id]';
                    var idElm = element.parent().parent().find('.alternative-asnwer');
                    idElm.val(answer.id);
                    idElm.attr('name', altIdName);
                    element.on('change', function () {
                        if(!this.checked) {
                            idElm.removeAttr('name')
                        }else{
                            idElm.attr('name', altIdName);
                        }
                    });
                });
            }
        });

    </script>


    <script type="text/template" id="askTemplate">
        <div class="row quiz-ask">
            <div class="col-sm-12">
                <div class="panel panel-warning">
                    <div class="panel-body">
                        <h3 class="ask-title"></h3>
                        {{--<h5 class="ask-weight"></h5>--}}
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-12 ask-alternatives">

                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </script>
    <script type="text/template" id="alternativeTemplate">
        <div class="form-group group-alternative">
            <div class="checkbox">
                <input type="hidden" class="alternative-asnwer">
                <label><input type="checkbox" class="alternative-value" value="1"><span class="alternative-title"></span></label>
            </div>
        </div>
    </script>
@endpush