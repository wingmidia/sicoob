@extends('intranet.layouts.master')

@section('title', $controllers->title)

@push('page.title')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <div class="row">
                <div class="col-md-8">
                    <h1 class="page-header text-overflow"><i class="{{ $controllers->icon }}"></i> {{ $controllers->title }} | <small><i class="fa fa-plus-circle"></i> Adicionando produção</small></h1>
                </div>
                <div class="col-md-4 text-right">
                    <a href="{{ route($controllers->route) }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Voltar</a>
                </div>
            </div>

        </div>
    </div>

@endpush

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <div class="panel panel-success">

                <!-- Circular Form Wizard -->
                <!--===================================================-->
                <div id="wizard-production">
                    <div class="wz-heading pad-ver bord-btm">

                        <!--Progress bar-->
                        <div class="progress progress-xs progress-light-base">
                            <div class="progress-bar progress-bar-success"></div>
                        </div>

                        <!--Nav-->
                        <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
                        <ul class="wz-nav-off">
                            <li class="col-xs-3">
                                <a data-toggle="tab" href="#wizard-production-tab1" title="Tipo de cliente" class="add-tooltip">
                                    <div class="icon-wrap icon-wrap-sm bg-success">
                                        <i class="wz-icon fa fa-user fa-2x icon-lg"></i>
                                        <i class="wz-icon-done fa fa-check-circle fa-2x icon-lg"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="col-xs-3">
                                <a data-toggle="tab" href="#wizard-production-tab2" title="Dados do cliente" class="add-tooltip">
                                    <div class="icon-wrap icon-wrap-sm bg-success">
                                        <i class="wz-icon fa fa-vcard fa-2x icon-lg"></i>
                                        <i class="wz-icon-done fa fa-check-circle fa-2x icon-lg"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="col-xs-3">
                                <a data-toggle="tab" href="#wizard-production-tab3" title="Produtos" class="add-tooltip">
                                    <div class="icon-wrap icon-wrap-sm bg-success">
                                        <i class="wz-icon fa fa-briefcase fa-2x icon-lg"></i>
                                        <i class="wz-icon-done fa fa-check-circle fa-2x icon-lg"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="col-xs-3">
                                <a data-toggle="tab" href="#wizard-production-tab4" title="Finalizar" class="add-tooltip">
                                    <div class="icon-wrap icon-wrap-sm bg-success">
                                        <i class="wz-icon fa fa-thumbs-up fa-2x icon-lg"></i>
                                        <i class="wz-icon-done fa fa-thumbs-up fa-2x icon-lg"></i>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <!--Form-->
                    <form id="form-production" action="{{ route($controllers->actions->store->route) }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="client_id" value="{{ old('client_id') }}">
                        <div class="panel-body">
                            <div class="tab-content">

                                <!--First tab-->
                                <div id="wizard-production-tab1" class="tab-pane fade">

                                    <div class="mar-btm text-center">
                                        <h4>Nova produção</h4>
                                        <p>Insira as informações da sua venda. Fique atento para preencher os dados corretamente.</p>
                                    </div>

                                    <hr>

                                    <fieldset>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="form-production-type_profile">Tipo</label>
                                                    <div class="radio">
                                                        <input id="form-production-type_profile-1" class="magic-radio" type="radio" name="type_profile" value="F" @if(old('type_profile') != 'J')checked="checked"@endif>
                                                        <label for="form-production-type_profile-1">Pessoa Física</label>

                                                        <input id="form-production-type_profile-2" class="magic-radio" type="radio" name="type_profile" value="J" @if(old('type_profile') == 'J')checked="checked"@endif>
                                                        <label for="form-production-type_profile-2">Pessoa Jurídica</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" id="form-production-box-cpf">
                                                    <label for="form-production-cpf" class="control-label">CPF</label>
                                                    <input id="form-production-cpf" type="text" class="form-control input-lg mask-cpf" name="cpf" value="{{ old('cpf') }}" autofocus>
                                                </div>
                                                <div class="form-group" id="form-production-box-cnpj" style="display: none">
                                                    <label for="form-production-cnpj" class="control-label">CNPJ</label>
                                                    <input id="form-production-cnpj" type="text" class="form-control input-lg mask-cnpj" name="cnpj" value="{{ old('cnpj') }}">
                                                </div>
                                            </div>
                                        </div>

                                    </fieldset>

                                </div>

                                <!--Second tab-->
                                <div id="wizard-production-tab2" class="tab-pane fade">
                                    <fieldset>
                                        <legend>Complete os dados do cliente</legend>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label for="form-production-name" class="control-label">Nome</label>
                                                    <input id="form-production-name" type="text" class="form-control" name="name" value="{{ old('name') }}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="form-production-date_birth" class="control-label">Data de nascimento</label>
                                                    <input id="form-production-date_birth" type="text" class="form-control mask-date" name="date_birth" value="{{ old('date_birth') }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group" id="form-production-box-company_name" style="display: none;">
                                                    <label for="form-production-company_name" class="control-label">Razão Social</label>
                                                    <input id="form-production-company_name" type="text" class="form-control" name="company_name" value="{{ old('company_name') }}">
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <legend>Dados de contato</legend>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="form-production-email" class="control-label">E-mail</label>
                                                    <input id="form-production-email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="form-production-cellphone" class="control-label">Celular/Whatsapp</label>
                                                    <input id="form-production-cellphone" type="text" class="form-control mask-phone" name="cellphone" value="{{ old('cellphone') }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="form-production-phone1" class="control-label">Telefone</label>
                                                    <input id="form-production-phone1" type="text" class="form-control mask-phone" name="phone1" value="{{ old('phone1') }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="form-production-phone2" class="control-label">Telefone alternativo</label>
                                                    <input id="form-production-phone2" type="text" class="form-control mask-phone" name="phone2" value="{{ old('phone2') }}">
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <legend>Endereço do cliente</legend>
                                        <div class="row">
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    <label for="form-production-address" class="control-label">Endereço</label>
                                                    <input id="form-production-address" type="text" class="form-control" name="address" value="{{ old('address') }}">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="form-production-number" class="control-label">Número</label>
                                                    <input id="form-production-number" type="text" class="form-control" name="number" value="{{ old('number') }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="form-production-complement" class="control-label">Complemento</label>
                                                    <input id="form-production-complement" type="text" class="form-control" name="complement" value="{{ old('complement') }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="form-production-district" class="control-label">Bairro</label>
                                                    <input id="form-production-district" type="text" class="form-control" name="district" value="{{ old('district') }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="form-production-city" class="control-label">Cidade</label>
                                                    <input id="form-production-city" type="text" class="form-control" name="city" value="{{ old('city') }}">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="form-production-state" class="control-label">Estado</label>
                                                    {!! Form::select('state', array_merge([''=> 'Selecione um estado'],getStates()), old('state'), ['id' => 'form-production-state', 'class' => 'form-control']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="form-production-postcode" class="control-label">CEP</label>
                                                    <input id="form-production-postcode" type="text" class="form-control mask-cep" name="postcode" value="{{ old('postcode') }}">
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>

                                <!--Third tab-->
                                <div id="wizard-production-tab3" class="tab-pane">
                                    <div id="alert-production-products-message"></div>
                                    <span id="alert-production-products-icon"></span>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <fieldset>
                                                <legend>Escolha os produtos</legend>
                                                @foreach($products as $product)
                                                    <button type="button" onclick="addProduct({{ $product->id }})" id="btn-production-product-{{ $product->id }}" class="btn btn-default btn-block text-overflow">{{ $product->title }}</button>
                                                @endforeach
                                            </fieldset>
                                        </div>
                                        <div class="col-md-8">
                                            <fieldset>
                                                <legend>Produtos selecionados</legend>
                                                <div id="box-production-products">
                                                    <div class="well text-center">
                                                        <p>Nenhum produto selecionado!</p>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>

                                <!--Fourth tab-->
                                <div id="wizard-production-tab4" class="tab-pane fade">
                                    <div class="mar-btm text-center">
                                        <h4>Concluir produção</h4>
                                        <p>Confira os dados da sua produção e clique no botão finalizar.</p>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-6 col-md-offset-3">
                                            <div class="panel panel-primary">
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <th>Nome</th>
                                                        <td id="form-production-box-confirmation-name"></td>
                                                    </tr>
                                                    <tr id="form-production-box-confirmation-box-cpf">
                                                        <th>CPF</th>
                                                        <td id="form-production-box-confirmation-cpf"></td>
                                                    </tr>
                                                    <tr id="form-production-box-confirmation-box-cnpj" style="display: none">
                                                        <th>CNPJ</th>
                                                        <td id="form-production-box-confirmation-cnpj"></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Celular/Whatsapp</th>
                                                        <td id="form-production-box-confirmation-cellphone"></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Produtos</th>
                                                        <td id="form-production-box-confirmation-products"></td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <hr>
                                            <div class="form-group">
                                                <div class="radio text-center">
                                                    <input id="form-production-contestation" class="magic-checkbox" type="checkbox" name="contestation" value="1">
                                                    <label for="form-production-contestation">Contestação</label>
                                                    <span class="help-block">Marque essa opção caso você queria contestar a indicação.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!--Footer button-->
                        <div class="panel-footer footer-fixed text-right bord-top bg-gray">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <div class="box-inline">
                                        <button type="button" class="previous btn btn-default btn-lg"><i class="fa fa-angle-left"></i> Anterior</button>
                                        <button type="button" class="next btn btn-primary btn-lg">Avançar <i class="fa fa-angle-right"></i></button>
                                        <button type="submit" class="finish btn btn-primary btn-lg" formnovalidate>Finalizar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!--===================================================-->
                <!-- End Circular Form Wizard -->

            </div>
            <br><br><br>
        </div>
    </div>

@endsection

@push('styles')
    {{ Html::style('intranet/plugins/bootstrap-toggle/css/bootstrap-toggle.min.css') }}
    {{ Html::style('intranet/plugins/bootstrap-validator/bootstrapValidator.min.css') }}
@endpush

@push('scripts')
    {{ Html::script('intranet/plugins/bootstrap-toggle/js/bootstrap-toggle.min.js') }}
    {{ Html::script('intranet/plugins/bootstrap-validator/bootstrapValidator2.min.js') }}
    {{ Html::script('intranet/node_modules/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}
    {{ Html::script('intranet/node_modules/jquery-mask-plugin/dist/jquery.mask.min.js') }}
    {{ Html::script('intranet/node_modules/moment/min/moment.min.js') }}
    {{ Html::script('intranet/node_modules/moment/locale/pt-br.js') }}
    {{ Html::script('intranet/node_modules/jsrender/jsrender.min.js') }}

    @verbatim
        <script id="tmpl-production-product" type="text/x-jsrender">
            <div>
                <div id="box-production-product-{{:index}}" class="box-production-product panel panel-default" data-title="{{:product.title}}">
                    <input type="hidden" name="products[{{:index}}][product_id]" value="{{:product.id}}">
                    {{if indication}}
                    <input type="hidden" name="products[{{:index}}][indication_id]" value="{{:indication.id}}">
                    {{/if}}
                    <div class="panel-heading">
                        <h3 class="panel-title text-uppercase">
                            <i class="fa fa-briefcase"></i>
                            <strong>{{:product.title}}</strong>
                            <button type="button" class="btn btn-danger btn-xs pull-right" style="margin-top: 10px;" onclick="$('#box-production-product-{{:index}}').remove();$('#btn-production-product-{{:id}}').attr('disabled', false);count_products--"><i class="fa fa-trash"></i></button>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <fieldset>
                            <legend>Dados da venda</legend>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Data</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" class="form-control mask-date" name="products[{{:index}}][sold_at]">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Valor</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">R$</span>
                                            <input type="text" class="form-control mask-money" name="products[{{:index}}][value]">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{if product.fields}}
                            {{for product.fields ~index=index}}
                            {{if type == 'text_small'}}
                            <div class="form-group">
                                <label class="control-label">{{:name}}</label>
                                <input type="text" class="form-control" name="products[{{:~index}}][fields][{{:id}}]" {{if required}} required {{/if}}>
                                <span class="help-block">{{:description}}</span>
                            </div>
                            {{/if}}
                            {{if type == 'text_larger'}}
                            <div class="form-group">
                                <label class="control-label">{{:name}}</label>
                                <textarea rows="5" class="form-control" name="products[{{:~index}}][fields][{{:id}}]" {{if required}} required {{/if}}></textarea>
                                <span class="help-block">{{:description}}</span>
                            </div>
                            {{/if}}
                            {{if type == 'date'}}
                            <div class="form-group">
                                <label class="control-label">{{:name}}</label>
                                <input type="date" class="form-control" name="products[{{:~index}}][fields][{{:id}}]" {{if required}} required {{/if}}>
                                <span class="help-block">{{:description}}</span>
                            </div>
                            {{/if}}
                            {{if type == 'select_single'}}
                            <div class="form-group">
                                <label class="control-label">{{:name}}</label>
                                <select class="form-control" name="products[{{:~index}}][fields][{{:id}}]" {{if required}} required {{/if}}>
                                    {{for options}}
                                    <option value="{{:key}}">{{:value}}</option>
                                    {{/for}}
                                </select>
                                <span class="help-block">{{:description}}</span>
                            </div>
                            {{/if}}
                            {{/for}}
                            {{/if}}
                        </fieldset>
                    </div>
                    {{if indication}}
                    <table class="table">
                        <tr class="active">
                            <th class="text-center">Indicado por</th>
                            <th class="text-center">Data da indicação</th>
                        </tr>
                        <tr class="active">
                            <td class="text-center">{{:indication.collaborator.name}}</td>
                            <td class="text-center">{{:indication.created_at_format}}</td>
                        </tr>
                    </table>
                    {{/if}}
                </div>
            </div>
       </script>
    @endverbatim

    <script>

        var count_products = 0;

        function addProduct(id){

            $('#btn-production-product-'+id).attr('disabled', true);

            if(count_products == 0)
                $("#box-production-products").empty();

            count_products++;

            $.post("{{ route($controllers->actions->product->verify->route) }}", {
                id: id,
                client_id: $('input[name="client_id"]').val()
            }).done(function( data ) {

                if(data.product.fields){

                    $.each(data.product.fields, function (key, field) {

                        if(field.options){

                            var options = JSON.parse(field.options);
                            var opts = [];

                            for ( var k in options){

                                op = {
                                    'key': k,
                                    'value': options[k]
                                };

                                opts.push(op);

                            }

                            data.product.fields[key].options = opts;

                        }

                    });

                }

                $("#box-production-products").append(
                    $.templates("#tmpl-production-product").render({
                        index: count_products,
                        id: id,
                        product: data.product,
                        indication: data.indication,
                    })
                );

                setMask();

            });

        }

        $('#wizard-production').bootstrapWizard({
            tabClass		    : 'wz-steps',
            nextSelector	    : '.next',
            previousSelector    : '.previous',
            onTabClick: function(tab, navigation, index) {
                return false;
            },
            onInit : function(){
                $('#wizard-production').find('.finish').hide().prop('disabled', true);
                $('#wizard-production').find('.finish').hide();
            },
            onTabShow: function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index+1;
                var $percent = (index/$total) * 100;
                var margin = (100/$total)/2;
                $('#wizard-production').find('.progress-bar').css({width:$percent+'%', 'margin': 0 + 'px ' + margin + '%'});

                navigation.find('li:eq('+index+') a').trigger('focus');

                // If it's the last tab then hide the last button and show the finish instead
                if($current >= $total) {
                    $('#wizard-production').find('.next').hide();
                    $('#wizard-production').find('.finish').show();
                    $('#wizard-production').find('.finish').attr('disabled', false);
                } else {
                    $('#wizard-production').find('.next').show();
                    $('#wizard-production').find('.finish').hide().prop('disabled', true);
                    $('#wizard-production').find('.finish').hide();
                }
            },
            onNext: function(tab, navigation, index){

                isValid = null;

                $('#form-production').formValidation('validate');

                if(isValid === false)
                    return false;

                // verifica se cliente existe
                if(index === 1){

                    let type_profile = $('input[name="type_profile"]:checked').val();

                    if( type_profile == 'F')
                        $('input[name="cnpj"]').val('');
                    else
                        $('input[name="cpf"]').val('');

                    $('input[name="client_id"]').val('');

                    $.post("{{ route($controllers->actions->client->verify->route) }}", {
                        type_profile: type_profile,
                        cpf: (type_profile == 'F') ? $('input[name="cpf"]').val() : '',
                        cnpj: (type_profile == 'J') ? $('input[name="cnpj"]').val() : ''
                    }).done(function( data ) {

                        if(data.id && (client_id === null || client_id !== data.id)) {

                            client_id = null;

                            if (data.id){
                                client_id = data.id;
                                $('input[name="client_id"]').val(data.id);
                            }

                            $('input[name="name"]').val(data.name);
                            $('input[name="company_name"]').val(data.company_name);
                            $('input[name="phone1"]').val(data.phone1);
                            $('input[name="phone2"]').val(data.phone2);
                            $('input[name="cellphone"]').val(data.cellphone);
                            $('input[name="email"]').val(data.email);
                            $('input[name="postcode"]').val(data.postcode);
                            $('input[name="address"]').val(data.address);
                            $('input[name="district"]').val(data.district);
                            $('input[name="complement"]').val(data.complement);
                            $('input[name="city"]').val(data.city);
                            $('select[name="state"]').val(data.state);

                            $('input[name="date_birth"]').val('');
                            if(data.date_birth)
                                $('input[name="date_birth"]').val(moment(data.date_birth).format('DD/MM/YYYY'));

                            setMask();

                        }

                    });

                }

                // dados para verificacao
                if(index === 3){

                    if($('.box-production-product').length < 1) {
                        $.niftyNoty({
                            type: 'danger',
                            container : '#wizard-production-tab3',
                            title : 'Ops!!',
                            message : 'Selecione no mínimo 1 produto para a sua produção',
                            timer: 2000,
                        });
                        return false;
                    }

                    $('#form-production-box-confirmation-name').text($('input[name="name"]').val());
                    $('#form-production-box-confirmation-cnpj').text($('input[name="cnpj"]').val());
                    $('#form-production-box-confirmation-cpf').text($('input[name="cpf"]').val());
                    $('#form-production-box-confirmation-cellphone').text($('input[name="cellphone"]').val());

                    var html = '<ul>';

                    $('.box-production-product').each(function() {
                        html += '<li>'+$(this).data('title')+'</li>';
                    });

                    html += '</ul>';

                    $('#form-production-box-confirmation-products').html(html);

                }

            }
        });

        $('input[name="type_profile"]').on('change', function(){

            $('#form-production-box-cpf, #form-production-box-cnpj, #form-production-box-company_name, #form-production-box-confirmation-box-cpf, #form-production-box-confirmation-box-cnpj').toggle();

        });

        var isValid;
        var client_id = null;

        $('#form-production').on('init.field.fv', function(e, data) {
            // data.field   --> The field name
            // data.element --> The field element
            if (data.field === 'products[]') {
                var $icon = data.element.data('fv.icon');
                $icon.appendTo('#alert-production-products-icon');
            }
        }).formValidation({
            message: 'Este valor não é valido',
            feedbackIcons: {
                valid: 'fa fa-check-circle fa-lg text-success',
                invalid: 'fa fa-times-circle fa-lg',
                validating: 'fa fa-refresh'
            },
            fields: {
                cpf: {
                    validators: {
                        notEmpty: {
                            message: 'O CPF é obrigatório.'
                        },
                        stringLength: {
                            min: 14,
                            max: 14,
                            message: 'Formato de CPF inválido.'
                        }
                    }
                },
                cnpj: {
                    validators: {
                        notEmpty: {
                            message: 'O CNPJ é obrigatório.'
                        },
                        stringLength: {
                            min: 18,
                            max: 18,
                            message: 'Formato de CNPJ inválido.'
                        }
                    }
                },
                name: {
                    validators: {
                        notEmpty: {
                            message: 'O Nome do cliente é obrigatório.'
                        }
                    }
                },
                'products[]': {
                    err: '#alert-production-products-message',
                    validators: {
                        notEmpty: {
                            message: 'O Produto é obrigatório.'
                        }
                    }
                }
            }
        }).on('success.field.fv', function(e, data) {
            // $(e.target)  --> The field element
            // data.bv      --> The BootstrapValidator instance
            // data.field   --> The field name
            // data.element --> The field element

            var $parent = data.element.parents('.form-group');

            // Remove the has-success class
            $parent.removeClass('has-success');

            // Hide the success icon
            //$parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]').hide();
        }).on('err.field.fv', function(e) {
            isValid = false;
        });

        var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
        },
        spOptions = {
            onKeyPress: function(val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            },
            placeholder: "(__) ____-____",
            selectOnFocus: true
        };

        function setMask(){
            $('.mask-date').mask('00/00/0000', {placeholder: "__/__/____", selectOnFocus: true});
            $('.mask-cep').mask('00000-000', {placeholder: "_____-___", selectOnFocus: true});
            $('.mask-phone').mask(SPMaskBehavior, spOptions);
            $('.mask-cpf').mask('000.000.000-00', {placeholder: "___.___.___-__", reverse: true, selectOnFocus: true});
            $('.mask-cnpj').mask('00.000.000/0000-00', {placeholder: "__.___.___/____-__", reverse: true, selectOnFocus: true});
            $('.mask-money').mask("#.##0,00", {placeholder: "0,00", reverse: true});
        }

        setMask();

    </script>

@endpush