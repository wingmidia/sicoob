@extends('intranet.layouts.master')

@section('title', $controllers->title)

@push('page.title')

    <div class="row">
        <div class="col-md-8">
            <h1 class="page-header text-overflow"><i class="{{ $controllers->icon }}"></i> Produção</h1>
        </div>
        <div class="col-md-4 text-right">
            @permission($controllers->actions->manager->permission)
            <a href="{{ route($controllers->actions->manager->route) }}" class="btn btn-info btn-lg"><i class="fa fa-pencil-square"></i> Gerenciar</a>
            @endpermission
            @permission($controllers->actions->add->permission)
            <a href="{{ route($controllers->actions->add->route) }}" class="btn btn-primary btn-lg"><i class="fa fa-plus-circle"></i> Adicionar</a>
            @endpermission
        </div>
    </div>

@endpush

@section('content')

    <div class="panel">
        {!! $datatable->table(['class' => 'table table-striped table-hover']) !!}
    </div>

@endsection

@push('styles')
    {!! Html::style('intranet/node_modules/datatables.net-bs/css/dataTables.bootstrap.css') !!}
    {!! Html::style('intranet/node_modules/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') !!}
@endpush

@push('scripts')

    {!! Html::script('intranet/node_modules/datatables.net/js/jquery.dataTables.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-bs/js/dataTables.bootstrap.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons/js/dataTables.buttons.min.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons/js/buttons.colVis.min.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons/js/buttons.html5.min.js') !!}
    {!! Html::script('intranet/node_modules/jszip/dist/jszip.min.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') !!}

    {!! $datatable->scripts() !!}

    <script>

        let html;

        function datatablesFormatStrong(data){

            html = '<strong>'+data+'</strong>';
            return html;

        }

        function datatablesFormatProductionStatus(data, type, full){

            if(data !== null) {

                console.log(data);

                switch (data){

                    case '0' :
                        html = '<div class="label label-table label-danger">Cancelada</div>';
                        break;

                    case '1' :
                        html = '<div class="label label-table label-success">Confirmada</div>';
                        break;

                    case '2' :
                        html = '<div class="label label-table label-purple">Em contestação</div>';
                        break;

                    case '3' :
                        html = '<div class="label label-table label-warning">Aguardando confirmação</div>';
                        break;

                    default:
                        html = '';

                }

                return html;

            } else
                return '';

        }

        function datatablesFormatActions(data, type, full){

            html = '<div class="text-center">';

            html += '<a href="{{ route($controllers->actions->show->route) }}/'+full['id']+'" class="btn btn-icon-toggle btn-sm btn-mint"><i class="fa fa-info-circle"></i></a>';

            {{--@if(isset($controllers->actions->edit))
                @permission($controllers->actions->edit->permission)
                    html += '<a href="{{ route($controllers->actions->edit->route) }}/'+full['id']+'" class="btn btn-icon-toggle btn-sm btn-info mar-lft"><i class="fa fa-pencil"></i></a>';
                @endpermission
            @endif--}}

            @if(isset($controllers->actions->delete))
                @permission($controllers->actions->delete->permission)
                    html += '<a href="{{ route($controllers->actions->delete->route) }}/'+full['id']+'" class="btn btn-icon-toggle btn-sm btn-danger mar-lft"><i class="fa fa-trash"></i></a>';
                @endpermission
            @endif

            html += '</div>';

            return html;
        }

    </script>
@endpush