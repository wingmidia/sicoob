@extends('intranet.layouts.master')

@section('title', $controllers->title)

@push('page.title')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="row">
                <div class="col-md-8">
                    <h1 class="page-header text-overflow"><i class="{{ $controllers->icon }}"></i> {{ $controllers->title }} | <small><i class="fa fa-pencil"></i> Editando</small></h1>
                </div>
                <div class="col-md-4 text-right">
                    <a href="{{ route($controllers->route) }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Voltar</a>
                    @if(!empty($item->id))
                        @permission($controllers->actions->add->permission)
                        <a href="{{ route($controllers->actions->add->route) }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Adicionar</a>
                        @endpermission
                    @endif
                </div>
            </div>
        </div>
    </div>

@endpush

@section('content')

    <form id="form-production" action="{{ route($controllers->actions->update->route, $item->id) }}" method="post">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="row">
                    <div class="col-md-8">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Resumo da produção</h3>
                            </div>
                            <table class="table table-hover">
                                <tr>
                                    <th width="20%">Colaborador</th>
                                    <td>{{ $item->collaborator->name }}</td>
                                </tr>
                                <tr>
                                    <th>Criada em</th>
                                    <td>{{ $item->created_at->format('d/m/Y') }}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Dados do cliente</h3>
                            </div>
                            <table class="table table-hover">
                                <tr>
                                    <th width="20%">Tipo</th>
                                    <td>{{ $item->client->getTypeProfileName() }}</td>
                                </tr>
                                @if($item->client->cpf)
                                    <tr>
                                        <th>CPF</th>
                                        <td>{{ $item->client->cpf }}</td>
                                    </tr>
                                @endif
                                @if($item->client->cnpj)
                                    <tr>
                                        <th>CNPJ</th>
                                        <td>{{ $item->client->cnpj }}</td>
                                    </tr>
                                @endif
                                <tr>
                                    <th>Nome</th>
                                    <td>{{ $item->client->name }}</td>
                                </tr>
                                @if($item->client->cnpj)
                                    <tr>
                                        <th>Razão Social</th>
                                        <td>{{ $item->client->company_name }}</td>
                                    </tr>
                                @endif
                            </table>
                        </div>
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Dados de contato</h3>
                            </div>
                            <table class="table table-hover">
                                @if($item->client->email)
                                    <tr>
                                        <th width="20%">E-mail</th>
                                        <td>{{ $item->client->email }}</td>
                                    </tr>
                                @endif
                                <tr>
                                    <th>Celular/Whatsapp</th>
                                    <td>{{ $item->client->cellphone }}</td>
                                </tr>
                                @if($item->client->phone1)
                                    <tr>
                                        <th>Telefone</th>
                                        <td>{{ $item->client->phone1 }}</td>
                                    </tr>
                                @endif
                                @if($item->client->phone2)
                                    <tr>
                                        <th>Telefone Alternativo</th>
                                        <td>{{ $item->client->phone2 }}</td>
                                    </tr>
                                @endif
                            </table>
                        </div>
                        @foreach($item->products as $product)
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="fa fa-angle-double-right"></i> {{ $product->title }}</h3>
                                </div>
                                <table class="table table-bordered table-hover">
                                    <tbody>
                                    <tr>
                                        <th width="25%">Data</th>
                                        <td width="25%">{{ \Carbon\Carbon::createFromFormat('Y-m-d', $product->pivot->sold_at)->format('d/m/Y') }}</td>
                                        <th width="25%">Valor</th>
                                        <td>R$ {{ number_format($product->pivot->value, 2, ',', '.')  }}</td>
                                    </tr>
                                    @foreach($item->fields->where('product_id', $product->id) as $field)
                                        <tr>
                                            <th colspan="1">{{ $field->name }}</th>
                                            <td colspan="3">{{ $field->pivot->value }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                @if($product->indication)
                                    <table class="table table-condensed">
                                        <tr class="active">
                                            <th class="text-center">Indicado por</th>
                                            <th class="text-center">Data da indicação</th>
                                        </tr>
                                        <tr class="active">
                                            <td class="text-center">{{ $product->indication->collaborator->name }}</td>
                                            <td class="text-center">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:m:s', $product->indication->created_at)->format('d/m/Y') }}</td>
                                        </tr>
                                    </table>
                                @endif
                            </div>
                        @endforeach
                        @if($item->status != 1 && $item->status != 0 && $item->collaborator_id == Auth::user()->collaborator->id)
                            <div class="text-right">
                                <a href="{{ route($controllers->actions->delete->route, $item->id) }}" class="btn btn-danger btn-sm mar-btm"><i class="fa fa-trash"></i> Remover produção</a>
                            </div>
                        @endif
                    </div>
                    <div class="col-md-4">
                        @permission('productions-manager')
                        @if($item->status == 0)
                            <div class="panel panel-danger panel-bordered">
                                @elseif($item->status == 1)
                                    <div class="panel panel-success panel-bordered">
                                        @elseif($item->status == 2)
                                            <div class="panel panel-purple panel-bordered">
                                                @elseif($item->status == 3)
                                                    <div class="panel panel-warning panel-bordered">
                                                        @endif

                                                        <div class="panel-heading">
                                                            <h3 class="panel-title">Atualizar</h3>
                                                        </div>

                                                        <div class="panel-body">
                                                            <div class="form-group">
                                                                <label for="form-production-status" class="control-label">Status</label>
                                                                <select name="status" id="form-production-status" class="form-control">
                                                                    <option value="0" @if($item->status == 0) selected @endif class="text-danger">CANCELADA</option>
                                                                    <option value="1" @if($item->status == 1) selected @endif class="text-success">CONFIRMADA</option>
                                                                    <option value="2" @if($item->status == 2) selected @endif class="text-purple">EM CONTESTAÇÃO</option>
                                                                    <option value="3" @if($item->status == 3) selected @endif class="text-warning">AGUARDANDO CONFIRMAÇÃO</option>
                                                                </select>
                                                            </div>
                                                            @if($item->status == 2)
                                                            <div class="well">
                                                                <div class="form-group">
                                                                    <label class="control-label">Indicações Contestadas</label>
                                                                    <hr>
                                                                    @foreach($item->products as $product)
                                                                        <label class="control-label"><i class="fa fa-angle-double-right"></i> {{ $product->title }}</label>
                                                                        {!! Form::select("indication[$product->id]", $collaborators, $product->indication->collaborator->id, ['class'=>'form-control']) !!}
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                            @endif
                                                            <button class="btn btn-primary btn-lg btn-block" type="submit"><i class="fa fa-save"></i> Salvar</button>
                                                        </div>

                                                    </div>
                                            </div>
                                    </div>
                            </div>
                        @endpermission
                    </div>
    </form>

@endsection

@push('styles')
@endpush

@push('scripts')
@endpush