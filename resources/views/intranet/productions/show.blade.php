@extends('intranet.layouts.master')

@section('title', $controllers->title)

@push('page.title')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="row">
                <div class="col-md-8">
                    <h1 class="page-header text-overflow"><i class="{{ $controllers->icon }}"></i> {{ $controllers->title }} | <small><i class="fa fa-info-circle"></i> Visualizando</small></h1>
                </div>
                <div class="col-md-4 text-right">
                    <a href="{{ route($controllers->route) }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Voltar</a>
                    @if(!empty($item->id))
                        @permission($controllers->actions->add->permission)
                        <a href="{{ route($controllers->actions->add->route) }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Adicionar</a>
                        @endpermission
                    @endif
                </div>
            </div>
        </div>
    </div>

@endpush

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="row">
                <div class="col-md-8">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Dados do cliente</h3>
                        </div>
                        <table class="table table-hover">
                            <tr>
                                <th width="20%">Tipo</th>
                                <td>{{ $item->client->getTypeProfileName() }}</td>
                            </tr>
                            @if($item->client->cpf)
                                <tr>
                                    <th>CPF</th>
                                    <td>{{ $item->client->cpf }}</td>
                                </tr>
                            @endif
                            @if($item->client->cnpj)
                                <tr>
                                    <th>CNPJ</th>
                                    <td>{{ $item->client->cnpj }}</td>
                                </tr>
                            @endif
                            <tr>
                                <th>Nome</th>
                                <td>{{ $item->client->name }}</td>
                            </tr>
                            @if($item->client->cnpj)
                                <tr>
                                    <th>Razão Social</th>
                                    <td>{{ $item->client->company_name }}</td>
                                </tr>
                            @endif
                        </table>
                    </div>
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Dados de contato</h3>
                        </div>
                        <table class="table table-hover">
                            @if($item->client->email)
                                <tr>
                                    <th width="20%">E-mail</th>
                                    <td>{{ $item->client->email }}</td>
                                </tr>
                            @endif
                            <tr>
                                <th>Celular/Whatsapp</th>
                                <td>{{ $item->client->cellphone }}</td>
                            </tr>
                            @if($item->client->phone1)
                                <tr>
                                    <th>Telefone</th>
                                    <td>{{ $item->client->phone1 }}</td>
                                </tr>
                            @endif
                            @if($item->client->phone2)
                                <tr>
                                    <th>Telefone Alternativo</th>
                                    <td>{{ $item->client->phone2 }}</td>
                                </tr>
                            @endif
                        </table>
                    </div>
                    @foreach($item->products as $product)
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-angle-double-right"></i> {{ $product->title }}</h3>
                            </div>
                            <table class="table table-bordered table-hover">
                                <tbody>
                                <tr>
                                    <th width="25%">Data</th>
                                    <td width="25%">{{ \Carbon\Carbon::createFromFormat('Y-m-d', $product->pivot->sold_at)->format('d/m/Y') }}</td>
                                    <th width="25%">Valor</th>
                                    <td>R$ {{ number_format($product->pivot->value, 2, ',', '.')  }}</td>
                                </tr>
                                @foreach($item->fields->where('product_id', $product->id) as $field)
                                    <tr>
                                        <th colspan="1">{{ $field->name }}</th>
                                        <td colspan="3">{{ $field->pivot->value }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            @if($product->indication)
                                <table class="table table-condensed">
                                    <tr class="active">
                                        <th class="text-center">Indicado por</th>
                                        <th class="text-center">Data da indicação</th>
                                    </tr>
                                    <tr class="active">
                                        <td class="text-center">{{ $product->indication->collaborator->name }}</td>
                                        <td class="text-center">{{ \Carbon\Carbon::createFromFormat('Y-m-d H:m:s', $product->indication->created_at)->format('d/m/Y') }}</td>
                                    </tr>
                                </table>
                            @endif
                        </div>
                    @endforeach
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Resumo da produção</h3>
                        </div>
                        <table class="table table-hover">
                            <tr>
                                <th width="20%">Colaborador</th>
                                <td>{{ $item->collaborator->name }}</td>
                            </tr>
                            <tr>
                                <th>Criada em</th>
                                <td>{{ $item->created_at->format('d/m/Y') }}</td>
                            </tr>
                        </table>
                    </div>
                    @if($item->status != 1 && $item->status != 0 && $item->collaborator_id == Auth::user()->collaborator->id)
                    <div class="text-right">
                        <a href="{{ route($controllers->actions->delete->route, $item->id) }}" class="btn btn-danger btn-sm mar-btm"><i class="fa fa-trash"></i> Remover produção</a>
                    </div>
                    @endif
                </div>
                <div class="col-md-4">
                    @if($item->status == 0)
                        <div class="panel panel-danger panel-colorful">
                            <div class="panel-body text-center">
                                <i class="fa fa-ban fa-4x"></i>
                                <h4 class="text-light">CANCELADA</h4>
                            </div>
                        </div>
                    @elseif($item->status == 1)
                        <div class="panel panel-success panel-colorful">
                            <div class="panel-body text-center">
                                <i class="fa fa-thumbs-up fa-4x"></i>
                                <h4 class="text-light">PRODUÇÃO CONFIRMADA</h4>
                            </div>
                        </div>
                    @elseif($item->status == 2)
                        <div class="panel panel-purple panel-colorful">
                            <div class="panel-body text-center">
                                <i class="fa fa-exclamation-triangle fa-4x"></i>
                                <h4 class="text-light">EM CONTESTAÇÃO</h4>
                            </div>
                        </div>
                    @elseif($item->status == 3)
                        <div class="panel panel-warning panel-colorful">
                            <div class="panel-body text-center">
                                <i class="fa fa-info-circle fa-4x"></i>
                                <h4 class="text-light">AGUARDANDO CONFIRMAÇÃO</h4>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection

@push('styles')
@endpush

@push('scripts')
@endpush