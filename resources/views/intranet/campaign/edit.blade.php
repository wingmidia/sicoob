@extends('intranet.layouts.master')

@section('title', $controllers->title)


@push('page.title')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <div class="row">
                <div class="col-md-8">
                    <h1 class="page-header text-overflow"><i class="{{ $controllers->icon }}"></i> {{ $controllers->title }} | <small><i class="fa fa-plus-circle"></i> Editando campanha</small></h1>
                </div>
                <div class="col-md-4 text-right">
                    <a href="{{ route($controllers->route) }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Voltar</a>
                </div>
            </div>

        </div>
    </div>

@endpush


@section('content')
    <div class="col-md-8 col-md-offset-2">
        <form id="form-campaign" action="{{route($controllers->actions->update->route)}}" method="POST">
            {{csrf_field()}}
            <input type="hidden" name="id" value="{{old('id', $campaign->id)}}">
            <div class="tab-base">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tabs-box-dados">Dados básicos</a></li>
                    <li><a data-toggle="tab" href="#tabs-box-produtos">Categorias</a></li>
                </ul>
                <div class="tab-content">
                    <div id="tabs-box-dados" class="tab-pane fade in active">
                        <div class="row">
                            <div class="col-md-8">
                                <fieldset>
                                    <legend>DADOS DA CAMPANHA</legend>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Nome</label>
                                                <input type="text" name="name" class="form-control" value="{{old('name', $campaign->name)}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Data início</label>
                                                <input id="form-campaign-start_at" type="text" class="form-control mask-date" name="start_at" data-fv-date-format="DD/MM/YYYY" value="{{old('start_at', $campaign->start_at->format('d/m/Y'))}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Data final</label>
                                                <input id="form-campaign-end_at" type="text" class="form-control mask-date" name="end_at" data-fv-date-format="DD/MM/YYYY" value="{{old('end_at', $campaign->end_at->format('d/m/Y'))}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Descrição</label>
                                                <textarea class="form-control" name="description"cols="30" rows="3">{{old('description', $campaign->description)}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-4">
                                <fieldset>
                                    <legend>PARÂMETROS</legend>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>STATUS</label>
                                                <div class="clearfix"></div>
                                                <input type="checkbox" class="form-control" name="active" value="1" data-toggle="toggle" data-on="Ativo" data-off="Inativo" data-size="small" data-onstyle="success" data-offstyle="danger" data-width="80" @if($campaign->active) checked @endif>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Foto</label>
                                                <div class="dropzone dz-clickable" id="myDrops" style="border: none;">
                                                    <div class="dz-default dz-message"><h4>Arraste sua foto ou clique aqui para selecionar imagem</h4></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="image" id="campaign-image" value="{{$campaign->image}}">
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <div id="tabs-box-produtos" class="tab-pane fade">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>LISTA DE CATEGORIAS</label>
                                    <div class="clearfix"></div>
                                    <select name="categories[]" id="campaign-categories" multiple="multiple" style="width: 80%">
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-footer text-right">
                        <button class="btn btn-primary btn-lg" type="submit"><i class="fa fa-save"></i> Salvar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection




@push('styles')
    {{ Html::style('intranet/plugins/bootstrap-validator/bootstrapValidator.min.css') }}
    {{ Html::style('intranet/plugins/bootstrap-toggle/css/bootstrap-toggle.min.css') }}
    {{ Html::style('intranet/node_modules/select2/dist/css/select2.min.css') }}
    {{ Html::style('intranet/node_modules/dropzone/dist/dropzone.css') }}
    <style>
        #campaign-categories{
            min-height: 80%;
        }

        .dropzone .dz-preview.dz-image-preview .dz-image{
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .close-btn{
            background: none;
            border: none;
            cursor: pointer;
        }
        .close-btn i{
            font-size: 2.7rem;
        }
        .dropzone img{
            width: 100px;
        }

    </style>
@endpush


@push('scripts')
    {{ Html::script('intranet/plugins/bootstrap-toggle/js/bootstrap-toggle.min.js') }}
    {{ Html::script('intranet/plugins/bootstrap-validator/bootstrapValidator2.min.js') }}
    {{ Html::script('intranet/node_modules/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}
    {{ Html::script('intranet/node_modules/jquery-mask-plugin/dist/jquery.mask.min.js') }}
    {{ Html::script('intranet/node_modules/moment/min/moment.min.js') }}
    {{ Html::script('intranet/node_modules/select2/dist/js/select2.min.js') }}
    {!! Html::script('intranet/node_modules/dropzone/dist/dropzone.js') !!}
    {{ Html::script('intranet/node_modules/moment/locale/pt-br.js') }}


    <script>
        var categories = [];
        var campaign = {};
        var campaignPath = "/intranet/assets/img/campaigns/";
        @if(!empty($campaign))
            campaign = {!! json_encode($campaign) !!}
        @endif
        Dropzone.autoDiscover = false;

        @if(!empty($campaign->categories))

                var objProducts  = {!! json_encode($categories) !!};

                var selectProducts  = {!! json_encode($campaign->categories) !!};

                objProducts.forEach(function (prod) {
                    var selected = false;
                    selectProducts.forEach(function (p) {
                       if(prod.id==p.id)
                           selected = true;
                    });
                    categories.push({
                        id: prod.id,
                        text: prod.name,
                        selected: selected
                    });
                });

        @endif



        $(document).ready(function () {


            // var zone = $("div#myDrops").dropzone({
            var zone = new Dropzone("div#myDrops", {
                url: "{{route('intranet.campaign.upload')}}" ,
                method: 'post',
                uploadMultiple: false,
                maxFiles: 1,
                params: {
                    _token: '{{csrf_token()}}'
                },
                success: function (file, response) {
                    if(response.success){
                        $('#campaign-image').val(response.name);
                    }
                },
                complete: function (res) {
                    // console.log('res', res);
                }
            });

            if(campaign.image) {
                var sizeFile = 0;

                var file = {name: campaign.image, size: sizeFile, width: 100, height: 100};

                zone.files.push(file);

                zone.on('thumbnail', function (file) {

                    var closeButton = $("<button class='close-btn add-tooltip' title='Remover foto'>");
                    closeButton.append("<i class='fa fa-times-circle text-danger'>");

                    closeButton.on('click', function () {
                        zone.removeFile(file);
                        $('#campaign-image').val('');
                    });
                    file.previewElement.querySelector('.dz-details').appendChild(closeButton.get(0));

                    var request = $.ajax({
                        url: campaignPath + campaign.image,
                        success: function (response, data) {
                            var fileSize = request.getResponseHeader('Content-Length');
                            file.previewElement.querySelector('.dz-size span[data-dz-size]').innerHTML = zone.filesize(fileSize);
                        }
                    });

                });


                zone.emit("addedfile", file);

                zone.emit("thumbnail", file, campaignPath + campaign.image);



            }


            $('#campaign-categories').select2({
                placeholder: 'Selecione um produto',
                tags: true,
                data: categories
            });

            $('#form-campaign').formValidation({
                feedbackIcons: {
                    valid: 'fa fa-check-circle fa-lg text-success',
                    invalid: 'fa fa-times-circle fa-lg',
                    validating: 'fa fa-refresh'
                },
                fields: {
                    name: {
                        validators: {
                            notEmpty: {
                                message: 'Nome é obrigatório'
                            },
                            stringLength: {
                                min: 3,
                                message: 'Nome inválido.'
                            }
                        }
                    },
                    start_at: {
                        validators: {
                            date: {
                                format: 'DD/MM/YYYY',
                                message: 'Data com formato inválido'
                            }
                        }
                    },
                    end_at: {
                        validators: {
                            date: {
                                format: 'DD/MM/YYYY',
                                message: 'Data com formato inválido'
                            }
                        }
                    }
                }
            });

        });
        function setMask(){
            $('.mask-date').mask('00/00/0000', {placeholder: "__/__/____", selectOnFocus: true});
            $('.mask-cep').mask('00000-000', {placeholder: "_____-___", selectOnFocus: true});

            $('.mask-cpf').mask('000.000.000-00', {placeholder: "___.___.___-__", reverse: true, selectOnFocus: true});
            $('.mask-cnpj').mask('00.000.000/0000-00', {placeholder: "__.___.___/____-__", reverse: true, selectOnFocus: true});
        }
        setMask();




    </script>
@endpush
