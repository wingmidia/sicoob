@extends('intranet.layouts.master')

@section('title', $controllers->title)

@push('page.title')

    <div class="row">
        <div class="col-md-8">
            <h1 class="page-header text-overflow"><i class="{{ $controllers->icon }}"></i> Campanhas</h1>
        </div>
        <div class="col-md-4 text-right">
            <a href="{{ route('intranet.campaign.ranking') }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Voltar</a>
            @permission($controllers->actions->create->permission)
            <a href="{{ route($controllers->actions->create->route) }}" class="btn btn-primary btn-lg"><i class="fa fa-plus-circle"></i> Adicionar</a>
            @endpermission
        </div>
    </div>

    <div id="deleteModal" class="modal fade delete-modal" tabindex="-1" role="dialog" aria-labelledby="deleteModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">VOCÊ REALMENTE DESEJA EXCLUIR A CAMPANHA?</h3>
                </div>
                <div class="modal-body">
                    <a class="btn btn-info delete-link">Sim</a>
                    <button data-dismiss="modal" class="btn btn-danger">Não</button>
                </div>
            </div>
        </div>
    </div>

@endpush

@section('content')
    <div class="panel">
        {!! $datatable->table(['class' => 'table table-striped table-hover']) !!}
    </div>
@endsection

@push('styles')
    {!! Html::style('intranet/node_modules/datatables.net-bs/css/dataTables.bootstrap.css') !!}
    {!! Html::style('intranet/node_modules/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') !!}
    <style>
        .act-mar:not(:first-child){
            margin-left: 10px;
        }
    </style>
@endpush

@push('scripts')

    {!! Html::script('intranet/node_modules/datatables.net/js/jquery.dataTables.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-bs/js/dataTables.bootstrap.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons/js/dataTables.buttons.min.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons/js/buttons.colVis.min.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons/js/buttons.html5.min.js') !!}
    {!! Html::script('intranet/node_modules/jszip/dist/jszip.min.js') !!}
    {!! Html::script('intranet/node_modules/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') !!}

    {!! $datatable->scripts() !!}

    <script>
        $(document).ready(function () {
            $(document).on('click', '.delete-btn', function () {
                var url = $(this).attr('data-link');
                $('.delete-modal .delete-link').attr('href', url);
                $('#deleteModal').modal('show');
            });
        });
        let html;
        function datatablesFormatActions(data, type, full){
            console.log(full);
            html = '<div class="text-center">'
            @if(isset($controllers->actions->edit))
                @permission($controllers->actions->edit->permission)
                    html += '<a title="Editar campanha" href="{{ route($controllers->actions->edit->route) }}/'+full['id']+'" class="btn btn-icon-toggle btn-sm btn-info act-mar add-tooltip"><i class="fa fa-pencil"></i></a>';
                @endpermission
            @endif

            @if(isset($controllers->actions->delete))
                @permission($controllers->actions->delete->permission)
                html += '<a data-link="{{ route($controllers->actions->delete->route) }}/'+full['id']+'" class="delete-btn btn btn-icon-toggle btn-sm btn-danger act-mar"><i class="fa fa-trash"></i></a>';
                @endpermission
            @endif

                html += '</div>';
            return html;
        }

        function datatablesFormatStrong(data){

            html = '<strong>'+data+'</strong>';
            return html;

        }

        function datatablesFormatCampaignStatus(data) {
            return  (data == 1) ? '<i class="fa fa-circle text-success" title="Ativo"></i> Ativo' : '<i class="fa fa-circle text-danger" title="Inativo"></i> Inativo';
        }

    </script>
@endpush