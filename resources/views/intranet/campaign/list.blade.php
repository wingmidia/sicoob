@extends('intranet.layouts.master')

@section('title', $controllers->title)

@push('page.title')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="row">
                <div class="col-md-8">
                    <h1 class="page-header text-overflow"><i class="{{ $controllers->icon }}"></i> {{ $controllers->title }}</h1>
                </div>
                <div class="col-md-4 text-right">

                    @permission('campaigns')
                    <a href="{{ route('intranet.campaign.index') }}" class="btn btn-info btn-lg"><i class="fa fa-pencil-square"></i> Gerenciar</a>
                    @endpermission
                </div>
            </div>
        </div>
    </div>

@endpush

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @if(count($activeCampaigns))
            @foreach($activeCampaigns as $campaign)
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="media">
                            <div class="media-left">
                                <img class="media-object img-rounded img-campaign" src="{{ ($campaign->image) ? url('/intranet/assets/img/campaigns/'.$campaign->image) : url('/intranet/assets/img/campaigns/default.png') }}" alt="{{$campaign->description}}" title="Campanha: {{$campaign->name}}">
                            </div>
                            <div class="media-body">
                                <h3 class="media-heading">
                                    {{$campaign->name}}
                                </h3>
                                <b>Período: {{$campaign->start_at->format('d/m/Y')}} - {{$campaign->end_at->format('d/m/Y')}}</b>
                                <p>
                                    {{$campaign->description}}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="panel-actions text-right">
                            @permission($controllers->actions->create->permission)
                            <a class="btn btn-warning add-tooltip act-mar" href="{{route('intranet.ranking.indication', $campaign->id)}}" title="Veja o ranking de colaboradores por indicação"><i class="fa fa-trophy"></i>&nbsp;Ranking Indicação</a>
                            <a class="btn btn-success add-tooltip act-mar" href="{{route('intranet.ranking.sale', $campaign->id)}}" title="Veja o ranking de colaboradores por vendas"><i class="fa fa-trophy"></i>&nbsp;Ranking Produção</a>
                            @endpermission
                        </div>
                    </div>
                </div>
            @endforeach
            @else
                <div class="alert alert-info">
                    Não existe campanhas ativas.
                </div>
            @endif
        </div>
    </div>

    @if(count($completeCampaigns))
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="row">
                <div class="col-md-8">
                    <h1 class="page-header text-overflow">Campanhas encerradas</h1>
                </div>
            </div>
            @foreach($completeCampaigns as $campaign)
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="media">
                            <div class="media-left">
                                <img class="media-object img-rounded img-campaign" src="{{ ($campaign->image) ? url('/intranet/assets/img/campaigns/'.$campaign->image) : url('/intranet/assets/img/campaigns/default.png') }}" alt="{{$campaign->description}}" title="Campanha: {{$campaign->name}}">
                            </div>
                            <div class="media-body">
                                <h3 class="media-heading">
                                    {{$campaign->name}}
                                </h3>
                                <b>Período: {{$campaign->start_at->format('d/m/Y')}} - {{$campaign->end_at->format('d/m/Y')}}</b>
                                <p>
                                    {{$campaign->description}}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="panel-actions text-right">
                            <a class="btn btn-warning add-tooltip act-mar" href="{{route('intranet.ranking.indication', $campaign->id)}}" title="Veja o ranking de colaboradores por indicação"><i class="fa fa-trophy"></i>&nbsp;Ranking Indicação</a>
                            <a class="btn btn-success add-tooltip act-mar" href="{{route('intranet.ranking.sale', $campaign->id)}}" title="Veja o ranking de colaboradores por vendas"><i class="fa fa-trophy"></i>&nbsp;Ranking Produção</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    @endif

@endsection

@push('styles')
    <style>
        .img-campaign{
            height: 12vh;
        }
    </style>
@endpush

@push('scripts')
@endpush