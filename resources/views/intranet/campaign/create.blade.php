@extends('intranet.layouts.master')

@section('title', $controllers->title)


@push('page.title')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <div class="row">
                <div class="col-md-8">
                    <h1 class="page-header text-overflow"><i class="{{ $controllers->icon }}"></i> {{ $controllers->title }} | <small><i class="fa fa-plus-circle"></i> Adicionando campanha</small></h1>
                </div>
                <div class="col-md-4 text-right">
                    <a href="{{ route($controllers->route) }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Voltar</a>
                </div>
            </div>

        </div>
    </div>

@endpush


@section('content')


    <div class="col-md-8 col-md-offset-2">
        <form id="form-campaign" action="{{route($controllers->actions->store->route)}}" method="POST" enctype="multipart/form-data">

            {{csrf_field()}}
            <div class="tab-base">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tabs-box-dados">Dados básicos</a></li>
                    <li><a data-toggle="tab" href="#tabs-box-produtos">Categorias</a></li>
                </ul>
                <div class="tab-content">
                    <div id="tabs-box-dados" class="tab-pane fade in active">
                        <div class="row">

                            <div class="col-md-8">
                                <fieldset>
                                    <legend>DADOS DA CAMPANHA</legend>


                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Nome</label>
                                                <input type="text" name="name" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <label>Data inínio</label>
                                                <input id="form-campaign-start_at" type="text" class="form-control mask-date" name="start_at" data-fv-date-format="DD/MM/YYYY">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Data final</label>
                                                <input id="form-campaign-end_at" type="text" class="form-control mask-date" name="end_at" data-fv-date-format="DD/MM/YYYY">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Descrição</label>
                                                <textarea class="form-control" name="description"cols="30" rows="3"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-4">

                                <fieldset>
                                    <legend>PARÂMETROS</legend>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>STATUS</label>
                                                <div class="clearfix"></div>
                                                <input type="checkbox" class="form-control" name="active" value="1" data-toggle="toggle" data-on="Ativo" data-off="Inativo" data-size="small" data-onstyle="success" data-offstyle="danger" data-width="80" checked >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Foto</label>
                                                <div class="dropzone dz-clickable" id="myDrops" style="border: none;">
                                                    <div class="dz-default dz-message"><h4>Arraste sua foto ou clique aqui para selecionar imagem</h4></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <input type="hidden" name="image_name" id="campaign-image">
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <div id="tabs-box-produtos" class="tab-pane fade">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>LISTA DE CATEGORIAS</label>
                                    <div class="clearfix"></div>
                                    <select name="categories[]" id="campaign-categories" multiple="multiple" style="width: 80%">
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach    
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-footer text-right">
                        <button id="sendButton" class="btn btn-primary btn-lg" type="submit"><i class="fa fa-save"></i> Salvar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@push('styles')
    {{ Html::style('intranet/plugins/bootstrap-validator/bootstrapValidator.min.css') }}
    {{ Html::style('intranet/plugins/bootstrap-toggle/css/bootstrap-toggle.min.css') }}
    {{ Html::style('intranet/node_modules/select2/dist/css/select2.min.css') }}
    {{ Html::style('intranet/node_modules/dropzone/dist/dropzone.css') }}
    <style>
        #campaign-categories{
            min-height: 80%;
        }
    </style>
@endpush

@push('scripts')
    {{ Html::script('intranet/plugins/bootstrap-toggle/js/bootstrap-toggle.min.js') }}
    {{ Html::script('intranet/plugins/bootstrap-validator/bootstrapValidator2.min.js') }}
    {{ Html::script('intranet/node_modules/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js') }}
    {{ Html::script('intranet/node_modules/jquery-mask-plugin/dist/jquery.mask.min.js') }}
    {{ Html::script('intranet/node_modules/moment/min/moment.min.js') }}
    {{ Html::script('intranet/node_modules/select2/dist/js/select2.min.js') }}
    {!! Html::script('intranet/node_modules/dropzone/dist/dropzone.js') !!}
    {{ Html::script('intranet/node_modules/moment/locale/pt-br.js') }}
    <script>
        Dropzone.autoDiscover = false;

        $(document).ready(function () {


            var zone = $("div#myDrops").dropzone({
                url: "{{route('intranet.campaign.upload')}}" ,
                method: 'post',
                uploadMultiple: false,
                maxFiles: 1,
                params: {
                    _token: '{{csrf_token()}}'
                },
                success: function (file, response) {
                    if(response.success){
                        $('#campaign-image').val(response.name);
                    }
                },
                complete: function (res) {
                    console.log('res', res);
                }
            });

            console.log(zone);

            $('#campaign-categories').select2({
                minimumSelectionLength: 1,
                placeholder: 'Selecione um produto'
            });

            $('#form-campaign').formValidation({
                feedbackIcons: {
                    valid: 'fa fa-check-circle fa-lg text-success',
                    invalid: 'fa fa-times-circle fa-lg',
                    validating: 'fa fa-refresh'
                },
                fields: {
                    name: {
                        validators: {
                            notEmpty: {
                                message: 'Nome é obrigatório'
                            },
                            stringLength: {
                                min: 3,
                                message: 'Nome inválido.'
                            }
                        }
                    },
                    start_at: {
                        validators: {
                            notEmpty: {
                                message: 'Data é obrigatória'
                            },
                            date: {
                                format: 'DD/MM/YYYY',
                                message: 'Data com formato inválido'
                            }
                        }
                    },
                    end_at: {
                        validators: {
                            notEmpty: {
                                message: 'Data é obrigatória'
                            },
                            date: {
                                format: 'DD/MM/YYYY',
                                message: 'Data com formato inválido'
                            }
                        }
                    },
                    'categories[]': {
                        validators: {
                            notEmpty: {
                                message: 'Selecione pelo menos um produto'
                            }
                        }
                    }
                }
            });

        });
        function setMask(){
            $('.mask-date').mask('00/00/0000', {placeholder: "__/__/____", selectOnFocus: true});
            $('.mask-cep').mask('00000-000', {placeholder: "_____-___", selectOnFocus: true});

            $('.mask-cpf').mask('000.000.000-00', {placeholder: "___.___.___-__", reverse: true, selectOnFocus: true});
            $('.mask-cnpj').mask('00.000.000/0000-00', {placeholder: "__.___.___/____-__", reverse: true, selectOnFocus: true});
        }
        setMask();


    </script>
@endpush