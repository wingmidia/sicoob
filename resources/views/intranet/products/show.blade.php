@extends('intranet.layouts.master')

@section('title', $controllers->title)

@push('page.title')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="row">
                <div class="col-md-8">
                    <h1 class="page-header text-overflow"><i class="{{ $controllers->icon }}"></i> {{ $controllers->title }} | <small>{!! (!empty($item->id)) ? '<i class="fa fa-pencil"></i> Editando: ' . $item->title : '<i class="fa fa-plus-circle"></i> Adicionando novo' !!}</small></h1>
                </div>
                <div class="col-md-4 text-right">
                    <a href="{{ route($controllers->route) }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Voltar</a>
                    @if(!empty($item->id))
                        <a href="{{ route($controllers->actions->add->route) }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Adicionar</a>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endpush

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form action="{{ route($controllers->actions->store->route) }}" method="post">
                {{ csrf_field() }}
                @if(isset($item->id))
                    <input type="hidden" name="id" value="{{ old('id', $item->id) }}">
                @endif
                <div class="tab-base">

                    <!--Nav Tabs-->
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tabs-box-dados">Dados básicos</a></li>
                        <li><a data-toggle="tab" href="#tabs-box-campos">Campos Personalizados</a></li>
                    </ul>

                    <!--Tabs Content-->
                    <div class="tab-content">
                        <div id="tabs-box-dados" class="tab-pane fade in active">
                            <div class="row">
                                <div class="col-md-9">
                                    <fieldset>
                                        <legend>Insira os dados básicos</legend>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="control-label">Nome</label>
                                                    <input type="text" class="form-control" name="title" value="{{ old('title', $item->title) }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="control-label">Descrição</label>
                                                    <textarea name="content" class="form-control summernote">{{ old('content', $item->content) }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="col-md-2">
                                    <fieldset>
                                        <legend>Parâmetros</legend>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="control-label">Status</label>
                                                    <div class="clearfix"></div>
                                                    <input type="checkbox" class="form-control" name="status" value="1" data-toggle="toggle" data-on="Ativo" data-off="Inativo" data-size="small" data-onstyle="success" data-offstyle="danger" data-width="80" @if(old('status', $item->status)) checked @endif>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Indicação</label>
                                                    <div class="clearfix"></div>
                                                    <input type="checkbox" class="form-control" name="indication" value="1" data-toggle="toggle" data-on="Ativo" data-off="Inativo" data-size="small" data-onstyle="success" data-offstyle="danger" data-width="80" @if(old('status', $item->indication)) checked @endif>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Categoria</label>
                                                    <div class="clearfix"></div>
                                                    {!!Form::select('category_id',$categories, $item->category_id, ['class' => 'form-control'])!!}
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div id="tabs-box-campos" class="tab-pane fade in">
                            <div>
                                <button type="button" onclick="addField()" class="btn btn-mint"><i class="fa fa-plus"></i> Adicionar campo</button>
                                <br><br>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <fieldset>
                                        <legend>Campos adicionados</legend>

                                        <div id="box-product-fields">

                                            <div class="well text-center">
                                                <p>Nenhum campo adicionado!</p>
                                            </div>
                                        </div>

                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="tab-footer text-right">
                            <button class="btn btn-primary btn-lg" type="submit"><i class="fa fa-save"></i> Salvar</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>



@endsection

@push('styles')
    {{ Html::style('intranet/plugins/bootstrap-toggle/css/bootstrap-toggle.min.css') }}
    {{ Html::style('intranet/node_modules/summernote/dist/summernote.css') }}
@endpush

@push('scripts')

    {{ Html::script('intranet/plugins/bootstrap-toggle/js/bootstrap-toggle.min.js') }}
    {{ Html::script('intranet/node_modules/summernote/dist/summernote.min.js') }}
    {{ Html::script('intranet/node_modules/jsrender/jsrender.min.js') }}

    @verbatim
    <script id="tmpl-product-field" type="text/x-jsrender">
        <div>
            <div id="box-product-field-{{:index}}" class="well">
                {{if field}}
                <input type="hidden" name="fields[{{:index}}][id]" value="{{:field.id}}">
                {{/if}}
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Tipo</label>
                            <select class="form-control form-products-fields-type" name="fields[{{:index}}][type]" data-index="{{:index}}">
                                <option value="text_small" {{if field}}{{if field.type == 'text_small'}} selected {{/if}}{{/if}}>Texto curto</option>
                                <option value="text_larger" {{if field}}{{if field.type == 'text_larger'}} selected {{/if}}{{/if}}>Texto longo</option>
                                <option value="date">Data</option>
                                <!--<option value="datetime">Data e Hora</option>-->
                                <option value="select_single">Seleção única</option>
                                <!--<option value="select_multiple">Seleção múltipla</option>-->
                                <!--<option value="radio">Marcação única</option>-->
                                <!--<option value="checkbox">Marcação múltipla</option>-->
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Preenchimento</label>
                            <select class="form-control" name="fields[{{:index}}][required]">
                                <option value="0" {{if field}}{{if field.required == 0}} selected {{/if}}{{/if}}>Opcional</option>
                                <option value="1" {{if field}}{{if field.required == 1}} selected {{/if}}{{/if}}>Obrigatório</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group text-right">
                            <button type="button" class="btn btn-danger btn-xs" onclick="$('#box-product-field-{{:index}}').remove()"><i class="fa fa-trash"></i></button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Nome do campo</label>
                            <input type="text" class="form-control" name="fields[{{:index}}][name]" value="{{if field}}{{:field.name}}{{/if}}">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="" class="control-label">Descrição</label>
                            <input type="text" class="form-control" name="fields[{{:index}}][description]" value="{{if field}}{{:field.description}}{{/if}}">
                        </div>
                    </div>
                </div>
                <div id="btn-product-field-{{:index}}-add-option" style="display:none">
                    <button type="button" onclick="addFieldOption({{:index}})" class="btn btn-mint"><i class="fa fa-plus"></i> Adicionar opção</button>
                    <br><br>
                </div>
                <div id="box-product-field-{{:index}}-options"></div>
            </div>
        </div>
       </script>
    @endverbatim

    @verbatim
        <script id="tmpl-product-field-option" type="text/x-jsrender">
            <div id="box-product-field-{{:index}}-option-{{:index_option}}" class="">
                <hr>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Nome</label>
                            <input type="text" class="form-control" name="fields[{{:index}}][options][{{:index_option}}][value]" value="{{if value}}{{:value}}{{/if}}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="" class="control-label">Valor</label>
                            <input type="text" class="form-control" name="fields[{{:index}}][options][{{:index_option}}][key]" value="{{if key}}{{:key}}{{/if}}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group text-right">
                            <button type="button" class="btn btn-danger btn-xs" onclick="$('#box-product-field-{{:index}}-option-{{:index_option}}').remove()"><i class="fa fa-trash"></i></button>
                        </div>
                    </div>
                </div>
            </div>
       </script>
    @endverbatim

    <script>

        $('.summernote').summernote({
            height : '230px'
        });

        $(document).on('change', '.form-products-fields-type', function(){

            var index = $(this).data('index');

            if(this.value == 'select_single' || this.value == 'select_multiple' || this.value == 'radio' || this.value == 'checkbox'){

                $('#btn-product-field-'+index+'-add-option').show();

            } else {

                $('#btn-product-field-'+index+'-add-option').hide();

            }

        });

        var count_fields = 0;
        var count_fields_options = [];

        var fields = {!! $item->fields !!};

        $.each(fields, function (key, field) {

            if(count_fields == 0)
                $("#box-product-fields").empty();

            count_fields++;
            count_fields_options[count_fields] = 0;

            $("#box-product-fields").append(
                $.templates("#tmpl-product-field").render({
                    index: count_fields,
                    field: field
                })
            );

            if(field.options){

                $('#btn-product-field-'+count_fields+'-add-option').show();

                var options = JSON.parse(field.options);

                for ( var k in options){

                    count_fields_options[count_fields]++;

                    $("#box-product-field-"+count_fields+"-options").append(
                        $.templates("#tmpl-product-field-option").render({
                            index: count_fields,
                            index_option: count_fields_options[count_fields],
                            key: k,
                            value: options[k]
                        })
                    );

                }

            }

        });

        function addField(){

            if(count_fields == 0)
                $("#box-product-fields").empty();

            count_fields++;
            count_fields_options[count_fields] = 0;

            $("#box-product-fields").append(
                $.templates("#tmpl-product-field").render({
                    index: count_fields
                })
            );

        }

        function addFieldOption(index){

            count_fields_options[index]++;

            $("#box-product-field-"+index+"-options").append(
                $.templates("#tmpl-product-field-option").render({
                    index: count_fields,
                    index_option: count_fields_options[index]
                })
            );

        }

    </script>

@endpush