@extends('layouts.sicoob.default')

@section('content')
<style type="text/css">
    .ctn-login{
        margin-bottom: 20px;
    }
    .ctn-login > .form-horizontal {
        max-width: 600px;
        margin: 0 auto;
    }
</style>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="fundo-branco sombra-caixa ctn-login">
                <h1 class="titulo-janelas">
                  <i class="icomoon icon-cog-outline"></i>
                  <div>LOGIN</div>
                </h1>
                
                <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">E-mail</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">Senha</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control" name="password" required>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Lembrar de mim
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Entrar
                            </button>

                            <!--a class="btn btn-link" href="{{ route('password.request') }}">
                                Esqueceu sua senha?
                            </a-->
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
