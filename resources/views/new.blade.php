@extends('layouts.sicoob.default')

@section('arquivos_scripts')
<link  href="/js/fancybox/dist/jquery.fancybox.min.css" rel="stylesheet">
<script src="/js/fancybox/dist/jquery.fancybox.min.js"></script>
@endsection

@section('blocos_script')
  @if(session('message'))
    swal('Sucesso!', '{{{ session('message') }}}', 'success')
  @endif
@endsection

@section('content')
<div class="fundo-branco sombra-caixa ctn-noticias">
  <h1 class="titulo-janelas">
    <i class="icomoon icon-bullhorn"></i>
    <div>NOTÍCIAS</div>
  </h1>

  <div class="conteudo-interno borda-topo-cinza">
    <div class="row">
      <div class="col-sm-12">
        <!--img src="{{ route('imagecache', ['template' => 'medium', 'filename' => $new->image_feacture]) }}" alt="{{ $new->title }}" title="{{ $new->title }}" class="img-responsive" align="left" vspace="10" hspace="20" /-->

        <h2 class="titulo-interno">{{ $new->title }}</h2>

        <div class="info">
          {{ $new->start_date->format('d F Y') }},
          <span>Departamento:</span>
          {{join(', ', collect($new->news_departaments)->pluck('name')->toArray())}}
        </div>

        <div class="conteudo">
          {!! $new->content !!}         
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-8 col-lg-6">
    <div class="fundo-branco sombra-caixa ctn-noticias">
      <h1 class="titulo-janelas">
        <div>
          @if($new->comments->count()==0)
            AINDA SEM COMENTÁRIOS
          @elseif($new->comments->count()==1)
            {{ $new->comments()->where('status', 1)->count() }} COMENTÁRIO
          @else
            {{ $new->comments()->where('status', 1)->count() }} COMENTÁRIOS
          @endif
        </div>
      </h1>
      <div class="conteudo-interno borda-topo-cinza">
        <div class="row">
          <div class="col-sm-12">
            <div class="media-list comments">
              @each('layouts.sicoob.partials.comments', collect($comments['data'])->where('status', 1), 'comment')
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-sm-4 col-lg-6">
    <div class="fundo-branco sombra-caixa ctn-noticias">
      <h1 class="titulo-janelas">
        <div>
        @if($new->comments->count()==0)
          SEJA O PRIMEIRO A COMENTAR
        @else
          DEIXE SEU COMENTÁRIO
        @endif
        </div>
      </h1>
      <div class="conteudo">
      @if(!Auth::check())
      <p>Para comentar é necessário efetuar o login. <a href="{{ route('login') }}"><strong>Acessar conta agora!</strong></a></p>
      @else
      <form action="{{ route('news.comment') }}" method="POST" class="form-horizontal">
        {{ csrf_field() }}
        <input type="hidden" name="replay_id" value="{{ request()->get('replay_id', 0) }}" />
        <input type="hidden" name="collaborator_id" value="{{ Auth::user()->collaborator->id }}" />
        <input type="hidden" name="new_id" value="{{ $new->id }}" />
        <input type="hidden" name="slug" value="{{ $new->slug }}" />
        
        <div class="form-group {{ $errors->has('comments') ? 'has-error' : ''}}">
          <label for="comments" class="col-sm-2 control-label">Comentário</label>
          <div class="col-sm-10">
            <textarea class="form-control" 
                      name="comments" 
                      rows="5" 
                      placeholder="Digite aqui seu comentário"
                      {{{ request()->has('replay_id') ? 'autofocus' : null }}}></textarea>
            @if($errors->has('comments'))
            <p class="help-block">{{ $errors->first('comments') }}</p>
            @endif
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
          @if(request()->has('replay_id'))
            <button type="submit" class="btn btn-default">Enviar Resposta</button>
          @else 
          <button type="submit" class="btn btn-default">Enviar</button>
          @endif
          </div>
        </div>
      </form>
      @endif
      </div>
    </div>
  </div>
</div>

@if($new->photos->count() > 0)
  <div class="fundo-branco sombra-caixa ctn-noticias">
    <h1 class="titulo-janelas">
      <i class="icomoon icon-bullhorn"></i>
      <div>GALERIA DE FOTOS</div>
    </h1>
    <div class="conteudo-interno borda-topo-cinza">
      <div class="row">
        <div class="col-sm-12">
          <div class="conteudo">
          @foreach ($new->photos->chunk(4) as $chunk)
            <div class="row">
             @foreach ($chunk as $photo)
              <div class="col-sm-2">  
                <a href="{{ route('imagecache', ['template' => 'original', 'filename' => $photo->image]) }}" data-fancybox="gallery">            
                  <img src="{{ route('imagecache', ['template' => 'medium', 'filename' => $photo->image]) }}" 
                       alt="" />
                </a>
              </div>
              @endforeach
            </div>
          @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
@endif
@endsection