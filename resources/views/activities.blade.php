@extends('layouts.sicoob.default')

@section('arquivos_estilos')
<link rel="stylesheet" href="/css/jquery-ui.css">
@endsection

@section('arquivos_scripts')
<script src="/js/jquery.meio.mask.min.js"></script>
  <script src="/js/jquery-ui.js"></script>
@endsection

@section('blocos_script')

  $('.remove').on('click', function(){
    var id = $(this).data('id');
    swal({
      title: 'Tem certeza que deseja excluir?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#00a091',
      confirmButtonText: 'Sim!',
      cancelButtonText: 'Não'
    }).then(function () {
     $.ajax({
          url: '/atividades/'+id,
          type: 'DELETE',
          headers: {
                'X-CSRF-TOKEN' : Laravel.csrfToken
          },
          success: function(result) {
            swal(
              'Sucesso!',
              'Atividade removida com Sucesso!',
              'success'
            ).then(function(){
              window.location = "{{{ route('activity.index') }}}"
            })
          }
      });
    })
  })
  


// Calendario para os campos de data
      $( ".date-picker" ).datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
        dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
        monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez']
      });

      // Máscaras para campos dos formulários
      $('.fmtData').setMask('date');
@endsection

@section('content')
  <div class="fundo-branco sombra-caixa ctn-atividades">
    <div class="row">
      <div class="col-sm-6">
        <h1 class="titulo-janelas">
          <i class="icomoon icon-cog-outline"></i>
          <div>ATIVIDADES</div>
        </h1>
      </div>

      <div class="col-sm-6 text-right">
        <a href="atividade/cadastro" class="btn transicao">CADASTRAR NOVA ATIVIDADE</a>
      </div>
    </div>

    @include('partials.activity-search')

    <div class="lista-tabela">
    @if(!$activities->total())
    <div class="alert alert-success" 
         role="alert"
         style="margin-top: 20px;">
      <p>Nenhum registro encontrado.</p>
    </div>
    @else
      <table class="table table-hover borda-topo-cinza">
        <thead>
          <tr>
            <th>ID</th>
            <th>Data/Hora Início</th>
            <th>Data/Hora Fim</th>
            <th>Produto</th>
            <th>Cliente</th>
            <th colspan="2">Status</th>
          </tr>
        </thead>
        <tbody>
          @foreach($activities as $activity)
          <tr>
            <td>{{ $activity->id }}</td>
            <td>{{ $activity->start_date->format('d/m/Y H:i') }}</td>
            <td>{{ $activity->end_date->format('d/m/Y H:i') }}</td>
            <td>{{ $activity->product->title }}</td>
            <td>{{ $activity->client->name }}</td>
            <td><i class="fa fa-{{iconStatusActivity($activity->status)}}" aria-hidden="true"></i> {{ trans('core.'.$activity->status) }}</td>
            <td>
              <a href="{{ route('activity.details', ['id' => $activity->id]) }}" title="Visualizar atividade"><i class="fa fa-eye"></i></a>
              <a href="javascript: void(0)" 
                 class="remove" 
                 data-id="{{ $activity->id }}"
                 title="Excluir atividade"><i class="fa fa-close"></i></a>
            </td>
          </tr>
          @endforeach
        </tbody>
        <tfoot>
          <tr>
            <td colspan="7">
              {{ $activities->links('vendor.pagination.default') }}
            </td>
          </tr>
        </tfoot>
      </table>
    @endif
    </div>
  </div>
@endsection