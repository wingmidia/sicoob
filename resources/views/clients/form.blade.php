<form name="formCliente" id="formCliente">
	{{ csrf_field() }}
	<div class="row">
		<div class="col-sm-6">
			<h4>Dados de Contato</h4>
		  <div class="form-group">    
		    <label class="radio-inline">
				  <input type="radio" name="type_profile" value="J" checked> Pessoa Jurídica
				</label>

		    <label class="radio-inline">
				  <input type="radio" name="type_profile" value="F"> Pessoa Fisica
				</label>
		  </div>

		  <div class="form-group" id="cpf" style="display: none">
		    <label for="cpf">CPF</label>
		    <input type="text" class="form-control cpf" name="cpf" placeholder="CPF" maxlength="14" />
		  </div>

		  <div class="form-group" id="cnpj">
		    <label for="cnpj">CNPJ</label>
		    <input type="text" class="form-control cnpj" name="cnpj" placeholder="CNPJ" maxlength="18" />
		  </div>

			<div class="form-group" id="company_name">
		    <label for="company_name">Nome da Empresa</label>
		    <input type="text" class="form-control" name="company_name" />
		  </div>

		  <div class="form-group">
		    <label for="name">Nome de Contato</label>
		    <input type="text" class="form-control" name="name" />
		  </div>

			<div class="form-group">
		    <label for="email">E-mail</label>
		    <input type="text" class="form-control" name="email" />
		  </div>
			
			<div class="form-group">
		    <label for="cellphone">Celular</label>
		    <div class="row">
			    <div class="col-xs-12 col-sm-4">
						<input type="text" class="form-control celphones" name="cellphone" placeholder="Celular" />
			    </div>
			    <div class="col-xs-12 col-sm-4">
						<input type="text" class="form-control celphones" name="phone1" placeholder="Telefone 1" />
			    </div>
			    <div class="col-xs-12 col-sm-4">
						<input type="text" class="form-control celphones" name="phone2" placeholder="Telefone 2" />
			    </div>
		    </div>	    
		  </div>
		</div>
		<div class="col-sm-6">
			<h4>Dados de Endereço</h4>
			<div class="form-group">
		    <label for="postcode">CEP</label>
		    <input type="text" class="form-control postcode" name="postcode" maxlength="9" style="max-width: 150px" />
		  </div>

			<div class="form-group">
		    <label for="address">Endereço</label>
		    <div class="row">
			    <div class="col-sm-8">
			    	<input type="text" class="form-control pull-left" name="address" />
			    </div>
			    <div class="col-sm-4">
			    	<input type="text" class="form-control pull-right" name="number" placeholder="Número"/>
			    </div>
		    </div>
		  </div>

		  <div class="form-group">
		    <label for="district">Bairro</label>
		    <input type="text" class="form-control" name="district" />
		  </div>

		  <div class="form-group">
		    <label for="complement">Complemento</label>
		    <input type="text" class="form-control" name="complement" />
		  </div>

		  <div class="form-group">
		    <label for="state">Estado - Cidade</label>
		    <div class="row">
		    	<div class="col-sm-4">
				    <select name="state" class="form-control">
				    	<option value="">Estado</option>
							<option value="AC">Acre</option>
							<option value="AL">Alagoas</option>
							<option value="AP">Amapá</option>
							<option value="AM">Amazonas</option>
							<option value="BA">Bahia</option>
							<option value="CE">Ceará</option>
							<option value="DF">Distrito Federal</option>
							<option value="ES">Espírito Santo</option>
							<option value="GO">Goiás</option>
							<option value="MA">Maranhão</option>
							<option value="MT">Mato Grosso</option>
							<option value="MS">Mato Grosso do Sul</option>
							<option value="MG">Minas Gerais</option>
							<option value="PA">Pará</option>
							<option value="PB">Paraíba</option>
							<option value="PR">Paraná</option>
							<option value="PE">Pernambuco</option>
							<option value="PI">Piauí</option>
							<option value="RJ">Rio de Janeiro</option>
							<option value="RN">Rio Grande do Norte</option>
							<option value="RS">Rio Grande do Sul</option>
							<option value="RO">Rondônia</option>
							<option value="RR">Roraima</option>
							<option value="SC">Santa Catarina</option>
							<option value="SP">São Paulo</option>
							<option value="SE">Sergipe</option>
							<option value="TO">Tocantins</option>	
				    </select>
			    </div>
			    <div class="col-sm-8">
						<input type="text" class="form-control" name="city" placeholder="Cidade" />
			    </div>
		    </div>
		  </div>
		</div>
	</div>
</form>

<div class="modal-footer">
  <button type="button" class="btn btn-default" style="color: #FFF; background-color: #CCC; border-color: #ccc; margin-right: 5px;" data-dismiss="modal">Cancelar</button>
  <button type="button" class="btn btn-primary save">Salvar</button>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('[name="type_profile"]').on('change', function(){
			var val = $(this).val()
			if(val === 'J'){
				$('#cnpj').css('display', 'block');
				$('#company_name').css('display', 'block');
				$('#cpf').css('display', 'none');

				
				$('[name="cpf"]').val('');
			} else {
				$('#cnpj').css('display', 'none');
				$('#company_name').css('display', 'none');
				$('#cpf').css('display', 'block');

				$('[name="cnpj"]').val('');
			}
		});

		$('.save').on('click', function(){
			$.ajax({
			  url: "{{ route('clientes.store') }}",
			  method: "POST",
			  dataType: 'json',
			  data: $('#formCliente').serialize(),
			  success: function(res){
			  	if($('#modal').length){
			      var mySelect = $('#client_id');
			      @if(request()->has('list'))
			      	window.location = '{{{ route('clientes.index') }}}';
			      @else
							mySelect.append(
			              $('<option></option>').val(res.data.id).html(res.data.name)
			          );
				      mySelect.val(res.data.id);
						  
						  $('#modal').modal('hide');	
			      @endif
			      
			  	}
			  	
			  },
			  error: function(jqXHR, textStatus, errorThrown) {
			  	if(jqXHR.status === 422){
				  	var response = jqXHR.responseJSON;
				  	var el;
			    	$.each(response, function(index, item){
			    		el = $('input[name="'+index+'"]');
			    		el.parent().addClass('has-error');
			    		
			    		el.parent().find(".help-block").remove();

			    		$.each(item, function(i, message){
								el.parent().append('<p class="help-block">'+message+'</p>')
			    		})
			    	});	
			  	}
		    	
			  }
			});
		})
	})
</script>