<form class="form-inline frm-busca">
  <div class="row">
    <div class="col-md-4">
      <div class="form-group">
        <div class="input-group">
          <input type="input" size="100" name="search" id="buscar-atividade" class="form-control" value="{{ request()->get('search', null) }}" placeholder="Busca" />
          <div class="input-group-addon">
            <i class="fa fa-search"></i>
          </div>
        </div>
      </div>
    </div>

    <div class="col-sm-12 col-md-8">
      <div class="form-group">
        <label for="status">Tipo:</label>
        <select name="type_profile" class="form-control">
          <option value="">Selecione: </option>
          <option value="F" {{ request()->get('type_profile', null) == 'F' ? 'selected' : null  }}>Pessoa Física</option>
          <option value="J" {{ request()->get('type_profile', null) == 'J' ? 'selected' : null  }}>Pessoa Jurídica</option>
          
        </select>
      </div>
      @role(['diretoria', 'ti'])
      <div class="form-group">
        <label for="status">Colaborador:</label>
        <select name="collaborator_id" class="form-control">
          <option value="">Selecione: </option>
          @foreach(getCollaborators() as $collaborator)
          <option value="{{$collaborator->id}}" {{ request()->get('collaborator_id', 0) == $collaborator->id ? 'selected' : null  }}>{{$collaborator->name}}</option>
          @endforeach
        </select>
      </div>
      @endrole

      <div class="form-group">
        <button class="btn btn-buscar transicao">BUSCAR</button>
      </div>
    </div>
  </div>
</form>