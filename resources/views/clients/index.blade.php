@extends('layouts.sicoob.default')

@section('arquivos_estilos')
<link rel="stylesheet" href="/css/jquery-ui.css">
@endsection

@section('arquivos_scripts')
<script src="/js/jquery.meio.mask.min.js"></script>
  <script src="/js/jquery-ui.js"></script>
@endsection

@section('blocos_script')

  $('.remove').on('click', function(){
    var id = $(this).data('id');

    swal({
      title: 'Tem certeza que deseja excluir?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#00a091',
      confirmButtonText: 'Sim!',
      cancelButtonText: 'Não'
    }).then(function () {
     $.ajax({
          url: '/clientes/'+id,
          type: 'DELETE',
          headers: {
                'X-CSRF-TOKEN' : Laravel.csrfToken
          },
          success: function(result) {
            swal(
              'Sucesso!',
              'Atividade removida com Sucesso!',
              'success'
            ).then(function(){
              window.location = "{{{ route('clientes.index') }}}"
            })
          }
      });
    })
  })
  
@endsection

@section('content')
  <div class="fundo-branco sombra-caixa ctn-atividades">
    <div class="row">
      <div class="col-sm-6">
        <h1 class="titulo-janelas">
          <i class="icomoon icon-cog-outline"></i>
          <div>CLIENTES</div>
        </h1>
      </div>

      <div class="col-sm-6 text-right">
        <a href="javascript:void(0)" 
           onclick="SximoModal('{{{ route('clientes.create', ['list' => true] ) }}}', 'Cadastro de Clientes')" 
           title="Adicionar Cliente"
           class="btn transicao">
        NOVO CLIENTE</a>
      </div>
    </div>

    @include('clients.search')

    <div class="lista-tabela">
    @if(!$clients->total())
    <div class="alert alert-success" 
         role="alert"
         style="margin-top: 20px;">
      <p>Nenhum registro encontrado.</p>
    </div>
    @else
      <table class="table table-hover borda-topo-cinza">
        <thead>
          <tr>
            <th>ID</th>
            <th>Nome</th>
            <th>Telefone</th>
            <th>E-mail</th>
            <th colspan="2">Ações</th>
          </tr>
        </thead>
        <tbody>
          @foreach($clients as $client)
          <tr>
            <td>{{ $client->id }}</td>
            <td>{{ $client->name }}</td>
            <td>{{ $client->cellphone }}</td>
            <td>{{ $client->email }}</td>
            <td>
              <a href="javascript:void(0)" 
                 onclick="SximoModal('{{{ route('clientes.edit', ['id' => $client->id] ) }}}', 'Editar de Clientes')" 
                 title="Editar"><i class="fa fa-edit"></i></a>
              <a href="javascript: void(0)" 
                 class="remove" 
                 data-id="{{ $client->id }}"
                 title="Excluir atividade"><i class="fa fa-close"></i></a>
            </td>
          </tr>
          @endforeach
        </tbody>
        <tfoot>
          <tr>
            <td colspan="7">
              {{ $clients->links('vendor.pagination.default') }}
            </td>
          </tr>
        </tfoot>
      </table>
    @endif
    </div>
  </div>

@endsection