<form name="formCliente" id="formCliente">
	{{ csrf_field() }}
	<div class="row">
		<div class="col-sm-6">
			<h4>Dados de Contato</h4>
		  <div class="form-group">    
		    <label class="radio-inline">
				  <input type="radio" name="type_profile" value="J" {{ $client->type_profile == 'J' ? 'checked' : null  }}> Pessoa Jurídica
				</label>

		    <label class="radio-inline">
				  <input type="radio" name="type_profile" value="F" {{ $client->type_profile == 'F' ? 'checked' : null  }}> Pessoa Fisica
				</label>
		  </div>

		  <div class="form-group" id="cpf" style="display: none">
		    <label for="cpf">CPF</label>
		    <input type="text" class="form-control cpf" name="cpf" placeholder="CPF" value="{{$client->cpf}}" maxlength="14" />
		  </div>

		  <div class="form-group" id="cnpj">
		    <label for="cnpj">CNPJ</label>
		    <input type="text" class="form-control cnpj" name="cnpj" placeholder="CNPJ" value="{{$client->cnpj}}" maxlength="18" />
		  </div>

			<div class="form-group" id="company_name">
		    <label for="company_name">Nome da Empresa</label>
		    <input type="text" class="form-control" name="company_name" value="{{$client->company_name}}" />
		  </div>

		  <div class="form-group">
		    <label for="name">Nome de Contato</label>
		    <input type="text" class="form-control" name="name" value="{{$client->name}}" />
		  </div>

			<div class="form-group">
		    <label for="email">E-mail</label>
		    <input type="text" class="form-control" name="email" value="{{$client->email}}" />
		  </div>
			
			<div class="form-group">
		    <label for="cellphone">Celular</label>
		    <div class="row">
			    <div class="col-xs-12 col-sm-4">
						<input type="text" class="form-control celphones" name="cellphone" value="{{$client->cellphone}}" placeholder="Celular" />
			    </div>
			    <div class="col-xs-12 col-sm-4">
						<input type="text" class="form-control celphones" name="phone1" value="{{$client->phone1}}" placeholder="Telefone 1" />
			    </div>
			    <div class="col-xs-12 col-sm-4">
						<input type="text" class="form-control celphones" name="phone2" value="{{$client->phone2}}" placeholder="Telefone 2" />
			    </div>
		    </div>	    
		  </div>
		</div>
		<div class="col-sm-6">
			<h4>Dados de Endereço</h4>
			<div class="form-group">
		    <label for="postcode">CEP</label>
		    <input type="text" class="form-control postcode" name="postcode" value="{{$client->postcode}}" maxlength="9" style="max-width: 150px" />
		  </div>

			<div class="form-group">
		    <label for="address">Endereço</label>
		    <div class="row">
			    <div class="col-sm-8">
			    	<input type="text" class="form-control pull-left" value="{{$client->address}}" name="address" />
			    </div>
			    <div class="col-sm-4">
			    	<input type="text" class="form-control pull-right" name="number" value="{{$client->number}}" placeholder="Número"/>
			    </div>
		    </div>
		  </div>

		  <div class="form-group">
		    <label for="district">Bairro</label>
		    <input type="text" class="form-control" name="district" value="{{$client->district}}" />
		  </div>

		  <div class="form-group">
		    <label for="complement">Complemento</label>
		    <input type="text" class="form-control" name="complement" value="{{$client->district}}" />
		  </div>

		  <div class="form-group">
		    <label for="state">Estado - Cidade</label>
		    <div class="row">
		    	<div class="col-sm-4">
				    <select name="state" class="form-control">
				    	<option value="">Estado</option>
							<option value="AC" {{ $client->state == 'AC' ? 'selected' : null  }}>Acre</option>
							<option value="AL" {{ $client->state == 'AL' ? 'selected' : null  }}>Alagoas</option>
							<option value="AP" {{ $client->state == 'AP' ? 'selected' : null  }}>Amapá</option>
							<option value="AM" {{ $client->state == 'AM' ? 'selected' : null  }}>Amazonas</option>
							<option value="BA" {{ $client->state == 'BA' ? 'selected' : null  }}>Bahia</option>
							<option value="CE" {{ $client->state == 'CE' ? 'selected' : null  }}>Ceará</option>
							<option value="DF" {{ $client->state == 'DF' ? 'selected' : null  }}>Distrito Federal</option>
							<option value="ES" {{ $client->state == 'ES' ? 'selected' : null  }}>Espírito Santo</option>
							<option value="GO" {{ $client->state == 'GO' ? 'selected' : null  }}>Goiás</option>
							<option value="MA" {{ $client->state == 'MA' ? 'selected' : null  }}>Maranhão</option>
							<option value="MT" {{ $client->state == 'MT' ? 'selected' : null  }}>Mato Grosso</option>
							<option value="MS" {{ $client->state == 'MS' ? 'selected' : null  }}>Mato Grosso do Sul</option>
							<option value="MG" {{ $client->state == 'MG' ? 'selected' : null  }}>Minas Gerais</option>
							<option value="PA" {{ $client->state == 'PA' ? 'selected' : null  }}>Pará</option>
							<option value="PB" {{ $client->state == 'PB' ? 'selected' : null  }}>Paraíba</option>
							<option value="PR" {{ $client->state == 'PR' ? 'selected' : null  }}>Paraná</option>
							<option value="PE" {{ $client->state == 'PE' ? 'selected' : null  }}>Pernambuco</option>
							<option value="PI" {{ $client->state == 'PI' ? 'selected' : null  }}>Piauí</option>
							<option value="RJ" {{ $client->state == 'RJ' ? 'selected' : null  }}>Rio de Janeiro</option>
							<option value="RN" {{ $client->state == 'RN' ? 'selected' : null  }}>Rio Grande do Norte</option>
							<option value="RS" {{ $client->state == 'RS' ? 'selected' : null  }}>Rio Grande do Sul</option>
							<option value="RO" {{ $client->state == 'RO' ? 'selected' : null  }}>Rondônia</option>
							<option value="RR" {{ $client->state == 'RR' ? 'selected' : null  }}>Roraima</option>
							<option value="SC" {{ $client->state == 'SC' ? 'selected' : null  }}>Santa Catarina</option>
							<option value="SP" {{ $client->state == 'SP' ? 'selected' : null  }}>São Paulo</option>
							<option value="SE" {{ $client->state == 'SE' ? 'selected' : null  }}>Sergipe</option>
							<option value="TO" {{ $client->state == 'TO' ? 'selected' : null  }}>Tocantins</option>	
				    </select>
			    </div>
			    <div class="col-sm-8">
						<input type="text" class="form-control" name="city" placeholder="Cidade" value="{{$client->city}}" />
			    </div>
		    </div>
		  </div>
		</div>
	</div>
</form>

<div class="modal-footer">
  <button type="button" class="btn btn-default" style="color: #FFF; background-color: #CCC; border-color: #ccc; margin-right: 5px;" data-dismiss="modal">Cancelar</button>
  <button type="button" class="btn btn-primary save">Salvar</button>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('[name="type_profile"]').on('change', function(){
			var val = $(this).val()
			if(val === 'J'){
				$('#cnpj').css('display', 'block');
				$('#company_name').css('display', 'block');
				$('#cpf').css('display', 'none');

				
				$('[name="cpf"]').val('');
			} else {
				$('#cnpj').css('display', 'none');
				$('#company_name').css('display', 'none');
				$('#cpf').css('display', 'block');

				$('[name="cnpj"]').val('');
			}
		});

		$('.save').on('click', function(){
			$.ajax({
			  url: "{{ route('clientes.update', ['id' => $client->id]) }}",
			  method: "PUT",
			  dataType: 'json',
			  data: $('#formCliente').serialize(),
			  success: function(res){
			  	if($('#modal').length){
			      var mySelect = $('#client_id');
			    
			     	window.location = '{{{ route('clientes.index') }}}';

			      
			  	}
			  	
			  },
			  error: function(jqXHR, textStatus, errorThrown) {
			  	if(jqXHR.status === 422){
				  	var response = jqXHR.responseJSON;
				  	var el;
			    	$.each(response, function(index, item){
			    		el = $('input[name="'+index+'"]');
			    		el.parent().addClass('has-error');
			    		
			    		el.parent().find(".help-block").remove();

			    		$.each(item, function(i, message){
								el.parent().append('<p class="help-block">'+message+'</p>')
			    		})
			    	});	
			  	}
		    	
			  }
			});
		})
	})
</script>