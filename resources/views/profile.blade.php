@extends('layouts.sicoob.default')

@section('content')
<div class="row" style="margin-bottom: 30px">
  <div class="col-md-6 col-md-offset-3">
		<div class="fundo-branco sombra-caixa ctn-contra-cheque">
		  <h1 class="titulo-janelas">
		    <i class="icomoon icon-cog-outline"></i>
		    <div>ALTERAR SENHA</div>
		  </h1>

			<div class="lista-links-importantes">
				<div class="row">
				  <div class="col-sm-12">
					<form action="{{ route('profile_post') }}" method="POST" class="form-horizontal" >
						{{ csrf_field() }}
						<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
						  <label for="password" class="col-md-3 control-label">Senha</label>

						  <div class="col-md-6">
						      <input  type="password" id="password" class="form-control" name="password" value="" required autofocus>

						      @if ($errors->has('password'))
						          <span class="help-block">
						              <strong>{{ $errors->first('password') }}</strong>
						          </span>
						      @endif
						  </div>
						</div>
						<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
						  <label for="password_confirmation" class="col-md-3 control-label">Confirmar Senha</label>

						  <div class="col-md-6">
						      <input  type="password" id="password_confirmation" class="form-control" name="password_confirmation" value="" required>

						      @if ($errors->has('password_confirmation'))
						          <span class="help-block">
						              <strong>{{ $errors->first('password_confirmation') }}</strong>
						          </span>
						      @endif
						  </div>
						</div>
						<div class="form-group">
						    <div class="col-md-8 col-md-offset-3">
						        <button type="submit" class="btn btn-primary">
						            Alterar Senha
						        </button>
						    </div>
						</div>
					</form>
				  </div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('vuejs')
@if(session('success'))
	<script type="text/javascript">
		swal(
		  'Sucesso!',
		  '{{{ session('success') }}}',
		  'success'
		).then(function(){
		  //window.location = "{{{ route('activity.index') }}}"
		})
	</script>
@endif
@endsection