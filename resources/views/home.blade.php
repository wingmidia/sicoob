@extends('layouts.sicoob.default')

@section('arquivos_scripts')
    <script src="/js/postscribe/postscribe.min.js"></script>
    <style type="text/css">
        .widgets_view {
            display: none;
        }
    </style>
@endsection

@section('content')

    <div class="content">

        <div class="row">

            <div class="col-md-8">

                <div class="row">

                    <div class="col-md-12">

                        @if($notification)
                            <div class="panel panel-default">

                                <div class="media">
                                    <div class="media-left media-top">
                                        <img src="/img/img-avisos.png" alt="Avisos" title="Avisos" class="img-aviso"/>
                                    </div>
                                    <div class="media-body">
                                        <br>
                                        <p>{{$notification->message}}</p>
                                    </div>
                                </div>

                            </div>
                        @endif

                        @if($banners)
                            <div class="panel panel-default">

                                <div class="panel-body">
                                    <div id="carousel-banner" class="carousel slide" data-ride="carousel">
                                        <!-- Imagem do banner -->
                                        <div class="carousel-inner" role="listbox">
                                            @foreach($banners as $k => $banner)
                                                <div class="item {{$k==0?'active':null }}">
                                                    @if($banner->url)
                                                        <a href="{{$banner->url}}">
                                                            @endif
                                                            <img src="{{route('imagecache', ['template' => 'original' ,'filename' => $banner->image])}}"
                                                                 alt="{{$banner->title}}"
                                                                 title="{{$banner->title}}"
                                                                 class="img-rounded img-responsive"/>
                                                            @if($banner->url)
                                                        </a>
                                                    @endif
                                                </div>
                                            @endforeach
                                        </div>

                                        <!-- Controles -->
                                        <a class="left carousel-control" href="#carousel-banner" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                            <span class="sr-only">Anterior</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-banner" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Próximo</span>
                                        </a>
                                    </div>
                                </div>

                            </div>
                        @endif

                    </div>

                </div>

                <div class="row">

                    <div class="col-md-7 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <i class="icomoon icon-bullhorn"></i> Últimas notícias
                            </div>
                            <div class="panel-body">
                                @foreach($news as $new)
                                    <div class="media">
                                        <div class="media-left media-top">
                                            <a href="{{ route('new.details', ['slug' => $new->slug]) }}">
                                                <img class="media-object img-rounded" src="{{ route('imagecache', ['template' => 'small', 'filename' => $new->image_feacture]) }}" alt="Foto {{ $new->title }}" title="{{ $new->title }}"/>
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading">
                                                <a href="{{ route('new.details', ['slug' => $new->slug]) }}">
                                                    {{$new->title}}
                                                </a>
                                            </h4>
                                            <div class="clearfix"></div>
                                            <a href="{{ route('new.details', ['slug' => $new->slug]) }}" class="btn btn-link btn-sm">Continuar lendo</a>
                                        </div>
                                    </div>
                                    <hr>
                                @endforeach

                                <a href="{{ route('news') }}" class="btn btn-link btn-block">
                                    Todas as notícias
                                </a>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-5 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Cotações
                            </div>
                            <div class="panel-body">
                                <select name="widgets" id="widgets" class="form-control">
                                    <option value="0">Selecione a cotação</option>
                                    <?php foreach ($widgets as $widget) { ?>
                                    <option value="{{$widget->id}}" @if($widget->id == 4) selected="selected" @endif>{{$widget->name}}</option>
                                    <?php } ?>
                                </select>
                                <br>
                                <div class="widgets_view" id="widgets_view">

                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                {{-- @permission('campaign-create') --}}
                @if(isset($campaign))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                        <i class="fa fa-trophy"></i>Ranking
                                    </h3>
                                </div>
                                <div class="panel-body">
                                    <fieldset>
                                        <legend>{{$campaign->name}}</legend>
                                        @if($exibition)
                                            <div class="tab-base">
                                                <ul class="nav nav-tabs">
                                                    <li class="active">
                                                        <a data-toggle="tab" href="#tab-ranking-production">Vendas</a>
                                                    </li>
                                                    <li>
                                                        <a data-toggle="tab" href="#tab-ranking-indication">Indicações</a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div id="tab-ranking-production" class="tab-pane fade in active">
    
                                                        <br>
    
                                                        @foreach($campaign->categories as $product)
    
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <h4><b>{{$product->name}}</b></h4>
                                                                </div>
                                                            </div>
    
                                                            @if(isset($production_ranking[$product->id]))
                                                                @foreach($production_ranking[$product->id] as $key=>$userRanking)
                                                                    <div class="media">
                                                                        <div class="media-left">
                                                                            <img src="{{route('imagecache', ['template' => 'profile', 'filename' => $userRanking->photo])}}"
                                                                                 alt="{{$userRanking->name}}">
                                                                        </div>
                                                                        <div class="media-body">
                                                                            <h4 class="media-heading">{{$key+1}}º {{$userRanking->name}}</h4>
                                                                            <h5 class="text-success"> {{$userRanking->sales}} vendas</h5>
                                                                            <h5 class="text-success"> Total: R$ {{number_format($userRanking->total, 2, ',', '.')}}</h5>
                                                                        </div>
                                                                    </div>
                                                                    @if($key>9)
                                                                        @break
                                                                    @endif
                                                                @endforeach
                                                            @else
                                                                <h4>Nenhuma venda encontrada</h4>
                                                            @endif
    
                                                            <hr>
    
                                                        @endforeach
    
                                                    </div>
                                                    <div id="tab-ranking-indication" class="tab-pane fade in">
    
                                                        <br>
    
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                @if(!empty($indication_ranking))
                                                                    @foreach($indication_ranking as $key=>$userRanking)
                                                                        <div class="media">
                                                                            <div class="media-left">
                                                                                <img src="{{route('imagecache', ['template' => 'profile', 'filename' => $userRanking->photo])}}"
                                                                                     alt="{{$userRanking->name}}">
                                                                            </div>
                                                                            <div class="media-body">
                                                                                <h4 class="media-heading">{{$key+1}}º  {{$userRanking->name}}</h4>
                                                                                <h5 class="text-success">{{$userRanking->indications}} indicações</h5>
                                                                            </div>
                                                                        </div>
                                                                    @endforeach
                                                                @else
                                                                    <h4>Nenhuma indicação encontrada</h4>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @else
                                            <h1>Rankeamentos Ocultados Para Apuração!</h1>
                                        @endif
                                    </fieldset>
                                </div>
                                <div class="panel-footer">
                                    <a href="{{route('intranet.campaign.ranking')}}" class="btn btn-link">VISUALIZAR TODOS</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                {{-- @endpermission --}}
            </div>

            <div class="col-md-4">

                @if($events->count() > 1)
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="icomoon icon-calendar"></i> Eventos
                        </div>
                        <div class="panel-body">

                            @foreach($events as $event)
                                <div class="media">
                                    <div class="media-left media-middle">
                                        <h3 class="panel-custom1">
                                            <a href="{{ route('event.details', ['slug' => $event->slug]) }}">
                                                {{ $event->event_date->format('d/m') }}
                                            </a>
                                        </h3>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">
                                            <a href="{{ route('event.details', ['slug' => $event->slug]) }}">
                                                {{ str_limit(strip_tags($event->title), 100) }}
                                            </a>
                                        </h4>
                                        <div class="clearfix"></div>
                                        <a href="{{ route('event.details', ['slug' => $event->slug]) }}" class="btn btn-link btn-xs">Visualizar evento</a>
                                    </div>
                                </div>
                            @endforeach
                            <hr>
                            <a href="{{ route('events') }}" class="btn btn-link btn-block">
                                Todos os eventos
                            </a>
                        </div>
                    </div>
                @endif

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="icomoon icon-messages"></i> Fórum
                    </div>
                    <div class="panel-body">
                        @if($threads->count() > 1)
                            @foreach($threads as $thread)
                                <div class="media">
                                    <div class="media-left media-middle">
                                        <a href="{{Forum::route('thread.show', $thread)}}">
                                            @if(!$thread->author)
                                                <img src="{{ route('imagecache', ['template' => 'profile', 'filename' => 'profile-noimage.jpg']) }}"
                                                     alt="Administrador"
                                                     title="Administrador"
                                                     class="media-object img-rounded"/>
                                            @else

                                                @if($thread->author->collaborator)
                                                    <img src="{{ route('imagecache', ['template' => 'profile', 'filename' => $thread->author->collaborator->photo]) }}"
                                                         alt="{{$thread->author->collaborator->name}}"
                                                         title="{{$thread->author->collaborator->name}}"
                                                         class="media-object img-rounded"/>
                                                @else
                                                    <img src="{{ route('imagecache', ['template' => 'profile', 'filename' => 'profile-noimage.jpg']) }}"
                                                         alt="{{$thread->author->collaborator->name}}"
                                                         title="{{$thread->author->collaborator->name}}"
                                                         class="media-object img-rounded"/>
                                                @endif
                                            @endif
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <a href="{{Forum::route('thread.show', $thread)}}">
                                            <div class="media-heading">{{$thread->author ? $thread->author->collaborator->name : 'Administrador'}}</div>
                                            {{$thread->title}}
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                            <hr>
                            <a href="{{ Forum::route('forum.index') }}" class="btn btn-link btn-block">
                                Visualizar mais
                            </a>
                        @else
                            <p class="text-center text-muted">
                                Nenhuma discussão aberta no momento. Faça login e participe =D
                            </p>
                        @endif
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="icomoon icon-gift"></i> Aniversariantes
                    </div>
                    <div class="panel-body">
                        @if($birth_month->count() > 1)
                            @foreach($birth_month as $birth)
                                <div class="media">
                                    <div class="media-left media-middle">
                                        <img src="{{ route('imagecache', ['template' => 'profile', 'filename' => $birth->photo]) }}"
                                             alt="{{$birth->name}}"
                                             title="{{$birth->name}}"
                                             class="img-object img-circle"/>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">
                                            {{ $birth->name }}
                                        </h4>
                                        @if($birth->date_birth->isBirthday() || $birth->date_birth->copy()->subDay(1)->isBirthDay())
                                            <a href="mailto:{{$birth->email}}?Subject=Parabéns!!" class="btn btn-primary btn-xs" title="Parabéns!!" target="_blank">
                                                Desejar Parabéns!!
                                            </a>
                                        @endif
                                    </div>
                                    <div class="media-right">
                                        <strong>{{$birth->date_birth->format('d/m')}}</strong>
                                    </div>
                                </div>
                            @endforeach
                            <hr>
                            <a href="{{ route('births.index') }}" class="btn btn-link btn-block">
                                Visualizar todos
                            </a>
                        @else
                            <p class="text-center text-muted">
                                Nenhum aniversariante este mês =/
                            </p>
                        @endif
                    </div>
                </div>

            </div>

        </div>

    </div>

@endsection

@section('vuejs')
    <script type="text/javascript">
        $('#widgets').on('change', function () {
            var id = $(this).val();

            $.ajax({
                url: "{{ url()->current() }}/widgets/" + id,
                method: 'GET',
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': Laravel.csrfToken
                },
                success: function (response) {
                    var data = response.data

                    if (data.widget) {
                        var widget = $.parseHTML(data.widget);

                        $('.widgets_view').css('display', 'block');
                        $('#widgets_view').html('');
                        postscribe('#widgets_view', data.widget);

                        $('.widgets.alert').css('display', 'none');
                    } else {
                        $('.widgets_view').css('display', 'none');
                        $('#widgets_view').html('');
                        $('.widgets.alert').css('display', 'block');
                    }
                }
            })
        });
        $('#widgets').trigger('change');
    </script>

@endsection