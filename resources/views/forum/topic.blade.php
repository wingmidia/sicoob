@extends('layouts.sicoob.default')

@section('content')

  <div class="row flexbox">
    <div class="col-lg-12">
      <div class="fundo-branco sombra-caixa ctn-forum">
        <div class="row">
          <div class="col-sm-6">
            <h1 class="titulo-janelas">
              <i class="icomoon icon-messages"></i>
              <div>{{ mb_strtoupper($topic->title) }}</div>
            </h1>
          </div>
        </div>

        <form class="form frm-busca">
          <div class="row">
            <div class="col-sm-7">
              <div class="form-group">
                <div class="input-group">
                  <input type="search" name="buscar-contra-cheque" id="buscar-contra-cheque" class="form-control" placeholder="Busca" />
                  <div class="input-group-addon">
                    <i class="fa fa-search"></i>
                  </div>
                </div>
              </div>
            </div>

            <div class="form-group">
              <button class="btn btn-buscar transicao">BUSCAR</button>
            </div>
          </div>
        </form>

        <div class="lista borda-topo-cinza">
          <table class="table table-bordered table-forum">
            <caption>POSTAGENS</caption>
            <thead>
              <tr>
                <th>TÍTULO</th>
                <th>STATUS</th>
                <th>DATA DA POSTAGEM</th>
              </tr>
            </thead>
            <tbody>
            @foreach($topic->posts as $post)
              <tr>
                <td>
                  <div class="item-topico">
                    <div class="dados-topico">
                      <div class="titulo-topico"><a href="{{ route('forum.post', ['slug' => $post->slug]) }}">{{$post->title}}</a></div>
                      <div class="resumo-topico"><a href="{{ route('forum.post', ['slug' => $post->slug]) }}">{{ str_limit(strip_tags($post->content))}}...</a></div>
                    </div>
                  </div>
                </td>
                <td>
                  <div class="status-topico">
                    {{$post->total_comments}} Comentários
                  </div>
                </td>
                <td>                
                  <div class="titulo-ultima-postagem">{{ $post->created_at->format('d/m/Y H:i') }}</div>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection