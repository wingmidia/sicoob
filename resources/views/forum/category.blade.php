@extends('layouts.sicoob.default')

@section('content')
  <div class="row flexbox">
    <div class="col-lg-12">
      <div class="fundo-branco sombra-caixa ctn-forum">
        <div class="row">
          <div class="col-sm-6">
            <h1 class="titulo-janelas">
              <i class="icomoon icon-messages"></i>
              <div>{{ mb_strtoupper($category->name) }}</div>
            </h1>
          </div>

          <div class="col-sm-6 text-right">
            <a href="{{ route('forum.create') }}" class="btn transicao">NOVA POSTAGEM</a>
          </div>
        </div>

        <form class="form frm-busca">
          <div class="row">
            <div class="col-sm-7">
              <div class="form-group">
                <div class="input-group">
                  <input type="search" name="buscar-contra-cheque" id="buscar-contra-cheque" class="form-control" placeholder="Busca" />
                  <div class="input-group-addon">
                    <i class="fa fa-search"></i>
                  </div>
                </div>
              </div>
            </div>

            <div class="form-group">
              <button class="btn btn-buscar transicao">BUSCAR</button>
            </div>
          </div>
        </form>

        <div class="lista borda-topo-cinza">
          <table class="table table-bordered table-forum">
            <caption>TÓPICOS</caption>
            <thead>
              <tr>
                <th>TÍTULO</th>
                <th>STATUS</th>
                <th>ÚLTIMA POSTAGEM</th>
              </tr>
            </thead>
            <tbody>
            @foreach($category->topics as $topic)
              <tr>
                <td>
                  <div class="item-topico">
                    <div class="dados-topico">
                      <div class="titulo-topico"><a href="{{ route('forum.topic', ['slug' => $topic->slug]) }}">{{$topic->title}}</a></div>
                      <div class="resumo-topico"><a href="{{ route('forum.topic', ['slug' => $topic->slug]) }}">{{ str_limit(strip_tags($topic->content))}}...</a></div>
                    </div>
                  </div>
                </td>
                <td>
                  <div class="status-topico">
                    {{$topic->total_posts}} Postagens
                  </div>
                </td>
                <td>
                @if($topic->last_post)
                  <div class="titulo-ultima-postagem">{{ $topic->last_post->title }}</div>
                  <div class="autor-postagem">por: <span>{{ $topic->last_post->collaborator ? $topic->last_post->collaborator->name : 'Administrador' }}</span></div>
                  <div class="data-postagem">{{$topic->last_post->created_at->format('d/m/Y H:i')}}</div>
                @else
                  <p>Nenhum postagem até o momento.</p>
                @endif
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection