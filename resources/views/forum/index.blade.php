@extends('layouts.sicoob.default')

@section('content')
  <div class="row flexbox">
    <div class="col-lg-12">
      <div class="fundo-branco sombra-caixa ctn-forum">
        <div class="row">
          <div class="col-sm-6">
            <h1 class="titulo-janelas">
              <i class="icomoon icon-messages"></i>
              <div>FÓRUM</div>
            </h1>
          </div>

          <div class="col-sm-6 text-right">
            <a href="{{ route('forum.create') }}" class="btn transicao">NOVA POSTAGEM</a>
          </div>
        </div>

        <form class="form frm-busca">
          <div class="row">
            <div class="col-sm-7">
              <div class="form-group">
                <div class="input-group">
                  <input type="search" name="buscar-contra-cheque" id="buscar-contra-cheque" class="form-control" placeholder="Busca" />
                  <div class="input-group-addon">
                    <i class="fa fa-search"></i>
                  </div>
                </div>
              </div>
            </div>

            <div class="form-group">
              <button class="btn btn-buscar transicao">BUSCAR</button>
            </div>
          </div>
        </form>

        <div class="lista borda-topo-cinza">
          <table class="table table-bordered table-forum">
            <caption>CATEGORIAS</caption>
            <thead>
              <tr>
                <th>TÍTULO</th>
                <th>STATUS</th>
                <th>ÚLTIMA POSTAGEM</th>
              </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)
              <tr>
                <td>
                  <div class="item-topico">
                    <div class="img-topico">
                      <a href="{{ route('forum.category', ['slug' => $category->slug]) }}"><img src="{{ route('imagecache', ['filename' => $category->icon, 'template' => 'icons']) }}" alt="Financeiro" title="Financeiro" class="img-responsive img-circle" /></a>
                    </div>
                    <div class="dados-topico">
                      <div class="titulo-topico"><a href="{{ route('forum.category', ['slug' => $category->slug]) }}">{{$category->name}}</a></div>
                      <div class="resumo-topico"><a href="{{ route('forum.category', ['slug' => $category->slug]) }}">{{$category->description}}</a></div>
                    </div>
                  </div>
                </td>
                <td>
                  <div class="status-topico">
                    <a href="#">{{$category->topics()->where('status', 'approved')->count()}} Tópicos</a><br />
                    {{$category->posts()->where('status', 'approved')->count()}} Postagens</a>
                  </div>
                </td>
                <td>
                  <div class="titulo-ultima-postagem">{{ $category->last_post->title }}</div>
                  <div class="autor-postagem">por: <span>{{ $category->last_post->collaborator ? $category->last_post->collaborator->name : 'Administrador' }}</span></div>
                  <div class="data-postagem">{{$category->last_post->created_at->format('d/m/Y H:i')}}</div>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection