@extends('layouts.sicoob.default')

@section('content')
  <div class="row flexbox">
    <div class="col-xs-8">
      <div class="fundo-branco sombra-caixa ctn-forum">
        <div class="row">
          <div class="col-sm-6">
            <h1 class="titulo-janelas">
              <i class="icomoon icon-messages"></i>
              <div>NOVA POSTAGEM</div>
            </h1>
          </div>
        </div>
        <div class="lista borda-topo-cinza">
          <div class="row">
            <div class="col-sm-12">
              <form action="{{ route('forum.store') }}" method="POST" class="form">
                {{ csrf_field() }}
                {{request()->flash()}}
                <input type="hidden" name="type" value="post" />
                <input type="hidden" name="status" value="waiting_approve" />
                
                <div class="row">
                  <div class="col-sm-3">
                    <div class="form-group {{ $errors->has('category_id') ? 'has-error' : null }}" style="max-width: 200px">
                      <label for="category_id">Categoria</label>
                      <select class="form-control" name="category_id" id="category_id">
                        <option value="">Selecione:</option>
                      @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                      @endforeach
                      </select>
                      @if($errors->has('category_id'))
                        <p class="help-block">{{$errors->first('category_id')}}</p>
                      @endif
                    </div>
                  </div>
                  <div class="col-sm-9">
                    <div class="form-group {{ $errors->has('topic_id') ? 'has-error' : null }}" style="max-width: 300px">
                      <label for="topic_id">Tópico</label>
                      <select class="form-control" id="topic_id" name="topic_id">
                        <option value="">-- Selecione uma Categoria --</option>
                      </select>
                      @if($errors->has('topic_id'))
                        <p class="help-block">{{$errors->first('topic_id')}}</p>
                      @endif
                    </div>
                  </div>
                </div>

                <div class="form-group {{ $errors->has('title') ? 'has-error' : null }}">
                  <label for="title">Título</label>
                  <input type="text" class="form-control" id="title" name="title">
                  @if($errors->has('title'))
                    <p class="help-block">{{$errors->first('title')}}</p>
                  @endif
                </div>

                <div class="form-group {{ $errors->has('content') ? 'has-error' : null }}">
                  <label for="content">Conteúdo da Postagem</label>
                  <textarea name="content" id="content" class="form-control" rows="20" style="width: 100%"></textarea>
                  @if($errors->has('content'))
                    <p class="help-block">{{$errors->first('content')}}</p>
                  @endif
                </div>
                <div class="row">
                  <div class="col-sm-2 col-sm-offset-10">
                    <button type="submit" class="btn btn-default pull-right">Salvar</button>  
                  </div>
                  
                </div>
              </form>
            </div>
          </div>
          
        </div>
      </div>
    </div>
  </div>

<script type="text/javascript">
  $('#category_id').on('change', function(){
    $.ajax({
        url: "{{ route('forum.combotopics') }}",
        dataType: 'json',
        data: { category_id: $(this).val() },
        success: function(res){
          var topics = res.data
          var mySelect = $('#topic_id');

          mySelect.find('option').remove();
          if(topics.length){
            mySelect.append(
                $('<option></option>').val('').html('Selecione: ')
            );
            $.each(topics, function(index, topic){
              mySelect.append(
                  $('<option></option>').val(topic.id).html(topic.title)
              );
            })  
          } else {
            
            mySelect.append(
                $('<option></option>').val('').html('-- Selecione uma Categoria --')
            );
            
          }
          
          
        },
        error: function(jqXHR, textStatus, errorThrown) {
          
          
        }
      });
  });
</script>
@endsection