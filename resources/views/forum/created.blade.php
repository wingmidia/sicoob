@extends('layouts.sicoob.default')

@section('content')
  <div class="row flexbox">
    <div class="col-xs-8">
      <div class="fundo-branco sombra-caixa ctn-forum">
        <div class="row">
          <div class="col-sm-6">
            <h1 class="titulo-janelas">
              <i class="icomoon icon-messages"></i>
              <div>NOVA POSTAGEM</div>
            </h1>
          </div>
        </div>
        <div class="lista borda-topo-cinza">
          
          <p>Sua postagem foi enviada para o administrador do sistema para aprovação.<br>
             <a href="{{ route('forum.list') }}"><strong>Voltar ao Fórum</strong></a></p>
          
        </div>
      </div>
    </div>
  </div>

@endsection