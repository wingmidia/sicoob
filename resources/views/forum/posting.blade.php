@extends('layouts.sicoob.default')

@section('content')

  <div class="row flexbox">
    <div class="col-lg-12">
      <div class="fundo-branco sombra-caixa ctn-forum">
        <div class="row">
          <div class="col-sm-6">
            <h1 class="titulo-janelas">
              <i class="icomoon icon-messages"></i>
              <div>{{ mb_strtoupper($post->title) }}</div>
            </h1>
          </div>
        </div>

        <div class="lista borda-topo-cinza">
          <div class="row">
            <div class="col-sm-2 col-lg-2 text-center">
              @if($post->collaborator)
                @include('layouts.sicoob.partials.profile_thumb', ['photo' => $post->collaborator->photo, 'name' => $post->collaborator->name])
              @else
                @include('layouts.sicoob.partials.profile_thumb', ['photo' => '', 'name' => 'Administrador'])
                
              @endif
                <p>
                  {{$post->collaborator ? $post->collaborator->name : 'Administrador'}}<br>
                  Categoria: <a href="{{ route('forum.category', ['slug' => $post->category->slug]) }}">{{$post->category->name}}</a><br>
                  Criado em: {{$post->created_at->format('d/m/Y H:i:s')}}
                </p>
                <p>
                  <a href=""><strong>Responder</strong></a>
                </p>

            </div>
            <div class="col-sm-10 col-lg-10">
              {!! $post->content !!}
            </div>
          </div>          
        </div>
      </div>
    </div>
  </div>
@endsection