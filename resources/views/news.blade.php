@extends('layouts.sicoob.default')

@section('blocos_script')
  $('#search-news').click(function(e){
    if($('#depto-noticia').val() !== ""){
      $('#search').attr('action', '/departamento/'+$('#depto-noticia').val())
    } else {
      $('#search').attr('action', '/noticias/')
    }
    $('#search').submit();
  });
@endsection

@section('content')
<div class="fundo-branco sombra-caixa ctn-noticias">
  <h1 class="titulo-janelas">
    <i class="icomoon icon-bullhorn"></i>
    <div>{{ $departament ? mb_strtoupper($departament->name) : 'NOTÍCIAS' }}</div>
  </h1>
  @if($departament)
  <div class="row">
    <div class="col-12">
      <div class="user-card box-shadow">
        <div class="row d-flex">
          <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2 text-center mb-15 div-img-departament">
            @if(empty($departament->photo))
              <img class="imgDepartament" src="{!!url('/')!!}/images/profile/19zJ7CPudWBeBJTzi9q6Lyvi2UxQaloFePuYfywS.jpeg" alt="photo">
            @else
              <img class="imgDepartament" src="{!!url('/storage')!!}/{{$departament->photo}}" alt="photo">
            @endif
          </div>
          <div class="info-area info-departament col-xs-12 col-sm-10 col-md-10 col-lg-10 col-xl-10">
            <p class="item">
              {{ mb_strtoupper($departament->name) }} {{ !empty($departament->description) ? '-' : '' }} {{ $departament->description }}
            </p>
            @if(!empty($departament->phone))
            <p class="item">
              {{ $departament->phone }}
            </p>
            @endif
            @if(!empty($departament->address))
            <p class="item">
              {{ $departament->address }} {{ !empty($departament->district) ? '-' : '' }} {{ $departament->district }}
            </p>
            @endif
            @if(!empty($departament->cnpj))
            <p class="item">
              CNPJ: {{ $departament->cnpj }}
            </p>
            @endif
            @if(!empty($departament->cep))
            <p class="item">
              CEP: {{ $departament->cep }}
            </p>
            @endif
            @if(!empty($departament->email))
            <p class="item">
              {{ $departament->email }}
            </p>
            @endif
          </div>
        </div>
        {{-- <div class="row">
          <div class="col-xs-12 link">
            <a href="">Conheça a Equipe</a>
          </div>
        </div> --}}
      </div>
    </div>
  </div>
  @endif
  <div class="row">
    @if($departament)
    <div class="col-xs-12 col-md-6">
      <div class="row">
        @foreach($departament->collaborators as $c)
        <div class="col-xs-12 col-md-6 col-lg-6 col-xl-6">
          <div class="user-card box-shadow">
            <div class="card-wrapper">
              <?php
                $photo = "19zJ7CPudWBeBJTzi9q6Lyvi2UxQaloFePuYfywS.jpeg";
                if(!empty($c->photo))
                  $photo = $c->photo;
              ?>
              <div class="photo-area" style="background: url('{!!url('/')!!}/images/thumb/{{$photo}}') center center no-repeat;">
                {{-- @if(empty($departament->photo))
                  <img class="img-thumbnail" src="{!!url('/')!!}/images/profile/19zJ7CPudWBeBJTzi9q6Lyvi2UxQaloFePuYfywS.jpeg" alt="photo">
                @else
                  <img class="img-thumbnail" src="{!!url('/')!!}/images/profile/{{$c->photo}}" alt="photo">
                @endif --}}
              </div>
              <div class="info-area">
                <p class="item">
                  {{$c->name}}
                </p>
                <p class="item">
                  {{$c->phone}}
                </p>
                @if($c->date_birth)
                <p class="item">
                  Aniversário: {{ $c->date_birth->format('d/m') }}
                </p>
                @endif
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12">
                <p class="email">
                  {{ $c->user->email }}
                </p>
              </div>
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </div>
    @endif
  <div class="col-xs-12 {{ $departament ? 'col-md-6' : '' }}">
      <form class="form-inline frm-busca" id="search" method="get">
        <div class="row custom-row">

          <div class="col-xs-12 col-lg-4">
            <label for="buscar-noticia">Buscar:</label>
            <div class="input-group">
                <input type="search" name="search" id="buscar-noticia" class="custom-input form-control" value="{{request()->get('search', null)}}" placeholder="" />
                <span class="input-group-addon custom-icon"><i class="fa fa-search"></i></span>
              </div>
            
            {{-- <div class="">
              <i class="fa fa-search"></i>
            </div> --}}
          </div>
          <div class="col-xs-9 col-lg-5">
            <label for="depto-noticia">Departamento:</label>
            <select name="depto-noticia" id="depto-noticia" class="custom-input form-control">
              <option value="">Selecione uma opção</option>
                @foreach(getDepartaments() as $departament)
                <option value="{{$departament->slug}}" {{ $slug==$departament->slug ? 'selected' : '' }}>{{$departament->name}}</option>
                @endforeach
            </select>
          </div>
          <div class="col-xs-3 text-right">
              <button type="button" id="search-news" class="btn btn-buscar transicao">BUSCAR</button>
          </div>         
        </div>
      </form>
      <div class="lista borda-topo-cinza">
        @if($news->total() == 0)
        <h4>Nenhum registro encontrado!</h4>
        @else
        <div class="row">
          @foreach($news as $new)
          <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="media item box-shadow" style="min-height:240px">
              <a href="{{ route('new.details', ['slug' => $new->slug]) }}">
                <div class="media-left media-middle ctn-imagem">
                  <img src="{{ route('imagecache', ['template' => 'medium', 'filename' => $new->image_feacture]) }}"
                    alt="{{ $new->title }}" title="{{ $new->title }}" class="media-object transicao" />
                </div>
                <div class="media-body detalhes">
                  <h2 class="titulo media-heading">{{ $new->title }}</h2>
                  <div class="info">
                    {{ $new->start_date->format('d/m/Y') }},
                    <span>Departamento:</span>
                    {{join(', ', collect($new->news_departaments)->pluck('name')->toArray())}}
                  </div>
                  <p>
                    {{ str_limit($new->description, 200) }}
                  </p>
                  <button class="btn transicao">VEJA MAIS</button>
                </div>
              </a>
            </div>
          </div>
          @endforeach
        </div>
        @endif
        {{ $news->links() }}
      </div>
    </div>
  </div>
</div>
@endsection