@extends('layouts.sicoob.default')

@section('arquivos_estilos')
<link rel="stylesheet" href="/css/jquery-ui.css">
@endsection

@section('arquivos_scripts')
<script src="/js/jquery.meio.mask.min.js"></script>
  <script src="/js/jquery-ui.js"></script>
@endsection

@section('blocos_script')
// Calendario para os campos de data
      $( ".date-picker" ).datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
        dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
        monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez']
      });

      // Máscaras para campos dos formulários
      $('.fmtData').setMask('date');
      $('.fmtHora').setMask('time');
@endsection

@section('content')
  <div class="fundo-branco sombra-caixa ctn-atividades">
    <h1 class="titulo-janelas">
      <i class="icomoon icon-cog-outline"></i>
      <div>ATIVIDADES</div>
    </h1>

    <form class="form-horizontal frm-atividade">
      <fieldset>
        <div class="form-group">
          <label for="produto" class="col-sm-1 control-label">Produto:</label>
          <div class="col-sm-5">
            <select name="produto" id="produto" class="form-control" disabled="disabled">
              @foreach(getProducts() as $product)
              <option value="{{$product->id}}" {{ $product->id==$activity->product_id ? 'selected' : null }}>{{$product->title}}</option>
              @endforeach
            </select>
          </div>

          <label for="data-inicial-atividade" class="col-sm-1 control-label">Data Inicial:</label>
          <div class="col-sm-2">
            <div class="input-group disabled">
              <input type="text" name="data-inicial-atividade" id="data-inicial-atividade" class="date-picker form-control" size="8" disabled="disabled" value="{{ $activity->start_date->format('d/m/Y') }}" />
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
            </div>
          </div>
          <label for="data-final-atividade" class="col-sm-1 control-label">Data Final:</label>
          <div class="col-sm-2">
            <div class="input-group disabled">
              <input type="text" name="data-final-atividade" id="data-final-atividade" class="date-picker form-control" size="8" disabled="disabled" value="{{ $activity->end_date->format('d/m/Y') }}" />
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="produto" class="col-sm-1 control-label">Cliente:</label>
          <div class="col-sm-5">
            <select name="produto" id="produto" class="form-control" disabled="disabled">
              @foreach(getClients() as $client)
                <option value="{{$client->id}}">{{$client->name}}</option>
              @endforeach
            </select>
          </div>

          <label for="hora-inicial-atividade" class="col-sm-1 control-label">Hora Inicial:</label>
          <div class="col-sm-2">
            <input type="text" name="hora-inicial-atividade" id="hora-inicial-atividade" class="form-control fmtHora" size="14" disabled="disabled" value="{{ $activity->start_date->format('H:i') }}" />
          </div>
          <label for="hora-final-atividade" class="col-sm-1 control-label">Hora Final:</label>
          <div class="col-sm-2">
            <input type="text" name="hora-final-atividade" id="hora-final-atividade" class="form-control fmtHora" size="14" disabled="disabled" value="{{ $activity->end_date->format('H:i') }}" />
          </div>
        </div>
      </fieldset>

      <div class="form-group">
        <label for="descricao-atividade" class="col-sm-1 control-label">Atividade:</label>
        <div class="col-sm-11">
          <textarea name="descricao-atividade" id="descricao-atividade" rows="6" class="form-control" disabled="disabled">{{ $activity->works }}</textarea>
        </div>
      </div>

      <div class="form-group">
        <label for="observacoes-atividade" class="col-sm-1 control-label">Observações:</label>
        <div class="col-sm-11">
          <textarea name="observacoes-atividade" id="observacoes-atividade" rows="6" class="form-control" disabled="disabled">{{ $activity->observation }}</textarea>
        </div>
      </div>

      <div class="form-group">
        <label for="status-atividade" class="col-sm-1 control-label">Status:</label>
        <div class="col-sm-6 col-md-2">
          <select name="status-atividade" id="status-atividade" class="form-control" disabled="disabled">
            <option value="">Selecione</option>
            <option value="Aberto">Aberto</option>
            <option value="Aguardando">Aguardando</option>
            <option value="Aprovado">Aprovado</option>
            <option value="Em andamento" selected="selected">Em andamento</option>
            <option value="Cancelado">Cancelado</option>
            <option value="Concluído">Concluído</option>
            <option value="Reprovado">Reprovado</option>
          </select>
        </div>

        <div class="col-sm-5 col-md-9">&nbsp;</div>
      </div>
    </form>
  </div>
@endsection