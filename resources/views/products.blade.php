@extends('layouts.sicoob.default')

@section('content')
  <div class="fundo-branco sombra-caixa ctn-produtos">
    <h1 class="titulo-janelas">
      <i class="icomoon icon-bag"></i>
      <div>PRODUTOS</div>
    </h1>

    <form class="form frm-busca">
      <div class="row">
        <div class="col-sm-7">
          <div class="form-group">
            <div class="input-group">
              <input type="search" name="buscar-produto" id="buscar-produto" class="form-control" placeholder="Busca" />
              <div class="input-group-addon">
                <i class="fa fa-search"></i>
              </div>
            </div>
          </div>
        </div>

        <div class="form-group">
          <button class="btn btn-buscar transicao">BUSCAR</button>
        </div>
      </div>
    </form>

    <div class="lista-produtos">
      <div class="row">
        @foreach($products as $product)
        <div class="col-sm-4 col-lg-3">
          <div class="item">
            
              <div class="ctn-imagem">
                <a href="{{ route('product.details', ['id' => $product->id]) }}"><img src="{{ route('imagecache', ['filename' => $product->image_feacture, 'template' => 'product']) }}" alt="{{ $product->name }}" title="{{ $product->name }}" class="img-responsive transicao" height="311" /></a>
              </div>
              <div class="detalhes-produto">
                <h2 class="nome-produto">{{ mb_strtoupper($product->title) }}</h2>
                <p class="descricao-produto"><a href="{{ route('product.details', ['id' => $product->id]) }}">{{ str_limit(strip_tags($product->content)) }}...</a></p>
                <a href="{{ route('product.details', ['id' => $product->id]) }}" class="btn btn-block transicao">MAIS DETALHES</a>
              </div>
            </a>
          </div>
        </div>
        @endforeach
      </div>

      {{ $products->links() }}
    </div>
  </div>
@endsection