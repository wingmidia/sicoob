@extends('layouts.sicoob.default')

@section('content')
  <div class="row flexbox">
    <div class="col-lg-12">
      <div class="fundo-branco sombra-caixa ctn-forum">
        <div class="row">
          <div class="col-sm-6">
            <h1 class="titulo-janelas">
              <i class="icomoon icon-messages"></i>
              <div>FÓRUM</div>
            </h1>
          </div>

          <div class="col-sm-6 text-right">
            <a href="#" class="btn transicao">CADASTRAR NOVO TÓPICO</a>
          </div>
        </div>

        <form class="form frm-busca">
          <div class="row">
            <div class="col-sm-7">
              <div class="form-group">
                <div class="input-group">
                  <input type="search" name="buscar-contra-cheque" id="buscar-contra-cheque" class="form-control" placeholder="Busca" />
                  <div class="input-group-addon">
                    <i class="fa fa-search"></i>
                  </div>
                </div>
              </div>
            </div>

            <div class="form-group">
              <button class="btn btn-buscar transicao">BUSCAR</button>
            </div>
          </div>
        </form>

        <div class="lista borda-topo-cinza">
          <table class="table table-bordered table-forum">
            <caption>CATEGORIAS</caption>
            <thead>
              <tr>
                <th>TÍTULO</th>
                <th>STATUS</th>
                <th>ÚLTIMA POSTAGEM</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <div class="item-topico">
                    <div class="img-topico">
                      <a href="#"><img src="img/conteudo/ico-forum-financeiro.gif" alt="Financeiro" title="Financeiro" class="img-responsive img-circle" /></a>
                    </div>
                    <div class="dados-topico">
                      <div class="titulo-topico"><a href="#">Financeiro</a></div>
                      <div class="resumo-topico"><a href="#">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium</a></div>
                    </div>
                  </div>
                </td>
                <td>
                  <div class="status-topico">
                    <a href="#">9 Tópicos</a><br />
                    10 Postagens</a>
                  </div>
                </td>
                <td>
                  <div class="titulo-ultima-postagem">Re: Renovação de crédito para ap...</div>
                  <div class="autor-postagem">por: <span>Maria Silva</span></div>
                  <div class="data-postagem">08 Fev. 2017, 09:26</div>
                </td>
              </tr>
              <tr>
                <td>
                  <div class="item-topico">
                    <div class="img-topico">
                      <a href="#"><img src="img/conteudo/ico-forum-juridico.gif" alt="Jurídico" title="Jurídico" class="img-responsive img-circle" /></a>
                    </div>
                    <div class="dados-topico">
                      <div class="titulo-topico"><a href="#">Jurídico</a></div>
                      <div class="resumo-topico"><a href="#">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium</a></div>
                    </div>
                  </div>
                </td>
                <td>
                  <div class="status-topico">
                    <a href="#">28 Tópicos<br />
                    6 Postagens</a>
                  </div>
                </td>
                <td>
                  <div class="titulo-ultima-postagem">Re: Renovação de crédito para ap...</div>
                  <div class="autor-postagem">por: <span>Maria Silva</span></div>
                  <div class="data-postagem">08 Fev. 2017, 09:26</div>
                </td>
              </tr>
              <tr>
                <td>
                  <a href="#">
                    <div class="item-topico">
                      <div class="img-topico">
                        <img src="img/conteudo/ico-forum-tutoriais.gif" alt="Tutoriais" title="Tutoriais" class="img-responsive img-circle" />
                      </div>
                      <div class="dados-topico">
                        <div class="titulo-topico"><a href="#">Tutoriais</a></div>
                        <div class="resumo-topico"><a href="#">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium</a></div>
                      </div>
                    </div>
                  </a>
                </td>
                <td>
                  <div class="status-topico">
                    <a href="#">9 Tópicos<br />
                    11 Postagens</a>
                  </div>
                </td>
                <td>
                  <div class="titulo-ultima-postagem">Re: Renovação de crédito para ap...</div>
                  <div class="autor-postagem">por: <span>Maria Silva</span></div>
                  <div class="data-postagem">08 Fev. 2017, 09:26</div>
                </td>
              </tr>
              <tr>
                <td>
                  <a href="#">
                    <div class="item-topico">
                      <div class="img-topico">
                        <a href="#"><img src="img/conteudo/ico-forum-fora-de-topico.gif" alt="Fora de tópico" title="Fora de tópico" class="img-responsive img-circle" /></a>
                      </div>
                      <div class="dados-topico">
                        <div class="titulo-topico"><a href="#">Fora de tópico</a></div>
                        <div class="resumo-topico"><a href="#">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium</a></div>
                      </div>
                    </div>
                  </a>
                </td>
                <td>
                  <div class="status-topico">
                    <a href="#">17 Tópicos<br />
                    9 Postagens</a>
                  </div>
                </td>
                <td>
                  <div class="titulo-ultima-postagem">Re: Renovação de crédito para ap...</div>
                  <div class="autor-postagem">por: <span>Maria Silva</span></div>
                  <div class="data-postagem">08 Fev. 2017, 09:26</div>
                </td>
              </tr>
            </tbody>
          </table>
          
          <table class="table table-bordered table-forum">
            <caption>OUTROS</caption>
            <thead>
              <tr>
                <th>TÍTULO</th>
                <th>STATUS</th>
                <th>ÚLTIMA POSTAGEM</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <a href="#">
                    <div class="item-topico">
                      <div class="img-topico">
                        <a href="#"><img src="img/conteudo/ico-forum-processo-seletivo-interno.gif" alt="Processo Seletivo Interno" title="Processo Seletivo Interno" class="img-responsive img-circle" /></a>
                      </div>
                      <div class="dados-topico">
                        <div class="titulo-topico"><a href="#">Processo Seletivo Interno</a></div>
                        <div class="resumo-topico"><a href="#">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium</a></div>
                      </div>
                    </div>
                  </div>
                </td>
                <td>
                  <div class="status-topico">
                    <a href="#">12 Tópicos<br />
                    14 Postagens</a>
                  </div>
                </td>
                <td>
                  <div class="titulo-ultima-postagem">Re: Renovação de crédito para ap...</div>
                  <div class="autor-postagem">por: <span>Maria Silva</span></div>
                  <div class="data-postagem">08 Fev. 2017, 09:26</div>
                </td>
              </tr>
              <tr>
                <td>
                  <div class="item-topico">
                    <div class="img-topico">
                      <a href="#"><img src="img/conteudo/ico-forum-anuncios.gif" alt="Anúncios" title="Anúncios" class="img-responsive img-circle" /></a>
                    </div>
                    <div class="dados-topico">
                      <div class="titulo-topico"><a href="#">Anúncios</a></div>
                      <div class="resumo-topico"><a href="#">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium</a></div>
                    </div>
                  </div>
                </td>
                <td>
                  <div class="status-topico">
                    2 Tópicos<br />
                    12 Postagens
                  </div>
                </td>
                <td>
                  <div class="titulo-ultima-postagem">Re: Renovação de crédito para ap...</div>
                  <div class="autor-postagem">por: <span>Maria Silva</span></div>
                  <div class="data-postagem">08 Fev. 2017, 09:26</div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection