@extends('layouts.sicoob.default')

@section('arquivos_estilos')
<link rel="stylesheet" href="/css/jquery-ui.css">
@endsection

@section('arquivos_scripts')
<script src="/js/jquery.meio.mask.min.js"></script>
  <script src="/js/jquery-ui.js"></script>
<link  href="/js/fancybox/dist/jquery.fancybox.min.css" rel="stylesheet">
<script src="/js/fancybox/dist/jquery.fancybox.min.js"></script>
@endsection

@section('blocos_script')
// Calendario para os campos de data
      $( ".date-picker" ).datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
        dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
        monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez']
      });

      // Máscaras para campos dos formulários
      $('.fmtData').setMask('date');
@endsection

@section('content')
  <div class="fundo-branco sombra-caixa ctn-eventos">
    <h1 class="titulo-janelas">
      <i class="icomoon icon-calendar"></i>
      <div>EVENTO</div>
    </h1>

    <div class="conteudo-interno borda-topo-cinza">
      <div class="row">
        <div class="col-sm-12">
          <img src="{{ route('imagecache', ['filename' => $event->image_feacture, 'template' => 'original']) }}" 
               alt="{{ $event->title }}" 
               title="{{ $event->title }}" 
               style="max-width: 410px;" 
               class="img-responsive" 
               align="left" 
               vspace="10" 
               hspace="20" />
          
          <div>
          <div class="data">
            <div class="box-data interno">
              <div class="data-dia">{{ $event->event_date->format('d') }}</div>
              <div class="data-mes">{{ $event->event_date->format('m') }}</div>
            </div>
          </div>

          <h2 class="titulo-interno">{{ $event->title }}</h2>

          <div class="info">
            <span><i class="fa fa-map-marker"></i></span> {{ $event->local }}
          </div>
          </div>

          <div class="conteudo">
            {!! $event->content !!}
          </div>
        </div>
      </div>
    </div>
  </div>
  @if($event->photos->count() > 0)
  <div class="fundo-branco sombra-caixa ctn-noticias">
    <h1 class="titulo-janelas">
      <i class="icomoon icon-bullhorn"></i>
      <div>GALERIA DE FOTOS</div>
    </h1>
    <div class="conteudo-interno borda-topo-cinza">
      <div class="row">
        <div class="col-sm-12">
          <div class="conteudo">
          @foreach ($event->photos->chunk(4) as $chunk)
            <div class="row">
             @foreach ($chunk as $photo)
              <div class="col-sm-2">  
                <a href="{{ route('imagecache', ['template' => 'original', 'filename' => $photo->image]) }}" data-fancybox="gallery">            
                  <img src="{{ route('imagecache', ['template' => 'medium', 'filename' => $photo->image]) }}" 
                       alt="" />
                </a>
              </div>
              @endforeach
            </div>
          @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
  @endif
@endsection