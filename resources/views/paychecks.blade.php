@extends('layouts.sicoob.default')

@section('arquivos_estilos')
<link rel="stylesheet" href="/css/jquery-ui.css">
@endsection

@section('arquivos_scripts')
<script src="/js/jquery.meio.mask.min.js"></script>
  <script src="/js/jquery-ui.js"></script>
@endsection

@section('blocos_script')
// Calendario para os campos de data
      $( ".date-picker" ).datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
        dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
        monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez']
      });

      // Máscaras para campos dos formulários
      $('.fmtData').setMask('date');
@endsection

@section('content')
  <div class="fundo-branco sombra-caixa ctn-contra-cheque">
    <h1 class="titulo-janelas">
      <i class="icomoon icon-note"></i>
      <div>RH - CONTRA CHEQUES</div>
    </h1>

    <form class="frm-busca">
      <div class="row">
        <!--div class="col-sm-6 col-md-5 col-lg-4">
          <div class="form-group">
            <div class="input-group">
              <input type="search" size="100" name="buscar-contra-cheque" id="buscar-contra-cheque" class="form-control" placeholder="Busca" />
              <div class="input-group-addon">
                <i class="fa fa-search"></i>
              </div>
            </div>
          </div>
        </div-->

        <div class="col-sm-2">
          <div class="form-group">
            <select name="month" class="form-control">
              @for($i=0;$i<=11;$i++)
              <option value="{{$i}}" {{ $i==(int)request()->get('month') || ((Carbon\Carbon::now()->month-1)==$i && !request()->has('month')) ? 'selected' : null }}>{{maskMonth($i)}}</option>
              @endfor
            </select>
          </div>
        </div>
        <div class="col-sm-6 form-inline">
          <div class="form-group">
            <select name="year" class="form-control" style="min-width: 90px">
              @for($i=$year_now;$i>=$year_init;$i--)
              <option value="{{$i}}" {{ request()->get('year')==$i || (Carbon\Carbon::now()->year==$i && !request()->has('year')) ? 'selected' : null }}>{{$i}}</option>
              @endfor
            </select>
          </div>

          <div class="form-group">
            <button class="btn btn-buscar transicao">BUSCAR</button>
          </div>
        </div>
      </div>
    </form>

    <div class="lista-tabela">
      <table class="table table-hover borda-topo-cinza">
        <thead>
          <tr>
            <th>Mês</th>
            <th>Ano</th>
            <th>Matrícula</th>
            <th colspan="2">Situação</th>
          </tr>
        </thead>
        <tbody>
          @foreach($paychecks as $paycheck)
          <tr>
            <td>{{ maskMonth($paycheck->month) }}</td>
            <td>{{ $paycheck->year }}</td>
            <td>{{ $paycheck->collaborator->code }} - {{ $paycheck->collaborator->name }}</td>
            <td><i class="fa fa-{{iconStatusPaychecks($paycheck->status)}}" aria-hidden="true"></i> {{ trans('core.'.$paycheck->status) }}</td>
            <td>
              <a href="{{ route('paycheck.view', ['filename' => $paycheck->filename]) }}" target="_blank" title="Visualizar o demonstrativo de pagamento de salário"><i class="fa fa-eye"></i></a>
              <a href="{{ route('paycheck.download', ['filename' => $paycheck->filename]) }}" title="Download do demonstrativo de pagamento de salário"><i class="fa fa-download"></i></a>
            </td>
          </tr>
          @endforeach
        </tbody>
        <tfoot>
          <tr>
            <td colspan="5">
              {{ $paychecks->links('vendor.pagination.default') }}
            </td>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
@endsection