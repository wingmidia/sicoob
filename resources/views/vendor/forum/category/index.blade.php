{{-- $category is passed as NULL to the master layout view to prevent it from showing in the breadcrumbs --}}
@extends ('forum::master', ['category' => null])

@section ('content')
<div class="row flexbox">
    <div class="col-lg-12">
        <div class="fundo-branco sombra-caixa ctn-forum">
            <div class="row">
              <div class="col-sm-6">
                <h1 class="titulo-janelas">
                  <i class="icomoon icon-messages"></i>
                  <div>FORUM - {{ strtoupper(trans('forum::general.index')) }}</div>
                </h1>
              </div>
             
            </div>
            
            <div class="lista borda-topo-cinza">
            @can ('createCategories')
                @include('forum::category.partials.form-create')
            @endcan
            @foreach ($categories as $category)
                <table class="table table-bordered table-forum table-index">
                    <caption>{{ mb_strtoupper($category->title)}}</caption>
                    <thead>
                        <tr>
                            <th>{{ trans_choice('forum::categories.category', 1) }}</th>
                            <th class="col-md-2">Status</th>
                            <th class="col-md-2">{{ trans('forum::threads.newest') }}</th>
                            <th class="col-md-2">{{ trans('forum::posts.last') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="category">
                            @include ('forum::category.partials.list', ['titleClass' => 'lead'])
                        </tr>
                        @if (!$category->children->isEmpty())
                            <tr>
                                <th colspan="5">{{ trans('forum::categories.subcategories') }}</th>
                            </tr>
                            @foreach ($category->children as $subcategory)
                                @include ('forum::category.partials.list', ['category' => $subcategory])
                            @endforeach
                        @endif
                    </tbody>
                </table>
            @endforeach
            </div>
        </div>
    </div>
</div>
@stop
