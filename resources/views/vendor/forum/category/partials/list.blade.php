<tr>
    <td {{ $category->threadsEnabled ? '' : 'colspan=5' }}>
        <div class="dados-topico">
          <div class="titulo-topico {{ isset($titleClass) ? $titleClass : '' }}"><a href="{{ Forum::route('category.show', $category) }}">{{ $category->title }}</a></div>
          <div class="resumo-topico">{{ $category->description }}</div>
        </div>
    </td>
    @if ($category->threadsEnabled)
        <td>
          <div class="status-topico">
            {{$category->thread_count}} {{ trans_choice('forum::threads.thread', 2) }}<br />
            {{$category->post_count}} {{ trans_choice('forum::posts.post', 2) }}
          </div>
        
        </td>
        <td>
            @if ($category->newestThread)
                <div class="titulo-ultima-postagem">
                    <a href="{{ Forum::route('thread.show', $category->newestThread) }}">{{ $category->newestThread->title }}</a>
                </div>
                <div class="autor-postagem">
                    por: {{ $category->newestThread->authorName }}
                </div>
                <div class="data-postagem">{{$category->newestThread->created_at->format('d/m/Y H:i:s')}}</div>
            @endif
        </td>
        <td>
            @if (isset($category->latestActiveThread->lastPost))
                <div class="titulo-ultima-postagem"><a href="{{ Forum::route('thread.show', $category->latestActiveThread->lastPost) }}">{{ $category->latestActiveThread->title }}</a></div>
                <div class="autor-postagem">por: {{ $category->latestActiveThread->lastPost->authorName }}</div>
                <div class="data-postagem">{{$category->latestActiveThread->created_at->format('d/m/Y H:i:s')}}</div>
                
            @endif
        </td>
    @endif
</tr>
