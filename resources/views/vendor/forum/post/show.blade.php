@extends ('forum::master', ['breadcrumb_other' => trans('forum::posts.view')])

@section ('content')
<div class="row flexbox">
    <div class="col-lg-12">
        <div class="fundo-branco sombra-caixa ctn-forum">
            <div class="row">
              <div class="col-sm-6">
                <h1 class="titulo-janelas">
                  <i class="icomoon icon-messages"></i>
                    <div>
                        {{ mb_strtoupper(trans('forum::posts.view')) }} ({{ mb_strtoupper($thread->title) }})
                    </div>
                </h1>
              </div>
            </div>
            <div id="post" class="lista borda-topo-cinza">

                <a href="{{ Forum::route('thread.show', $thread) }}" class="btn btn-default">&laquo; {{ trans('forum::threads.view') }}</a>

                <table class="table">
                    <thead>
                        <tr>
                            <th class="col-md-2">
                                {{ trans('forum::general.author') }}
                            </th>
                            <th>
                                {{ trans_choice('forum::posts.post', 1) }}
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @include ('forum::post.partials.list', compact('post'))
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
    
@stop
