@extends ('forum::master', ['breadcrumb_other' => trans('forum::threads.new_thread')])

@section ('content')
<div class="row flexbox">
    <div class="col-lg-12">
        <div class="fundo-branco sombra-caixa ctn-forum">
            <div class="row">
              <div class="col-sm-6">
                <h1 class="titulo-janelas">
                  <i class="icomoon icon-messages"></i>
                    <div>
                        {{ mb_strtoupper(trans('forum::threads.new_thread')) }} ({{ mb_strtoupper($category->title) }})
                    </div>
                </h1>
              </div>
            </div>

            <div class="lista borda-topo-cinza">
                <div id="create-thread">
                    <form method="POST" action="{{ Forum::route('thread.store', $category) }}">
                        {!! csrf_field() !!}
                        {!! method_field('post') !!}

                        <div class="form-group">
                            <label for="title">{{ trans('forum::general.title') }}</label>
                            <input type="text" name="title" value="{{ old('title') }}" class="form-control">
                        </div>

                        <div class="form-group">
                            <textarea name="content" class="form-control">{{ old('content') }}</textarea>
                        </div>

                        <button type="submit" class="btn btn-success pull-right">{{ trans('forum::general.create') }}</button>
                        <a href="{{ URL::previous() }}" class="btn btn-default">{{ trans('forum::general.cancel') }}</a>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>


@stop
