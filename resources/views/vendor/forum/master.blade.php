<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <title>
        @if (isset($thread))
            {{ $thread->title }} -
        @endif
        @if (isset($category))
            {{ $category->title }} -
        @endif
        {{ trans('forum::general.home_title') }}
    </title>
  <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico" />

  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />

  <!-- Fonts -->
  <link rel="stylesheet" href="/css/bootstrap.min.css" />
  <link rel="stylesheet" href="/css/style.css" />
  <link rel="stylesheet" href="/css/font-awesome.min.css" />
  <link rel="stylesheet" href="/js/sweetalert2/sweetalert2.min.css" />
  @yield('arquivos_estilos')

  <script src="/js/jquery.min.js"></script>
    <script src="/js/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>

  <script>
      window.Laravel = {!! json_encode([
          'csrfToken' => csrf_token(),
      ]) !!};
  </script>
  
  <!--script src="/js/sweetalert2/sweetalert2.min.js"></script-->
  @yield('arquivos_scripts')
  <!-- Scripts -->
  
  <script type='text/javascript'>
    $(document).ready(function() {
      /* Menu retratil */
      var statusMenu = 0; /* valor inicial para o menu fechado */

      $('.btMenu').on('click touchstart', function(e){
        if(statusMenu == 0){
          $('#nav-hamburger').addClass('open');
          $('.menu-principal .nav li a').addClass('open');
          $('.menu-principal .nav li a i.icomoon').addClass('icon-retina');
          statusMenu = 1; /* Menu aberto */
        } else{
          $('#nav-hamburger').removeClass('open');
          $('.menu-principal .nav li a').removeClass('open');
          $('.menu-principal .nav li a i.icomoon').removeClass('icon-retina');
          statusMenu = 0; /* Menu fechado */
        };
        $('.menu-principal .nav li a div').toggle();
      });

      // Modal fixa a direita da tela com formulario de contato
      $('.ctn-form-contato-depto .ctn-botao').click(function(e) {
        $(this).parent().toggleClass("fechado aberto");
      });

      // Para exibir box para login do colaborador
      $('.btn-acessar').click(function() {
        $('.box-login').toggle();
      });

      // Efeito para manter o menu com o fundo colorido quando exibido o menu filho
      $('header #menu-navegacao ul li.dropdown').mouseover(function() {
        if($(this).children('ul').is(':visible')) {
          $(this).children('a').css({'background': '#0B3535'});
        }
      }).mouseout(function(){
        $(this).children('a').css({'background': '#7DB61C'});
      });


      $('.postcode').mask('#####-###', {reverse: true});
      $('.cpf').mask('###.###.###-##', {reverse: true});
      $('.cnpj').mask('##.###.###/####-##', {reverse: true});

      var SPMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
      },
      spOptions = {
        onKeyPress: function(val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options);
          }
      };

      $('.celphones').mask(SPMaskBehavior, spOptions);

      // Outros scripts
      @yield('blocos_script')
    });
  </script>
  <style type="text/css">
    textarea.form-control{
      height: 200px;
    }
    .breadcrumb{
      margin-top: 20px;
      margin-bottom: 0px
    }
  </style>

</head>
<body>

  @include('layouts.sicoob.partials.header')

  <main>
    <div class="container-fluid">
      <div class="row flexbox">
        <div class="col-lg-0 fundo-verde-escuro sombra-caixa">
          @include('layouts.sicoob.partials.menu')
        </div>
        <div class="col-lg-12">
          @include ('forum::partials.breadcrumbs')
          @include ('forum::partials.alerts')
          @yield('content')
        </div>
      </div>
    </div>
  </main>

  
  @include('layouts.sicoob.partials.footer')

  <script type="text/javascript">
    var toggle = $('input[type=checkbox][data-toggle-all]');
    var checkboxes = $('table tbody input[type=checkbox]');
    var actions = $('[data-actions]');
    var forms = $('[data-actions-form]');
    var confirmString = "{{ trans('forum::general.generic_confirm') }}";

    function setToggleStates() {
        checkboxes.prop('checked', toggle.is(':checked')).change();
    }

    function setSelectionStates() {
        checkboxes.each(function() {
            var tr = $(this).parents('tr');

            $(this).is(':checked') ? tr.addClass('active') : tr.removeClass('active');

            checkboxes.filter(':checked').length ? $('[data-bulk-actions]').removeClass('hidden') : $('[data-bulk-actions]').addClass('hidden');
        });
    }

    function setActionStates() {
        forms.each(function() {
            var form = $(this);
            var method = form.find('input[name=_method]');
            var selected = form.find('select[name=action] option:selected');
            var depends = form.find('[data-depends]');

            selected.each(function() {
                if ($(this).attr('data-method')) {
                    method.val($(this).data('method'));
                } else {
                    method.val('patch');
                }
            });

            depends.each(function() {
                (selected.val() == $(this).data('depends')) ? $(this).removeClass('hidden') : $(this).addClass('hidden');
            });
        });
    }

    setToggleStates();
    setSelectionStates();
    setActionStates();

    toggle.click(setToggleStates);
    checkboxes.change(setSelectionStates);
    actions.change(setActionStates);

    forms.submit(function() {
        var action = $(this).find('[data-actions]').find(':selected');

        if (action.is('[data-confirm]')) {
            return confirm(confirmString);
        }

        return true;
    });

    $('form[data-confirm]').submit(function() {
        return confirm(confirmString);
    });
    </script>

</body>
</html>