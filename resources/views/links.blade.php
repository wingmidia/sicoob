@extends('layouts.sicoob.default')

@section('content')
<div class="fundo-branco sombra-caixa ctn-contra-cheque">
  <h1 class="titulo-janelas">
    <i class="icomoon icon-link"></i>
    <div>LINKS IMPORTANTES</div>
  </h1>

  <div class="lista-links-importantes">
    <div class="row">
      @foreach($links as $link)
      <div class="col-xs-4 col-sm-3 col-md-2 text-center link-importante transicao">
        <a href="{{ $link->link_url }}" target="_blank">
          <figure>
            <img src="{{ route('imagecache', ['filename' => $link->logo, 'template' => 'links']) }}" alt="{{ $link->name }}" title="{{ $link->name }}" class="img-responsive img-thumbnail" />
            <figcaption>{{ $link->name }}</figcaption>
          </figure>
        </a>
          <a href="{{ $link->link_url }}" target="_blank" class="btn btn-downloads transicao">Acessar Site</a>
        
      </div>
      @endforeach
    </div>
  </div>
</div>
@endsection