<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
  <title>Sicoob Credipatos</title>
  <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico" />

  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />

  <!-- Fonts -->
  <link rel="stylesheet" href="/css/bootstrap.min.css" />
  <link rel="stylesheet" href="/css/style.css" />
  <link rel="stylesheet" href="/css/font-awesome.min.css" />
  <link rel="stylesheet" href="/js/sweetalert2/sweetalert2.min.css" />
  <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-default/index.css">
  @yield('arquivos_estilos')

  <script src="/js/jquery.min.js"></script>
  <script src="/js/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
  <script src="/js/jquery.meio.mask.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>

  <script>
      window.Laravel = {!! json_encode([
          'csrfToken' => csrf_token(),
      ]) !!};
  </script>
  
  <script src="/js/sweetalert2/sweetalert2.min.js"></script>
  @yield('arquivos_scripts')
  <!-- Scripts -->
  
  <script type='text/javascript'>
    $(document).ready(function() {
      /* Menu retratil */
      var statusMenu = 0; /* valor inicial para o menu fechado */

      $('.btMenu').on('click touchstart', function(e){
        if(statusMenu == 0){
          $('#nav-hamburger').addClass('open');
          $('.menu-principal .nav li a').addClass('open');
          $('.menu-principal .nav li a i.icomoon').addClass('icon-retina');
          statusMenu = 1; /* Menu aberto */
        } else{
          $('#nav-hamburger').removeClass('open');
          $('.menu-principal .nav li a').removeClass('open');
          $('.menu-principal .nav li a i.icomoon').removeClass('icon-retina');
          statusMenu = 0; /* Menu fechado */
        };
        $('.menu-principal .nav li a div').toggle();
      });

      // Modal fixa a direita da tela com formulario de contato
      $('.ctn-form-contato-depto .ctn-botao').click(function(e) {
        $(this).parent().toggleClass("fechado aberto");
      });

      // Para exibir box para login do colaborador
      $('.btn-acessar').click(function() {
        $('.box-login').toggle();
      });

      // Efeito para manter o menu com o fundo colorido quando exibido o menu filho
      $('header #menu-navegacao ul li.dropdown').mouseover(function() {
        if($(this).children('ul').is(':visible')) {
          $(this).children('a').css({'background': '#0B3535'});
        }
      }).mouseout(function(){
        $(this).children('a').css({'background': '#7DB61C'});
      });


      /*$('.postcode').mask('#####-###', {reverse: true});
      $('.cpf').mask('###.###.###-##', {reverse: true});
      $('.cnpj').mask('##.###.###/####-##', {reverse: true});

      var SPMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
      },
      spOptions = {
        onKeyPress: function(val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options);
          }
      };

      $('.celphones').mask(SPMaskBehavior, spOptions);*/

      // Outros scripts
      @yield('blocos_script')
    });
  </script>

</head>
<body>

  @include('layouts.sicoob.partials.header')

  <main>
    <div class="container-fluid">
      <div class="row flexbox">
        <div class="col-lg-0 fundo-verde-escuro sombra-caixa">
          @include('layouts.sicoob.partials.menu')
        </div>
        <div class="col-lg-12">
          @yield('content')
          @if(basename(url()->current()) != 'login')
            @include('layouts.sicoob.partials.outros-produtos')
          @endif
        </div>
      </div>
    </div>
  </main>

  
  @include('layouts.sicoob.partials.footer')

  @yield('vuejs')



<div class="modal fade" id="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="modal-header bg-default">
      
      <button type="button " class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 class="modal-title">Modal title</h4>
    </div>
    <div class="modal-body" id="modal-content">

    </div>

    </div>
  </div>
</div>

<script type="text/javascript">
  
  function SximoModal( url , title)
  {
    $('#modal-content').html(' ....Carregando , aguarde ...');
    $('.modal-title').html(title);
    $('#modal-content').load(url,function(){
    });
    $('#modal').modal('show');  
  }
</script>

</body>
</html>