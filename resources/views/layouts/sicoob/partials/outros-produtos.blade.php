<?php 
$bannersFooter = \App\Models\Banner::active()->type('footer')->get();
?>
<div class="row">
@foreach($bannersFooter as $k => $banner)
  <div class="col-sm-3">

    @if($banner->url)
    <a href="{{$banner->url}}">
    @endif
      <div class="box-produto sombra-caixa">
        <img src="{{route('imagecache', ['template' => 'original' ,'filename' => $banner->image])}}" 
                   alt="{{$banner->title}}" 
                   title="{{$banner->title}}"
                   class="img-responsive" />      
      </div>
    @if($banner->url)
    </a>
    @endif
  </div>
@endforeach
  
  <!-- <div class="col-sm-3">
    <a href="#">
      <div class="box-produto fundo-verde sombra-caixa">
        <img src="/img/ico-envelope-email.png" alt="" title="Acesse o webmail" class="img-responsive" />
        <div class="descricao-produto">ACESSE O <br />WEBMAIL</div>
      </div>
    </a>
  </div>
  
  <div class="col-sm-3">
    <a href="#">
      <div class="box-produto fundo-verde-pastel sombra-caixa">
        <img src="/img/ico-documentos.png" alt="Normas e manual do colaborador" title="Normas e manual do colaborador" class="img-responsive" />
        <div class="descricao-produto">NORMAS E MANUAL <br />DO COLABORADOR</div>
      </div>
    </a>
  </div>
  
  <div class="col-sm-3">
    <a href="#">
      <div class="box-produto fundo-verde-escuro sombra-caixa">
        <img src="/img/ico-galeria-fotos.png" alt="Galeria de fotos" title="Galeria de fotos" class="img-responsive" />
        <div class="descricao-produto">GALERIA <br />DE FOTOS</div>
      </div>
    </a>
  </div> -->
</div>