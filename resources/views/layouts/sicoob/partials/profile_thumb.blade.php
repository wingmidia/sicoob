@if($photo)
  <img src="{{ route('imagecache', ['template' => 'profile', 'filename' => $photo]) }}" 
       alt="{{$name}}" 
       title="{{$name}}" 
       class="img-responsive img-circle borda-branca" />
@else
  <img src="{{ route('imagecache', ['template' => 'profile', 'filename' => 'profile-noimage.jpg']) }}" 
       alt="{{$name}}" 
       title="{{$name}}" 
       class="img-responsive img-circle borda-branca" />
@endif