  <div class="media {{ request()->get('replay_id', 0) == $comment['id'] ? 'replay' : null }}">
    <div class="media-left">
      
        <img class="media-object img-circle" 
             src="{{ route('imagecache', ['template' => 'profile', 'filename' => ($comment['avatar'] ? $comment['avatar'] :'profile-noimage.jpg')]) }}" 
             alt="{{$comment['autor']}}">
    </div>
    <div class="media-body">
      <h4 class="media-heading">
        {{ $comment['autor'] }} - <small>{{$comment['created_at']}}</small>
        @if(request()->get('replay_id', 0) != $comment['id'])
        <a href="{{ route('new.details', ['slug' => $comment['slug']]) }}?replay_id={{$comment['id']}}"
           class="pull-right">
          <strong>Responder</strong>
        </a>
        @endif
      </h4>
      {{ $comment['comments'] }}

      @each('layouts.sicoob.partials.comments', collect($comment['replays'])->where('status', 1), 'comment')
    </div>
  </div>