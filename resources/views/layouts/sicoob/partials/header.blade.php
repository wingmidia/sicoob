  <header>
    <div class="container-fluid">
      <div class="row ctn-cabecalho">
        <div class="col-md-3 col-lg-2">
          <a href="/"><img src="/img/logo-sicoob-credipatos.png" alt="SICOOB Credipatos" title="SICOOB Credipatos" class="img-responsive" /></a>
        </div>

        <div class="col-md-4 col-lg-6">&nbsp;</div>

        @if(Auth::check())
        <div class="col-md-5 col-lg-4 ctn-usuario-logado">
          <div class="row">
            <div class="col-lg-5 text-right">
              Olá {{ Auth::user()->name }}
            </div>

            <div class="col-lg-3 text-center">
            @if(Auth::user()->photo)
              <img src="{{ route('imagecache', ['template' => 'profile', 'filename' => Auth::user()->photo]) }}" 
                   alt="{{Auth::user()->name}}" 
                   title="{{Auth::user()->name}}" 
                   class="img-responsive img-circle borda-branca" />
            @else
              <img src="{{ route('imagecache', ['template' => 'profile', 'filename' => 'profile-noimage.jpg']) }}" 
                   alt="{{Auth::user()->name}}" 
                   title="{{Auth::user()->name}}" 
                   class="img-responsive img-circle borda-branca" />
            @endif
              
            </div>

            <div class="col-lg-4">
              <ul class="list-unstyled">
                <li><a href="javascript:void(0)" onclick="$('#logout').submit()">Sair</a></li>
                <form action="{{ route('logout') }}" method="POST" id="logout" class="hide">
                  {{ csrf_field() }}
                </form>
                <li><a href="{{ route('profile') }}">Alterar senha</a></li>
              </ul>
            </div>
          </div>
        </div>
        @else
          <div class="col-md-5 col-lg-4 ctn-usuario-logado">
            <div class="row">
              <div class="col-lg-12 text-right">
                <button class="btn btn-acessar"> <i class="fa fa-sign-in"></i>&nbsp; ACESSAR <strong>ÁREA DO COLABORADOR</strong></button>
              </div>
            </div>

            <div class="box-login sombra-caixa">
              <form class="form" role="form" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <div class="form-group">
                  <input type="email" id="email" class="form-control" name="email" value="" placeholder="E-mail" required>
                </div>

                <div class="form-group">
                  <input id="password" type="password" class="form-control" name="password" placeholder="Senha" required>
                </div>

                <button type="submit" class="btn btn-primary btn-block transicao"><strong>ENTRAR</strong></button>
                <!-- <a href="#" class="lnk-verde transicao" title="Esqueceu sua senha?">Esqueceu sua senha?</a> -->
              </form>
            </div>
          </div>
        @endif
      </div>

      <div class="row ctn-navegacao sombra-caixa">
        <div class="col-sm-12">
          <nav id="menu-navegacao" class="collapse navbar-collapse" role="navigation">
            <ul class="nav nav-pills nav-justified">
              <li>
                <a href="javascript:;" class="btMenu" title="Menu">
                  <div id="nav-hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                  </div>
                </a>
              </li>
              @foreach(getDepartaments() as $menu_departament)
              <li>
                <a href="{{ route('news.departament', [$menu_departament->slug]) }}" class="transicao">{{ mb_strtoupper($menu_departament->name) }}</a>
              </li>
              @endforeach
              @if(Entrust::hasRole(['admin', 'gerente', 'diretoria', 'ti']))
              <li class="dropdown">
                <a href="#" class="transicao">GERÊNCIA</a>
                <ul>
                  <li>
                    <a href="{{ route('clientes.index', ['type_profile' => 'F']) }}" class="transicao">PESSOA FÍSICA</a>
                  </li>
                  <li>
                    <a href="{{ route('clientes.index', ['type_profile' => 'J']) }}" class="transicao">PESSOA JURÍDICA</a>
                  </li>
                </ul>
              </li>
              @endif
              @permission(['roles', 'products', 'indication-status'])
                <li class="dropdown">
                  <a href="#" class="transicao"><i class="fa fa-cog"></i></a>
                  <ul>
                    @permission('indication-status')
                    <li>
                      <a href="{{ route('intranet.indications.status.index') }}" class="transicao">Status de indicação</a>
                    </li>
                    @endpermission
                    @permission('products')
                    <li>
                      <a href="{{ route('intranet.products.index') }}" class="transicao">Produtos</a>
                    </li>
                    @endpermission
                    @permission('roles')
                    <li>
                      <a href="{{ route('intranet.roles.index') }}" class="transicao">Perfil de usuário</a>
                    </li>
                    @endpermission
                  </ul>
                </li>
              @endpermission
            </ul>
          </nav>
        </div>
      </div>
    </div>

    <!--div class="ctn-form-contato-depto text-center transicao fechado">
      <div class="ctn-botao">
        <a href="javascript: void(0);" class="lnk-form-contato">
          <i class="icomoon icon-email"></i>
        </a>
      </div>

      <div class="ctn-form">
        <form class="form">
          <div class="form-group">
            <img src="/img/conteudo/usuario-logado.jpg" alt="Maria da Silva" title="Maria da Silva" class="
          img-responsive img-circle borda-branca">
          </div>
          
          <div class="form-group">
            <p>Olá Maria da Silva, deixe uma mensagem para o responsável do departamento</p>
          </div>
          
          <div class="form-group">
            <select class="form-control">
              <option>Selecione o departamento</option>
            </select>
          </div>

          <div class="form-group">
            <input type="text" name="assunto" id="assunto" class="form-control" />
          </div>

          <div class="form-group">
            <textarea name="mensagem" id="mensagem" class="form-control"></textarea>
          </div>

          <button name="enviar" id="enviar" class="btn btn-block">ENVIAR</button>
        </form>
      </div>
    </div-->
  </header>