<nav id="menu-principal" class="collapse menu-principal navbar-collapse" role="navigation" data-target="#menu-principal" aria-expanded="false" aria-controls="menu-principal">
  <ul class="nav">
    <li>
      <a href="/noticias" class="transicao" title="NOTÍCIAS">
        <i class="icomoon transicao icon-bullhorn"></i>
        <div>NOTÍCIAS</div>
      </a>
    </li>
    <li>
      <a href="{{ route('intranet.indications.index') }}" class="transicao" title="INDICAÇÕES E PROSPECÇÃO">
        <i class="fa transicao fa-compass"></i>
        <div>INDICAÇÕES E PROSPECÇÃO</div>
      </a>
    </li>
    <li>
      <a href="{{ route('intranet.productions.index') }}" class="transicao" title="PRODUÇÃO">
        <i class="fa transicao fa-usd"></i>
        <div>PRODUÇÃO</div>
      </a>
    </li>
    <li>
      <a href="{{ route('intranet.campaign.ranking') }}" class="transicao" title="CAMPANHAS">
        <i class="fa transicao fa-trophy"></i>
        <div>CAMPANHAS</div>
      </a>
    </li>
    <li>
      <a href="{{ route('intranet.quiz.index') }}" class="transicao" title="QUIZZES">
        <i class="fa transicao fa-question-circle"></i>
        <div>QUIZZES</div>
      </a>
    </li>
    <li>
      <a href="/rh-contra-cheques" class="transicao" title="RH - CONTRA CHEQUES">
        <i class="icomoon icon-note"></i>
        <div>RH - CONTRA CHEQUES</div>
      </a>
    </li>
    <li>
      <a href="/eventos" class="transicao" title="EVENTOS">
        <i class="icomoon transicao icon-calendar"></i>
        <div>EVENTOS</div>
      </a>
    </li>
    <li>
      <a href="/forum" class="transicao" title="FÓRUM">
        <i class="icomoon transicao icon-messages"></i>
        <div>FÓRUM</div>
      </a>
    </li>
    <li>
      <a href="/documentos" class="transicao" title="DOCUMENTOS">
        <i class="icomoon transicao icon-file-text2"></i>
        <div>DOCUMENTOS</div>
      </a>
    </li>
    <li>
      <a href="/links-importantes" class="transicao" title="LINKS IMPORTANTES">
        <i class="icomoon transicao icon-link"></i>
        <div>LINKS IMPORTANTES</div>
      </a>
    </li>
@if(!Entrust::hasRole(['marketing', 'rh']))
    <li>
      <a href="/atividades" class="transicao" title="ATIVIDADES">
        <i class="icomoon transicao icon-cog-outline"></i>
        <div>ATIVIDADES</div>
      </a>
    </li>
@endif
    <li>
      <a href="/produtos" class="transicao" title="PRODUTOS">
        <i class="icomoon transicao icon-bag"></i>
        <div>PRODUTOS</div>
      </a>
    </li>
  </ul>
</nav>