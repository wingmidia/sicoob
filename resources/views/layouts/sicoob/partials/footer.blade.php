
  <footer>
    <div class="container-fluid">
      <div class="row ctn-rodape">
        <div class="col-sm-2">
          <a href="/"><img src="/img/logo-sicoob-credipatos-branco.png" alt="SICOOB Credipatos" title="SICOOB Credipatos" class="img-responsive" /></a>
        </div>
        
        <div class="col-sm-7">
          <nav class="navegacao-rodape">
            <ul class="nav nav-pills">
              <li>
                <a href="http://www.sicoobcredipatos.com.br/index.php?page=1&id_institucional=1" target="_blank">INSTITUCIONAL</a>
              </li>
              <li>
                <a href="http://www.sicoobcredipatos.com.br/index.php?page=3" target="_blank">JORNAL</a>
              </li>
              <li>
                <a href="http://www.sicoobcredipatos.com.br/index.php?page=7" target="_blank">AGÊNCIAS</a>
              </li>
              <li>
                <a href="https://www.facebook.com/credipatos/" target="_blank">REDES SOCIAIS</a>
              </li>
            </ul>
          </nav>
        </div>
        
        <div class="col-sm-3 text-right">
          FALE COM A GENTE (34) 3818-2699
        </div>
      </div>
    </div>
  </footer>
 