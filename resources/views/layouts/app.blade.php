<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Integra CMS') }}</title>

    <!-- Styles -->
    <!-- BEGIN STYLESHEETS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
    <!--link type="text/css" rel="stylesheet" href="/assets/css/theme-default/bootstrap.css?1422792965" />
    <link type="text/css" rel="stylesheet" href="/assets/css/theme-default/materialadmin.css?1425466319" />
    <link type="text/css" rel="stylesheet" href="/assets/css/theme-default/font-awesome.min.css?1422529194" />
    <link type="text/css" rel="stylesheet" href="/assets/css/theme-default/material-design-iconic-font.min.css?1421434286" /-->

    <!--link type="text/css" rel="stylesheet" href="/assets/css/theme-default/libs/bootstrap-datepicker/datepicker3.css?1424887858" /-->
    <!--link type="text/css" rel="stylesheet" href="/assets/css/theme-2/libs/summernote/summernote.css" /-->
    
    <link href="{{ asset('css/admin/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body class="menubar-hoverable header-fixed menubar-first menubar-pin  ">
    <!-- BEGIN BASE-->
    <div id="app">      

    </div><!--end #base-->
    <!-- END BASE -->

    <!-- Scripts -->
    <script src="{{ asset('js/admin/app.js') }}"></script>

    <!--script src="assets/js/libs/jquery/jquery-1.11.2.min.js"></script-->
    <script src="assets/js/libs/jquery/jquery-migrate-1.2.1.min.js"></script>
    <!--script src="assets/js/libs/bootstrap/bootstrap.min.js"></script-->
    <script src="assets/js/libs/spin.js/spin.min.js"></script>
    <script src="assets/js/libs/autosize/jquery.autosize.min.js"></script>
    <!--script src="assets/js/libs/summernote/summernote.min.js"></script>
    <script src="assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    <script src="assets/js/libs/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.js"></script-->
    <script src="assets/js/libs/nanoscroller/jquery.nanoscroller.min.js"></script>
    <script src="assets/js/core/source/App.js"></script>
    <script src="assets/js/core/source/AppNavigation.js"></script>
    <script src="assets/js/core/source/AppOffcanvas.js"></script>
    <script src="assets/js/core/source/AppCard.js"></script>
    <script src="assets/js/core/source/AppForm.js"></script>
    <script src="assets/js/core/source/AppNavSearch.js"></script>
    <script src="assets/js/core/source/AppVendor.js"></script>
    <script src="assets/js/core/demo/Demo.js"></script>

</body>
</html>
