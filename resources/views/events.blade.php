@extends('layouts.sicoob.default')

@section('arquivos_estilos')
<link rel="stylesheet" href="/css/jquery-ui.css">
@endsection

@section('arquivos_scripts')
<script src="/js/jquery.meio.mask.min.js"></script>
<script src="/js/jquery-ui.js"></script>
@endsection

@section('blocos_script')
// Calendario para os campos de data
      $( ".date-picker" ).datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
        dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
        monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez']
      });

      // Máscaras para campos dos formulários
      $('.fmtData').setMask('date');
@endsection

@section('content')
  <div class="fundo-branco sombra-caixa ctn-eventos">
    <h1 class="titulo-janelas">
      <i class="icomoon icon-calendar"></i>
      <div>EVENTOS</div>
    </h1>

    <form class="form-inline frm-busca">
      <div class="row">
        <div class="col-sm-12 col-md-5 col-lg-6">
          <div class="form-group">
            <div class="input-group">
              <input type="search" size="130" name="search" id="buscar-evento" value="{{ request()->get('search') }}" class="form-control" placeholder="Busca" />
              <div class="input-group-addon">
                <i class="fa fa-search"></i>
              </div>
            </div>
          </div>
        </div>

        <div class="col-sm-12 col-md-7 col-lg-6">
          <div class="form-group">
            <label for="data-inicial">Período:</label>
            <div class="input-group">
              <input type="text" name="start_date" id="data-inicial"  value="{{ request()->get('start_date') }}" class="date-picker fmtData form-control" size="8" />
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
            </div>

            à 

            <div class="input-group">
              <input type="text" name="end_date" id="data-final"  value="{{ request()->get('end_date') }}" class="date-picker fmtData form-control" size="8" />
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
            </div>
          </div>

          <div class="form-group">
            <button class="btn btn-buscar transicao">BUSCAR</button>
          </div>
        </div>
      </div>
    </form>

    <div class="lista borda-topo-cinza">
      @foreach($events as $event)
      <div class="media item">
        <a href="{{ route('event.details', ['slug' => $event->slug]) }}">
          <div class="media-left media-middle ctn-imagem data">
            <img src="{{ route('imagecache', ['filename' => $event->image_feacture, 'template' => 'event']) }}" alt="{{ $event->title }}" title="{{ $event->title }}" class="media-object transicao" />
            
            <div class="box-data">
              <div class="data-dia">{{ $event->event_date->format('d') }}</div>
              <div class="data-mes">{{ $event->event_date->format('m') }}</div>
            </div>
          </div>

          <div class="media-body detalhes">
            <h2 class="titulo media-heading">{{ $event->title }}</h2>

            <div class="info">
              <span><i class="fa fa-map-marker"></i></span> SICOOB CREDPATOS{{-- ' - '.$event->start_date->format('H:i') --}}
            </div>

            <p>
              {{ str_limit($event->description, 200) }}
            </p>

            <button class="btn transicao">VEJA MAIS</button>
          </div>
        </a>
      </div>
      @endforeach

      {{ $events->links() }}
    </div>
  </div>
@endsection