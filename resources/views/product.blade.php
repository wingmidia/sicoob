@extends('layouts.sicoob.default')

@section('content')
  <div class="fundo-branco sombra-caixa ctn-produtos">
    <h1 class="titulo-janelas">
      <i class="icomoon icon-bag"></i>
      <div>PRODUTOS - {{ $product->title }}</div>
    </h1>

    <div class="conteudo-interno borda-topo-cinza">
      <div class="row">
        <div class="col-sm-12">
          <img src="{{ route('imagecache', ['filename' => $product->image_feacture, 'template' => 'product']) }}" alt="{{ $product->title }}" title="{{ $product->title }}" class="img-responsive" align="left" vspace="10" hspace="20" />

          <h2 class="titulo-interno">{{ $product->title }}</h2>

          <div class="conteudo">
            {!! $product->content !!}
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection