<form class="form-inline frm-busca">
      <div class="row">
        <div class="col-md-4">
          <div class="form-group">
            <div class="input-group">
              <input type="input" size="100" name="search" id="buscar-atividade" class="form-control" value="{{ request()->get('search', null) }}" placeholder="Busca" />
              <div class="input-group-addon">
                <i class="fa fa-search"></i>
              </div>
            </div>
          </div>
        </div>

        <div class="col-sm-12 col-md-8">
          <div class="form-group">
            <label for="data-inicial">Período:</label>
            <div class="input-group">
              <input type="text" name="start_date" id="data-inicial" value="{{ request()->get('start_date', null) }}" class="date-picker fmtData form-control" size="8" />
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
            </div>

            <label for="data-final">à</label>

            <div class="input-group">
              <input type="text" name="end_date" id="data-final" value="{{ request()->get('end_date', null) }}" class="date-picker fmtData form-control" size="8" />
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label for="status">Situação:</label>
            <select name="status" id="status-atividade" class="form-control">
              <option value="">Selecione: </option>
              @foreach(statusActivity() as $key => $name)
              <option value="{{$key}}" {{ $key == request()->get('status', null) ? 'selected' : null }}>{{$name}}</option>
              @endforeach
            </select>
          </div>

          <div class="form-group">
            <button class="btn btn-buscar transicao">BUSCAR</button>
          </div>
        </div>
      </div>
    </form>