@extends('layouts.sicoob.default')


@section('content')
  <div class="row" style="margin-bottom: 30px">
    <div class="col-md-6 col-md-offset-3">
        <div class="fundo-branco sombra-caixa ctn-login">
            <h1 class="titulo-janelas">
              <i class="icomoon icon-cog-outline"></i>
              <div>404 - Página não encontrada</div>
            </h1>
        </div>
    </div>
  </div>
@endsection