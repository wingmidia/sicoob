// reverse
Vue.filter('dateFormat', function(value, format) {
  // slice to make a copy of array, then reverse the copy
  return moment(value).format(format);
});


// reverse
Vue.filter('refMask', function(val) {
  var response;
	switch(val){
		case 0: response = 'Janeiro'; break;
		case 1: response = 'Fevereiro'; break;
		case 2: response = 'Março'; break;
		case 3: response = 'Abril'; break;
		case 4: response = 'Maio'; break;
		case 5: response = 'Junho'; break;
		case 6: response = 'Julho'; break;
		case 7: response = 'Agosto'; break;
		case 8: response = 'Setembro'; break;
		case 9: response = 'Outubro'; break;
		case 10: response = 'Novembro'; break;
		case 11: response = 'Dezembro'; break;
	}
	return response;
});
