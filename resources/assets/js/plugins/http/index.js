import axios from 'axios'
import interceptors from './interceptors'
//import { apiUrl } from '../../config'
const IS_LOCAL = process.env.NODE_ENV !== 'production'
// allow use http client without Vue instance
export const http = axios.create({
  //baseURL: IS_LOCAL ? 'http://sicoob.dev/api/' : 'http://172.19.69.200/api/',
  baseURL: 'http://172.19.69.200/api/',
})

/**
* Helper method to set the header with the token
*/
export function setToken(token) {
  http.defaults.headers.common.Authorization = `Bearer ${token}`
}

// receive store and data by options
// https://vuejs.org/v2/guide/plugins.html
export default function install(Vue, { store, router }) {
  interceptors(http, store, router)
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty
  Object.defineProperty(Vue.prototype, '$http', {
    get() {
      return http
    },
  })
}
