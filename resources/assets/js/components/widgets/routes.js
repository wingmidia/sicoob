
/**
* Components are lazy-loaded - See "Grouping Components in the Same Chunk"
* http://router.vuejs.org/en/advanced/lazy-loading.html
*/
/* eslint-disable global-require */
import Main from './Main'
import List from './Table'
import Form from './Form'

const meta = {
  requiresAuth: true,
}

export default [
  {
    name: 'widgets.index',
    path: '/widgets',
    component: Main,
    //redirect: { name: 'departaments.list' },
    meta,
    children: [
      {
        name: 'widgets.new',
        path: 'create',
        component: Form,
        meta,
      }, {
        name: 'widgets.edit',
        path: ':id/edit',
        component: Form,
        meta,
      },
    ],
  }
]
