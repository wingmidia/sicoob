
/**
* Components are lazy-loaded - See "Grouping Components in the Same Chunk"
* http://router.vuejs.org/en/advanced/lazy-loading.html
*/
/* eslint-disable global-require */
import Main from './Main'
import List from './Table'
import Form from './Form'

const meta = {
  requiresAuth: true,
}

export default [
  {
    name: 'documents.index',
    path: '/documents',
    component: Main,
    redirect: { name: 'documents.list' },
    meta,
    children: [
      {
        name: 'documents.list',
        path: 'list',
        component: List,
        meta,
      },
      {
        name: 'documents.new',
        path: 'create',
        component: Form,
        meta,
      }
    ],
  },
]
