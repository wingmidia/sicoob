
/**
* Components are lazy-loaded - See "Grouping Components in the Same Chunk"
* http://router.vuejs.org/en/advanced/lazy-loading.html
*/
/* eslint-disable global-require */
import Main from './Main'
import List from './Table'
import Form from './Form'
import Photos from './Photos'

const meta = {
  requiresAuth: true,
}

export default [
  {
    name: 'news.index',
    path: '/news',
    component: Main,
    redirect: { name: 'news.list' },
    meta,
    children: [
      {
        name: 'news.list',
        path: 'list',
        component: List,
        meta,
      },
      {
        name: 'news.new',
        path: 'create',
        component: Form,
        meta,
      }, {
        name: 'news.edit',
        path: ':id/edit',
        component: Form,
        meta,
      }, {
        name: 'news.photos',
        path: ':id/photos',
        component: Photos,
        meta,
      },
    ],
  },
]
