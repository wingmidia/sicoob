
/**
* Components are lazy-loaded - See "Grouping Components in the Same Chunk"
* http://router.vuejs.org/en/advanced/lazy-loading.html
*/
/* eslint-disable global-require */
import Main from './Main'
import List from './Table'
import Form from './Form'

const meta = {
  requiresAuth: true,
}

export default [
  {
    name: 'roles.index',
    path: '/roles',
    component: Main,
    redirect: { name: 'roles.list' },
    meta,
    children: [
      {
        name: 'roles.list',
        path: 'list',
        component: List,
        meta,
      },
      {
        name: 'roles.new',
        path: 'create',
        component: Form,
        meta,
      }, {
        name: 'roles.edit',
        path: ':id/edit',
        component: Form,
        meta,
      },
    ],
  },
]
