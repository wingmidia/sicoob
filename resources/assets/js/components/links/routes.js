
/**
* Components are lazy-loaded - See "Grouping Components in the Same Chunk"
* http://router.vuejs.org/en/advanced/lazy-loading.html
*/
/* eslint-disable global-require */
import Main from './Main'
import List from './Table'
import Form from './Form'

const meta = {
  requiresAuth: true,
}

export default [
  {
    name: 'links.index',
    path: '/links',
    component: Main,
    redirect: { name: 'links.list' },
    meta,
    children: [
      {
        name: 'links.list',
        path: 'list',
        component: List,
        meta,
      },
      {
        name: 'links.new',
        path: 'create',
        component: Form,
        meta,
      }, {
        name: 'links.edit',
        path: ':id/edit',
        component: Form,
        meta,
      },
    ],
  },
]
