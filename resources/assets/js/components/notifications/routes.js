
/**
* Components are lazy-loaded - See "Grouping Components in the Same Chunk"
* http://router.vuejs.org/en/advanced/lazy-loading.html
*/
/* eslint-disable global-require */
import Main from './Main'
import Form from './Form'

const meta = {
  requiresAuth: true,
}

export default [
  {
    name: 'notifications.index',
    path: '/notifications',
    component: Main,
    meta,
    children: [
      {
        name: 'notifications.new',
        path: 'create',
        component: Form,
        meta,
      }, 
      {
        name: 'notifications.edit',
        path: ':id/edit',
        component: Form,
        meta,
      }
    ],
  }
]
