
/**
* Components are lazy-loaded - See "Grouping Components in the Same Chunk"
* http://router.vuejs.org/en/advanced/lazy-loading.html
*/
/* eslint-disable global-require */
import Main from './Main'
import List from './Table'
import Form from './Form'

const meta = {
  requiresAuth: true,
}

export default [
  {
    name: 'banners.index',
    path: '/banners',
    component: Main,
    redirect: { name: 'banners.list' },
    meta,
    children: [
      {
        name: 'banners.list',
        path: 'list',
        component: List,
        meta,
      },
      {
        name: 'banners.new',
        path: 'create',
        component: Form,
        meta,
      }, {
        name: 'banners.edit',
        path: ':id/edit',
        component: Form,
        meta,
      },
    ],
  },
]
