
/**
* Components are lazy-loaded - See "Grouping Components in the Same Chunk"
* http://router.vuejs.org/en/advanced/lazy-loading.html
*/
/* eslint-disable global-require */
import Main from './Main'
import List from './Table'
import Form from './Form'

const meta = {
  requiresAuth: true,
}

export default [
  {
    name: 'collaborators.index',
    path: '/collaborators',
    component: Main,
    redirect: { name: 'collaborators.list' },
    meta,
    children: [
      {
        name: 'collaborators.list',
        path: 'list',
        component: List,
        meta,
      },
      {
        name: 'collaborators.new',
        path: 'create',
        component: Form,
        meta,
      }, {
        name: 'collaborators.edit',
        path: ':id/edit',
        component: Form,
        meta,
      },
    ],
  },
]
