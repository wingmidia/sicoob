
/**
* Components are lazy-loaded - See "Grouping Components in the Same Chunk"
* http://router.vuejs.org/en/advanced/lazy-loading.html
*/
/* eslint-disable global-require */
import Main from './Main'
import List from './Table'
import Form from './Form'

const meta = {
  requiresAuth: true,
}

export default [
  {
    name: 'products.index',
    path: '/products',
    component: Main,
    redirect: { name: 'products.list' },
    meta,
    children: [
      {
        name: 'products.list',
        path: 'list',
        component: List,
        meta,
      },
      {
        name: 'products.new',
        path: 'create',
        component: Form,
        meta,
      }, {
        name: 'products.edit',
        path: ':id/edit',
        component: Form,
        meta,
      },
    ],
  },
]
