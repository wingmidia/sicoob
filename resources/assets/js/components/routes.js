// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import
import { routes as news } from './news'
import { routes as departaments } from './departaments'
import { routes as dashboard } from './dashboard'
import { routes as links } from './links'
import { routes as banners } from './banners'
import { routes as products } from './products'
//import { routes as forum } from './forum'
//import { routes as forum_categories } from './forum/categories'
import { routes as collaborators } from './collaborators'
import { routes as events } from './events'
import { routes as roles } from './roles'
import { routes as paychecks } from './paychecks'
import { routes as notifications } from './notifications'
import { routes as users } from './users'
import { routes as documents } from './documents'
import { routes as auth } from './auth'
import { routes as widgets } from './widgets'

// https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Operators/Spread_operator
// Thus a new array is created, containing all objects that match the routes.
// ...dashboard must be the last one because of the catchall instruction
export default [...auth, ...widgets, ...departaments, ...documents, ...paychecks, ...notifications, ...news, ...products, ...events, ...collaborators, ...links, ...banners, ...dashboard]
