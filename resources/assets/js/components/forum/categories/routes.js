
/**
* Components are lazy-loaded - See "Grouping Components in the Same Chunk"
* http://router.vuejs.org/en/advanced/lazy-loading.html
*/
/* eslint-disable global-require */
import Main from './Main'
import Form from './Form'

const meta = {
  requiresAuth: true,
}

export default [
  {
    name: 'forum_category.index',
    path: '/forum/categories',
    component: Main,
    meta,
    children: [
      {
        name: 'forum_category.new',
        path: 'create',
        component: Form,
        meta,
      }, 
      {
        name: 'forum_category.edit',
        path: ':id/edit',
        component: Form,
        meta,
      }
    ],
  }
]
