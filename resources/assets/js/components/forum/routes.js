
/**
* Components are lazy-loaded - See "Grouping Components in the Same Chunk"
* http://router.vuejs.org/en/advanced/lazy-loading.html
*/
/* eslint-disable global-require */
import Main from './Main'
import List from './Table'
import Form from './Form'

const meta = {
  requiresAuth: true,
}

export default [
  {
    name: 'forum.index',
    path: '/forum',
    component: Main,
    redirect: { name: 'forum.topics' },
    meta,
    children: [
      {
        name: 'forum.topics',
        path: 'topics',
        component: List,
        meta,
      },
      {
        name: 'forum.posts',
        path: ':id/posts',
        component: List,
        meta,
      },
      {
        name: 'forum.new',
        path: 'create',
        component: Form,
        meta,
      }, 
      {
        name: 'forum.edit',
        path: ':id/edit',
        component: Form,
        meta,
      }
    ],
  },
]
