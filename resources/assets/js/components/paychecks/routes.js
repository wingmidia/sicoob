
/**
* Components are lazy-loaded - See "Grouping Components in the Same Chunk"
* http://router.vuejs.org/en/advanced/lazy-loading.html
*/
/* eslint-disable global-require */
import Main from './Main'
import List from './Table'
import Form from './Form'
import Multiple from './Multiple'

const meta = {
  requiresAuth: true,
}

export default [
  {
    name: 'paychecks.index',
    path: '/paychecks',
    component: Main,
    redirect: { name: 'paychecks.list' },
    meta,
    children: [
      {
        name: 'paychecks.list',
        path: 'list',
        component: List,
        meta,
      },
      {
        name: 'paychecks.new',
        path: 'create',
        component: Form,
        meta,
      }, 
      {
        name: 'paychecks.multiple',
        path: 'multiple',
        component: Multiple,
        meta,
      }, 
      {
        name: 'paychecks.edit',
        path: ':id/edit',
        component: Form,
        meta,
      },
    ],
  },
]
