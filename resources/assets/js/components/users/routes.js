
/**
* Components are lazy-loaded - See "Grouping Components in the Same Chunk"
* http://router.vuejs.org/en/advanced/lazy-loading.html
*/
/* eslint-disable global-require */
import Main from './Main'
import List from './Table'
import Form from './Form'

const meta = {
  requiresAuth: true,
}

export default [
  {
    name: 'users.index',
    path: '/users',
    component: Main,
    redirect: { name: 'users.list' },
    meta,
    children: [
      {
        name: 'users.list',
        path: 'list',
        component: List,
        meta,
      },
      {
        name: 'users.new',
        path: 'create',
        component: Form,
        meta,
      }, {
        name: 'users.edit',
        path: ':id/edit',
        component: Form,
        meta,
      },
    ],
  },
]
