/**
* Components are lazy-loaded - See "Grouping Components in the Same Chunk"
* http://router.vuejs.org/en/advanced/lazy-loading.html
*/
/* eslint-disable global-require */
import Main from '../components/main'
import Singin from '../components/forms/singin'
import Singup from '../components/forms/singup'

const children = [{
  name: 'auth.singin',
  path: 'singin',
  component: Singin,
  meta: { requiresAuth: false },
}, /*{
  name: 'auth.singup',
  path: 'singup',
  component: Singup,
  meta: { requiresAuth: false },
}*/]

export default [{
  children,
  name: 'auth',
  path: '/auth',
  component: Main,
  redirect: { name: 'auth.singin' },
  meta: { requiresAuth: false },
}]
