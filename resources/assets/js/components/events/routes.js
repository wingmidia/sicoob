
/**
* Components are lazy-loaded - See "Grouping Components in the Same Chunk"
* http://router.vuejs.org/en/advanced/lazy-loading.html
*/
/* eslint-disable global-require */
import Main from './Main'
import List from './Table'
import Form from './Form'
import Photos from './Photos'

const meta = {
  requiresAuth: true,
}

export default [
  {
    name: 'events.index',
    path: '/events',
    component: Main,
    redirect: { name: 'events.list' },
    meta,
    children: [
      {
        name: 'events.list',
        path: 'list',
        component: List,
        meta,
      },
      {
        name: 'events.new',
        path: 'create',
        component: Form,
        meta,
      }, {
        name: 'events.edit',
        path: ':id/edit',
        component: Form,
        meta,
      }, {
        name: 'events.photos',
        path: ':id/photos',
        component: Photos,
        meta,
      },
    ],
  },
]
