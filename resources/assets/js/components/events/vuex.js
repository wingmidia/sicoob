import * as TYPES from '../../store/types'

const state = {
    news: [],
    newSelected: {},
    search: '',
    page: 1
}

/**
    GETTERS
*/
const getters = {
  news: state => state.news,
  search: state => state.search
}

const actions = {

    getNews ({ commit }, page) {
        
        return this.$http.get('news', {
                params: {
                    page: page, 
                    search: state.search
                }
            });
    },

    storeNew({ commit }, news) {
        this.$http.put('news', news).then(res => {
            var response = res.data.data
            
            commit(TYPES.UPDATE_NEW, response.data)
        })
    },

    updateNew({ commit }, news) {
        this.$http.put('news/'+news.id, news).then(res => {
            var response = res.data.data
            
            commit(TYPES.UPDATE_NEW, response.data)
        })
    },

    deleteNew({dispatch, commit}, new_id){ 
        return this.$http.delete('news/'+new_id);
    },

}

/**
    MUTATIONS
*/
const mutations = {
    [TYPES.RECEIVE_NEWS] (state, news) {
        state.news = [...state.news, ...news]
    },

    [TYPES.SELECT_NEW] (state, data) {
        state.news_seleted = data
    },

    [TYPES.UPDATE_NEW] (state, data) {        
        //state.news = news
    },

    // mutate variables    
    [TYPES.SET_PAGE_NEWS] (state) {
        state.page_news++;    
    }
}



var module = {
    state,
    getters,
    mutations,
    actions
}

export default { module }