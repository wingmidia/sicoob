
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

//import VueBreadcrumbs from 'vue-breadcrumbs'
//Vue.use(VueBreadcrumbs)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('comments', require('./components/news/Comments.vue'));

Vue.component('summernote', require('./components/components/Summernote.vue'));

import { sync } from 'vuex-router-sync'
import Root from './Root' 
import store from './store' // vuex store instance
import router from './router' // vue-router instance

import httpPlugin from './plugins/http'
Vue.use(httpPlugin, { store, router })

sync(store, router) // done.

const app = new Vue({
    el: '#app',
    store,
		router,
		render: h => h(Root),
});
