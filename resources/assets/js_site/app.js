
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

//import VueBreadcrumbs from 'vue-breadcrumbs'
//Vue.use(VueBreadcrumbs)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('folders', require('./components/documents/Folders.vue'));
Vue.component('documents', require('./components/documents/Main.vue'));

import Root from './Root' 

import httpPlugin from './plugins/http'
Vue.use(httpPlugin)

import { sync } from 'vuex-router-sync'

import store from './store' // vuex store instance
import router from './router' // vue-router instance

sync(store, router) // done.

const app = new Vue({
    el: '#app',
    store,
		router,
  	render: h => h(Root)
});
