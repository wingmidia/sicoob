import * as TYPES from './mutations-types'
import { http } from '../../../plugins/http'
import { getData } from '../../../../js/utils/get'

const state = {
    documents: [],
    folder: '',
    parent: '',
    isAdmin: false
}

/**
    GETTERS
*/
const getters = {
  documents: state => state.documents,
  folder: state => state.folder,
  parent: state => state.parent,
  isAdmin: state => state.isAdmin
}

const actions = {

    getDocuments ({ commit }, folder = '') {
        let params = {
            params: {
                folder: folder
            }
        }
        
        return http.get('documentos/root/', params).then(res => {
            
            var documents = res.data.data
            commit(TYPES.UPDATE_DOCUMENT, documents) 

            commit(TYPES.SET_FOLDER, folder)

        });
    },

    getDirectories({ commit }){
        return http.get('documentos/directories')
                   .then(getData)
                   .then(data => {
                     var directories = data.data
                     commit(TYPES.SET_DIRECTORIES, directories)
                   });
    },

    treeView({ commit }){
        return http.get('documentos/tree-view')
                   .then(getData)
                   .then(data => {
                        var treeView = data.data
                        
                        commit(TYPES.SET_TREEVIEW, treeView)
                   });
    },

    storeDocument({ commit }, document) {
        http.put('documentos', document).then(res => {
            var response = res.data.data
            
            commit(TYPES.UPDATE_DOCUMENT, response.data)
        })
    },

    move({ dispatch }, {origin, dest, name, type})
    {
        http.put('documentos/move', {origin, dest, name, type}).then(data => {
            dispatch('getDocuments', state.folder) 
        })
    },

    copy({ dispatch }, {origin, dest, name, type})
    {
        http.put('documentos/copy', {origin, dest, name, type});
    },

    createFolder({ commit }, new_folder) {
        let directory = state.folder ? state.folder : '/'
        
        http.post('documentos/folder', {directory, new_folder}).then(res => {
            var file = res.data.data
            
            commit(TYPES.ADD_FILES, file)
        })
    },

    addFiles({ commit }, file){
        commit(TYPES.ADD_FILES, file)
    },


    renameDocument({ commit }, payload) {

        http.put('documentos/rename', { original: payload.original.path, target: payload.target }).then(res => {
            var response = res.data.data
            
            commit(TYPES.UPDATE_NAME, payload)
        })
    },

    deleteDocument({dispatch, commit}, payload){ 

        return http.put('documentos/delete', payload).then(res => {
            var response = res.data.data
            commit(TYPES.DELETE_DOCUMENT, payload)
        });
    },

    checkAdmin({ commit }){
        http.get('me').then(res =>{
            var isAdmin = res.data.isAdmin
            commit(TYPES.IS_ADMIN, isAdmin) 
        })
    }

}

/**
    MUTATIONS
*/
const mutations = {
    [TYPES.RECEIVE_DOCUMENTS] (state, documents) {
        state.documents = [...documents]
    },

    [TYPES.ADD_FILES] (state, file) {
        state.documents.push(file)
    },

    [TYPES.UPDATE_DOCUMENT] (state, documents) {        
        state.documents = documents
    },

    [TYPES.DELETE_DOCUMENT] (state, data) {
        _.remove(state.documents,  (n) => {
              return n.id == data.id;
            });

        state.documents = [...state.documents]
    },
    
    [TYPES.UPDATE_NAME] (state, payload){
        var file = payload.original
        var rename = payload.target        
        var index = state.documents.indexOf(file)
        
        if(file.type == 'folder'){
            file.filename = rename
        } else {
            file.filename = rename.replace('.'+file.extension, '')+'.'+file.extension    
        }

        state.documents[index] = file
        state.documents = [...state.documents, {}]
        var documents = _.remove(state.documents, p => {            
              return p.id;
            })

        state.documents = documents
    },

    [TYPES.SET_FOLDER] (state, folder) {        
        state.folder = folder
    },

    [TYPES.IS_ADMIN] (state, isAdmin) {        
        state.isAdmin = isAdmin
    }
}



var module = {
    state,
    getters,
    mutations,
    actions
}

export default { module }