// http://vuex.vuejs.org/en/mutations.html#using-constants-for-mutation-types
export const RECEIVE_DOCUMENTS = 'RECEIVE_DOCUMENTS'
export const UPDATE_DOCUMENT = 'UPDATE_DOCUMENT'
export const UPDATE_NAME = 'UPDATE_NAME'
export const DELETE_DOCUMENT = 'DELETE_DOCUMENT'
export const SET_FOLDER = 'SET_FOLDER'
export const ADD_FILES = 'ADD_FILES'
export const IS_ADMIN = 'IS_ADMIN'



