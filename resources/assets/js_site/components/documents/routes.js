
/**
* Components are lazy-loaded - See "Grouping Components in the Same Chunk"
* http://router.vuejs.org/en/advanced/lazy-loading.html
*/
/* eslint-disable global-require */
import Main from './Main'
import Documents from './Documents'

export default [
  {
    name: 'documents.index',
    path: '/documents',
    component: Main
  },
  {
    name: 'catchall',
    path: '*',
    component: Main
  },
]
