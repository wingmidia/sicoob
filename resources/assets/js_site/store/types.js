//news matitions

export const RECEIVE_NEWS  = 'RECEIVE_NEWS'
export const UPDATE_NEW    = 'UPDATE_NEW'
export const SELECT_NEW    = 'SELECT_NEW'
export const SET_PAGE_NEWS = 'SET_PAGE_NEWS'


export const MAIN_SET_MESSAGE = 'MAIN_SET_MESSAGE'
export const MAIN_SET_FETCHING = 'MAIN_SET_FETCHING'

export const CATEGORIES_SET_DATA = 'CATEGORIES_SET_DATA'
export const PRODUCTS_SET_DATA = 'PRODUCTS_SET_DATA'

