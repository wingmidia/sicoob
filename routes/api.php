<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
Route::post('/auth/token/issue', 'Admin\AuthController@issueToken');
Route::post('/auth/token/refresh', 'Admin\AuthController@refreshToken');

Route::get('/departaments/combobox', 'Admin\DepartamentsController@combobox');
Route::get('/collaborators/combobox', 'Admin\CollaboratorsController@combobox');

Route::group(['middleware' => 'jwt.auth'], function () {
	Route::resource('/departaments', 'Admin\DepartamentsController', ['except' => ['show', 'create']]);

	Route::post('/links/upload', 'Admin\LinksController@upload');
	Route::resource('/links', 'Admin\LinksController', ['except' => ['show', 'create']]);

	Route::post('/events/upload', 'Admin\EventsController@upload');
	Route::resource('/events', 'Admin\EventsController', ['except' => ['show', 'create']]);

	Route::post('/events/photos/upload', 'Admin\EventsPhotosController@upload');
	Route::resource('/events/photos', 'Admin\EventsPhotosController', ['except' => ['show', 'edit', 'update', 'create', 'store']]);

	Route::post('/collaborators/upload', 'Admin\CollaboratorsController@upload');
	Route::resource('/collaborators', 'Admin\CollaboratorsController', ['except' => ['show', 'create']]);

	Route::post('/products/upload', 'Admin\ProductsController@upload');
	Route::resource('/products', 'Admin\ProductsController', ['except' => ['show', 'create']]);


	Route::get('/roles/combobox', 'Admin\RolesController@combobox');
	Route::get('/roles/permissions', 'Admin\RolesController@permissions');
	Route::resource('/roles', 'Admin\RolesController', ['except' => ['show', 'create']]);

	Route::post('/news/upload', 'Admin\NewsController@upload');
	Route::resource('/news', 'Admin\NewsController', ['except' => ['show', 'create']]);

	Route::post('/news/photos/upload', 'Admin\NewsPhotosController@upload');
	Route::resource('/news/photos', 'Admin\NewsPhotosController', ['except' => ['show', 'edit', 'update', 'create', 'store']]);

	Route::resource('/news/comments', 'Admin\NewsCommentsController', ['except' => ['show', 'create']]);

	Route::resource('/notifications', 'Admin\NotificationsController', ['except' => ['show', 'create']]);

	Route::post('/paychecks/upload', 'Admin\PaychecksController@upload');
	Route::delete('/paychecks/trash', 'Admin\PaychecksController@destroyFile');
	Route::resource('/paychecks', 'Admin\PaychecksController', ['except' => ['show', 'create']]);

	Route::post('/banners/upload', 'Admin\BannersController@upload');
	Route::resource('/banners', 'Admin\BannersController', ['except' => ['show', 'create']]);

	Route::post('/documents/upload', 'Admin\DocumentsController@upload');
	Route::resource('/documents', 'Admin\DocumentsController', ['except' => ['show', 'create', 'store', 'update', 'edit']]);

	Route::resource('/widgets', 'Admin\WidgetsController', ['except' => ['show', 'create']]);

	Route::resource('/users', 'Admin\UsersController', ['except' => ['show', 'create']]);
});