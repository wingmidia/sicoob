<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

Route::get('/', 'HomeController@index');
Route::get('/widgets/{id}', 'HomeController@getWidget')->name('widgets');

Route::get('/atividade/cadastro', 'ActivitiesController@register');
Route::get('/atividade/{id}', 'ActivitiesController@details')->name('activity.details');
Route::post('/atividades/novo', 'ActivitiesController@store')->name('activity.store');
Route::delete('/atividades/{id}', 'ActivitiesController@destroy');
Route::get('/atividades', 'ActivitiesController@index')->name('activity.index');


Route::get('/departamento/{slug}', 'NewsController@index')->name('news.departament');
Route::post('/noticia/comment', 'NewsController@comment')->name('news.comment');
Route::get('/noticia/{slug}', 'NewsController@details')->name('new.details');
Route::get('/noticias', 'NewsController@index')->name('news');

Route::get('/evento/{slug}', 'EventsController@details')->name('event.details');
Route::get('/eventos', 'EventsController@index')->name('events');


Route::get('/produtos/{id}', 'ProductsController@details')->name('product.details');
Route::get('/produtos', 'ProductsController@index');

Route::get('/links-importantes', 'LinksController@index');

Route::get('/contra-cheques/visualizacao/{filename}', 'PaychecksController@view')->name('paycheck.view');
Route::get('/contra-cheques/download/{filename}', 'PaychecksController@download')->name('paycheck.download');
Route::get('/rh-contra-cheques', 'PaychecksController@index');

Route::get('/aniversariantes', 'BirthsController@index')->name('births.index');

Route::put('/documentos/delete', 'DocumentsController@delete');
Route::put('/documentos/rename', 'DocumentsController@rename');
Route::put('/documentos/move', 'DocumentsController@move');
Route::put('/documentos/copy', 'DocumentsController@copy');
Route::get('/documentos/download', 'DocumentsController@download');
Route::post('/documentos/folder', 'DocumentsController@folder');
Route::post('/documentos/upload', 'DocumentsController@upload');
Route::get('/documentos/directories', 'DocumentsController@getDirectories');
Route::get('/documentos/root', 'DocumentsController@all');
Route::get('/documentos', 'DocumentsController@index');

Route::get('me', function(){
	return response()->json([
		'isAdmin' => Auth::user()->hasRole(['ti', 'admin', 'diretoria']),
		//'roles' => Auth::user()->roles->map(function($role){ return $role->name; }),
		//'departaments' => Auth::user()->collaborator->departaments,
	], 200, [], JSON_PRETTY_PRINT);
});

Route::resource('/clientes', 'ClientsController');

//rota extra ATENÇÃO
Route::get('/paychecks/download', 'Admin\PaychecksController@download');


Route::get('meu-perfil', 'ProfileControler@index')
				->name('profile')
				->middleware('auth');

Route::post('meu-perfil', 'ProfileControler@changePassword')
				->name('profile_post')
				->middleware('auth');

Auth::routes();
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/admin', 'Admin\AdminController@index');

// Adicionado por Samuel Edson

Route::namespace('Intranet')->prefix('intranet')->group(function () {

    Route::prefix('auth')->group(function () {

        Route::get('login', 'AuthController@login')->name('intranet.auth.login');
        Route::post('login', 'AuthController@authenticate')->name('intranet.auth.login');
        Route::get('logout', 'AuthController@logout')->name('intranet.auth.logout');

    });

    Route::get('/', 'HomeController@home')->name('intranet.home');

    Route::middleware('auth')->group(function () {

        Route::get('teste', 'HomeController@teste')->name('intranet.teste');

        Route::prefix('roles')->middleware('permission:roles')->group(function () {

            Route::get('/', 'RoleController@index')
                ->name('intranet.roles.index');

            Route::get('show/{id?}', 'RoleController@show')
                ->name('intranet.roles.show');

            Route::post('store', 'RoleController@store')
                ->name('intranet.roles.store');

            Route::get('destroy/{id?}', 'RoleController@destroy')
                ->name('intranet.roles.destroy');

        });

        Route::prefix('departaments')->middleware('permission:departaments')->group(function () {

            Route::get('/', 'DepartamentController@index')
                ->name('intranet.departaments.index');

            Route::get('show/{id?}', 'DepartamentController@show')
                ->name('intranet.departaments.show');

            Route::post('store', 'DepartamentController@store')
                ->name('intranet.departaments.store');

            Route::get('destroy/{id?}', 'DepartamentController@destroy')
                ->name('intranet.departaments.destroy');

        });

        Route::prefix('products')->middleware('permission:products')->group(function () {

            Route::get('/', 'ProductController@index')
                ->name('intranet.products.index');

            Route::get('show/{id?}', 'ProductController@show')
                ->name('intranet.products.show');

            Route::post('store', 'ProductController@store')
                ->name('intranet.products.store');

            Route::get('destroy/{id?}', 'ProductController@destroy')
                ->name('intranet.products.destroy');

        });

        Route::prefix('categories')->middleware('permission:products')->group(function () {

            Route::get('/', 'CategoryController@index')
                ->name('intranet.categories.index');

            Route::get('show/{id?}', 'CategoryController@show')
                ->name('intranet.categories.show');

            Route::post('store', 'CategoryController@store')
                ->name('intranet.categories.store');

            Route::get('destroy/{id?}', 'CategoryController@destroy')
                ->name('intranet.categories.destroy');

        });

        Route::prefix('indications')->middleware('permission:indications')->group(function () {

            Route::prefix('status')->middleware('permission:indication-status')->group(function () {

                Route::get('/', 'IndicationStatusController@index')
                    ->name('intranet.indications.status.index');

                Route::get('show/{id?}', 'IndicationStatusController@show')
                    ->name('intranet.indications.status.show');

                Route::post('store', 'IndicationStatusController@store')
                    ->name('intranet.indications.status.store');

                Route::get('destroy/{id?}', 'IndicationStatusController@destroy')
                    ->name('intranet.indications.status.destroy');

            });

            Route::get('/', 'IndicationController@index')
                ->name('intranet.indications.index');

            Route::get('by-me', 'IndicationController@byMe')
                ->name('intranet.indications.byme');

            Route::get('create', 'IndicationController@create')
                ->name('intranet.indications.create')
                ->middleware('permission:indications-create');

            Route::post('/', 'IndicationController@store')
                ->name('intranet.indications.store')
                ->middleware('permission:indications-create');

            Route::get('/{id}', 'IndicationController@show')
                ->name('intranet.indications.show');

            Route::put('/{id}', 'IndicationController@update')
                ->name('intranet.indications.update')
                ->middleware('permission:indications-update');

            Route::post('/transfer-propection', 'IndicationController@transferPropecting')
                ->name('intranet.indications.transfer-propection')
                ->middleware('permission:indications-update');

            Route::get('{id?}/destroy', 'IndicationController@destroy')
                ->name('intranet.indications.destroy')
                ->middleware('permission:indications-delete');

            Route::post('client/verify', 'ClientController@getClient')
                ->name('intranet.indications.client.verify');

            Route::post('client/products-indicated', 'ClientController@getProductsIndicated')
                ->name('intranet.indications.client.product_options');

        });

        Route::prefix('productions')->middleware('permission:productions')->group(function () {

            Route::get('/', 'ProductionController@index')
                ->name('intranet.productions.index');

            Route::get('manager', 'ProductionController@manager')
                ->name('intranet.productions.manager')
                ->middleware('permission:productions-manager');

            Route::get('create', 'ProductionController@create')
                ->name('intranet.productions.create')
                ->middleware('permission:productions-create');

            Route::post('/', 'ProductionController@store')
                ->name('intranet.productions.store')
                ->middleware('permission:productions-create');

            Route::get('/{id?}', 'ProductionController@show')
                ->name('intranet.productions.show');

            Route::get('edit/{id?}', 'ProductionController@edit')
                ->name('intranet.productions.edit')
                ->middleware('permission:productions-update');

            Route::put('/{id}', 'ProductionController@update')
                ->name('intranet.productions.update')
                ->middleware('permission:productions-update');

            Route::get('destroy/{id?}', 'ProductionController@destroy')
                ->name('intranet.productions.destroy')
                ->middleware('permission:productions-delete');

            Route::post('client/verify', 'ClientController@getClient')
                ->name('intranet.productions.client.verify');

            Route::post('product/verify', 'ProductController@getProductIndication')
                ->name('intranet.productions.product.verify');

        });

        Route::prefix('quizzes')->middleware('permission:quiz')->group(function (){

            Route::get('/', 'QuizController@index')
                ->name('intranet.quiz.index');

            Route::get('create', 'QuizController@create')
                ->name('intranet.quiz.create')
                ->middleware('permission:quiz-manager');

            Route::get('delete/{id?}', 'QuizController@delete')
                ->name('intranet.quiz.delete')
                ->middleware('permission:quiz-manager');

            Route::post('store', 'QuizController@store')
                ->name('intranet.quiz.store')
                ->middleware('permission:quiz-manager');

            Route::get('edit/{id?}', 'QuizController@edit')
                ->name('intranet.quiz.edit')
                ->middleware('permission:quiz-manager');

            Route::get('manager', 'QuizController@manager')
                ->name('intranet.quiz.manager')
                ->middleware('permission:quiz');

            Route::get('resultados/{quizId?}', 'QuizController@result')
                ->name('intranet.quiz.result')
                ->middleware('permission:quiz');


        });

        Route::prefix('answer')->group(function (){
            Route::get('/{id?}', 'QuizController@answer')
                ->name('intranet.quiz.answer')
                ->middleware('permission:quiz-answer');

            Route::get('{id}/{coll_id?}', 'QuizController@seeResult')
                ->name('intranet.quiz.result.see')
                ->middleware('permission:quiz');

            Route::get('see/{coll_id}/{id?}', 'QuizController@selfResult')
                ->name('intranet.quiz.result.self');

            Route::post('do', 'QuizController@doAnswer')
                ->name('intranet.quiz.answer.do')
                ->middleware('permission:quiz-answer');

        });

        Route::prefix('campaings')->group(function (){
           Route::get('/', 'CampaignController@index')
                    ->name('intranet.campaign.index')
                    ->middleware('permission:campaigns');

           Route::get('create', 'CampaignController@create')
                    ->name('intranet.campaign.create')
                    ->middleware('permission:campaign-create');

           Route::get('edit/{id?}', 'CampaignController@edit')
                    ->name('intranet.campaign.edit')
                    ->middleware('permission:campaign-edit');

           Route::post('update', 'CampaignController@update')
                    ->name('intranet.campaign.update')
                    ->middleware('permission:campaign-edit');

           Route::post('store', 'CampaignController@store')
                    ->name('intranet.campaign.store')
                    ->middleware('permission:campaign-create');

           Route::post('upload', 'CampaignController@uploadImage')
                    ->name('intranet.campaign.upload')
                    ->middleware('permission:campaign-create');

           Route::get('delete/{id?}', 'CampaignController@delete')
                    ->name('intranet.campaign.delete')
                    ->middleware('permission:campaign-delete');

           Route::get('ranking', 'CampaignController@listCampaignsRanking')
                    ->name('intranet.campaign.ranking')
                    ->middleware('permission:campaigns');

        });

        Route::prefix('config')->group(function (){
            Route::get('/', 'ConfigController@index')
                     ->name('intranet.config.index')
                     ->middleware('permission:custom-config');
                     
            Route::post('update', 'ConfigController@update')
                     ->name('intranet.config.update')
                     ->middleware('permission:custom-config');
 
         });
    });
    Route::prefix('ranking')->group(function (){

        Route::get('/sale/{id}', 'RankingController@sales')
                ->name('intranet.ranking.sale');

        Route::get('/indication/{id}', 'RankingController@indications')
                ->name('intranet.ranking.indication');

    });

});



