<?php
namespace App\DataTables\Html;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Lang;
use Yajra\Datatables\Html\Builder as BuilderBase;
use Collective\Html\FormBuilder;
use Collective\Html\HtmlBuilder;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\View\Factory;
use Illuminate\Routing\UrlGenerator;
use Yajra\Datatables\Html\Column;
use App\Models\Category;

class Builder extends BuilderBase
{

    /**
     * @param Repository   $config
     * @param Factory      $view
     * @param HtmlBuilder  $html
     * @param UrlGenerator $url
     * @param FormBuilder  $form
     */
    public function __construct(
        Repository $config,
        Factory $view,
        HtmlBuilder $html,
        UrlGenerator $url,
        FormBuilder $form
    ) {
        $this->config     = $config;
        $this->view       = $view;
        $this->html       = $html;
        $this->url        = $url;
        $this->collection = new Collection;
        $this->form       = $form;
        $this->attributes = [
            'language' => Lang::trans('datatables'),
            'deferRender' => true,
            'stateSave' => true,
            'dom'          => "<'dt-panelmenu clearfix'<'row'<'col-xs-6'B><'col-xs-6'f>>>".
                "<'row'<'col-md-12'tr>>".
                "<'dt-panelfooter clearfix'<'row'<'col-sm-4'li><'col-sm-8'p>>>",
            'buttons' => [
                /*[
                    'extend' => 'colvis',
                    'text'  => '<i class="fa fa-columns"></i> <span class="hidden-xs">COLUNAS</span>'
                ],
                [
                    'extend' => 'excelHtml5',
                    'text'  => '<i class="fa fa-file-excel-o"></i> <span class="hidden-xs">EXPORTAR</span>'
                ]*/
            ]
        ];
    }

    /**
     * Encode columns render function.
     *
     * @param array $parameters
     * @return array
     */
    protected function encodeColumnFunctions(array $parameters)
    {
        $columnFunctions = [];
        foreach ($parameters['columns'] as $i => $column) {
            //unset($parameters['columns'][$i]['exportable']);
            //unset($parameters['columns'][$i]['printable']);
            //unset($parameters['columns'][$i]['footer']);

            if (isset($column['render'])) {
                $columnFunctions[$i]                 = $column['render'];
                $parameters['columns'][$i]['render'] = "#column_function.{$i}#";
            }
        }

        return [$columnFunctions, $parameters];
    }


    /**
     * Add a action column.
     *
     * @param  array $attributes
     * @return $this
     */
    public function addAction(array $attributes = [])
    {
        $attributes = array_merge([
            'defaultContent' => '',
            'data'           => 'action',
            'name'           => 'action',
            'title'          => 'Ações',
            'render'         => 'datatablesFormatActions(data, type, full)',
            'orderable'      => false,
            'searchable'     => false,
            'exportable'     => false,
            'printable'      => false,
            'footer'         => '',
            'width' => '10%'
        ], $attributes);
        $this->collection->push(new Column($attributes));

        return $this;
    }

    public function addIndex(array $attributes = [])
    {
        $attributes = array_merge([
            'data'           => 'id',
            'name'           => 'id',
            'title'          => 'ID',
            'width' => '5%'
        ], $attributes);
        $this->collection->push(new Column($attributes));

        return $this;
    }

    public function addTitle(array $attributes = [])
    {
        $attributes = array_merge([
            'data'           => 'title',
            'name'           => 'title',
            'title'          => 'Título',
            'render'         => 'datatablesFormatStrong(data)',
        ], $attributes);
        $this->collection->push(new Column($attributes));

        return $this;
    }

    public function addCategory($categories, array $attributes = [])
    {   
        //dd(json_encode($categories));
        $attributes = array_merge([
            'data'           => 'category_id',
            'name'           => 'category_id',
            'title'          => 'Categoria',
            'render'         => 'datatablesCategory(data,'.json_encode($categories).')',
        ], $attributes);
        $this->collection->push(new Column($attributes));

        return $this;
    }

    public function addStatus(array $attributes = [])
    {
        $attributes = array_merge([
            'data'           => 'status',
            'name'           => 'status',
            'title'          => 'Status',
            'render'         => 'datatablesFormatStatus(data)',
            'width' => '10%'
        ], $attributes);
        $this->collection->push(new Column($attributes));

        return $this;
    }

    public function addActive(array $attributes = [])
    {
        $attributes = array_merge([
            'data'           => 'active',
            'name'           => 'active',
            'title'          => 'Ativo',
            'render'         => 'datatablesFormatActive(data)',
            'width' => '10%'
        ], $attributes);
        $this->collection->push(new Column($attributes));

        return $this;
    }

    public function addActionShow(array $attributes = [])
    {
        $attributes = array_merge([
            'defaultContent' => '',
            'data'           => 'action_show',
            'name'           => 'action_show',
            'title'          => 'Visualizar',
            'render'         => 'datatablesFormatActionShow(data)',
            'orderable'      => false,
            'searchable'     => false,
            'exportable'     => false,
            'printable'      => false,
            'footer'         => '',
            'width' => '10%'
        ], $attributes);
        $this->collection->push(new Column($attributes));

        return $this;
    }

    public function addButtonColVis()
    {

        $button = [
            'extend' => 'colvis',
            'text'  => '<i class="fa fa-columns"></i> <span class="hidden-xs">COLUNAS</span>'
        ];

        $this->attributes['buttons'][] = $button;

        return $this;
    }

    public function addButtonExcel()
    {

        $button = [
            'extend' => 'excelHtml5',
            'text'  => '<i class="fa fa-file-excel-o"></i> <span class="hidden-xs">EXPORTAR</span>'
        ];

        $this->attributes['buttons'][] = $button;

        return $this;
    }

}