<?php

namespace App\Http\Middleware;

use Closure;

class IntranetAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!$request->session()->has('user')) {
            return redirect('/intranet/auth/login');
        }

        return $next($request);
    }
}
