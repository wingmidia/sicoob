<?php
/**
 * Created by PhpStorm.
 * User: Wing Mídia
 * Date: 06/04/2018
 * Time: 17:00
 */

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class QuizCreateRequest extends FormRequest {

    public function authorize(){
        return true;
    }

    public function rules(){
        return [
            'name' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'asks' => 'required'
        ];
    }
}