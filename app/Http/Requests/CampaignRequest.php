<?php
/**
 * Created by PhpStorm.
 * User: Wing Mídia
 * Date: 17/04/2018
 * Time: 14:24
 */

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class CampaignRequest extends FormRequest {


    public function authorize(){
        return true;
    }

    public function rules(){
        return [
            'name' => 'required',
            'start_at'=> 'required',
            'end_at' => 'required'
        ];
    }
}