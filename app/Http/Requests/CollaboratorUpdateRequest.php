<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CollaboratorUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = request()->get('user');
        return [
            'code' => 'required',
            'name' => 'required',
            'user.role_id' => 'required',
            'user.email' => [
                'required',
                //Rule::unique('users')->ignore($user['id']),
            ],
            'user.password' => 'min:6',
            'user.password_confirmation' => 'required_with:user.password|min:6|same:user.password',
            'departaments' => 'required'
        ];
    }
}
