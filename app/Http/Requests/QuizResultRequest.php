<?php
/**
 * Created by PhpStorm.
 * User: Wing Mídia
 * Date: 10/04/2018
 * Time: 16:20
 */

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class QuizResultRequest extends FormRequest {

    public function authorize(){
        return true;
    }

    public function rules(){
        return [];
    }
}