<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CollaboratorCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required',
            'name' => 'required',
            'user.email' => 'email|required|unique:users,email',
            'user.role_id' => 'required',
            'user.password' => 'required|min:6',
            'user.password_confirmation' => 'required|min:6|same:user.password',
            'departaments' => 'required'
        ];
    }
}
