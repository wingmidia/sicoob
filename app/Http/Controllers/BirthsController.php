<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Collaborator;
use Carbon\Carbon;

class BirthsController extends Controller
{
    public function index()
    {
		$month = Carbon::now()->month;
        $birth_month = Collaborator::whereRaw('month(date_birth) = '.$month)
									->orderByRaw('day(date_birth) asc')->get();
        
        return view('births', compact('birth_month'));
    }
}
