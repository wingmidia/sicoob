<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\LinksRepository;
use App\Criteria\StatusActiveCriteria;

class LinksController extends Controller
{
  private $links;

  public function __construct(LinksRepository $links)
  { 
    $this->links = $links;
    $this->links->pushCriteria(StatusActiveCriteria::class);
  }

  public function index()
  {
    $links = $this->links->paginate();
      
    return view('links', compact('links'));
  }
}
