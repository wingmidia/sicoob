<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\DocumentsCreateRequest;
use App\Http\Requests\DocumentsUpdateRequest;
use App\Repositories\DocumentRepository;
use App\Validators\DocumentValidator;

use App\Models\StorageManager;
use Auth;

class DocumentsController extends Controller
{
    protected $storage;

    public function __construct(StorageManager $storage)
    {
        $this->middleware('auth');
        $this->storage = $storage;
    }

    public function index()
    {
        if(Auth::user()->hasRole(['ti', 'admin', 'diretoria'])){
            $this->storage->createDirectoriesDepartaments();
        }
        
        return view('documents');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request)
    {       
        $folder = $request->get('folder', null);
        $all = $this->storage->all($folder);

        return $this->respondOk($all);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  DocumentsCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(DocumentsCreateRequest $request)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $document = $this->repository->create($request->all());

            $response = [
                'message' => 'Document created.',
                'data'    => $document->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $document = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $document,
            ]);
        }

        return view('Document.show', compact('document'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $document = $this->repository->find($id);

        return view('Document.edit', compact('document'));
    }

    public function rename(Request $request)
    {
        $rename = $this->storage->move($request->get('original'), '/'.$request->get('target'));

        $response = [
            'message' => 'Documento renomeado com sucesso!',
            'data'    => $rename,
        ];

        return $this->respondOk($response);
    }

    public function upload(Request $request)
    {
        if (!$request->hasFile('documents')) {
            return $this->respondError(500, 'Arquivo não existe!');
        }

        $file = $this->storage->upload($request, $request->get('folder'), $request->get('finename'));
        
        return $this->respondOk($file);
    }

    public function folder(Request $request)
    {
        if (!$request->has('directory')) {
            return $this->respondError(500, 'Pasta não existe!');
        }

        $folder = $request->get('directory').'/'.$request->get('new_folder');

        $created = $this->storage->makeDirectory($folder);

        return $this->respondOk($created);
    }

    public function download(Request $request)
    {

        if(!$request->has('path')){
            return abort(404);
        }

        $path = $request->get('path');

        $exists = $this->storage->exists($path);

        if(!$exists){
           return abort(404);
        }

        $file = $this->storage->path_original($path);

        return response()->download($file);
    }

    public function getDirectories()
    {
        $directories = $this->storage->allFolders();

        return $this->respondOk($directories);
    }

    public function move(Request $request)
    {
        $origin = $request->get('origin');
        $dest   = $request->get('dest').'/'.$request->get('name');    

        if($request->get('type') == 'file'){
            $move = $this->storage->moveDirectory($origin, $dest);
        } else {
            $move = $this->storage->move($origin, $dest);
        }
        
        return $this->respondOk($move);
    }

    public function copy(Request $request)
    {
        $origin = $request->get('origin');
        $dest   = $request->get('dest').'/'.$request->get('name');
        
        $copy = $this->storage->copy($origin, $dest);        
        
        return $this->respondOk($copy);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        
        $file = $request->all();

        if($file['type'] == 'file'){
            $deleted = $this->storage->deleteFile($file['path']);
        } else {
            $deleted = $this->storage->deleteFolder($file['path']);
        }

        return $this->respondOk([
                        'message' => 'Document deleted.',
                        'deleted' => $deleted,
                    ]);        
    }
}