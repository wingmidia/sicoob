<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\NewsRepository;
use App\Models\Departament;
use App\Models\News;
use App\Criteria\NewsActiveCriteria;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

use App\Http\Requests\NewsCommentCreateRequest;

use Auth;

class NewsController extends Controller
{
  private $news;

  public function __construct(NewsRepository $news)
  { 
    $this->news = $news;
    $this->news->pushCriteria(app(NewsActiveCriteria::class));
  }

  public function index(Request $request, $slug = '')
  {
    $departament = null;
    if($slug){
      $departament = Departament::with(['collaborators' => function ($q) {
        $q->where('status', 1)->with('user');
    }])->where('slug', $slug)->first();

      if(!$departament)
        return abort(404);      
    }

    $query = News::active();

    $departament_id = 0;
    if($slug){
      $departament_id = $departament->id;
      $query->whereHas('news_departaments', function($q) use ($departament_id){
        $q->where('departament_id', $departament_id);
      });
    }

    $query->private($departament_id);

    if($request->has('search')){
      $search = $request->get('search');
      $query->where(function($q) use ($search){
        $q->where('title', 'LIKE', '%'.$search.'%');
        $q->where('description', 'LIKE', '%'.$search.'%');
      });
    }

    $news = $query->orderByRaw('start_date Desc, id desc')->paginate(8);

    return view('news', compact('news', 'slug', 'departament'));
  }

  public function details( $slug )
  {
    $new = News::where('slug', $slug)->active()->first();

    if($new->permission == 'private' && !Auth::check())
    {
      abort(404);
    }

    if ($new->permission == 'private' && Auth::check()) {

      $departaments = collect($new->departaments)->pluck('id');
      
      if(!Auth::user()->permission_departament($departaments->toArray())){
        abort(404);
      }
    }

    $repositoryComments = app('App\Repositories\NewsCommentRepository');
    $comments = $repositoryComments->findWhere(['replay_id' => 0, 'new_id' => $new->id, 'status' => 1]);
    
    return view('new', compact('new', 'comments', 'slug'));
  }

  public function comment(NewsCommentCreateRequest $request)
  {
      try {
          $validator = app('App\Validators\NewsCommentValidator');
          $repository = app('App\Repositories\NewsCommentRepository');

          $validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

          $newsComment = $repository->create(array_merge(['status' => 1], $request->all()));

          return redirect()->route('new.details', ['slug' => $request->get('slug')])->with('message', 'Comentário enviado!');

      } catch (ValidatorException $e) {
          return redirect()->back()->withErrors($e->getMessageBag())->withInput();
      }
  }
}
