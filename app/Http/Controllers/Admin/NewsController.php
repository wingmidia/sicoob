<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\NewsCreateRequest;
use App\Http\Requests\NewsUpdateRequest;
use App\Repositories\NewsRepository;
use App\Validators\NewsValidator;

use App\Models\Departament;
use App\Models\NewsDepartament;


class NewsController extends Controller
{

    /**
     * @var NewsRepository
     */
    protected $repository;

    /**
     * @var NewsValidator
     */
    protected $validator;

    public function __construct(NewsRepository $repository, NewsValidator $validator)
    {        
        $this->repository = $repository;
        $this->validator  = $validator;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $news = $this->repository->scopeQuery(function($query){
                        return $query->orderBy('id','desc');
                    })->paginate();

        return $this->respondOk($news);
        
    }

    public function edit(Request $request, $id)
    {
        $news = $this->repository->with(['news_departaments'])->find($id);
                
        return $this->respondOk($news);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  NewsCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(NewsCreateRequest $request)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $news = $this->repository->create($request->except('id', 'departaments'));

            $departaments = collect($request->only('departaments')['departaments'])->pluck('id');
            $news->news_departaments()->sync($departaments);

            $response = [
                'message' => 'Notícia criada com sucesso!',
                'data'    => $news->toArray(),
            ];

            return $this->respondOk($response);
            
        } catch (ValidatorException $e) {

            return $this->respondValidation($e);
            
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  NewsUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(NewsUpdateRequest $request, $id)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            
            $news = $this->repository->update($request->except('id', 'departaments'), $id);
            $news->news_departaments()->detach();

            $departaments = collect($request->only('departaments')['departaments'])->pluck('id');
            $news->news_departaments()->sync($departaments);
            
            $response = [
                'message' => 'Notícia atualizada com sucesso!',
                'data'    => $news->toArray(),
            ];

            return $this->respondOk($response);

        } catch (ValidatorException $e) {

            return $this->respondValidation($e);
        
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        return $this->respondOk([
                'message' => 'News deleted.',
                'deleted' => $deleted,
            ]);
    }

    /**
     * Upload image feacture.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */

    public function upload(Request $request)
    {
        if (!$request->hasFile('image_feacture')) {
            return $this->respondError(500, 'Arquivo não existe!');
        }

        $image = $request->image_feacture;
        $save = $image->store('content');

        $filename = str_replace('content/', '', $save);

        $this->fixOrientationImage(storage_path('app/content/'.$filename));
        
        return $this->respondOk(['image' => $filename]);
    }
}
