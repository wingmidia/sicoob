<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\NewsCommentCreateRequest;
use App\Http\Requests\NewsCommentUpdateRequest;
use App\Repositories\NewsCommentRepository;
use App\Validators\NewsCommentValidator;
use App\Criteria\NewsFilterCriteria;

class NewsCommentsController extends Controller
{

    /**
     * @var NewsCommentRepository
     */
    protected $repository;

    /**
     * @var NewsCommentValidator
     */
    protected $validator;

    public function __construct(NewsCommentRepository $repository, NewsCommentValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $this->repository->pushCriteria(NewsFilterCriteria::class);
        
        $newsComments = $this->repository->findWhere(['replay_id' => 0]);

        return $this->respondOk($newsComments['data']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  NewsCommentCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(NewsCommentCreateRequest $request)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $newsComment = $this->repository->create($request->all());

            $response = [
                'message' => 'NewsComment created.',
                'data'    => $newsComment['data'],
            ];

            return $this->respondOk($response);

        } catch (ValidatorException $e) {
            return $this->respondValidation($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $newsComment = $this->repository->find($id);

        return $this->respondOk($newsComment);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  NewsCommentUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(NewsCommentUpdateRequest $request, $id)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $newsComment = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'NewsComment updated.',
                'data'    => $newsComment['data'],
            ];

            return $this->respondOk($response);

        } catch (ValidatorException $e) {
            return $this->respondValidation($e);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comments = $this->repository->findWhere(['replay_id' => $id]);
        if($comments){
            
            foreach ($comments['data'] as $comment) {
                $this->repository->delete($comment['id']);
            }
        }

        $deleted = $this->repository->delete($id);

        return $this->respondOk([
                'message' => 'NewsComment deleted.',
                'deleted' => $deleted,
            ]);
    }
}
