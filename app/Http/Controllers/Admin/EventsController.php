<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\EventCreateRequest;
use App\Http\Requests\EventUpdateRequest;
use App\Repositories\EventRepository;
use App\Validators\EventValidator;


class EventsController extends Controller
{

    /**
     * @var EventRepository
     */
    protected $repository;

    /**
     * @var EventValidator
     */
    protected $validator;

    public function __construct(EventRepository $repository, EventValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $events = $this->repository->paginate();

        return $this->respondOk($events);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  EventCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(EventCreateRequest $request)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $event = $this->repository->create($request->all());

            $response = [
                'message' => 'Event created.',
                'data'    => $event->toArray(),
            ];

            return $this->respondOk($response);
        } catch (ValidatorException $e) {
            return $this->respondValidation($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $event = $this->repository->find($id);

        return $this->respondOk($event);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  EventUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(EventUpdateRequest $request, $id)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $event = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Event updated.',
                'data'    => $event->toArray(),
            ];

            return $this->respondOk($response);
        } catch (ValidatorException $e) {
            return $this->respondValidation($e);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        return $this->respondOk([
                'message' => 'Event deleted.',
                'deleted' => $deleted,
            ]);
    }

    /**
     * Upload image profile.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */

    public function upload(Request $request)
    {
        if (!$request->hasFile('image_feacture')) {
            return $this->respondError(500, 'Arquivo não existe!');
        }

        $logo = $request->image_feacture;
        $save = $logo->store('content');

        $filename = str_replace('content/', '', $save);

        $this->fixOrientationImage(storage_path('app/content/'.$filename));
        
        return $this->respondOk(['image' => $filename]);
    }
}
