<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\EventsPhotoCreateRequest;
use App\Http\Requests\EventsPhotoUpdateRequest;
use App\Repositories\EventsPhotoRepository;
use App\Validators\EventsPhotoValidator;

use App\Criteria\EventsFilterCriteria;
use Image;

class EventsPhotosController extends Controller
{

    /**
     * @var EventsPhotoRepository
     */
    protected $repository;

    /**
     * @var EventsPhotoValidator
     */
    protected $validator;

    public function __construct(EventsPhotoRepository $repository, EventsPhotoValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $this->repository->pushCriteria(EventsFilterCriteria::class);
        $eventsPhotos = $this->repository->all();

        return $this->respondOk($eventsPhotos);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);


        return $this->respondOk([
                'message' => 'Imagem removida.',
                'deleted' => $deleted,
            ]);
    }

    /**
     * Upload image feacture.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */

    public function upload(Request $request)
    {
        if (!$request->hasFile('image')) {
            return $this->respondError(500, 'Arquivo não existe!');
        }


        $image = $request->image;
        
        $save = $image->store('content');

        $filename = str_replace('content/', '', $save);

        $this->fixOrientationImage(storage_path('app/content/'.$filename));
        
        $eventsPhotos = $this->repository->create(['event_id' => $request->get('event_id'), 'image' => $filename]);
        
        return $this->respondOk(['photo' => $eventsPhotos->toArray()]);
    }
}
