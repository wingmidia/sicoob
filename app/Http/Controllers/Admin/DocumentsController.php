<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\DocumentsCreateRequest;
use App\Http\Requests\DocumentsUpdateRequest;
use App\Repositories\DocumentsRepository;
use App\Validators\DocumentsValidator;


class DocumentsController extends Controller
{

    /**
     * @var DocumentsRepository
     */
    protected $repository;

    /**
     * @var DocumentsValidator
     */
    protected $validator;

    public function __construct(DocumentsRepository $repository, DocumentsValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $documents = $this->repository->with(['departaments'])->paginate();

        return $this->respondOk($documents);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        return $this->respondOk([
                                    'message' => 'Documents deleted.',
                                    'deleted' => $deleted,
                                ]);
    }

    
    public function upload(Request $request)
    {
        if (!$request->hasFile('documents')) {
            return $this->respondError(500, 'Arquivo não existe!');
        }

        $data = json_decode($request->get('data'));
        
        $file  = $request->documents;
        $save  = $file->store('documents');

        $filename = str_replace('documents/', '', $save);
        $ext = $file->guessExtension();
        $mime_type = $file->getClientMimeType();
        $original_name = $file->getClientOriginalName();
        

        $inputs = ['extension' => $ext, 'mimetype' => $mime_type, 'filename' => $filename, 'original_name' => $original_name, 'status' => 1];
        $documents = $this->repository->create($inputs);

        $departaments = collect($data->departaments)->pluck('id');
        $documents->departaments()->sync($departaments);
        
        return $this->respondOk($documents);
    }
}
