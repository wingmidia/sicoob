<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\LinkCreateRequest;
use App\Http\Requests\LinkUpdateRequest;
use App\Repositories\LinkRepository;
use App\Validators\LinkValidator;


class LinksController extends Controller
{

    /**
     * @var LinkRepository
     */
    protected $repository;

    /**
     * @var LinkValidator
     */
    protected $validator;

    public function __construct(LinkRepository $repository, LinkValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $links = $this->repository->paginate();

        return $this->respondOk($links);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  LinkCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(LinkCreateRequest $request)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $link = $this->repository->create($request->all());

            $response = [
                'message' => 'Link created.',
                'data'    => $link->toArray(),
            ];

            return $this->respondOk($response);

        } catch (ValidatorException $e) {
            return $this->respondValidation($e);
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $link = $this->repository->find($id);

        return $this->respondOk($link);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  LinkUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(LinkUpdateRequest $request, $id)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $link = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Link updated.',
                'data'    => $link->toArray(),
            ];

            return $this->respondOk($response);

        } catch (ValidatorException $e) {
            return $this->respondValidation($response);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        return $this->respondOk([
                'message' => 'Link deleted.',
                'deleted' => $deleted,
            ]);
    }

    /**
     * Upload image profile.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */

    public function upload(Request $request)
    {
        if (!$request->hasFile('logo')) {
            return $this->respondError(500, 'Arquivo não existe!');
        }

        $logo = $request->logo;
        $save = $logo->store('logos');

        $filename = str_replace('logos/', '', $save);

        $this->fixOrientationImage(storage_path('app/logos/'.$filename));
        
        return $this->respondOk(['image' => $filename]);
    }
}
