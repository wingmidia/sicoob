<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\PaycheckCreateRequest;
use App\Http\Requests\PaycheckUpdateRequest;
use App\Repositories\PaycheckRepository;
use App\Validators\PaycheckValidator;
use Storage;
use App\Models\Collaborator;


class PaychecksController extends Controller
{

    /**
     * @var PaycheckRepository
     */
    protected $repository;

    /**
     * @var PaycheckValidator
     */
    protected $validator;

    public function __construct(PaycheckRepository $repository, PaycheckValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $paychecks = $this->repository->with(['collaborator'])->paginate();

        return $this->respondOk($paychecks);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PaycheckCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(PaycheckCreateRequest $request)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $paycheck = $this->repository->create(array_merge(['status' => 'SENDING'], $request->all()));

            $response = [
                'message' => 'Paycheck created.',
                'data'    => $paycheck->toArray(),
            ];

            return $this->respondOk($response);

        } catch (ValidatorException $e) {
            return $this->respondValidation($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $paycheck = $this->repository->find($id);

        return $this->respondOk($paycheck);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  PaycheckUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(PaycheckUpdateRequest $request, $id)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $paycheck = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Paycheck updated.',
                'data'    => $paycheck->toArray(),
            ];

            return $this->respondOk($response);

        } catch (ValidatorException $e) {
            return $this->respondValidation($e);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        return $this->respondOk([
                'message' => 'Paycheck deleted.',
                'deleted' => $deleted,
            ]);
    }

    /**
     * Upload image feacture.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */

    public function upload(Request $request)
    {
        if (!$request->hasFile('filename')) {
            return $this->respondError(500, 'Arquivo não existe!');
        }

        $paycheck = null;
        $collaborator = null;

        if($request->has('multiple')){
            
            $inputs = array_merge(['status' => 'SENDING', 'filename' => 'vazio'], 
                                  ['month' => $request->get('month'), 'year' => $request->get('year')]);

            $original_name = $request->filename->getClientOriginalName();
            $original_name = explode(' - ', $original_name);
            $code = 0;
            if(is_array($original_name)){
                $code = $original_name[0];
            }

            if($collaborator = $this->getCollaborator($code)){
                $paycheck = $this->repository->create(array_merge(['collaborator_id' => $collaborator->id], $inputs));
            }

            if(is_null($paycheck)){
                return $this->respondError(500, 'Colaborador não encontrado!');
            }
            
        }

        $image = $request->filename;
        $save = $image->store('paychecks');

        $filename = str_replace('paychecks/', '', $save);

        if($paycheck){
            $this->repository->update(['filename' => $filename], $paycheck->id);
            $collaborator = $paycheck->collaborator;
        }
        
        return $this->respondOk(['filename' => $filename, 'collaborator' => $collaborator]);
    }

    public function download(Request $request)
    {
        if (!$request->has('filename')) {
            return $this->respondError(500, 'Arquivo não existe!');
        }

        $file = $request->get('filename');
        $exists = Storage::exists('paychecks/'.$file);
        $pathToFile = storage_path('app/paychecks/'.$file);
        
        return $exists ? response()->download($pathToFile) : 'arquivo não existe';
    }

    public function destroyFile(Request $request)
    {
        if (!$request->has('filename')) {
            return $this->respondError(500, 'Arquivo não existe!');
        }

        $file = $request->get('filename');

        $exists = Storage::exists('paychecks/'.$file);
        
        if($exists){
            Storage::delete('paychecks/'.$file);
            return $this->respondOk(['deleted' => true]);    
        }

        return $this->respondError(500, 'Arquivo não existe!');
        
    }

    private function getCollaborator($code)
    {
        $collaborator = Collaborator::where('code', $code)->first();

        return $collaborator;
    }
}
