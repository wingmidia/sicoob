<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\DepartamentsCreateRequest;
use App\Http\Requests\DepartamentsUpdateRequest;
use App\Repositories\DepartamentRepository;
use App\Validators\DepartamentValidator;


class DepartamentsController extends Controller
{

    /**
     * @var DepartamentRepository
     */
    protected $repository;

    /**
     * @var DepartamentValidator
     */
    protected $validator;

    public function __construct(DepartamentRepository $repository, DepartamentValidator $validator)
    {      
        $this->repository = $repository;
        $this->validator  = $validator;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $departaments = $this->repository->paginate(10);

        return $this->respondOk($departaments);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  DepartamentsCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(DepartamentsCreateRequest $request)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $departament = $this->repository->create($request->all());

            $response = [
                'message' => 'Departaments created.',
                'data'    => $departament->toArray(),
            ];

            return $this->respondOk($response);

        } catch (ValidatorException $e) {
            return $this->respondValidation($e);
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $departament = $this->repository->find($id);

        return $this->respondOk($departament);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  DepartamentsUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(DepartamentsUpdateRequest $request, $id)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            
            $departament = $this->repository->update($request->only('name'), $id);

            $response = [
                'message' => 'Departaments updated.',
                'data'    => $departament->toArray(),
            ];

            return $this->respondOk($response);

        } catch (ValidatorException $e) {

            return $this->respondValidation($e);

        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        return $this->respondOk([
                'message' => 'Departaments deleted.',
                'deleted' => $deleted,
            ]);
    }

    public function combobox()
    {
        $departaments = $this->repository->all();
        
        return $this->respondOk($departaments);
    }
}
