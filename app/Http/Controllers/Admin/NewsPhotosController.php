<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\NewsPhotoCreateRequest;
use App\Http\Requests\NewsPhotoUpdateRequest;
use App\Repositories\NewsPhotoRepository;
use App\Validators\NewsPhotoValidator;
use App\Criteria\NewsFilterCriteria;


class NewsPhotosController extends Controller
{

    /**
     * @var NewsPhotoRepository
     */
    protected $repository;

    /**
     * @var NewsPhotoValidator
     */
    protected $validator;

    public function __construct(NewsPhotoRepository $repository, NewsPhotoValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $this->repository->pushCriteria(NewsFilterCriteria::class);
        $newsPhotos = $this->repository->all();

        return $this->respondOk($newsPhotos);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        return $this->respondOk($deleted);
    }

    /**
     * Upload image feacture.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */

    public function upload(Request $request)
    {
        if (!$request->hasFile('image')) {
            return $this->respondError(500, 'Arquivo não existe!');
        }

        $image = $request->image;
        $save = $image->store('content');

        $filename = str_replace('content/', '', $save);

        $this->fixOrientationImage(storage_path('app/content/'.$filename));

        $newsPhoto = $this->repository->create(['new_id' => $request->get('new_id'), 'image' => $filename]);
        
        return $this->respondOk(['photo' => $newsPhoto->toArray()]);
    }
}
