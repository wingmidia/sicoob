<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\WidgetCreateRequest;
use App\Http\Requests\WidgetUpdateRequest;
use App\Repositories\WidgetRepository;
use App\Validators\WidgetValidator;


class WidgetsController extends ApiController
{

    /**
     * @var WidgetRepository
     */
    protected $repository;

    /**
     * @var WidgetValidator
     */
    protected $validator;

    public function __construct(WidgetRepository $repository, WidgetValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $widgets = $this->repository->paginate();

        return $this->respondOk($widgets);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  WidgetCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(WidgetCreateRequest $request)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $widget = $this->repository->create($request->all());

            $response = [
                'message' => 'Widget criado com sucesso.',
                'data'    => $widget->toArray(),
            ];

            return $this->respondOk($response);

        } catch (ValidatorException $e) {
            return $this->respondValidation($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $widget = $this->repository->find($id);

        return $this->respondOk($widget);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  WidgetUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(WidgetUpdateRequest $request, $id)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $widget = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Widget atualizado com sucesso.',
                'data'    => $widget->toArray(),
            ];

            return $this->respondOk($response);

        } catch (ValidatorException $e) {
            return $this->respondValidation($e);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        return $this->respondOk([
                'message' => 'Widget excluído.',
                'deleted' => $deleted,
            ]);
    }
}
