<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\UsersCreateRequest;
use App\Http\Requests\UsersUpdateRequest;
use App\Repositories\UserRepository;
use App\Validators\UsersValidator;


class UsersController extends Controller
{

    /**
     * @var UserRepository
     */
    protected $repository;

    /**
     * @var UsersValidator
     */
    protected $validator;

    public function __construct(UserRepository $repository, UsersValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $users = $this->repository->paginate();

        return $this->respondOk($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  UsersCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(UsersCreateRequest $request)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $inputs = $request->all();
            $inputs['password'] = bcrypt($request->get('password'));

            $user = $this->repository->create($inputs);

            $response = [
                'message' => 'Users created.',
                'data'    => $user->toArray(),
            ];

            return $this->respondOk($response);

        } catch (ValidatorException $e) {
            return $this->respondValidation($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $user = $this->repository->find($id);

        return $this->respondOk($user);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  UsersUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(UsersUpdateRequest $request, $id)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $inputs = $request->all();
            if($request->has('password')){
                $inputs['password'] = bcrypt($request->get('password'));
            }

            $user = $this->repository->update($inputs, $id);

            $response = [
                'message' => 'Users updated.',
                'data'    => $user->toArray(),
            ];

            return $this->respondOk($response);

        } catch (ValidatorException $e) {
            return $this->respondValidation($e);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        return $this->respondOk([
                                    'message' => 'Users deleted.',
                                    'deleted' => $deleted,
                                ]);
    }
}
