<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Traits\Responses;
use Illuminate\Http\Request;
use App\Traits\QueryParameters;
use Illuminate\Contracts\Routing\ResponseFactory;

abstract class ApiController extends Controller
{
    use Responses;

    /**
     * Transform.
     *
     * @var \App\Transformers\Transform
     */
    protected $transform;

    /**
     * Creates a new class instance.
     *
     * @param Request         $request
     * @param ResponseFactory $response
     * @param Transform       $transform
     */
    public function __construct(Request $request, ResponseFactory $response)
    {
        $this->request = $request;
        $this->response = $response;        
    }
}
