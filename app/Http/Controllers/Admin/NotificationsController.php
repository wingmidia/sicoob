<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\NotificationCreateRequest;
use App\Http\Requests\NotificationUpdateRequest;
use App\Repositories\NotificationRepository;
use App\Validators\NotificationValidator;


class NotificationsController extends Controller
{

    /**
     * @var NotificationRepository
     */
    protected $repository;

    /**
     * @var NotificationValidator
     */
    protected $validator;

    public function __construct(NotificationRepository $repository, NotificationValidator $validator)
    {        
        $this->repository = $repository;
        $this->validator  = $validator;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $notifications = $this->repository->paginate();

        return $this->respondOk($notifications);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  NotificationCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(NotificationCreateRequest $request)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $notification = $this->repository->create($request->all());

            $response = [
                'message' => 'Notification created.',
                'data'    => $notification->toArray(),
            ];

            return $this->respondOk($response);

        } catch (ValidatorException $e) {
            return $this->respondValidation($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $notification = $this->repository->find($id);

        return $this->respondOk($notification);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  NotificationUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(NotificationUpdateRequest $request, $id)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $notification = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Notification updated.',
                'data'    => $notification->toArray(),
            ];

            return $this->respondOk($response);

        } catch (ValidatorException $e) {

            return $this->respondValidation($e);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        return $this->respondOk([
                                    'message' => 'Notification deleted.',
                                    'deleted' => $deleted,
                                ]);
    }
}
