<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\CollaboratorCreateRequest;
use App\Http\Requests\CollaboratorUpdateRequest;
use App\Repositories\CollaboratorRepository;
use App\Validators\CollaboratorValidator;
use App\Repositories\UserRepository;
use App\User;


class CollaboratorsController extends Controller
{

    /**
     * @var CollaboratorRepository
     */
    protected $repository;

    /**
     * @var CollaboratorRepository
     */
    protected $user;

    /**
     * @var CollaboratorValidator
     */
    protected $validator;

    public function __construct(CollaboratorRepository $repository, CollaboratorValidator $validator, UserRepository $user)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->user       = $user;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $collaborators = $this->repository->with(['user'])->paginate();

        return $this->respondOk($collaborators);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CollaboratorCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CollaboratorCreateRequest $request)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            
            $departaments = $request->only('departaments');

            $inputs_user = $request->only('name', 'email', 'user', 'status');
            $inputs_user['password'] = bcrypt($inputs_user['user']['password']);
            $inputs_user['email'] =$inputs_user['user']['email'];
            $role_id = $inputs_user['user']['role_id'];

            unset($inputs_user['user']);

            $user = $this->user->create($inputs_user);
            if($role_id){
                $this->saveRole($role_id, $user);
            }

            $collaborator = $this->repository->create(array_merge(['user_id' => $user->id], $request->only('code', 'name', 'photo', 'phone', 'date_birth', 'status')));
                        
            $collaborator->rel_departaments()->saveMany(array_map(function($departament) {
                return new \App\Models\CollaboratorDepartament(['departament_id' => $departament['id']]);
            }, $departaments['departaments']));

            $response = [
                'message' => 'Collaborator created.',
                'data'    => $collaborator->toArray(),
            ];

            return $this->respondOk($response);
        } catch (ValidatorException $e) {
            return $this->respondValidation($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $collaborator = $this->repository->with(['user', 'rel_departaments'])->find($id);
        
        $collaborator->departaments = $collaborator->rel_departaments->map(function($departament){
            return ['id' => $departament->id, 'name' => $departament->name];
        });

        unset($collaborator->rel_departaments);
        //unset($collaborator->user);
        
        return $this->respondOk($collaborator);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  CollaboratorUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(CollaboratorUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $departaments = $request->only('departaments');

            $inputs_user = $request->only('name', 'user', 'status');
            if($request->has('user.password')){
                $inputs_user['password'] = bcrypt($inputs_user['user']['password']);    
            }
            $inputs_user['email'] = $inputs_user['user']['email'];

            $role_id = $inputs_user['user']['role_id'];

            unset($inputs_user['user']);
            
            $user = $this->user->update($inputs_user, $request->get('user_id'));

            if($role_id != $user->role_id){
                $this->saveRole($role_id, $user);
            }
            
            $collaborator = $this->repository->update(array_merge(['user_id' => $user->id], $request->only('code', 'name', 'photo', 'phone', 'date_birth', 'status')), $id);

            $collaborator->rel_departaments()->delete();
            $collaborator->rel_departaments()->saveMany(array_map(function($departament) {
                return new \App\Models\CollaboratorDepartament(['departament_id' => $departament['id']]);
            }, $departaments['departaments']));

            $response = [
                'message' => 'Collaborator updated.',
                'data'    => $collaborator->toArray(),
            ];

            return $this->respondOk($response);
        } catch (ValidatorException $e) {
            return $this->respondValidation($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $collaborator = $this->repository->find($id);
        
        $deleted = $this->user->delete($collaborator->user_id);

        return $this->respondOk([
                                    'message' => 'Collaborator deleted.',
                                    'deleted' => $deleted,
                                ]);
    }

    /**
     * Upload image feacture.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */

    public function upload(Request $request)
    {
        if (!$request->hasFile('photo')) {
            return $this->respondError(500, 'Arquivo não existe!');
        }

        $image = $request->photo;
        $save = $image->store('content');

        $filename = str_replace('content/', '', $save);
        
        return $this->respondOk(['image' => $filename]);
    }

    public function combobox()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $user = $this->repository->all();
        
        return $this->respondOk($user);
    }

    private function saveRole($role_id, $user)
    {
        $user->roles()->sync([$role_id]);
    }
}
