<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\BannerCreateRequest;
use App\Http\Requests\BannerUpdateRequest;
use App\Repositories\BannerRepository;
use App\Validators\BannerValidator;


class BannersController extends Controller
{

    /**
     * @var BannerRepository
     */
    protected $repository;

    /**
     * @var BannerValidator
     */
    protected $validator;

    public function __construct(BannerRepository $repository, BannerValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $banners = $this->repository->paginate();

        return $this->respondOk($banners);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  BannerCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(BannerCreateRequest $request)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $banner = $this->repository->create($request->all());

            $response = [
                'message' => 'Banner created.',
                'data'    => $banner->toArray(),
            ];

            return $this->respondOk($response);

        } catch (ValidatorException $e) {
            return $this->respondValidation($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $banner = $this->repository->find($id);

        return $this->respondOk($banner);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  BannerUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update(BannerUpdateRequest $request, $id)
    {

        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $banner = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Banner updated.',
                'data'    => $banner->toArray(),
            ];

            return $this->respondOk($response);

        } catch (ValidatorException $e) {

            return $this->respondValidation($e);

        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        return $this->respondOk([
                'message' => 'Banner deleted.',
                'deleted' => $deleted,
            ]);
    }

    /**
     * Upload image banners.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */

    public function upload(Request $request)
    {
        if (!$request->hasFile('image')) {
            return $this->respondError(500, 'Arquivo não existe!');
        }

        $logo = $request->image;
        $save = $logo->store('banners');

        $filename = str_replace('banners/', '', $save);
        
        $this->fixOrientationImage(storage_path('app/banners/'.$filename));
        
        return $this->respondOk(['image' => $filename]);
    }
}
