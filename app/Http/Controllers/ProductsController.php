<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ProductRepository;
use App\Criteria\StatusActiveCriteria;

class ProductsController extends Controller
{
  private $products;

  public function __construct(ProductRepository $products)
  { 
    $this->products = $products;
    $this->products->pushCriteria(StatusActiveCriteria::class);
  }

  public function index()
  {
    $products = $this->products->paginate(8);

    return view('products', compact('products'));
  }

  public function details( $id )
  {
    $product = $this->products->find($id);

    return view('product', compact('product'));
  }
}
