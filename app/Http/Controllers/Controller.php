<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Image;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	protected function respond($data, $status, $status_code=200, $error=null) {
        $response = [
            'response' => $status,
            'data' => $data,
        ];

        if (!empty($error))
            $response['messages'] = $error;

        $options = env('APP_ENV') == 'testing' ? 0 : JSON_PRETTY_PRINT;

        return response()->json($response, $status_code, [], $options);
    }

    protected function respondOk($data) {
        return $this->respond($data, 'ok', 200);
    }
    protected function respondNotFound($message='') {
        return $this->respond(null, 'notfound', 404, $message);
    }
    protected function respondError($code=500, $message='') {
        return $this->respond(null, 'error', $code, $message);
    }
    protected function respondValidation($validator) {
        return $this->respond(
            null,
            'validation',
            422,
            $validator
        );
    }   

    protected function respondInvalidUser($message) {
        return $this->respondError(401, $message);
    }

    protected function fixOrientationImage($path)
    {
        Image::make($path)
             ->orientate()
             ->save($path);
    }
}
