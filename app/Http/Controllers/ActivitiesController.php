<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ActivityRepository;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\ActivityCreateRequest;
use App\Http\Requests\ActivityUpdateRequest;
use App\Validators\ActivityValidator;
use Carbon\Carbon;
use Auth;
use Entrust;

class ActivitiesController extends Controller
{
  private $activities;

  /**
   * @var EventValidator
   */
  protected $validator;

  public function __construct(ActivityRepository $activities, ActivityValidator $validator)
  { 
    $this->middleware('auth');
    $this->middleware('role:admin|ti|diretoria|gerente');

    $this->activities = $activities;
    $this->validator = $validator;
  }

  public function index()
  {
    $activities = $this->activities->with(['product', 'client'])->paginate();

    return view('activities', compact('activities'));
  }

  public function details( $id )
  {
    $activity = $this->activities->find($id);

    return view('activity', compact('activity'));
  }

  public function register()
  {
    return view('activity-register', compact('activity'));
  }

  public function store(ActivityCreateRequest $request)
  {
    try {

        $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

        $inputs = $request->only('product_id', 'client_id', 'works', 'observation', 'status');
        
        
        $inputs['start_date'] = Carbon::createFromFormat('d/m/Y H:i', $request->get('date_start').' '.$request->get('time_start'));
        $inputs['end_date']   = Carbon::createFromFormat('d/m/Y H:i',  $request->get('date_end').' '.$request->get('time_end'));
        $inputs['collaborator_id'] = Auth::user()->collaborator->id;
        
        $activity = $this->activities->create($inputs);

        $response = [
            'message' => 'Nova Atividade foi criada com sucesso!',
            'data'    => $activity->toArray(),
        ];

        return redirect()->route('activity.index')->with('message', $response['message']);
    } catch (ValidatorException $e) {
        return redirect()->back()->withErrors($e->getMessageBag())->withInput();
    }
  }

  public function destroy($id)
  {
    $deleted = $this->activities->delete($id);

    return $this->respondOk([$deleted]);
  }
}
