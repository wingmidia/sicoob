<?php

namespace App\Http\Controllers;

use App\Repositories\CampaignRepository;
use App\Repositories\IndicationRepository;
use App\Repositories\ProductionRepository;
use Illuminate\Http\Request;
use App\Repositories\NewsRepository;
use App\Repositories\EventRepository;
use App\Models\News;
use App\Models\Collaborator;
use Riari\Forum\Models\Thread;
use App\Models\Banner;
use App\Models\Widget;
use App\Models\Notification;
use Carbon\Carbon;
use App\Criteria\EventsActiveCriteria;
use App\Models\Config;

class HomeController extends SiteController
{
    private $news;
    private $events;

    public function __construct(NewsRepository $news, EventRepository $events)
    {
        $this->news = $news;
        $this->events = $events;
    }

    public function index(ProductionRepository $productionRepository, IndicationRepository $indicationRepository, CampaignRepository $campaignRepository)
    {
        $this->events->pushCriteria(app(EventsActiveCriteria::class));
        $events = $this->events->scopeQuery(function ($query) {
            return $query->orderBy('event_date', 'asc');
        })->paginate(3);

        $getNotification = Notification::active()->get();
        $notification = $getNotification->count() ? $getNotification->random(1)->first() : null;


        $banners = Banner::active()->type('main')->get();

        $news = News::active()
            ->feacture()
            ->private(0)
            ->orderByRaw('start_date desc')
            ->limit(3)
            ->get();


        $birth_month = Collaborator::where('status',1)->whereRaw("MONTH(date_birth) = MONTH('" . Carbon::now()->toDateString() . "')")
            ->whereRaw("DAY(date_birth) <= DAY('" . Carbon::now()->lastOfMonth()->toDateString() . "')")
            ->orderByRaw('MONTH(date_birth), DAY(date_birth)')
            ->limit(6)
            ->get();

        $threads = Thread::recent()
            ->where('locked', 0)
            ->whereHas('category', function ($q) {
                return $q->where('private', 0);
            })
            ->orderBy('id', 'desc')
            ->limit(3)
            ->get();

        $widgets = Widget::where('status', 1)->orderBy('name')->get();

        $campaign = $campaignRepository->all()->last();

        $production_ranking = [];

        foreach ($campaign->categories as $product){

            $filter_products = [
                'categories' => [$product->id]
            ];

            $production_ranking[$product->id] = $productionRepository->getUserRanking($campaign->id, $filter_products)->get();

        }

        $indication_ranking = $indicationRepository->getUserRanking($campaign->id)->get();

        $exibition = Config::where('key','rank')->first();
        $exibition = $exibition->value == 'on';

        return view('home',
            [
                'events' => $events,
                'news' => $news,
                'birth_month' => $birth_month,
                'threads' => $threads,
                'notification' => $notification,
                'banners' => $banners,
                'widgets' => $widgets,
                'production_ranking' => $production_ranking,
                'indication_ranking' => $indication_ranking,
                'campaign' => $campaign,
                'exibition' => $exibition
            ]);
    }

    public function getWidget($id = null)
    {
        $widget = Widget::where('status', 1)->find($id);

        if (!$widget) {
            return $this->respondOk(['widget' => null]);
        }

        return $this->respondOk(['widget' => $widget->widget]);
    }
}
