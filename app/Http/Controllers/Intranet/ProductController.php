<?php

namespace App\Http\Controllers\Intranet;

use App\Http\Requests\ProductRequest;
use App\Models\Indication;
use App\Models\Product;
use App\Repositories\ClientRepository;
use App\Repositories\IndicationRepository;
use App\Repositories\ProductFieldsRepository;
use App\Repositories\ProductRepository;
use App\Validators\ProductFieldsValidator;
use App\Validators\ProductValidator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\Html\Builder;
use Illuminate\Support\Facades\View;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Models\Category;

/**
 * Class ProductController
 * @package App\Http\Controllers\Intranet
 */
class ProductController extends Controller
{

    /**
     * @var ProductRepository
     */
    private $repository;

    /**
     * @var ProductValidator
     */
    private $validator;

    /**
     * ProductController constructor.
     * @param ProductRepository $repository
     * @param ProductValidator $validator
     */
    public function __construct(ProductRepository $repository, ProductValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;

        $this->controllers = arrayToObject([
            'title' => 'Produtos',
            'icon' => 'fa fa-briefcase',
            'route' => 'intranet.products.index',
            'actions' => [
                'add' => [
                    'route' => 'intranet.products.show',
                    'permission' => 'products'
                ],
                'edit' => [
                    'route' => 'intranet.products.show',
                    'permission' => 'products'
                ],
                'store' => [
                    'route' => 'intranet.products.store'
                ],
                'delete' => [
                    'route' => 'intranet.products.destroy',
                    'permission' => 'products'
                ]
            ],
        ]);

        View::share('controllers', $this->controllers);
    }


    /**
     * @param Builder $builder
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Builder $builder){

        if (request()->ajax()) {
            return $this->repository->getDatatables()->make(true);
        }

        $datatable = $builder
            ->addIndex()
            ->addTitle(['title' => 'Nome'])
            ->addCategory(Category::all()->pluck('name','id'))
            ->addStatus(['data' => 'indication', 'name' => 'indication', 'title' => 'Indicação'])
            ->addStatus()
            ->addAction()
            ->addButtonColVis();

        return view('intranet.layouts.datatables.index', compact( 'datatable'));

    }

    /**
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id = null)
    {

        if($id){

            $item = $this->repository
                ->find($id);

        } else {

            $item = new Product();

        }

        $field_types = [
            'text_small' => 'Texto curto',
            'text_larger' => 'Texto longo',
            'date' => 'Data',
            'datetime' => 'Data e Hora',
            'select_single'    => 'Seleção única',
            'select_multiple'    => 'Seleção múltipla',
            'radio'    => 'Marcação única',
            'checkbox'    => 'Marcação múltipla',
        ];

        $categories = Category::all()->pluck('name','id');

        return view('intranet.products.show', compact('item', 'field_types', 'categories'));

    }

    /**
     * @param ProductRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(
        ProductRequest $request,
        ProductFieldsRepository $productFieldsRepository,
        ProductFieldsValidator $productFieldsValidator
    ){

        //dd($request->all());
        try {

            $inputs = $request->all();

            $inputs['status'] = ($request->has('status')) ? 1 : 0;
            $inputs['indication'] = ($request->has('indication')) ? 1 : 0;

            if(!empty($request->id)){

                $this->validator->with($inputs)->passesOrFail(ValidatorInterface::RULE_UPDATE);

                $item = $this->repository->update($inputs, $request->id);

            } else {

                $this->validator->with($inputs)->passesOrFail(ValidatorInterface::RULE_CREATE);

                $item = $this->repository->create($inputs);

            }

            if($request->has('fields')){

                $fields = $request->input('fields');
                

                foreach ($fields as $field) {

                    if(!empty($field['type']) && !empty($field['name'])){

                        $field['product_id'] = $item->id;

                        if(!empty($field['options'])){

                            $options = [];

                            foreach ($field['options'] as $option) {

                                $options[$option['key']] = $option['value'];

                            }

                            $field['options'] = json_encode($options);

                        }

                        if(!empty($field['id'])){

                            $productFieldsValidator->with($field)->passesOrFail(ValidatorInterface::RULE_UPDATE);

                            $productFieldsRepository->update($field, $field['id']);

                        } else {

                            $productFieldsValidator->with($field)->passesOrFail(ValidatorInterface::RULE_CREATE);

                            $productFieldsRepository->create($field);

                        }

                    }

                }

            }

            return redirect()
                ->route('intranet.products.show', $item->id)
                ->with('alert', collect([
                    'message' => 'Produto salvo com sucesso!',
                    'status' => 'success'
                ]));

        } catch (ValidatorException $e) {

            return redirect()->route('intranet.products.index')->with('alert', 'Erro ao salvar! ' . $e);

        }


    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id){

        try {

            $this->repository->delete($id);

            return redirect()
                ->route('intranet.products.index')
                ->with('alert', collect([
                    'message' => 'Produto removido com sucesso!',
                    'status' => 'success'
                ]));

        } catch (\Exception $e) {

            return redirect()
                ->route('intranet.products.index')
                ->with('alert', collect([
                    'message' => 'O produto não pode ser removido. Tente novamente!' . $e->getMessage(),
                    'status' => 'danger'
                ]));


        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProductIndication(Request $request){

        $product = $this->repository;

        $product = $product->find($request->input('id'));

        $product->fields;

        $indication = Indication::join('indication_product', 'indications.id', '=', 'indication_product.indication_id')
            ->where([
                ['indications.client_id', $request->input('client_id')],
                ['indication_product.product_id', $request->input('id')],
                ['indication_product.concluded', 0],
            ])
            ->first();

        if($indication) {

            $indication->collaborator;
            $indication->created_at_format = $indication->created_at->format('d/m/Y');

        }

        return response()->json([
            'product' => $product,
            'indication' => $indication
        ]);

    }

}
