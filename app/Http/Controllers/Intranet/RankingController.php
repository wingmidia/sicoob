<?php

namespace App\Http\Controllers\intranet;

use App\DataTables\Html\Builder;
use App\Models\Collaborator;
use App\Models\Departament;
use App\Models\IndicationStatus;
use App\Models\Category;
use App\Repositories\CampaignRepository;
use App\Repositories\IndicationRepository;
use App\Repositories\CategoryionRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use App\Models\Config;

class RankingController extends Controller {


    /**
     * RankingController constructor.
     */
    public function __construct()
    {
        $this->controllers = arrayToObject([
            'title' => 'Ranking',
            'icon' => 'fa fa-trophy',
            'route' => 'intranet.ranking.sale',
            'actions'=> [
                'indications'=>[
                    'route' => 'intranet.ranking.indication'
                ]
            ]
        ]);

        View::share('controllers', $this->controllers);
    }

    public function sales(Builder $builder, CategoryRepository $productionRepository, CampaignRepository $campaignRepository, $id){

        $campaign = $campaignRepository->find($id);

        if(request()->ajax()){
            $request = \Request::query();

            $filters = [];
            if(isset($request['filters'])) {
                parse_str($request['filters'], $filters);
            }

            return $productionRepository->getRankingDataTables($id, $filters)->make(true);
        }

        $departaments = Departament::where('prospection', true)->get();
        $categories = Category::whereHas('campaigns', function ($query) use ($id) {
            $query->where('campaigns.id', '=', $id);
        })->get();

        $datatable = $builder
            ->ajax([
                'data'=> 'renderTable'
            ])
            ->addColumn(['data' => 'id', 'name' => 'users.id', 'title' => 'POSIÇÃO', 'width' => '5%', 'render' => 'datatablesIndexRanking(meta)'])
            ->addTitle(['data' => 'name', 'name' => 'users.name', 'title' => 'Nome do colaborador'])
            ->addColumn(['data' => 'total', 'name' => 'users.total', 'title' => 'Valor total em vendas', 'width' => '10%', 'searchable' => false])
            ->addColumn(['data' => 'sales', 'name' => 'users.sales', 'title' => 'Quantidade de vendas', 'width' => '10%', 'searchable' => false])
            ->parameters([
                'buttons' => [
                    ['text'  => '<i class="fa fa-filter"></i> <span class="hidden-xs filter-btn">FILTRAR</span>']
                ]
            ])
            ->addButtonColVis();

        if(Auth::user()->can('activity-admin'))
            $datatable = $datatable
                ->addButtonExcel();

        $exibition = Config::where('key','rank')->first();
        $exibition = $exibition->value == 'on';

        return view('intranet.ranking.sale', compact('datatable'))
                    ->with('categories', $categories)
                    ->with('campaign', $campaign)
                    ->with('departaments', $departaments)
                    ->with('exibition',$exibition);
    }

    public function indications(Builder $builder, IndicationRepository $indicationRepository, CampaignRepository $campaignRepository, $id){


        $campaign = $campaignRepository->find($id);

        $departaments = Departament::where('prospection', true)->get();
        $categories = Category::whereHas('campaigns', function ($query) use ($id) {
            $query->where('campaigns.id', '=', $id);
        })->get();

        $indicationsStatus = IndicationStatus::all();

        if(request()->ajax()){
            $request = \Request::query();

            $filters = [];
            if(isset($request['filters'])) {
                parse_str($request['filters'], $filters);
            }

            return $indicationRepository->getRankingDataTables($id, $filters)->make(true);
        }

        $datatable = $builder
            ->ajax([
                'data'=> 'renderTable'
            ])
            ->addColumn(['data' => 'id', 'name' => 'users.id', 'title' => 'POSIÇÃO', 'width' => '5%', 'render' => 'datatablesIndexRanking(meta)'])
            ->addTitle(['data' => 'name', 'name' => 'users.name', 'title' => 'Nome do colaborador'])
            ->addColumn(['data' => 'indications', 'name' => 'indications', 'title' => 'Quantidade de indicações', 'width' => '10%'])
            ->parameters([
                'buttons' => [
                    ['text'  => '<i class="fa fa-filter"></i> <span class="hidden-xs filter-btn">FILTRAR</span>']
                ]
            ])
            ->addButtonColVis();

        \Debugbar::info($builder->parameters());

        if(Auth::user()->can('activity-admin'))
            $datatable = $datatable
                ->addButtonExcel();

        $exibition = Config::where('key','rank')->first();
        $exibition = $exibition->value == 'on';

        return view('intranet.ranking.indication', compact('datatable'))
            ->with('categories', $categories)
            ->with('campaign', $campaign)
            ->with('indicationsStatus', $indicationsStatus)
            ->with('departaments', $departaments)
            ->with('exibition',$exibition);
    }

}
