<?php

namespace App\Http\Controllers\Intranet;

use App\DataTables\Html\Builder;
use App\Http\Requests\ProductionCreateRequest;
use App\Http\Requests\ProductionUpdateRequest;
use App\Models\Indication;
use App\Repositories\ClientRepository;
use App\Repositories\CollaboratorRepository;
use App\Repositories\IndicationRepository;
use App\Repositories\ProductionRepository;
use App\Repositories\ProductRepository;
use App\Validators\ClientValidator;
use App\Validators\IndicationValidator;
use App\Validators\ProductionValidator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Auth;

/**
 * Class ProductionController
 * @package App\Http\Controllers\Intranet
 */
class ProductionController extends Controller
{

    /**
     * @var ProductionRepository
     */
    private $repository;

    /**
     * @var ProductionValidator
     */
    private $validator;

    /**
     * ProductionController constructor.
     * @param ProductionRepository $repository
     * @param ProductionValidator $validator
     */
    public function __construct(ProductionRepository $repository, ProductionValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;

        $this->controllers = arrayToObject([
            'title' => 'Produção',
            'icon' => 'fa fa-usd',
            'route' => 'intranet.productions.index',
            'actions' => [
                'add' => [
                    'route' => 'intranet.productions.create',
                    'permission' => 'productions-create'
                ],
                'store' => [
                    'route' => 'intranet.productions.store'
                ],
                'show' => [
                    'route' => 'intranet.productions.show'
                ],
                'edit' => [
                    'route' => 'intranet.productions.edit',
                    'permission' => 'productions-update'
                ],
                'update' => [
                    'route' => 'intranet.productions.update'
                ],
                'delete' => [
                    'route' => 'intranet.productions.destroy',
                    'permission' => 'productions-delete'
                ],
                'manager' => [
                    'route' => 'intranet.productions.manager',
                    'permission' => 'productions-manager'
                ],
                'client' => [
                    'verify' => [
                        'route' => 'intranet.productions.client.verify'
                    ]
                ],
                'product' => [
                    'verify' => [
                        'route' => 'intranet.productions.product.verify'
                    ]
                ]
            ],
        ]);

        View::share('controllers', $this->controllers);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Builder $builder)
    {
        if (request()->ajax()) {
            return $this->repository->getDatatables()->make(true);
        }

        $datatable = $builder
            ->addColumn(['data' => 'created_at', 'name' => 'productions.created_at', 'title' => 'Criada em', 'width' => '10%'])
            ->addTitle(['data' => 'client_name', 'name' => 'clients.name', 'title' => 'Nome do cliente'])
            ->addColumn(['data' => 'products_count', 'name' => 'products_count', 'title' => 'Qtd de produtos', 'width' => '10%', 'searchable' => false])
            ->addColumn(['data' => 'value', 'name' => 'value', 'title' => 'Valor Total', 'width' => '10%', 'searchable' => false])
            ->addColumn(['data' => 'production_status', 'name' => 'productions.status', 'title' => 'Status', 'width' => '16%', 'render' => 'datatablesFormatProductionStatus(data)'])
            ->addAction()
            ->addButtonColVis();

        if(Auth::user()->can('productions-export'))
            $datatable = $datatable
                ->addButtonExcel();

        return view('intranet.productions.index', compact('datatable'));
    }

    public function manager(Builder $builder)
    {

        if (request()->ajax()) {
            return $this->repository->getDatatablesManager()->make(true);
        }

        $datatable = $builder
            ->addTitle(['data' => 'collaborator_name', 'name' => 'collaborators.name', 'title' => 'Colaborador'])
            ->addTitle(['data' => 'client_name', 'name' => 'clients.name', 'title' => 'Nome do cliente'])
            ->addColumn(['data' => 'products_count', 'name' => 'products_count', 'title' => 'Qtd de produtos', 'width' => '10%', 'searchable' => false])
            ->addColumn(['data' => 'value', 'name' => 'value', 'title' => 'Valor Total', 'width' => '10%'])
            ->addColumn(['data' => 'created_at', 'name' => 'productions.created_at', 'title' => 'Criada em', 'width' => '10%'])
            ->addColumn(['data' => 'production_status', 'name' => 'productions.status', 'title' => 'Status', 'width' => '15%', 'render' => 'datatablesFormatProductionStatus(data)'])
            ->addAction()
            ->addButtonColVis()
            ->addButtonExcel();

        return view('intranet.layouts.datatables.index', compact( 'datatable'));
    }

    /**
     * @param ProductRepository $productRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(ProductRepository $productRepository)
    {
        $products = $productRepository->ofIndication()->all();

        return view('intranet.productions.create', compact('products'));
    }

    /**
     * @param ProductionCreateRequest $request
     * @param ClientRepository $clientRepository
     * @param ClientValidator $clientValidator
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(
        ProductionCreateRequest $request,
        ClientRepository $clientRepository,
        ClientValidator $clientValidator,
        IndicationRepository $indicationRepository,
        IndicationValidator $indicationValidator
    )
    {

        try{

            $collaborator = Auth::user()->collaborator;

            // Cliente

            $inputs_client = $request->only(
                'type_profile',
                'name',
                'date_birth',
                'email',
                'cellphone',
                'phone1',
                'phone2',
                'address',
                'number',
                'complement',
                'district',
                'city',
                'state',
                'postcode'
            );

            if($request->input('type_profile') == 'F'){
                $inputs_client['cpf'] = $request->input('cpf');
            } else {
                $inputs_client['cnpj'] = $request->input('cnpj');
                $inputs_client['company_name'] = $request->input('company_name');
            }

            if(!empty($inputs_client['date_birth']))
                $inputs_client['date_birth'] = Carbon::createFromFormat('d/m/Y', $inputs_client['date_birth'])->toDateString();

            if(!empty($request->input('client_id'))){

                $inputs_client['collaborator_id'] = $collaborator->id;

                $clientValidator->with($inputs_client)->passesOrFail(ValidatorInterface::RULE_UPDATE);

                $client = $clientRepository->update($inputs_client, $request->input('client_id'));

            } else {

                $clientValidator->with($inputs_client)->passesOrFail(ValidatorInterface::RULE_CREATE);

                $client = $clientRepository->create($inputs_client);

            }

            // Producao

            $values = [
                'client_id' => $client->id,
                'collaborator_id' => $collaborator->id,
                'status' => ($request->has('contestation')) ? 2 : 3,
            ];

            $this->validator->with($values)->passesOrFail(ValidatorInterface::RULE_CREATE);

            $production = $this->repository->create($values);

            // Produtos e indicacao

            $products = $request->input('products');

            $indications = [];

            foreach ($products as $product){

                if(!isset($product['indication_id']) || empty($product['indication_id'])){

                    // cria indicacao

                    $values_indication = [
                        'client_id' => $client->id,
                        'collaborator_id' => $collaborator->id,
                        'indication_status_id' => 9001,
                        'display_at' => Carbon::now()
                    ];

                    $indicationValidator->with($values_indication)->passesOrFail(ValidatorInterface::RULE_CREATE);

                    $indication = $indicationRepository->create($values_indication);

                    $indication->products()->attach($product['product_id'], ['concluded' => true]);

                    $product['indication_id'] = $indication->id;

                } else{

                    $indication = $indicationRepository->find($product['indication_id']);

                    if(!$indication->indication_status_id || $indication->indication_status_id == 9000) {

                        $indicationRepository->update(
                            [
                                'indication_status_id' => 9001
                            ],
                            $indication->id
                        );

                    }

                }

                $indications[] = $product['indication_id'];

                $production->products()->attach($product['product_id'], [
                    'indication_id' => $product['indication_id'],
                    'sold_at' => Carbon::createFromFormat('d/m/Y', $product['sold_at'])->toDateString(),
                    'value' => str_replace(',', '.', str_replace('.', '', $product['value'])),
                ]);

                // campos personalizados

                if(!empty($product['fields'])){

                    foreach ($product['fields'] as $field_id => $field_value){

                        if(empty($field_value))
                            $field_value = '';

                        $production->fields()->attach($field_id, [
                           'value' => $field_value
                        ]);

                    }

                }

            }

            // Marca as indicacoes revertidas como concluida
            foreach ($indications as $indication_id)
                $indicationRepository->find($indication_id)->products()->syncWithoutDetaching([$product['product_id'] => ['concluded' => true]]);

            return redirect()
                ->route('intranet.productions.index')
                ->with('alert', collect([
                    'message' => 'Produção incluída com sucesso!',
                    'status' => 'success'
                ]));

        } catch (ValidatorException $e) {

            return redirect()->route('intranet.productions.index')->with('alert', 'Erro ao incluir a produção, tente novamente! ' . $e);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(IndicationRepository $indicationRepository, $id)
    {
        $item = $this->repository->find($id);

        foreach ($item->products as &$product) {

            if ($product->pivot->indication_id)
                $product->indication = $indicationRepository->find($product->pivot->indication_id);

            if($product->indication){

                $product->indication->collaborator;
                $product->indication->created_at_format = $product->indication->created_at->format('d/m/Y');

            }

        }

        return view('intranet.productions.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(
        IndicationRepository $indicationRepository,
        CollaboratorRepository $collaboratorRepository,
        $id)
    {

        $item = $this->repository->find($id);

        foreach ($item->products as &$product) {

            if ($product->pivot->indication_id)
                $product->indication = $indicationRepository->find($product->pivot->indication_id);

            if($product->indication){

                $product->indication->collaborator;
                $product->indication->created_at_format = $product->indication->created_at->format('d/m/Y');

            }

        }

        $collaborators = $collaboratorRepository->all()->pluck('name', 'id');

        return view('intranet.productions.edit', compact('item', 'can_edit', 'collaborators'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(
        ProductionUpdateRequest $productionUpdateRequest,
        IndicationRepository $indicationRepository,
        IndicationValidator $indicationValidator,
        $id)
    {
        try{

            $inputs = $productionUpdateRequest->only('status');

            $this->validator->with($inputs)->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $production = $this->repository->update($inputs, $id);

            foreach ($production->products as $product) {

                $product->indication = $indicationRepository->find($product->pivot->indication_id);

                $concluded = false;
                $indication_status_id = 9000;

                if($productionUpdateRequest->input('status') > 0){

                    $concluded = true;
                    $indication_status_id = 9001;

                }

                // conclusao da indicacao em razao do status

                $product->indication->products()->syncWithoutDetaching([$product->id => ['concluded' => $concluded]]);

                // mudanca do status de prospeccao da indicacao

                if(!$product->indication->indication_status_id || $product->indication->indication_status_id == 9000 || $product->indication->indication_status_id == 9001)
                    $indicationRepository->update(
                        [
                            'indication_status_id' => $indication_status_id
                        ],
                        $product->indication->id
                    );

            }


            if($productionUpdateRequest->has('indication')){

                $indications = $productionUpdateRequest->input('indication');

                $add_indications = [];
                $delete_indications = [];

                foreach ($production->products as $product) {

                    $indication = $indicationRepository->find($product->pivot->indication_id);

                    if($indication->collaborator_id != $indications[$product->id]){

                        $add_indications[$indications[$product->id]][] = $product->id;
                        $delete_indications[$indication->id][] = $product->id;

                    }

                }

                if(!empty($add_indications)){

                    foreach ($add_indications as $collaborator_id => $products_id) {

                        // cria indicacao

                        $values_indication = [
                            'client_id' => $production->client->id,
                            'collaborator_id' => $collaborator_id,
                            'indication_status_id' => 9001,
                            'display_at' => Carbon::now()
                        ];

                        $indicationValidator->with($values_indication)->passesOrFail(ValidatorInterface::RULE_CREATE);

                        $indication = $indicationRepository->create($values_indication);

                        $indication->products()->attach($products_id, ['concluded' => true]);

                        // troca indicacao da producao

                        DB::table('production_product')
                            ->where('production_id', $production->id)
                            ->whereIn('product_id', $products_id)
                            ->update([
                                'indication_id' => $indication->id
                            ]);

                    }

                }

                if(!empty($delete_indications)){

                    foreach ($delete_indications as $indication_id => $products_id) {

                        $indication = $indicationRepository->find($indication_id);

                        $indication->products()->detach($products_id);

                        if(count($indication->products) < 1)
                            $indicationRepository->delete($indication->id);

                    }

                }

            }

            return redirect()
                ->route('intranet.productions.edit', $id)
                ->with('alert', collect([
                    'message' => 'Produção atualizada com sucesso!',
                    'status' => 'success'
                ]));

        } catch (ValidatorException $e) {

            return redirect()->route('intranet.productions.index')->with('alert', 'Erro ao atualizar a producão, tente novamente! ' . $e);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(IndicationRepository $indicationRepository, $id)
    {
        $item = $this->repository->find($id);

        if($item->status != 1 && $item->status != 0 && $item->collaborator_id == Auth::user()->collaborator->id){

            foreach ($item->products as $product) {

                $product->indication = $indicationRepository->find($product->pivot->indication_id);

                if (!$product->indication->indication_status_id || $product->indication->indication_status_id == 9001)
                    $indicationRepository->update(
                        [
                            'indication_status_id' => null
                        ],
                        $product->indication->id
                    );

                $indicationRepository->find($product->indication)->products()->syncWithoutDetaching([$product->id => ['concluded' => false]]);

                //$indicationRepository->setConcluded(0, null, $item->client_id, $product->id);

            }

            $this->repository->delete($id);

            return redirect()
                ->route('intranet.productions.index')
                ->with('alert', collect([
                    'message' => 'Producao removida com sucesso!',
                    'status' => 'success'
                ]));

        } else {
            return redirect()->route('intranet.productions.index')->with('alert', 'Erro ao remover a producao, tente novamente! ');
        }
    }
}
