<?php

namespace App\Http\Controllers\Intranet;

use App\DataTables\Html\Builder;
use App\Http\Requests\IndicationCreateRequest;
use App\Http\Requests\IndicationUpdateRequest;
use App\Repositories\ClientRepository;
use App\Repositories\CollaboratorRepository;
use App\Repositories\DepartamentRepository;
use App\Repositories\IndicationRepository;
use App\Repositories\IndicationStatusRepository;
use App\Repositories\ProductRepository;
use App\Repositories\ProspectionRepository;
use App\Validators\ClientValidator;
use App\Validators\IndicationValidator;
use Auth;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Models\Collaborator;
use \Illuminate\Http\Request;
use App\Models\Indication;

/**
 * Class IndicationController
 * @package App\Http\Controllers\Intranet
 */
class IndicationController extends Controller
{

    /**
     * @var IndicationRepository
     */
    private $repository;

    /**
     * @var IndicationValidator
     */
    private $validator;

    /**
     * IndicationController constructor.
     * @param IndicationRepository $repository
     * @param IndicationValidator $validator
     */
    public function __construct(IndicationRepository $repository, IndicationValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;

        $this->controllers = arrayToObject([
            'title' => 'Indicação',
            'icon' => 'fa fa-compass',
            'route' => 'intranet.indications.index',
            'actions' => [
                'show' => [
                    'route' => 'intranet.indications.show'
                ],
                'add' => [
                    'route' => 'intranet.indications.create',
                    'permission' => 'indications-create'
                ],
                'store' => [
                    'route' => 'intranet.indications.store'
                ],
                'update' => [
                    'route' => 'intranet.indications.update'
                ],
                'delete' => [
                    'route' => 'intranet.indications.destroy',
                    'permission' => 'indications-delete'
                ],
                'byme' => [
                    'route' => 'intranet.indications.byme'
                ],
                'client' => [
                    'verify' => [
                        'route' => 'intranet.indications.client.verify'
                    ],
                    'product_options' => [
                        'route' => 'intranet.indications.client.product_options'
                    ]
                ]
            ],
        ]);

        View::share('controllers', $this->controllers);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Builder $builder)
    {

        if (request()->ajax()) {
            return $this->repository->getDatatablesProspection()->make(true);
        }

        $datatable = $builder
            ->addTitle(['data' => 'client_name', 'name' => 'clients.name', 'title' => 'Nome do cliente'])
            ->addColumn(['data' => 'cellphone', 'name' => 'clients.cellphone', 'title' => 'Telefone', 'width' => '10%'])
            ->addColumn(['data' => 'created_at', 'name' => 'indications.created_at', 'title' => 'Criada em', 'width' => '10%'])
            ->addColumn(['data' => 'updated_at', 'name' => 'indications.updated_at', 'title' => 'Atualizada em', 'width' => '10%'])
            ->addColumn(['data' => 'display_at', 'name' => 'indications.display_at', 'title' => 'Retornar em', 'width' => '10%', 'render' => 'datatablesFormatIndicationDisplayAt(data, type, full)'])
            ->addColumn(['data' => 'status_name', 'name' => 'indication_status.name', 'title' => 'Status', 'width' => '15%', 'render' => 'datatablesFormatIndicationStatus(data, type, full)'])
            ->addActionShow(['title' => 'Prospectar', 'width' => '5%'])
            ->addButtonColVis();

        $estatistica = arrayToObject([
            // total de indicacoes
            'total' => [
                'geral' => $this->repository
                    ->getByPeriod(
                        Carbon::now()
                            ->subMonth(12)
                            ->firstOfMonth()
                            ->toDateString(),
                        Carbon::now()
                            ->lastOfMonth()
                            ->toDateString()
                    )
                    ->count('indication_product.product_id'),
                'mes_anterior' => $this->repository
                    ->getByPeriod(
                        Carbon::now()
                            ->subMonth()
                            ->firstOfMonth()
                            ->toDateString()
                    )
                    ->count('indication_product.product_id'),
                'mes_atual' => $this->repository
                    ->getByPeriod(
                        Carbon::now()
                            ->firstOfMonth()
                            ->toDateString()
                    )
                    ->count('indication_product.product_id')
            ],
            // total de indicacoes prospectadas
            'prospectadas' => [
                'geral' => $this->repository
                    ->getByPeriod(
                        Carbon::now()
                            ->subMonth(12)
                            ->firstOfMonth()
                            ->toDateString(),
                        Carbon::now()
                            ->lastOfMonth()
                            ->toDateString(),
                        true
                    )
                    ->count('indication_product.product_id'),
                'mes_anterior' => $this->repository
                    ->getByPeriod(
                        Carbon::now()
                            ->subMonth()
                            ->firstOfMonth()
                            ->toDateString(),
                        null,
                        true
                    )
                    ->count('indication_product.product_id'),
                'mes_atual' => $this->repository
                    ->getByPeriod(
                        Carbon::now()
                            ->firstOfMonth()
                            ->toDateString(),
                        null,
                        true
                    )
                    ->count('indication_product.product_id')
            ],
            // total de indicacoes com vendas efetuadas
            'convertidas' => [
                'geral' => $this->repository
                    ->getByPeriod(
                        Carbon::now()
                            ->subMonth(12)
                            ->firstOfMonth()
                            ->toDateString(),
                        Carbon::now()
                            ->lastOfMonth()
                            ->toDateString(),
                        null,
                        1
                    )
                    ->count('indication_product.product_id'),
                'mes_anterior' => $this->repository
                    ->getByPeriod(
                        Carbon::now()
                            ->subMonth()
                            ->firstOfMonth()
                            ->toDateString(),
                        null,
                        null,
                        1
                    )
                    ->count('indication_product.product_id'),
                'mes_atual' => $this->repository
                    ->getByPeriod(
                        Carbon::now()
                            ->firstOfMonth()
                            ->toDateString(),
                        null,
                        null,
                        1
                    )
                    ->count('indication_product.product_id')
            ],
        ]);        

        return view('intranet.indications.index', compact('datatable', 'estatistica'));
    }

    public function byMe(Builder $builder)
    {

        if (request()->ajax()) {
            return $this->repository->getDatatables()->make(true);
        }

        $datatable = $builder
            ->addColumn(['data' => 'cpf', 'name' => 'clients.cpf', 'title' => 'CPF', 'width' => '10%'])
            ->addColumn(['data' => 'cnpj', 'name' => 'clients.cnpj', 'title' => 'CNPJ', 'width' => '10%'])
            ->addTitle(['data' => 'client_name', 'name' => 'clients.name', 'title' => 'Nome do cliente'])
            ->addColumn(['data' => 'created_at', 'name' => 'indications.created_at', 'title' => 'Criada em', 'width' => '10%'])
            ->addColumn(['data' => 'updated_at', 'name' => 'indications.updated_at', 'title' => 'Atualizada em', 'width' => '10%'])
            ->addActionShow()
            ->addButtonColVis();

        return view('intranet.layouts.datatables.index', compact( 'datatable'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(
        ProductRepository $products,
        DepartamentRepository $departamentRepository,
        CollaboratorRepository $employersRepository
    )
    {

        $products = $products->ofIndication()->all();

        $departaments = $departamentRepository->ofProspection()->pluck('name', 'id');

        $employers = $employersRepository->ofActive()->pluck('name', 'id');

        return view('intranet.indications.create', compact('products', 'departaments', 'employers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(
        IndicationCreateRequest $request,
        ClientRepository $clientRepository,
        ClientValidator $clientValidator,
        CollaboratorRepository $collaboratorRepository
    )
    {

        try {

            $collaborator = Auth::user()->collaborator;

            // Cliente

            $inputs_client = $request->only(
                'type_profile',
                'name',
                'date_birth',
                'email',
                'cellphone',
                'phone1',
                'phone2'
            );

            if($request->input('type_profile') == 'F'){
                $inputs_client['cpf'] = $request->input('cpf');
            } else {
                $inputs_client['cnpj'] = $request->input('cnpj');
                $inputs_client['company_name'] = $request->input('company_name');
            }

            if(!empty($inputs_client['date_birth']))
                $inputs_client['date_birth'] = Carbon::createFromFormat('d/m/Y', $inputs_client['date_birth'])->toDateString();

            if(!empty($request->input('client_id'))){

                $inputs_client['collaborator_id'] = $collaborator->id;

                $clientValidator->with($inputs_client)->passesOrFail(ValidatorInterface::RULE_UPDATE);

                $client = $clientRepository->update($inputs_client, $request->input('client_id'));

            } else {

                $clientValidator->with($inputs_client)->passesOrFail(ValidatorInterface::RULE_CREATE);

                $client = $clientRepository->create($inputs_client);

            }

            // Indicacao

            $values = [
                'client_id' => $client->id,
                'collaborator_id' => $collaborator->id,
                'display_at' => Carbon::now()
            ];

            $this->validator->with($values)->passesOrFail(ValidatorInterface::RULE_CREATE);

            $indication = $this->repository->create($values);

            // Produtos

            $indication->products()->sync($request->input('products'));

            // Prospeccao

            if($request->has('collection')){

                $indication->prospections()->attach($collaborator->id);

            } if($request->has('employer_id') && $request->input('employer_id') !== "-1") {

                $indication->prospections()->attach($request->input('employer_id'));

            } else if($request->has('departament_id') && $request->input('departament_id') !== "-1") {

                $indication->prospections()->sync(
                    $collaboratorRepository->pickUpCollaboratorForProspecting(
                        [$request->input('departament_id')]
                    )
                );

            }

            return redirect()
                ->route('intranet.indications.index')
                ->with('alert', collect([
                    'message' => 'Indicação incluída com sucesso!',
                    'status' => 'success'
                ]));

        } catch (ValidatorException $e) {

            return redirect()->route('intranet.indications.index')->with('alert', 'Erro ao incluir a indicação, tente novamente! ' . $e);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, IndicationStatusRepository $indicationStatusRepository)
    {

        $collaborator = Auth::user()->collaborator;

        $item = $this->repository->find($id);

        $can_prospect = $item->prospections->contains('id', $collaborator->id);

        if($item->collaborator_id != $collaborator->id && !$can_prospect)
            return redirect()->route('intranet.indications.index')->with('alert', 'Você não pode visualizar essa indicação!');

        $indication_status = $indicationStatusRepository->all();

        $employers = Collaborator::where('status','1')->pluck('name', 'id');

        return view('intranet.indications.show', compact('item', 'indication_status', 'can_prospect', 'employers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(IndicationUpdateRequest $indicationUpdateRequest, $id)
    {

        try{

            $inputs = $indicationUpdateRequest->only('indication_status_id');

            if(empty($inputs['indication_status_id']))
                $inputs['indication_status_id'] = null;

            if($indicationUpdateRequest->has('display_at'))
                $inputs['display_at'] = Carbon::createFromFormat('Y-m-d\TH:i', $indicationUpdateRequest->input('display_at'))->toDateTimeString();

            $this->validator->with($inputs)->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $this->repository->update($inputs, $id);

            return redirect()
                ->route('intranet.indications.show', $id)
                ->with('alert', collect([
                    'message' => 'Indicação atualizada com sucesso!',
                    'status' => 'success'
                ]));

        } catch (ValidatorException $e) {

            return redirect()->route('intranet.indications.index')->with('alert', 'Erro ao atualizar a indicação, tente novamente! ' . $e);

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $item = $this->repository->find($id);

        if(!$item->status && $item->collaborator_id == Auth::user()->collaborator->id){

            $this->repository->delete($id);

            return redirect()
                ->route('intranet.indications.index')
                ->with('alert', collect([
                    'message' => 'Indicação removida com sucesso!',
                    'status' => 'success'
                ]));

        } else {
            return redirect()->route('intranet.indications.index')->with('alert', 'Erro ao remover a indicação, tente novamente! ');
        }

    }

    /**
     * transfer prospecting
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function transferPropecting(Request $request)
    {
        try{
            $inputs = $request->only('indication_employer_id');

            if($inputs['indication_employer_id'] == "-1") {
                return back()->with('alert', 'Selecione primeiro um colaborador que receberá a propespecção!');

            } else {
                $item_id = $request->only('item_id');

                $indication = Indication::find($item_id['item_id']);

                $indication->prospections()->sync([$inputs['indication_employer_id']]);

                return redirect()
                    ->route('intranet.indications.index')
                    ->with('alert', collect([
                        'message' => 'Indicação transferida com sucesso!',
                        'status' => 'success'
                    ]));

            }
        } catch (ValidatorException $e) {

            return redirect()->route('intranet.indications.index')->with('alert', 'Erro ao atualizar a indicação, tente novamente! ' . $e);

        }
    }
}
