<?php

namespace App\Http\Controllers\Intranet;

use App\Http\Requests\CategoryRequest;
use App\Models\Indication;
use App\Repositories\ClientRepository;
use App\Repositories\IndicationRepository;
use App\Repositories\CategoryRepository;
use App\Validators\CategoryValidator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DataTables\Html\Builder;
use Illuminate\Support\Facades\View;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Models\Category;

class CategoryController extends Controller
{

    /**
     * @var CategoryRepository
     */
    private $repository;

    /**
     * @var CategoryValidator
     */
    private $validator;

    /**
     * CategoryController constructor.
     * @param CategoryRepository $repository
     * @param CategoryValidator $validator
     */
    public function __construct(CategoryRepository $repository, CategoryValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;

        $this->controllers = arrayToObject([
            'title' => 'Categorias',
            'icon' => 'fa fa-briefcase',
            'route' => 'intranet.categories.index',
            'actions' => [
                'add' => [
                    'route' => 'intranet.categories.show',
                    'permission' => 'products'
                ],
                'edit' => [
                    'route' => 'intranet.categories.show',
                    'permission' => 'products'
                ],
                'store' => [
                    'route' => 'intranet.categories.store'
                ],
                'delete' => [
                    'route' => 'intranet.categories.destroy',
                    'permission' => 'products'
                ]
            ],
        ]);

        View::share('controllers', $this->controllers);
    }


    /**
     * @param Builder $builder
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Builder $builder){

        if (request()->ajax()) {
            return $this->repository->getDatatables()->make(true);
        }

        $datatable = $builder
            ->addIndex()
            ->addTitle(['title' => 'Nome','data' => 'name'])
            ->addAction()
            ->addButtonColVis();

        return view('intranet.layouts.datatables.index', compact( 'datatable'));

    }

    /**
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id = null)
    {

        if($id){

            $item = $this->repository
                ->find($id);

        } else {

            $item = new Category();

        }

        return view('intranet.categories.show', compact('item'));

    }

    /**
     * @param CategoryRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(
        CategoryRequest $request
    ){

        try {

            $inputs = $request->all();

            if(!empty($request->id)){

                $this->validator->with($inputs)->passesOrFail(ValidatorInterface::RULE_UPDATE);

                $item = $this->repository->update($inputs, $request->id);

            } else {

                $this->validator->with($inputs)->passesOrFail(ValidatorInterface::RULE_CREATE);

                $item = $this->repository->create($inputs);

            }

            return redirect()
                ->route('intranet.categories.show', $item->id)
                ->with('alert', collect([
                    'message' => 'Produto salvo com sucesso!',
                    'status' => 'success'
                ]));

        } catch (ValidatorException $e) {

            return redirect()->route('intranet.categories.index')->with('alert', 'Erro ao salvar! ' . $e);

        }


    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id){

        try {

            $this->repository->delete($id);

            return redirect()
                ->route('intranet.categories.index')
                ->with('alert', collect([
                    'message' => 'Categoria removida com sucesso!',
                    'status' => 'success'
                ]));

        } catch (\Exception $e) {

            return redirect()
                ->route('intranet.categories.index')
                ->with('alert', collect([
                    'message' => 'A categoria não pode ser removida. Tente novamente!' . $e->getMessage(),
                    'status' => 'danger'
                ]));


        }

    }

}