<?php

namespace App\Http\Controllers\Intranet;

use App\DataTables\Html\Builder;
use App\Http\Requests\DepartamentsRequest;
use App\Models\Departament;
use App\Repositories\DepartamentRepository;
use App\Validators\DepartamentValidator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class DepartamentController
 * @package App\Http\Controllers\Intranet
 */
class DepartamentController extends Controller
{


    /**
     * @var DepartamentRepository
     */
    private $repository;

    /**
     * @var DepartamentValidator
     */
    private $validator;

    /**
     * DepartamentController constructor.
     * @param DepartamentRepository $repository
     * @param DepartamentValidator $validator
     */
    public function __construct(DepartamentRepository $repository, DepartamentValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;

        $this->controllers = arrayToObject([
            'title' => 'Departamentos',
            'icon' => 'fa fa-tags',
            'route' => 'intranet.departaments.index',
            'actions' => [
                'add' => [
                    'route' => 'intranet.departaments.show',
                    'permission' => 'departaments'
                ],
                'edit' => [
                    'route' => 'intranet.departaments.show',
                    'permission' => 'departaments'
                ],
                'store' => [
                    'route' => 'intranet.departaments.store'
                ],
                'delete' => [
                    'route' => 'intranet.departaments.destroy',
                    'permission' => 'departaments'
                ]
            ],
        ]);

        View::share('controllers', $this->controllers);
    }

    /**
     * @param Builder $builder
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Builder $builder){

        if (request()->ajax()) {
            return $this->repository->getDatatables()->make(true);
        }

        $datatable = $builder
            ->addIndex()
            ->addTitle(['data' => 'name', 'name' => 'name', 'title' => 'Nome'])
            ->addAction()
            ->addButtonColVis();

        return view('intranet.layouts.datatables.index', compact( 'datatable'));

    }

    /**
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id = null)
    {

        if($id){

            $item = $this->repository
                ->find($id);

        } else {

            $item = new Departament();

        }

        return view('intranet.departaments.show', compact('item'));

    }

    /**
     * @param IndicationStatusRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(DepartamentsRequest $request){

        try {

            $inputs = $request->all();

            $inputs['prospection'] = ($request->has('prospection')) ? 1 : 0;

            
            if ($request->hasFile('photo')) {
                $image = $request->photo;
                $save = $image->store('public');
        
                $filename = str_replace('public/', '', $save);

                $inputs['photo'] = $filename;
            } else {
                if(isset($inputs['photo']))
                    unset($inputs['photo']);
            }

            if(!empty($request->id)){

                $this->validator->with($inputs)->passesOrFail(ValidatorInterface::RULE_UPDATE);

                $item = $this->repository->update($inputs, $request->id);

            } else {

                $this->validator->with($inputs)->passesOrFail(ValidatorInterface::RULE_CREATE);

                $item = $this->repository->create($inputs);

            }

            return redirect()
                ->route('intranet.departaments.show', $item->id)
                ->with('alert', collect([
                    'message' => 'Departamento salvo com sucesso!',
                    'status' => 'success'
                ]));

        } catch (ValidatorException $e) {

            return redirect()->route('intranet.departaments.index')->with('alert', 'Erro ao salvar! ' . $e);

        }


    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id){

        try {

            $this->repository->delete($id);

            return redirect()
                ->route('intranet.departaments.index')
                ->with('alert', collect([
                    'message' => 'Departamento removido com sucesso!',
                    'status' => 'success'
                ]));

        } catch (\Exception $e) {

            return redirect()
                ->route('intranet.departaments.index')
                ->with('alert', collect([
                    'message' => 'O Departamento não pode ser removido. Tente novamente!' . $e->getMessage(),
                    'status' => 'danger'
                ]));


        }

    }


}
