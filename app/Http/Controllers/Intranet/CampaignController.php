<?php
/**
 * Created by PhpStorm.
 * User: Wing Mídia
 * Date: 17/04/2018
 * Time: 11:16
 */

namespace App\Http\Controllers\Intranet;


use App\DataTables\Html\Builder;
use App\Http\Controllers\Controller;
use App\Http\Requests\CampaignRequest;
use App\Models\Campaign;
use App\Models\Product;
use App\Models\QuizResult;
use App\Repositories\CampaignRepository;
use App\Repositories\ProductRepository;
use App\Repositories\CategoryRepository;
use App\Validators\CampaignValidator;
use Carbon\Carbon;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Prettus\Validator\Contracts\ValidatorInterface;
use App\Models\Category;


class CampaignController extends Controller {

    private $validator;
    private $repository;

    /**
     * CampaignController constructor.
     * @param $validator
     * @param $repository
     */
    public function __construct(CampaignRepository $repository)
    {
        $this->repository = $repository;

        $this->controllers = arrayToObject([
          'title' => 'Campanhas',
          'icon' => 'fa fa-trophy',
          'route' => 'intranet.campaign.index',
            'actions' => [
                'create'=>[
                    'route' => 'intranet.campaign.create',
                    'permission' => 'campaign-create'
                ],
                'store'=>[
                    'route' => 'intranet.campaign.store',
                    'permission' => 'campaign-create'
                ],
                'edit'=>[
                    'route' => 'intranet.campaign.edit',
                    'permission' => 'campaign-edit'
                ],
                'update'=>[
                    'route' => 'intranet.campaign.update',
                    'permission' => 'campaign-edit'
                ],
                'delete' => [
                    'route' => 'intranet.campaign.delete',
                    'permission' => 'campaign-delete'
                ]
            ]
        ]);

        View::share('controllers', $this->controllers);

    }

    public function index(Builder $builder){

        if(request()->ajax())
            return $this->repository->getDataTables()->make(true);

        $datatable = $builder
            //->addColumn(['data' => 'created_at', 'name' => 'campaigns.created_at', 'title' => 'Criado em', 'width' => '10%'])
            ->addTitle(['data' => 'name', 'name' => 'campaigns.name', 'title' => 'Nome da campanha'])
            ->addColumn(['data' => 'start_at', 'name' => 'campaigns.start_at', 'title' => 'Data de início', 'width' => '10%'])
            ->addColumn(['data' => 'end_at', 'name' => 'campaigns.end_at', 'title' => 'Data final', 'width' => '10%'])
            //->addTitle(['data' => 'description', 'name' => 'campaigns.description', 'title' => 'Descrição'])
            ->addColumn(['data' => 'active', 'name' => 'campaigns.active', 'title' => 'Status', 'width' => '16%', 'render' => 'datatablesFormatCampaignStatus(data,type,full)'])
            ->addAction()
            ->addButtonColVis();

        if(Auth::user()->can('campaign-export'))
            $datatable = $datatable
                ->addButtonExcel();


        return view('intranet.campaign.index', compact('datatable'));
    }

    public function create(CategoryRepository $categoryRepository){
        $categories = Category::select()->get();

        return view('intranet.campaign.create', compact('categories'));
    }

    public function store(CampaignRequest $request, CampaignValidator $validator){
        $request->only(
            'name',
            'start_at',
            'end_at',
            'description',
            'active',
            'categories'
        );

        $storeCampaign = [];
        $storeCampaign['name'] = $request->input('name');
        $storeCampaign['image'] = $request->input('image_name');
        $storeCampaign['start_at'] = Carbon::createFromFormat('d/m/Y', $request->input('start_at'))->startOfDay();
        $storeCampaign['end_at'] = Carbon::createFromFormat('d/m/Y', $request->input('end_at'))->endOfDay();
        $storeCampaign['description'] = $request->input('description');
        $storeCampaign['active'] = $request->input('active');

        $validator->with($storeCampaign)->passesOrFail(ValidatorInterface::RULE_CREATE);

        $categories = $request->input('categories');

        \Debugbar::info($categories);

        $storeCampaign = $this->repository->create($storeCampaign);

        $storeCampaign->categories()->sync($categories);


        return redirect()
                ->route('intranet.campaign.index')
                ->with('altert', collect([
                    'message' => 'Campanha criada com sucesso',
                    'status' => 'success'
                ]));

    }

    public function edit($id){
        if(empty($id)){
            return redirect()
                    ->route('intranet.campaign.index')
                    ->with('alert', collect([
                        'message' => 'Campanha não encontrada',
                        'status' => 'success'
                    ]));
        }

        $campaign = $this->repository->find($id);

        $categories = Category::select()->get();

        $campaign->categories;

        if(empty($campaign)){
            return redirect()
                ->route('intranet.campaign.index')
                ->with('alert', collect([
                    'message' => 'Campanha não encontrada',
                    'status' => 'success'
                ]));
        }

        return view('intranet.campaign.edit', compact('campaign'))->with('categories', $categories);
    }

    public function update(CampaignRequest $request)
    {
        $request->only(
            'id',
            'name',
            'start_at',
            'end_at',
            'description',
            'active',
            'categories'
        );
        $campaignId = $request->input('id');
        if(empty($campaignId)){
            return redirect()
                ->route('intranet.campaign.index')
                ->with('altert', collect([
                    'message' => 'Não foi possível editar campanha',
                    'status' => 'success'
                ]));
        }

        $editCampaign = [];
        $editCampaign['name'] = $request->input('name');
        $editCampaign['image'] = $request->input('image');
        $editCampaign['start_at'] = Carbon::createFromFormat('d/m/Y', $request->input('start_at'))->startOfDay();
        $editCampaign['end_at'] = Carbon::createFromFormat('d/m/Y', $request->input('end_at'))->endOfDay();
        $editCampaign['description'] = $request->input('description');
        $editCampaign['active'] = $request->input('active');

        $categories = $request->input('categories');

        $updateCampaign = $this->repository->update($editCampaign, $campaignId);

        $prods = $updateCampaign->categories()->sync($categories);

        return redirect()
            ->route('intranet.campaign.index')
            ->with('alert', collect([
                'message' => 'Campanha editada com sucesso',
                'status' => 'success'
            ]));
    }

    public function delete($id){
        if(empty($id)){
            return redirect()
                    ->route('intranet.campaign.index')
                    ->with('alert', collect([
                        'message' => 'Campanha não encontrada',
                        'status' => 'success'
                    ]));
        }

        $this->repository->delete($id);

        return redirect()
            ->route('intranet.campaign.index')
            ->with('alert', collect([
                'message' => 'Campanha deletada com sucesso',
                'status' => 'success'
            ]));

    }

    public function listCampaignsRanking(CampaignRepository $campaignRepository){
        $now = Carbon::now();

        $activeCampaigns = Campaign::whereDate('end_at', '>=', $now)
//                                    ->whereDate('end_at', '>=', $now)
                                    ->where('active', true)
                                    ->get();
        $completeCampaigns = Campaign::where('end_at', '<', $now)->get();
//            ->orWhere('end_at', '<', $now)->get();

        return \view('intranet.campaign.list')
                        ->with('activeCampaigns', $activeCampaigns)
                        ->with('completeCampaigns', $completeCampaigns);
    }

    public function uploadImage(Request $request){
        if($request->hasFile('file')){
            if($request->file('file')->isValid()){
                try{
                    $file = $request->file('file');
                    $now = date('Y_m_d_');

                    $name = "campaign_$now".rand(11111, 99999) . '.' . $file->getClientOriginalExtension();
                    $request->file('file')->move("intranet/assets/img/campaigns", $name);
                    return ['name'=>$name, 'success'=>true];
                }catch (FileNotFoundException $ex){
                    \Debugbar::info('nao', $ex);
                    return ['success'=>false];
                }
            }
        }
    }

}