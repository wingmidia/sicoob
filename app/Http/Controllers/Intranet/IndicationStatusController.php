<?php

namespace App\Http\Controllers\Intranet;

use App\DataTables\Html\Builder;
use App\Http\Requests\IndicationStatusRequest;
use App\Models\IndicationStatus;
use App\Repositories\IndicationStatusRepository;
use App\Validators\IndicationStatusValidator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class IndicationStatusController
 * @package App\Http\Controllers\Intranet
 */
class IndicationStatusController extends Controller
{

    /**
     * @var IndicationStatusRepository
     */
    private $repository;

    /**
     * @var IndicationStatusValidator
     */
    private $validator;

    /**
     * IndicationStatusController constructor.
     * @param IndicationStatusRepository $repository
     * @param IndicationStatusValidator $validator
     */
    public function __construct(IndicationStatusRepository $repository, IndicationStatusValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;

        $this->controllers = arrayToObject([
            'title' => 'Status de indicação',
            'icon' => 'fa fa-compass',
            'route' => 'intranet.indications.status.index',
            'actions' => [
                'add' => [
                    'route' => 'intranet.indications.status.show',
                    'permission' => 'indication-status'
                ],
                'edit' => [
                    'route' => 'intranet.indications.status.show',
                    'permission' => 'indication-status'
                ],
                'store' => [
                    'route' => 'intranet.indications.status.store'
                ],
                'delete' => [
                    'route' => 'intranet.indications.status.destroy',
                    'permission' => 'indication-status'
                ]
            ],
        ]);

        View::share('controllers', $this->controllers);
    }

    /**
     * @param Builder $builder
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Builder $builder){

        if (request()->ajax()) {
            return $this->repository->getDatatables()->make(true);
        }

        $datatable = $builder
            ->addIndex()
            ->addTitle(['data' => 'name', 'name' => 'name', 'title' => 'Nome'])
            ->addActive(['data' => 'state', 'name' => 'state', 'title' => 'Status efetivado'])
            ->addStatus()
            ->addAction()
            ->addButtonColVis();

        return view('intranet.layouts.datatables.index', compact( 'datatable'));

    }

    /**
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id = null)
    {

        if($id){

            $item = $this->repository
                ->find($id);

        } else {

            $item = new IndicationStatus();

        }

        return view('intranet.indications.status.show', compact('item'));

    }

    /**
     * @param IndicationStatusRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(IndicationStatusRequest $request){

        try {

            $inputs = $request->all();

            $inputs['status'] = ($request->has('status')) ? 1 : 0;
            $inputs['state'] = ($request->has('state')) ? 1 : 0;

            if(!empty($request->id)){

                $this->validator->with($inputs)->passesOrFail(ValidatorInterface::RULE_UPDATE);

                $item = $this->repository->update($inputs, $request->id);

            } else {

                $this->validator->with($inputs)->passesOrFail(ValidatorInterface::RULE_CREATE);

                $item = $this->repository->create($inputs);

            }

            return redirect()
                ->route('intranet.indications.status.show', $item->id)
                ->with('alert', collect([
                    'message' => 'Status salvo com sucesso!',
                    'status' => 'success'
                ]));

        } catch (ValidatorException $e) {

            return redirect()->route('intranet.indications.status.index')->with('alert', 'Erro ao salvar! ' . $e);

        }


    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id){

        try {

            $this->repository->delete($id);

            return redirect()
                ->route('intranet.indications.status.index')
                ->with('alert', collect([
                    'message' => 'Status removido com sucesso!',
                    'status' => 'success'
                ]));

        } catch (\Exception $e) {

            return redirect()
                ->route('intranet.indications.status.index')
                ->with('alert', collect([
                    'message' => 'O status não pode ser removido. Tente novamente!' . $e->getMessage(),
                    'status' => 'danger'
                ]));


        }

    }

}
