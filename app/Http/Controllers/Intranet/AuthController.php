<?php

namespace App\Http\Controllers\Intranet;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(){

        return view('intranet.auth.login');

    }

    public function logout(){

        Auth::logout();

        return redirect()->route('intranet.auth.login');

    }

    public function authenticate(Request $request){

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if($request->has('email') && $request->has('password')){

            $email = $request->input('email');
            $password = $request->input('password');

            if (Auth::attempt(['email' => $email, 'password' => $password, 'status' => 1])) {
                return redirect()->intended('/intranet');
            }

        }

    }
}
