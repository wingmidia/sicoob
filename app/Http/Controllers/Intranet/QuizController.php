<?php
/**
 * Created by PhpStorm.
 * User: Wing Mídia
 * Date: 06/04/2018
 * Time: 16:31
 */

namespace App\Http\Controllers\Intranet;


use App\DataTables\Html\Builder;
use App\Http\Controllers\Controller;
use App\Http\Requests\QuizCreateRequest;
use App\Http\Requests\QuizResultRequest;
use App\Models\Quiz;
use App\Models\QuizAnswer;
use App\Models\QuizAsk;
use App\Models\QuizAskAlternative;
use App\Models\QuizResult;
use App\Repositories\QuizRepository;
use App\Repositories\QuizResultRepository;
use App\Repositories\UserRepository;
use App\User;
use App\Validators\QuizValidator;
use Barryvdh\Debugbar\Middleware\Debugbar;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Prettus\Validator\Contracts\ValidatorInterface;


class QuizController extends Controller {


    private $repository;
    private $validator;

    /**
     * QuizController constructor.
     * @param $repository
     * @param $validator
     */
    public function __construct(QuizRepository $repository, QuizValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->controllers = arrayToObject([
            'title' => 'Quizzes',
            'icon' => 'fa fa-question-circle',
            'route' => 'intranet.quiz.index',
            'actions'=> [
                'manager' => [
                    'route' => 'intranet.quiz.manager',
                    'permission' => 'quiz-manager'
                ],
                'delete' => [
                    'route' => 'intranet.quiz.delete',
                    'permission' => 'quiz-manager'
                ],
                'result' => [
                    'route' => 'intranet.quiz.result',
                    'permission' => 'quiz-manager'
                ],
                'add' => [
                    'route' => 'intranet.quiz.create',
                    'permission' => 'quiz-manager'
                ],
                'store'=> [
                    'route' => 'intranet.quiz.store',
                    'permission' => 'quiz-manager'
                ],
                'edit' => [
                    'route' => 'intranet.quiz.edit',
                    'permission' => 'quiz-manager',
                ],
                'answer' => [
                    'route' => 'intranet.quiz.answer',
                    'see'=> 'intranet.quiz.result.see',
                    'self'=> 'intranet.quiz.result.self',
                    'permission'=> 'quiz'
                ],
                'do_awnser' => [
                    'route' => 'intranet.quiz.answer.do',
                    'permission'=> 'quiz-answer'
                ],
            ]
        ]);
        View::share('controllers', $this->controllers);
    }

    public function index(Builder $builder, QuizResultRepository $result){

        $collaborratorId = Auth::user()->collaborator->id;

        if(request()->ajax()){
            return $this->repository->getDataTables($collaborratorId)->make(true);
        }

        $datatable = $builder
            //->addColumn(['data' => 'created_at', 'name' => 'quizzes.created_at', 'title' => 'Criado em', 'width' => '10%'])
            ->addTitle(['data' => 'name', 'name' => 'quizzes.name', 'title' => 'Nome do quiz'])
            //->addTitle(['data' => 'description', 'name' => 'quizzes.description', 'title' => 'Descrição'])
            ->addColumn(['data' => 'start_date', 'name' => 'quizzes.start_date', 'title' => 'Data de início', 'width' => '10%'])
            ->addColumn(['data' => 'end_date', 'name' => 'quizzes.end_date', 'title' => 'Data final', 'width' => '10%'])
            ->addColumn(['data' => 'active', 'name' => 'quizzes.active', 'title' => 'Status', 'width' => '16%', 'render' => 'datatablesFormatQuizStatus(data)'])
            ->addAction()
            ->addButtonColVis();

        if(Auth::user()->can('quizzes-export'))
            $datatable = $datatable
                ->addButtonExcel();

        return view('intranet.quiz.index', compact('datatable'))->with('collaboratorId', $collaborratorId);
    }

    public function manager(Builder $builder){
        \Debugbar::info(Carbon::now()->endOfDay());
        $collaborratorId = Auth::user()->collaborator->id;

        if(request()->ajax()){
            return $this->repository->getDataTables($collaborratorId)->make(true);
        }

        $datatable = $builder
            //->addColumn(['data' => 'created_at', 'name' => 'quizzes.created_at', 'title' => 'Criado em', 'width' => '10%'])
            ->addTitle(['data' => 'name', 'name' => 'quizzes.name', 'title' => 'Nome do quiz'])
            //->addTitle(['data' => 'description', 'name' => 'quizzes.description', 'title' => 'Descrição'])
            ->addColumn(['data' => 'start_date', 'name' => 'quizzes.start_date', 'title' => 'Data de início', 'width' => '10%'])
            ->addColumn(['data' => 'end_date', 'name' => 'quizzes.end_date', 'title' => 'Data final', 'width' => '10%'])
            ->addColumn(['data' => 'active', 'name' => 'quizzes.active', 'title' => 'Status', 'width' => '10%', 'render' => 'datatablesFormatQuizStatus(data)'])
            ->addColumn(['data' => 'action', 'name' => 'action', 'title' => 'Ações', 'width' => '15%', 'render' => 'datatablesFormatActions(data, type, full)'])
            //->addAction()
            ->addButtonColVis();

        if(Auth::user()->can('quizes-export'))
            $datatable = $datatable
                ->addButtonExcel();

        return view('intranet.quiz.manager.index', compact('datatable'));
    }

    public function create()
    {
        \Debugbar::info(Carbon::now());
        return view('intranet.quiz.manager');
    }

    public function store(
        QuizCreateRequest $request,
        QuizRepository $quizRepository,
        QuizValidator $quizValidator
    ) {

        $request->only('name',
                'start_date',
                'end_date',
                'time',
                'active',
                'asks'
            );

        $input_quiz = [];

        $input_quiz['id'] = $request->input('id');
        $input_quiz['name'] = $request->input('name');
        $input_quiz['description'] = $request->input('description');
        $input_quiz['start_date'] = Carbon::createFromFormat('d/m/Y',$request->input('start_date'))
                                            ->format('Y-m-d H:i:s');
        $input_quiz['end_date'] = Carbon::createFromFormat('d/m/Y',$request->input('end_date'))
                                            ->endOfDay()->format('Y-m-d H:i:s');
        $input_quiz['time'] = $request->input('time');
        $input_quiz['active'] = $request->input('active');
        $input_quiz['active'] = empty($input_quiz['active'])?false:$input_quiz['active'];

        \Debugbar::info('qz', $input_quiz);

        $input_quiz_asks = $request->input('asks');


        \Debugbar::info($input_quiz_asks);


        $quizValidator->with($input_quiz)->passesOrFail(ValidatorInterface::RULE_CREATE);

        $quizId = empty($input_quiz['id'])?null:$input_quiz['id'];

        $createdQuiz = $quizRepository->updateOrCreate(
            ['id'=> $quizId],
            $input_quiz
        );

        $quizIds = [];
        foreach ($input_quiz_asks as $quizAsk) {

            $askAlternatives = [];

            if(!empty($quizAsk['alternatives'])) {
                $askAlternatives = $quizAsk['alternatives'];
                unset($quizAsk['alternatives']);
            }

            $quizAsk['active'] = empty($quizAsk['active'])?false:$quizAsk['active'];

            $askId = empty($quizAsk['id'])?null:$quizAsk['id'];

            $createdAsk = $createdQuiz->asks()->updateOrCreate(
                ['id'=>$askId],
                $quizAsk
            );

            $quizIds[] = $createdAsk['id'];

            \Debugbar::info($askAlternatives);
            $alternativesIds = [];
            foreach ($askAlternatives as $askAlternative) {
                $alternativeId = empty($askAlternative['id'])?null:$askAlternative['id'];
                $askAlternative['active'] = empty($askAlternative['active'])?false:$askAlternative['active'];
                $askAlternative['correct'] = empty($askAlternative['correct'])?false:$askAlternative['correct'];
                $addAlternative = $createdAsk->alternatives()->updateOrCreate(
                    ['id'=>$alternativeId],
                    $askAlternative
                );
                $alternativesIds[] = $addAlternative['id'];
            }
            $createdAsk->alternatives()->whereNotIn('id', $alternativesIds)->delete();
        }

        $createdQuiz->asks()->whereNotIn('id', $quizIds)->delete();

        return redirect()
            ->route('intranet.quiz.index')
            ->with('alert', collect([
                'message' => 'Registro feito com sucesso!',
                'status' => 'success'
            ]));

    }

    public function edit($id){

        $finded = $this->repository->find($id);

        if(empty($finded)) return;

        \Debugbar::info($finded);


        foreach ($finded->asks as $ask){
            $ask->alternatives;
        };

        return view('intranet.quiz.manager', compact('finded'));
    }

    public function answer(QuizResultRepository $resultRepository, UserRepository $userRepository, $id){


        $quiz = $this->repository->find($id);

        foreach ($quiz->asks as $ask){
            $ask->alternatives;
        };

        $now = Carbon::now();

        $visitor = false;
        if(!$quiz->active)
            return redirect()->route('intranet.quiz.index')->with('alert', collect([
                'message' => 'Este Quizz está desativado! ',
                'status' => 'warning'
            ]));

        //if($now->lt($quiz->start_date) || $now->gt($quiz->end_date))
        if($now->startOfDay()->lte($quiz->start_date) && $now->endOfDay()->gte($quiz->end_date))
            return redirect()->route('intranet.quiz.index')->with('alert', collect([
                'message' => 'Infelizmente não estamos no período para receber respostas para este Quizz! ',
                'status' => 'warning'
            ]));

        $collaborratorId = Auth::user()->collaborator->id;

        $resultFind = $resultRepository->updateOrCreate([
            'quiz_id'=>$id,
            "collaborator_id" => $collaborratorId,
            'terminated' => 0,
            'started_at' => Carbon::now()->toDateTimeString()
        ]);

        \Debugbar::info('colb', $collaborratorId);

        $resultFind = $resultRepository->findWhere([
            'quiz_id'=>$id,
            "collaborator_id" => $collaborratorId
        ])->first();

        if($quiz->time) {
            $timeEnd = $resultFind->started_at->copy()->addMinutes($quiz->time);

            if ($now->gt($timeEnd))
                return redirect()->route('intranet.quiz.index')->with('alert', collect([
                    'message' => 'Tempo de resposta do quiz expirou! ',
                    'status' => 'warning'
                ]));
        }

        $resultFind->answers;

        return view('intranet.quiz.answer.index', compact('quiz'), compact('resultFind'))->with('visitor', $visitor);
    }

    public function seeResult(QuizResultRepository $resultRepository, UserRepository $userRepository, $id, $coll_id){
        $collaborratorId = $coll_id;

        $quiz = $this->repository->find($id);

        foreach ($quiz->asks as $ask){
            $ask->alternatives;
        };

        $visitor = true;

        $resultFind = $resultRepository->findWhere([
            'quiz_id'=>$id,
            "collaborator_id" => $collaborratorId
        ])->first();

        if(!$resultFind->terminated) {
            return redirect()->route('intranet.quiz.result', $quiz->id)
                ->with('alert', collect([
                    'status' => 'warning',
                    'message' => 'Não é possível ver resultado do quiz pois o ainda não foi finalizado! '
                ]));
        }

        $pontuation = $resultFind->pontuation();

        $resultFind->answers;

        return view('intranet.quiz.manager.result', compact('quiz'), compact('resultFind'))
            ->with([
                'visitor'=> $visitor,
                'total'=> $pontuation['result']
            ]);
    }

    public function selfResult(QuizResultRepository $resultRepository, UserRepository $userRepository, $coll_id, $id){
        $collaborratorId = $coll_id;

        if(!empty($coll_id)){
            $collaborratorId = Auth::user()->collaborator->id;

            if($coll_id!=$collaborratorId){
                return redirect()->route('intranet.quiz.index')->with('alert', collect([
                    'message' => 'Você não tem permissão para ver o resultado deste questionário ',
                    'status' => 'success'
                ]));
            }
        }else{
            return redirect()->route('intranet.quiz.index')->with('alert', collect([
                'message' => 'Resultado não encontrado ',
                'status' => 'success'
            ]));
        }

        $quiz = $this->repository->find($id);

        foreach ($quiz->asks as $ask){
            $ask->alternatives;
        };

        $visitor = false;

        $resultFind = $resultRepository->findWhere([
            'quiz_id'=>$id,
            "collaborator_id" => $collaborratorId
        ])->first();


        $pontuation = $resultFind->pontuation();

        $resultFind->answers;
        return view('intranet.quiz.manager.result', compact('quiz'), compact('resultFind'))
            ->with([
                'visitor'=> $visitor,
                'total'=> $pontuation['result']
            ]);
    }

    public function doAnswer(QuizResultRepository $resultRepository, UserRepository $userRepository, QuizResultRequest $request){
        $asks = $request->input('asks');

        $quiz_id = $request->input('quiz_id');

        $collaborratorId = Auth::user()->collaborator->id;

        $userResult = $resultRepository->findWhere([
            'quiz_id' => $quiz_id,
            'collaborator_id' => $collaborratorId
        ])->first();

        $userResult->update(['terminated'=>true]);

        $quiz = $userResult->quiz()->first();

        $now = Carbon::now();

        //if($now->lt($quiz->start_date) || $now->gt($quiz->end_date))
        if($now->startOfDay()->lte($quiz->start_date) && $now->endOfDay()->gte($quiz->end_date))
            return redirect()->route('intranet.quiz.index')->with('alert',collect([
                'message' => 'Infelizmente não estamos no período para receber respostas para este Quizz! ',
                'status' => 'success'
            ]));


        $alternativeIds = [];

        if(isset($asks)) {
            foreach ($asks as $askId => $ask) {

                foreach ($ask['alternatives'] as $altId => $alternative) {

                    $answerId = empty($alternative['answer_id']) ? null : $alternative['answer_id'];

                    $newAnswer = $userResult->answers()->updateOrCreate(
                        ['id' => $answerId],
                        [
                            'quiz_ask_id' => $askId,
                            'quiz_ask_alternative_id' => $altId
                        ]
                    );

                    $alternativeIds[] = $newAnswer['id'];
                }
            }
        }
        $pontuation = $userResult->pontuation();


        if(sizeof($alternativeIds)>0)
            $userResult->answers()->whereNotIn('id', $alternativeIds)->delete();



        $userResult->update([
            'terminated' => true,
            'right_answers' => $pontuation['rightAlternatives'],
            'wrong_answers' => $pontuation['wrongAlternatives'],
            'ended_at'=> Carbon::now()
        ]);

        return redirect()->route('intranet.quiz.index')->with('alert', collect([
            'message' => 'Questionário respondido com sucesso!',
            'status' => 'success'
        ]));
    }

    function result(Builder $builder, QuizResultRepository $repository, $quizId){

        if($quizId=='undefined'||empty($quizId)){
            return redirect()->route('intranet.quiz.manager')->with('alert', collect([
                'message' => 'Quiz não encontrado!',
                'status' => 'success'
            ]));
        }

        $quiz = $this->repository->find($quizId);

        if(request()->ajax()){
            return $repository->getDataTables($quizId)->make(true);
        }

        $datatable = $builder
            //->addColumn(['data' => 'created_at', 'name' => 'quiz_results.created_at', 'title' => 'Criado em', 'width' => '10%'])
            ->addTitle(['data' => 'name', 'name' => 'collaborators.name', 'title' => 'Colaborador'])
            ->addTitle(['data' => 'right_answers', 'name' => 'quiz_results.right_answers', 'title' => 'Acertos'])
            ->addTitle(['data' => 'wrong_answers', 'name' => 'quiz_results.wrong_answers', 'title' => 'Erros'])
            ->addColumn(['data' => 'pontuacao', 'name' => 'pontuacao', 'title' => 'Pontuação'])
            ->addColumn(['data' => 'started_at', 'name' => 'quiz_results.started_at', 'title' => 'Início em', 'width' => '10%'])
            ->addColumn(['data' => 'ended_at', 'name' => 'quiz_results.ended_at', 'title' => 'Finalizado em', 'width' => '10%'])
            ->addColumn(['data' => 'end_at', 'name' => 'quiz_results.end_at', 'title' => 'Duração', 'width' => '5%'])
            ->addColumn(['data' => 'terminated', 'name' => 'quiz_results.terminated', 'title' => 'Status', 'width' => '10%', 'render' => 'datatablesFormatResultStatus(data)'])
            ->addColumn(['data' => 'action', 'name' => 'action', 'title' => 'Ações', 'width' => '10%', 'render' => 'datatablesFormatActions(data, type, full)'])
            //->addAction()
            ->addButtonColVis();

        if(Auth::user()->can('quizes-export'))
            $datatable = $datatable
                ->addButtonExcel();

        return view('intranet.quiz.manager.results', compact('datatable'), compact('quiz'));

    }

    public function delete($id){
        if(!empty($id)){
            $this->repository->delete($id);
            return redirect()->route('intranet.quiz.manager')->with('alert', collect([
                'message' => 'Quiz excluido com sucesso!',
                'status' => 'success'
            ]));
        }else {
            return redirect()->route('intranet.quiz.manager')->with('alert', collect([
                'message' => 'Quiz não encontrado',
                'status' => 'success'
            ]));
        }

    }


}