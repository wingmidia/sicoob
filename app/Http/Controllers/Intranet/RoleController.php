<?php

namespace App\Http\Controllers\Intranet;

use App\DataTables\Html\Builder;
use App\Http\Requests\RoleRequest;
use App\Models\Role;
use App\Repositories\PermissionRepository;
use App\Repositories\RoleRepository;
use App\Http\Controllers\Controller;
use App\Validators\RoleValidator;
use Illuminate\Support\Facades\View;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Zizaco\Entrust\EntrustFacade;

/**
 * Class RoleController
 * @package App\Http\Controllers\Intranet
 */
class RoleController extends Controller
{

    /**
     * @var RoleRepository
     */
    protected $repository;

    /**
     * @var RoleValidator
     */
    protected $validator;

    /**
     * @var PermissionRepository
     */
    protected $permissions_repository;

    /**
     * RoleController constructor.
     * @param RoleRepository $repository
     * @param RoleValidator $validator
     * @param PermissionRepository $permissions_repository
     */
    public function __construct(
        RoleRepository $repository,
        RoleValidator $validator,
        PermissionRepository $permissions_repository
    )
    {
        $this->repository = $repository;
        $this->validator = $validator;

        $this->permissions_repository = $permissions_repository;

        $this->controllers = arrayToObject([
            'title' => 'Perfil de usuário',
            'icon' => 'fa fa-cog',
            'route' => 'intranet.roles.index',
            'actions' => [
                'add' => [
                    'route' => 'intranet.roles.show',
                    'permission' => 'roles'
                ],
                'edit' => [
                    'route' => 'intranet.roles.show',
                    'permission' => 'roles'
                ],
                'store' => [
                    'route' => 'intranet.roles.store'
                ],
                'delete' => [
                    'route' => 'intranet.roles.destroy',
                    'permission' => 'roles'
                ]
            ]
        ]);

        View::share('controllers', $this->controllers);
    }

    /**
     * @param Builder $builder
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Builder $builder){

        if (request()->ajax()) {
            return $this->repository->getDatatables()->make(true);
        }

        $datatable = $builder
            ->addIndex()
            ->addTitle(['data' => 'display_name', 'name' => 'display_name', 'title' => 'Nome'])
            ->addColumn(['data' => 'name', 'name' => 'name', 'title' => 'Apelido'])
            ->addAction()
            ->addButtonColVis();

        return view('intranet.layouts.datatables.index', compact( 'datatable'));

    }

    /**
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id = null)
    {

        if($id){

            $item = $this->repository
                ->find($id);

        } else {

            $item = new Role();

        }

        $permissions = $this->permissions_repository->orderBy('display_name')->all();

        return view('intranet.roles.show', compact('item', 'permissions'));

    }

    /**
     * @param RoleRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(RoleRequest $request){

        try {

            if(!empty($request->id)){

                $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

                $item = $this->repository->update($request->all(), $request->id);

            } else {

                $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

                $item = $this->repository->create($request->all());

            }

            $item->perms()->sync($request->input('permissions'));

            return redirect()
                ->route('intranet.roles.show', $item->id)
                ->with('alert', collect([
                    'message' => 'Perfil salvo com sucesso!',
                    'status' => 'success'
                ]));

        } catch (ValidatorException $e) {

            return redirect()->route('intranet.roles.show')->with('status', 'Erro ao salvar! ' . $e);

        }


    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id){

        try {

            $this->repository->delete($id);

            return redirect()
                ->route('intranet.roles.index')
                ->with('alert', collect([
                    'message' => 'Perfil removido com sucesso!',
                    'status' => 'success'
                ]));

        } catch (\Exception $e) {

            return redirect()
                ->route('intranet.roles.index')
                ->with('alert', collect([
                    'message' => 'O perfil não pode ser removido. Tente novamente!' . $e->getMessage(),
                    'status' => 'danger'
                ]));


        }

    }

}
