<?php

namespace App\Http\Controllers\Intranet;

use App\Repositories\ClientRepository;
use App\Repositories\ProductRepository;
use App\Validators\ClientValidator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class ClientController
 * @package App\Http\Controllers\Intranet
 */
class ClientController extends Controller
{

    /**
     * @var ClientRepository
     */
    private $repository;

    /**
     * @var ClientValidator
     */
    private $validator;

    /**
     * ClientController constructor.
     * @param ClientRepository $repository
     * @param ClientValidator $validator
     */
    public function __construct(ClientRepository $repository, ClientValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getClient(Request $request){

        $client = $this->repository;

        $where = [];

        if(!empty($request->input('type_profile')))
            $where['type_profile'] = $request->input('type_profile');

        if(!empty($request->input('cpf')))
            $where['cpf'] = str_replace(['.', '-'], '', $request->input('cpf'));

        if(!empty($request->input('cnpj')))
            $where['cnpj'] = str_replace(['.', '/', '-'], '', $request->input('cnpj'));

        if(!empty($where))
            $client = $client->findWhere($where);

        return response()->json($client->first());

    }

    public function getProductsIndicated(Request $request){

        return response()->json(
            $this->repository
                ->getProductsIndicated($request->input('client_id'))
        );

    }

}
