<?php
/**
 * Created by PhpStorm.
 * User: Wing Mídia
 * Date: 06/04/2018
 * Time: 16:31
 */

namespace App\Http\Controllers\Intranet;


use App\DataTables\Html\Builder;
use App\Http\Controllers\Controller;
use App\Http\Requests\QuizCreateRequest;
use App\Http\Requests\QuizResultRequest;
use App\Models\Quiz;
use App\Models\QuizAnswer;
use App\Models\QuizAsk;
use App\Models\QuizAskAlternative;
use App\Models\QuizResult;
use App\Repositories\QuizRepository;
use App\Repositories\QuizResultRepository;
use App\Repositories\UserRepository;
use App\User;
use App\Validators\QuizValidator;
use Barryvdh\Debugbar\Middleware\Debugbar;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Prettus\Validator\Contracts\ValidatorInterface;
use App\Models\Config;
use Illuminate\Support\Facades\DB;

class ConfigController extends Controller {

    /**
     * DepartamentController constructor.
     * @param DepartamentRepository $repository
     * @param DepartamentValidator $validator
     */
    public function __construct()
    {

        $this->controllers = arrayToObject([
            'title' => 'Configurações',
            'icon' => 'fa fa-cog',
            'route' => 'intranet.config.index',
            'actions' => [
                'update' => [
                    'route' => 'intranet.config.update',
                    'permission' => 'custom-config'
                ]
            ],
        ]);

        View::share('controllers', $this->controllers);
    }

    public function index(){
        $config = Config::all();
        return view('intranet.config.show', compact('config'));
    }

    public function update(Request $request){

        $config = Config::all();
        $post = $request->all();
        
        if(!isset($post['config']))
            $post['config'] = [];

        //dd($post);

        foreach($config as $c) {
            if($c->type == 'checkbox') {
                if(isset($post['config'][$c->id]) && $c->value == 'off') {
                    $c->value = 'on';
                    $c->save();
                } else if(!isset($post['config'][$c->id]) && $c->value == 'on') {
                    $c->value = 'off';
                    $c->save();
                }
            } else if($c->type == 'text') {
                if(isset($post['config'][$c->id]) && !empty($post['config'][$c->id])) {
                    $c->value = $post['config'][$c->id];
                    $c->save();
                }
            }
        }

        return redirect()->route('intranet.config.index', ['config'=>$config]);
    }


}