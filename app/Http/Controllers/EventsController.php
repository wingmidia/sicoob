<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\EventRepository;
use App\Criteria\EventsActiveCriteria;
use App\Criteria\EventsSearchCriteria;


class EventsController extends Controller
{
  private $events;

  public function __construct(EventRepository $events)
  { 
    $this->events = $events;
    $this->events->pushCriteria(app(EventsActiveCriteria::class));
  }

  public function index()
  {
    $this->events->pushCriteria(app(EventsSearchCriteria::class));
    $events = $this->events->scopeQuery(function($query){
                            return $query->orderBy('event_date','asc');
                        })->paginate(4);

    return view('events', compact('events'));
  }

  public function details( $slug )
  {
    $event = $this->events->findWhere([['slug', '=',$slug]])->first();
      
    if(!$event)
    {
      abort(404);
    }

    return view('event', compact('event'));
  }
}
