<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PaycheckRepository;
use App\Criteria\PaychecksCollaboratorCriteria;
use Storage;
use Carbon\Carbon;

class PaychecksController extends Controller
{
  private $paychecks;

  public function __construct(PaycheckRepository $paychecks)
  { 
    $this->middleware('auth');
    $this->paychecks = $paychecks;
    $this->paychecks->pushCriteria(PaychecksCollaboratorCriteria::class);
  }

  public function index()
  {
    $paychecks = $this->paychecks->with(['collaborator'])->paginate();

    $year_init = Carbon::now()->subYears(30)->year;
    $year_now = Carbon::now()->year;
      
    return view('paychecks', compact('paychecks', 'year_init', 'year_now'));
  }

  public function download($filename)
  {
      if (!$filename) {
        return abort(404);
      }

      $exists = Storage::exists('paychecks/'.$filename);
      if(!$exists){
        return abort(404); 
      }

      $paycheck = $this->paychecks->findWhere([
          ['filename', '=', $filename]
        ])->first();
      
      $this->paychecks->update([ 'status'=> 'OPEN' ], $paycheck->id);

      $pathToFile = storage_path('app/paychecks/'.$filename);
      
      return $exists ? response()->download($pathToFile) : 'arquivo não existe';
  }


  public function view($filename)
  {
      if (!$filename) {
        return abort(404);
      }

      $exists = Storage::exists('paychecks/'.$filename);
      if(!$exists){
        return abort(404); 
      }

      $paycheck = $this->paychecks->findWhere([
          ['filename', '=', $filename]
        ])->first();
      
      $this->paychecks->update([ 'status'=> 'OPEN' ], $paycheck->id);

      $pathToFile = storage_path('app/paychecks/'.$filename);
      
      return $exists ? response()->file($pathToFile) : 'arquivo não existe';
  }
}