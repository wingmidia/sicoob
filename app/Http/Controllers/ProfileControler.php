<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\User;
use Auth;

class ProfileControler extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
    	return view('profile');
    }

    public function changePassword(Request $request)
    {
    	$validator = Validator::make($request->all(), [
			    'password' => 'required|min:6',
	        'password_confirmation' => 'required|same:password',
			])->validate();
	    
	    if ($validator) {
	    	return redirect('meu-perfil')
	            ->withErrors($validator);
      }

      $user = User::find(Auth::user()->id);
      $user->password = bcrypt($request->get('password'));
      $user->save();

      return redirect('meu-perfil')
      					->with('success', 'Senha atualizado com sucesso!');
    }
}
