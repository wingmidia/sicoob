<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Auth;
/**
 * Class ClientsCriteria
 * @package namespace App\Criteria;
 */
class ClientsCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $request = request();
        
        /*if(Auth::user()->hasRole(['diretoria', 'ti'])){
            if($request->has('collaborator_id')){
                $model = $model->where('collaborator_id', $request->get('collaborator_id'));
            }
        } else{
            $model = $model->where('collaborator_id', Auth::user()->collaborator->id);    
        }*/

        if(request()->has('type_profile')){
            $type_profile = request()->get('type_profile');
            $model = $model->where('type_profile', $type_profile);
        }

        return $model;
    }
}
