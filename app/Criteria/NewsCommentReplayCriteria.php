<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class NewsCommentReplayCriteria
 * @package namespace App\Criteria;
 */
class NewsCommentReplayCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if(request()->has('replay_id')){
            $model = $model->where('replay_id', request()->get('replay_id'));    
        }

        return $model;
    }
}
