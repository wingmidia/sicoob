<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

use Auth;
use App\Models\News;

/**
 * Class NewsActiveCriteriaCriteria
 * @package namespace App\Criteria;
 */
class NewsActiveCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model->where('status', 1)
              ->where('permission', 'public')
              ->where('start_date', '<=', date('Y-m-d H:i:s'))
              ->where('end_date', '>=', date('Y-m-d H:i:s'));

        return $model;
    }
}
