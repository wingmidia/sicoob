<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Carbon\Carbon;

/**
 * Class EventsSearchCriteria
 * @package namespace App\Criteria;
 */
class EventsSearchCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $request = request();

        $model = $model->where(function($q) use ($request){
                            
                            if($request->has('search')){
                                $q->where(function($q1) use ($request){
                                    $q1->orWhere('title', 'LIKE', '%'.$request->get('search').'%');
                                    $q1->orWhere('local', 'LIKE', '%'.$request->get('search').'%');
                                    $q1->orWhere('content', 'LIKE', '%'.$request->get('search').'%');
                                    
                                });
                            }
                            if($request->has('start_date')){
                                $start_date = Carbon::createFromFormat('d/m/Y', $request->get('start_date'));
                                $q->where('start_date', '>=', $start_date);
                            }   
                            if($request->has('end_date')){
                                $end_date = Carbon::createFromFormat('d/m/Y', $request->get('end_date'));
                                $q->where('start_date', '<=', $end_date);    
                            }
                        
                        });
        return $model;
    }
}
