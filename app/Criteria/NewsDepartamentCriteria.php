<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

use Auth;
/**
 * Class NewsDepartamentCriteria
 * @package namespace App\Criteria;
 */
class NewsDepartamentCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if(Auth::check()){
           // $departaments = collect(Auth::user()->collaborator->departaments)->pluck('id');

            //$model->join('news_departaments', 'news.id', '=', 'news_departaments.new_id');
                        //->whereIn('news_departaments.departament_id', $departaments);

            
        }
        $model->where('permission', 'private');
        //$model->where('permission', 'public');

        return $model;
    }
}
