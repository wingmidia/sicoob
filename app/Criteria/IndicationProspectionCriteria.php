<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Auth;
/**
 * Class IndicationCollaboratorCriteria
 * @package namespace App\Criteria;
 */
class IndicationProspectionCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {

        $model
            ->join('collaborator_indication', 'indications.id', '=', 'collaborator_indication.indication_id')
            ->where('collaborator_indication.collaborator_id', Auth::user()->collaborator->id);

        return $model;
    }
}
