<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Carbon\Carbon;
use Auth;

/**
 * Class PaychecksCollaboratorCriteria
 * @package namespace App\Criteria;
 */
class PaychecksCollaboratorCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where(function($q){
            $q->where('collaborator_id', Auth::user()->collaborator->id);
            
            //$q->where('month', request()->get('month', Carbon::now()->month));

            //$q->where('year', request()->get('year', Carbon::now()->year));
            

        });

        return $model;
    }
}
