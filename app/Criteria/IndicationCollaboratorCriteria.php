<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Auth;
/**
 * Class IndicationCollaboratorCriteria
 * @package namespace App\Criteria;
 */
class IndicationCollaboratorCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {

        $model->where('indications.collaborator_id', Auth::user()->collaborator->id);

        return $model;
    }
}
