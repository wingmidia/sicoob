<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Carbon\Carbon;

/**
 * Class EventsActiveCriteria
 * @package namespace App\Criteria;
 */
class EventsActiveCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('status', 1)
                       ->where(function($q){
                            $q->where('start_date', '<=', Carbon::now());
                            $q->where(function($q1){
                                $q1->where('end_date', '>=', Carbon::now());
                                $q1->orWhereNull('end_date');
                            });
                       });

        return $model;
    }
}
