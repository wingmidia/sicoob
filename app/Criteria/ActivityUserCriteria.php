<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Auth;
use Carbon\Carbon;
/**
 * Class ActivityUserCriteria
 * @package namespace App\Criteria;
 */
class ActivityUserCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $request = request();

        $model->where('collaborator_id', Auth::user()->collaborator->id);
        
        $model->where(function($q) use ($request){

            if($request->has('start_date')){
                $start_date = Carbon::createFromFormat('d/m/Y', $request->get('start_date'));
                $q->where('end_date', '>=', $start_date);
            }

            if($request->has('end_date')){
                $end_date = Carbon::createFromFormat('d/m/Y', $request->get('end_date'));
                $q->where('end_date', '<=', $end_date);
            }
        });

        if($request->has('status')){
            $model->where('status', $request->get('status'));
        }

        return $model;
    }
}
