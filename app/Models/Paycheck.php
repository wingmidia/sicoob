<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Paycheck extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['collaborator_id', 'month', 'year', 'filename', 'status'];

    public function collaborator()
    { 
      return $this->belongsTo('App\Models\Collaborator');
    }
}
