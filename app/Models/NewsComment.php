<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class NewsComment extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['replay_id', 'new_id', 'collaborator_id', 'comments', 'status'];

     /**
      * @return array
      */
     public function transform()
     {
        $repository = app('App\Repositories\NewsCommentRepository');

        return [
            'id'         => (int) $this->id,
            'replay_id'  => (int) $this->replay_id,
            'new_id'     => (int) $this->new_id,
            'slug'       => $this->new->slug,
            'autor'      => $this->collaborator->name,
            'avatar'     => $this->collaborator->photo,
            'comments'   => $this->comments,
            'replays'    => $repository->findWhere(['replay_id' => $this->id])['data'],
            'status'     => $this->status,

            /* place your other model properties here */

            'created_at' => $this->created_at->format('d/m/Y H:i:s'),
            'updated_at' => $this->updated_at->format('d/m/Y H:i:s')
        ];
     }

    public function replay()
    {
        return $this->hasMany(NewsComment::class, 'replay_id');
    }

    public function new()
    {
    	return $this->belongsTo(News::class, 'new_id');
    }

    public function collaborator()
    {
    	return $this->belongsTo(Collaborator::class, 'collaborator_id');
    }
}
