<?php
/**
 * Created by PhpStorm.
 * User: Wing Mídia
 * Date: 05/04/2018
 * Time: 16:55
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Quiz extends Model implements Transformable {

    use TransformableTrait;

    protected $fillable = ['name', 'description', 'start_date', 'end_date', 'time', 'active'];

    protected  $dates = ['start_date', 'end_date'];

    public function asks(){
        return $this->hasMany('App\Models\QuizAsk');
    }

}