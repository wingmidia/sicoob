<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Product extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['title', 'content', 'image_feacture', 'indication', 'status', 'category_id'];

    public function activities()
    { 
      return $this->hasMany('App\Models\Activity');
    }

    public function indications()
    {
        return $this->belongsToMany('App\Models\Indication');
    }

    public function fields()
    {
        return $this->hasMany('App\Models\ProductFields');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }
}
