<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Client extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['collaborator_id', 'type_profile', 'name', 'company_name', 'cpf', 'cnpj', 'phone1', 'phone2', 'cellphone',
    											 'email', 'photo', 'address', 'district', 'number', 'complement', 'city', 'state', 'postcode',
    											 'date_birth', 'activity', 'status'];

    public function getNameAttribute($value){
        return ucfirst($value);
    }

    public function getCompanyNameAttribute($value){
        return ucfirst($value);
    }

    public function setCpfAttribute($value)
    {
        $this->attributes['cpf'] = str_replace(['.', '-'], '', $value);
    }

    public function getCpfAttribute($value)
    {
        return ($value) ? mask(str_replace(['.', '-'], '', $value), '###.###.###-##') : '';
    }

    public function setCnpjAttribute($value)
    {
        $this->attributes['cnpj'] = str_replace(['.', '-', '/'], '', $value);
    }

    public function getCnpjAttribute($value)
    {
        return ($value) ? mask(str_replace(['.', '-'], '', $value), '##.###.###/####-##') : '';
    }

    public function getEmailAttribute($value){
        return strtolower($value);
    }

    public function getTypeProfileName(){
        return ($this->attributes['type_profile'] == 'F') ? 'Pessoa Física' : 'Pessoa Jurídica';
    }

    public function indications()
    {
        return $this->hasMany('App\Models\Indication');
    }

}
