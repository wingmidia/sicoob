<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Documents extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['filename', 'original_name', 'extension', 'mimetype', 'type', 'permission'];

    public function departaments()
    {
        return $this->belongsToMany(Departament::class, 'documents_departaments', 'document_id', 'departament_id');
    }
}
