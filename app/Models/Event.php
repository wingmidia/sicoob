<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Event extends Model implements Transformable
{
	
	use Sluggable, TransformableTrait;

    protected $fillable = ['title', 'local', 'content', 'start_date', 'end_date', 'event_date', 'image_feacture', 'status'];

    protected $dates = ['start_date', 'end_date', 'event_date'];

	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function photos()
    {
        return $this->hasMany(EventsPhoto::class, 'event_id');
    }
}
