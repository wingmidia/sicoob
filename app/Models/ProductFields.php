<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ProductFields extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['product_id', 'type', 'name', 'description', 'options', 'required'];

    public function product()
    { 
      return $this->belongsTo('App\Models\Product');
    }

}
