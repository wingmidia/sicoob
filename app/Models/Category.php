<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $fillable = ['name'];

    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function campaigns(){
        return $this->belongsToMany(Campaign::class, 'campaign_categories', 'category_id','campaign_id');
    }
}
