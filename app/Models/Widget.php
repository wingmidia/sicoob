<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Widget extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['name', 'widget', 'status'];

}
