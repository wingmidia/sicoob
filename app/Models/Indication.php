<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Indication extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['client_id', 'collaborator_id', 'indication_status_id', 'display_at'];

    protected $dates = ['display_at'];

    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }

    public function collaborator()
    {
        return $this->belongsTo('App\Models\Collaborator');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product')->withPivot('concluded');
    }

    public function status()
    {
        return $this->belongsTo('App\Models\IndicationStatus', 'indication_status_id');
    }

    public function prospections()
    {
        return $this->belongsToMany('App\Models\Collaborator');
    }

}
