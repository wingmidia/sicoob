<?php
/**
 * Created by PhpStorm.
 * User: Wing Mídia
 * Date: 10/04/2018
 * Time: 16:28
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class QuizAskAlternative extends Model {

    protected $fillable = ['title', 'correct', 'active'];

    public function ask(){
        return $this->belongsTo(QuizAsk::class, 'quiz_ask_id', 'id');
    }

}