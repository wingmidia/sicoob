<?php
/**
 * Created by PhpStorm.
 * User: Wing Mídia
 * Date: 05/04/2018
 * Time: 16:57
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class QuizAsk extends Model implements Transformable {

    use TransformableTrait;

    protected $fillable = [/*'id',*/ 'title', 'weight', 'active'];


    public function quiz(){
        return $this->belongsTo('App\Models\Quiz');
    }

    public function alternatives(){
        return $this->hasMany('App\Models\QuizAskAlternative');
    }


}