<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class EventsPhoto extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['event_id','image'];

    public $timestamps = false;

    public function event()
    {
    	return $this->belongsTo(Event::class);
    }

}
