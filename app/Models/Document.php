<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

use Auth;

class Document extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['filename', 'original_name', 'extension', 'mimetype', 'type', 'permission'];

    public function departaments()
    {
        return $this->belongsToMany(Departament::class, 'documents_departaments', 'document_id', 'departament_id');
    }


    public function scopePrivate($query)
    {
        $departament_collaborator = Auth::user()->collaborator->departaments;
        $departament_ids = collect($departament_collaborator)->pluck('id')->toArray();

        return $query->whereHas('departaments', function($q) use ($departament_ids){
        					return $q->whereIn('departament_id', $departament_ids);
        			 });
    }
}
