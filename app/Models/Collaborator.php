<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

use App\Models\Departament;

class Collaborator extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['user_id', 'code', 'name', 'photo', 'phone', 'date_birth', 'status'];

    protected $dates = ['date_birth'];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function paychecks()
    {
        return $this->hasMany('App\Models\Paycheck');
    }

    public function rel_departaments()
    {
    	return $this->hasMany('App\Models\CollaboratorDepartament', 'collaborator_id', 'id');
    }

    public function departaments()
    {
        return $this->belongsToMany('App\Models\Collaborator', 'collaborator_departaments', 'collaborator_id', 'departament_id');
    }

    public function indications()
    {
        return $this->hasMany('App\Models\Indication');
    }

    public function getEmailAttribute()
    {
    	return $this->user->email;
    }

    public function getDepartamentsAttribute($value)
    {

    	$departaments = $this->rel_departaments->pluck('departament_id')->toArray();
    	
    	return Departament::whereIn('id', $departaments)
    											->get()
    											->toArray();
    }

    public function prospections()
    {
        return $this->belongsToMany('App\Models\Indication');
    }

    public function listDepartaments()
    {

        return collect($this->departaments)->whereNotIn('slug', 'geral');

    }

    public function listCoworkers()
    {

        return Collaborator::join('collaborator_departaments', 'collaborators.id', '=', 'collaborator_departaments.collaborator_id')
            ->whereIn('collaborator_departaments.departament_id', $this->listDepartaments()->pluck('id'))
            ->get();

    }

    public function productions(){
        return $this->hasMany(Production::class);
    }
}
