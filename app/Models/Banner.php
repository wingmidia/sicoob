<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Carbon\Carbon;

class Banner extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['title', 'url', 'image', 'start_date', 'end_date', 'type', 'status'];

    protected $dates = ['start_date', 'end_date'];

    public function scopeType($query, $type)
    {
        return $query->where('type', $type);
    }

    public function scopeActive($query)
    {
    	return $query->where('status', 1)
    							 ->where(function($q){
    							 		$q->where('start_date', '<=', Carbon::now());
    							 		$q->orWhereNull('start_date');
    							 })
    							 ->where(function($q){
    							 		$q->where('end_date', '>=', Carbon::now());
    							 		$q->orWhereNull('end_date');
    							 });
    }
}
