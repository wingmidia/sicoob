<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Activity extends Model implements Transformable
{
    use TransformableTrait;

    protected $dates = ['start_date', 'end_date'];

    protected $fillable = ['collaborator_id', 'product_id', 'client_id', 'works', 'observation', 'status', 'start_date', 'end_date'];
    
    public function user()
    { 
      return $this->belongsTo('App\User');
    }

    public function client()
    { 
      return $this->belongsTo('App\Models\Client');
    }

    public function product()
    { 
      return $this->belongsTo('App\Models\Product');
    }

}
