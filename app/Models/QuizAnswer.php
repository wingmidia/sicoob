<?php
/**
 * Created by PhpStorm.
 * User: Wing Mídia
 * Date: 05/04/2018
 * Time: 16:55
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class QuizAnswer extends Model implements Transformable {

    use TransformableTrait;

    protected $fillable = ['quiz_ask_id', 'quiz_ask_alternative_id'];

    protected $table = 'quiz_ask_answers';

    public function alternative(){
        return $this->belongsTo(QuizAskAlternative::class, 'quiz_ask_alternative_id', 'id');
    }

    public function ask(){
        return $this->belongsTo(QuizAsk::class, 'quiz_ask_id', 'id');
    }



}