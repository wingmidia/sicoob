<?php

namespace App\Models;

use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Zizaco\Entrust\EntrustPermission;
use Cviebrock\EloquentSluggable\Sluggable;

class Permission extends EntrustPermission implements Transformable
{
    use TransformableTrait, Sluggable;

    protected $fillable = ['display_name', 'description'];

	public function sluggable()
    {
        return [
            'name' => [
                'source' => 'display_name'
            ]
        ];
    }

}
