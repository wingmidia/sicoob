<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Carbon\Carbon; 

class Notification extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['message', 'date_expired', 'status'];    

    public function scopeActive($query)
    {
    	return $query->where('status', 1)
    							 ->where('date_expired', '>=', Carbon::now());
    }

}
