<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Production extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['client_id', 'collaborator_id', 'status'];

    public function client()
    { 
      return $this->belongsTo('App\Models\Client');
    }

    public function collaborator()
    {
        return $this->belongsTo('App\Models\Collaborator');
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product', 'production_product')->withPivot('value', 'sold_at', 'indication_id');
    }

    public function fields()
    {
        return $this->belongsToMany('App\Models\ProductFields', 'production_product_field')->withPivot('value');
    }
}
