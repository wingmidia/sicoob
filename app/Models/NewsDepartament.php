<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsDepartament extends Model
{
		public $incrementing = false;

    protected $fillable = ['new_id', 'departament_id'];
		
		public $timestamps = false;

    public function new()
    {	
    	return $this->belongsTo('App\Models\NewsDepartament', 'new_id');
    }

    public function departament()
    {	
    	return $this->belongsTo('App\Models\Departament', 'departament_id');
    }
}
