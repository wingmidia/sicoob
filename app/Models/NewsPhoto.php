<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class NewsPhoto extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['new_id','image'];

    public $timestamps = false;

    public function new()
    {
    	return $this->belongsTo(News::class);
    }

}
