<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class IndicationStatus extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'indication_status';

    protected $fillable = ['name', 'state', 'status'];



}
