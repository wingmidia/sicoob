<?php

namespace App\Models;

use Storage;
use File;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;

use App\Models\Departament;
use Auth;

class StorageManager
{
		
	protected $folder_root;

	protected $path;

	public function __construct()
	{
		$this->folder_root = 'documents';
	}

	public function all($path = ''){
		
		$folders = array_merge($this->directories($path), $this->files($path));
		
		if(Auth::user()->hasRole(['ti', 'admin', 'diretoria'])){
			return $folders;			
		}
		if(is_null($path) || $path == '/'){
			$departaments = collect(Auth::user()->collaborator->departaments)->pluck('name')->toArray();
			
			$folders = array_filter($folders, function($p) use ($departaments){
				return in_array($p['filename'], $departaments);
			});
		}

		return $folders;
	}

	public function files($path)
	{
		return array_map(function($f){
			return [
						'id' 		=> bcrypt($f),
						'type' 		=> 'file', 
						'filename' 	=> basename($f), 
						'path' 	    => str_replace($this->folder_root, '', $f), 
						'size' 		=> Storage::getSize($f), 
						'extension' => $this->extension($f),
						'mimetype' 	=> Storage::getMimetype($f)
					];
		}, Storage::files($this->folder_root.'/'.$path));
	}

	public function directories($path)
	{
		return array_map(function($f){
			return [
						'id' 		=> bcrypt($f),
						'type' 		=> 'folder', 
						'filename' 	=> basename($f), 
						'path' 	    => str_replace($this->folder_root, '', $f), 
						'size' 		=> Storage::getSize($f), 
						'extension' => 'folder',
						'mimetype' 	=> Storage::getMimetype($f)
					];
		}, Storage::directories($this->folder_root.'/'.$path));    
	}

	public function makeDirectory($path)
	{
		
		$f = $this->folder_root.$path;
		Storage::makeDirectory($f);
		
		$this->cache_clear();

		return [
						'id' 		=> bcrypt($f),
						'type' 		=> 'folder', 
						'filename' 	=> basename($f), 
						'path' 	    => str_replace($this->folder_root, '', $f), 
						'size' 		=> Storage::getSize($f), 
						'extension' => 'folder',
						'mimetype' 	=> Storage::getMimetype($f)
					];;
	}

	public function upload(Request $request, $path = '/', $filename = '')
	{

		$file = Storage::putFileAs($this->folder_root.$path, $request->file('documents'), $filename);
		
		$this->cache_clear();

		return [
			'id' 				=> bcrypt($file),
			'type' 			=> 'file', 
			'filename' 	=> basename($file), 
			'path' 	    => str_replace($this->folder_root, '', $file), 
			'size' 			=> Storage::getSize($file), 
			'extension' => $this->extension($file),
			'mimetype' 	=> Storage::getMimetype($file)
		];
	}

  public function copy($origin, $dest)
	{		
		$origin = $this->folder_root.$origin;
		$dest   = $dest;
		
		$copy = Storage::copy($origin, $dest);

		return $copy;
	}	

	public function move($origin, $dest)
	{	
		$origin = storage_path('app/'.$this->folder_root.$origin);
		$dest   = storage_path('app/'.$dest);
		
		$move = File::moveDirectory($origin, $dest);	
		
		return $move;
	}	


	public function moveDirectory($origin, $dest)
	{		
		$origin = storage_path('app/'.$this->folder_root.$origin);
		$dest   = storage_path('app/'.$dest);
		
		$move = File::moveDirectory($origin, $dest);

		return $move;
	}


	public function deleteFile($path)
	{		
		return Storage::delete($this->folder_root.$path);
	}

	public function deleteFolder($path)
	{
		return Storage::deleteDirectory($this->folder_root.$path);
	}

	public function allFolders()
	{
		return Storage::allDirectories($this->folder_root);
	}

	public function exists($file)
	{	
		$file = storage_path('app/'.$this->folder_root.$file);
		
		return file_exists($file);
	}

	public function path_original($file)
	{	
		return storage_path('app/'.$this->folder_root.'/'.$file);
	}

	private function cache_clear()
	{
		Cache::forget('files');
		Cache::forget('directories');		
	}

	public function disable_cache()
	{
		$this->cache_clear();
	}

	public function createDirectoriesDepartaments()
	{
		$departaments = Departament::get();

		foreach ($departaments as $departament) {
			$dir = $this->folder_root.'/'.$departament->name;
			$path = storage_path('app/'.$dir);
			if(is_dir($path))
			{
				continue;
			}
			
			Storage::makeDirectory($dir);
		}
	}

	private function extension($file)
	{

		$ext = pathinfo(storage_path('app/'.$file), PATHINFO_EXTENSION);

		if(!file_exists(public_path('img/icones/'.$ext.'.png'))){
			return 'default';
		}
		
		return strtolower($ext);
	}
}

