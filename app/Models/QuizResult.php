<?php
/**
 * Created by PhpStorm.
 * User: Wing Mídia
 * Date: 05/04/2018
 * Time: 16:56
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class QuizResult extends Model implements Transformable {

    use TransformableTrait;

    protected $fillable = ['started_at', 'ended_at', 'quiz_id', 'collaborator_id', 'right_answers', 'wrong_answers', 'terminated'];

    protected $dates = ['started_at', 'ended_at'];

    public function answers(){
        return $this->hasMany('App\Models\QuizAnswer', 'quiz_result_id', 'id');
    }

    public function quiz(){
        return $this->belongsTo('App\Models\Quiz');
    }

    public function collaborator(){
        return $this->belongsTo('App\Models\Collaborator');
    }

    public function pontuation(){

        $totalAsks = $this->quiz()->first()->asks()->where('active', true)->sum('weight');


        $answers = $this->answers()->get();

        $responses = [];

        $rightAlternatives = 0;
        $right = 0;
        $wrongAlternatives = 0;
        $wrong = 0;

        foreach ($answers as $answer){
            $answersAsk = $answer->ask()->first();
            if($answersAsk->active)
                $responses[$answer->quiz_ask_id]['correct'] = true;
                $responses[$answer->quiz_ask_id]['pontuation'] = $answersAsk->weight;
        }


        foreach ($answers as $answer){
            if(!$answer->alternative()->first()->correct && $answer->alternative()->first()->active)
                $responses[$answer->quiz_ask_id]['correct'] = false;
        }

        foreach ($responses as $response){
            if($response['correct']) {
                $right += $response['pontuation'];
                $rightAlternatives++;
            }
            else{
                $wrong += $response['pontuation'];
                $wrongAlternatives++;
            }
        }

        $result = 0;

        if($totalAsks>0) $result = $right/$totalAsks;


        return [
            'total' => $totalAsks,
            'right' => $right,
            'rightAlternatives' => $rightAlternatives,
            'wrong' => $wrong,
            'wrongAlternatives' => $wrongAlternatives,
            'result' => $result
        ];
    }

}