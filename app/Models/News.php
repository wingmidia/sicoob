<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;
use Auth;

class News extends Model implements Transformable
{
    use TransformableTrait, Sluggable;

    protected $dates = ['start_date', 'end_date'];

    protected $fillable = ['title', 'description', 'content', 'start_date', 'end_date', 'image_feacture', 'status', 'permission'];

    protected $appends = ['approve_comments'];

	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('active', function (Builder $query) {
            
        });
    }


    public function news_departaments()
    {
        return $this->belongsToMany(\App\Models\Departament::class, 'news_departaments', 'new_id', 'departament_id');
    }

    public function photos()
    {
        return $this->hasMany(\App\Models\NewsPhoto::class, 'new_id');
    }

    public function comments()
    {
        return $this->hasMany(NewsComment::class, 'new_id');
    }

    public function getApproveCommentsAttribute()
    {
        return $this->comments()->where('status', 0)->count();
    }


    public function scopeFeacture($query)
    {
        $departaments = [];
        if(Auth::check()){
            $departaments = collect(Auth::user()->collaborator->departaments)->pluck('id')->toArray();
        }
        
        return $query->whereHas('news_departaments', function($q) use ($departaments){
            $q->where('slug', 'geral');
            if($departaments){
                $q->orWhereIn('departament_id', $departaments);
            }
        });
    }

    public function scopeActive($query)
    {
        return $query->where(function($q){
                    $q->where('status', 1);
                    $q->where('start_date', '<=', Carbon::now());
                    $q->where(function($q){
                        $q->where('end_date', '>=', Carbon::now());
                        $q->orWhereNull('end_date');
                    });
                });
    }

    public function scopePrivate($query, $departament_id)
    {
        if(Auth::check()){
            
            $departament_collaborator = Auth::user()->collaborator->departaments;

            if($departament_collaborator){
              $departament_ids = collect($departament_collaborator)->pluck('id')->toArray();

              if(in_array($departament_id, $departament_ids)){
                $query->where(function($q) use ($departament_collaborator){
                    $q->whereIn('permission', ['private', 'public']);         
                });    

              } else {
                $news_filter = News::join('news_departaments', 'news.id', '=', 'news_departaments.new_id', 'left')
                            ->whereIn('id', $departament_ids)
                            ->get();
                            
                if($news_filter->count()){
                    $news_id = $news_filter->pluck('id')->toArray();

                    $query->where(function($q) use ($news_id){
                        $q->orWhere(function($q1) use($news_id){
                            $q1->whereIn('id', $news_id);
                        });
                        $q->orWhere('permission', 'public');
                    });     
                }
                 
              }

            } else {

              $query->where('permission', 'public');

            }

        } else {

            $query->where('permission', 'public');
        }

        return $query;
    }
}
