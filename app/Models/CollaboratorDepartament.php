<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CollaboratorDepartament extends Model
{
		public $incrementing = false;

    protected $fillable = ['collaborator_id', 'departament_id'];
		
		public $timestamps = false;

    public function collaborator()
    {	
    	return $this->belongsTo('App\Models\Collaborator', 'collaborator_id');
    }

    public function departament()
    {	
    	return $this->belongsTo('App\Models\Departament', 'departament_id');
    }
}
