<?php
/**
 * Created by PhpStorm.
 * User: Wing Mídia
 * Date: 17/04/2018
 * Time: 10:51
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Campaign extends Model {

    protected $fillable = ['name', 'start_at', 'end_at', 'description', 'image', 'active'];
    protected $dates = ['start_at', 'end_at'];

    public function products(){
        return $this->belongsToMany(Product::class, 'campaign_product', 'campaign_id', 'product_id');
    }
    
    public function categories(){
        return $this->belongsToMany(Category::class, 'campaign_categories', 'campaign_id', 'category_id');
    }
}