<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Cviebrock\EloquentSluggable\Sluggable;

class Departament extends Model implements Transformable
{
    use TransformableTrait, Sluggable;

    protected $fillable = ['name', 'prospection', 'phone', 'address', 'district', 'cnpj', 'cep', 'email', 'photo', 'description'];

    public $timestamps = false;

	public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function news()
    {	
    	return $this->hasMany('App\Models\NewsDepartament');
    }

    public function collaborators()
    {
        return $this->belongsToMany('App\Models\Collaborator', 'collaborator_departaments', 'departament_id', 'collaborator_id');
    }

}
