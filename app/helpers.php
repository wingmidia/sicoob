<?php
use App\Models\Departament;
use App\Models\Product;
use App\Models\Client;
use App\Models\Collaborator;
use Illuminate\Support\Facades\Auth;

function iconStatusActivity( $status )
{
  $iconStatus = "";

  switch ($status) {
    case 'open':
      $iconStatus = "tasks";
      break;
    case 'waiting':
      $iconStatus = "clock-o";
      break;
    case 'approved':
      $iconStatus = "thumbs-up";
      break;
    case 'canceled':
      $iconStatus = "ban";
      break;
    case 'complete':
      $iconStatus = "check";
      break;
    case 'reproved':
      $iconStatus = "thumbs-down";
      break;
    case 'active':
      $iconStatus = "flag";
      break;
    
    default:
      $iconStatus = "clock-o";
      break;
  }

  return $iconStatus;
}

function iconStatusPaychecks( $status )
{
  $iconStatus = "";

  switch ($status) {
    case 'OPEN':
      $iconStatus = "check aberto";
      break;
    case 'SENDING':
      $iconStatus = "paper-plane";
      break;
    case 'PENDING':
      $iconStatus = "clock-o aberto";
      break;
    case 'COMPLETE':
      $iconStatus = "check";
      break;
    
    default:
      $iconStatus = "clock-o";
      break;
  }

  return $iconStatus;
}

function getDepartaments()
{
  $departaments = Departament::orderBy('name')->get();

  return $departaments;
}

function getProducts()
{
  $products = Product::where('status', 1)->orderBy('title')->get();

  return $products;
}

function getCollaborators()
{
  $collaborators = Collaborator::where('status', 1)->orderBy('name')->get();

  return $collaborators;
}

function getClients()
{
  $clients = null;
  if(Auth::check()){
    $collaborator_id = Auth::user()->collaborator->id;
    $clients = Client::where('collaborator_id', (int)$collaborator_id)->orderBy('name')->get();  
  }
  

  return $clients;
}

function maskMonth($month){
  $text = '';
  switch ($month) {
    case 0:
      $text = 'Janeiro';
      break;
    case 1:
      $text = 'Fevereiro';
      break;
    case 2:
      $text = 'Março';
      break;
    case 3:
      $text = 'Abril';
      break;
    case 4:
      $text = 'Maio';
      break;
    case 5:
      $text = 'Junho';
      break;
    case 6:
      $text = 'Julho';
      break;
    case 7:
      $text = 'Agosto';
      break;
    case 8:
      $text = 'Setembro';
      break;
    case 9:
      $text = 'Outubro';
      break;
    case 10:
      $text = 'Novembro';
      break;
    case 11:
      $text = 'Dezembro';
      break;
  }

  return $text;
}

function statusActivity(){
  $status = [
    'open' => 'Aberto',
    'active' => 'Ativo',
    'waiting' => 'Aguardando',
    'approved' => 'Aprovado',
    'progress' => 'Em andamento',
    'canceled' => 'Cancelado',
    'complete' => 'Concluído',
    'reproved' => 'Reprovado'
  ];

  return $status;
}

function arrayToObject($array){

    return json_decode(json_encode($array, JSON_FORCE_OBJECT), false);

}

function getStates(){

    return [
        'AC'=>'Acre',
        'AL'=>'Alagoas',
        'AP'=>'Amapá',
        'AM'=>'Amazonas',
        'BA'=>'Bahia',
        'CE'=>'Ceará',
        'DF'=>'Distrito Federal',
        'ES'=>'Espírito Santo',
        'GO'=>'Goiás',
        'MA'=>'Maranhão',
        'MT'=>'Mato Grosso',
        'MS'=>'Mato Grosso do Sul',
        'MG'=>'Minas Gerais',
        'PA'=>'Pará',
        'PB'=>'Paraíba',
        'PR'=>'Paraná',
        'PE'=>'Pernambuco',
        'PI'=>'Piauí',
        'RJ'=>'Rio de Janeiro',
        'RN'=>'Rio Grande do Norte',
        'RS'=>'Rio Grande do Sul',
        'RO'=>'Rondônia',
        'RR'=>'Roraima',
        'SC'=>'Santa Catarina',
        'SP'=>'São Paulo',
        'SE'=>'Sergipe',
        'TO'=>'Tocantins'
    ];

}

function mask($val, $mask){

    $maskared = '';
    $k = 0;

    for($i = 0; $i<=strlen($mask)-1; $i++){

        if($mask[$i] == '#'){
            if(isset($val[$k]))
                $maskared .= $val[$k++];
        }else{
            if(isset($mask[$i]))
                $maskared .= $mask[$i];
        }

    }

    return $maskared;
}