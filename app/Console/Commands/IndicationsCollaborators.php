<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Mail\SendMailable;
use Illuminate\Support\Facades\Mail;
use App\Models\Collaborator;
use Illuminate\Support\Facades\DB;

use App;
use Config;

class IndicationsCollaborators extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'indications:collaborators';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envio das indicações pendentes por email!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = DB::table('configs')->get()->pluck('value','key');

        $conf = [
            'driver' => $data['driverSMTP'],
            'host' => $data['hostSMTP'],
            'port' => $data['portSMTP'],
            'from' => [
                'address' => $data['addressSMTP'],
                'name' => $data['nameSMTP']
            ],
            'encryption' => $data['encryptionSMTP'],
            'username' => $data['usernameSMTP'],
            'password' => $data['passwordSMTP'],
            'sendmail' => '/usr/sbin/sendmail -bs',
            'markdown' => [
                'theme' => 'default',
                'paths' => [
                    resource_path('views/vendor/mail'),
                ],
            ],
            'stream' => [
                'ssl' => [
                    'allow_self_signed' => true,
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                ],
            ],
        ];

        Config::set('mail', $conf);

        $app = App::getInstance();
        $app->register('Illuminate\Mail\MailServiceProvider');

        $temp = DB::table('collaborators')
        ->select('collaborators.name','collaborators.id','users.email')
        ->leftJoin('users', 'collaborators.user_id', '=', 'users.id')
        ->where('collaborators.status',1)
        ->get();

        foreach($temp as $x) {
            $temp2 = DB::table('indications')
            ->select('clients.name','indications.created_at','indications.indication_status_id')
            ->leftJoin('clients', 'indications.client_id', '=', 'clients.id')
            ->leftJoin('collaborator_indication', 'collaborator_indication.indication_id', '=', 'indications.id')
            ->where('collaborator_indication.collaborator_id',$x->id)
            ->whereNull('indications.indication_status_id')
            ->get();

            if(count($temp2) == 0) continue;

            $val = 5;

            $texto = "";

            $texto .= $x->name." este email é enviado automaticamente, favor não responder.<br><br>";
            $texto .= "As indicações atualmente pendentes são:<br><br><table style=\"border-collapse: collapse;border: 1px solid black;\">";
            $texto .= "<tr>";
            $texto .= "<th style=\"border: 1px solid black; padding:".$val."px;\">Nome do Cliente</th>";
            $texto .= "<th style=\"border: 1px solid black; padding:".$val."px;\">Data da Criação</th>";
            $texto .= "</tr>";

            foreach($temp2 as $y) {
                //if ($y->indication_status_id != null) continue;

                $texto .= "<tr>";
                $texto .= "<td style=\"border: 1px solid black; padding:".$val."px;\">" . $y->name . "</td>";
                $texto .= "<td style=\"border: 1px solid black; padding:".$val."px;\">" . \Carbon\Carbon::parse($y->created_at)->format('d/m/Y H:m:i') . "</td>";
                $texto .= "</tr>";
            }

            $texto .= "</table>";

            $texto .= "<br><br>Não sendo de seu interesse em fazer essa prospecção, por gentileza transmitir a indicação para outro colaborador em ate 24 horas.";

            Mail::to($x->email)->send(new SendMailable($texto,"Indicações Pendentes"));

        }

        
    }
}
