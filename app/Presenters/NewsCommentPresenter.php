<?php

namespace App\Presenters;

use App\Transformers\NewsCommentTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class NewsCommentPresenter
 *
 * @package namespace App\Presenters;
 */
class NewsCommentPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new NewsCommentTransformer();
    }
}
