<?php

namespace App\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Event implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->fit(350, 204);
    }
}