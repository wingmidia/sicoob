<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Zizaco\Entrust\Traits\EntrustUserTrait;


class User extends Authenticatable implements Transformable
{
    use Notifiable, 
        TransformableTrait, 
        EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'status'
    ];   

    protected $appends = [
        'photo', 'role_id'
    ];   

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function collaborator()
    {   
        return $this->hasOne('App\Models\Collaborator');
    }

    public function getPhotoAttribute(){
        return ($this->collaborator) ? $this->collaborator->photo : null;
    }

    public function getRoleIdAttribute()
    {
        $role = $this->roles;

        return $role->count() > 0 ? $role->first()->id : null;
    }

    public function permission_departament($departments)
    {
        $permission = false;
        $departament_collaborator = $this->collaborator->departaments;
        if($departament_collaborator){
            $departament_ids = collect($departament_collaborator)->pluck('id')->toArray();
            if(is_array($departments)){
                foreach ($departments as $department_id) {
                    if(in_array($department_id, $departament_ids)){
                        $permission = true;
                        break;
                    }
                }
            } else {
                $department_id = $departments;
                if(in_array($department_id, $departament_collaborator)){
                    $permission = true;
                }
            }

        }
        
        return $permission;
    }
}

