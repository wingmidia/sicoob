<?php
/**
 * Created by PhpStorm.
 * User: Wing Mídia
 * Date: 17/04/2018
 * Time: 14:22
 */

namespace App\Validators;


use Prettus\Validator\LaravelValidator;
use \Prettus\Validator\Contracts\ValidatorInterface;

class CampaignValidator extends LaravelValidator {
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [],
        ValidatorInterface::RULE_UPDATE => []
    ];
}