<?php
/**
 * Created by PhpStorm.
 * User: Wing Mídia
 * Date: 06/04/2018
 * Time: 16:52
 */

namespace App\Validators;


use Prettus\Validator\LaravelValidator;
use \Prettus\Validator\Contracts\ValidatorInterface;

class QuizValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [],
        ValidatorInterface::RULE_UPDATE => [],
    ];

}