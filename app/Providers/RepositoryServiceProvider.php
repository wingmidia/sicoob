<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Repositories\ActivityRepository::class, \App\Repositories\ActivityRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\EventRepository::class, \App\Repositories\EventRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\LinksRepository::class, \App\Repositories\LinksRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\NewsRepository::class, \App\Repositories\NewsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\DepartamentRepository::class, \App\Repositories\DepartamentRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PaycheckRepository::class, \App\Repositories\PaycheckRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProductRepository::class, \App\Repositories\ProductRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProductionRepository::class, \App\Repositories\ProductionRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProductFieldsRepository::class, \App\Repositories\ProductFieldsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\UserRepository::class, \App\Repositories\UserRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PermissionRepository::class, \App\Repositories\PermissionRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\LinkRepository::class, \App\Repositories\LinkRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CollaboratorRepository::class, \App\Repositories\CollaboratorRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\RoleRepository::class, \App\Repositories\RoleRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\DocumentRepository::class, \App\Repositories\DocumentRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\NotificationRepository::class, \App\Repositories\NotificationRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\BannerRepository::class, \App\Repositories\BannerRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\NewsPhotoRepository::class, \App\Repositories\NewsPhotoRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\NewsCommentRepository::class, \App\Repositories\NewsCommentRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\EventsPhotoRepository::class, \App\Repositories\EventsPhotoRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\EventsPhotoRepository::class, \App\Repositories\EventsPhotoRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ClientRepository::class, \App\Repositories\ClientRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\DocumentsRepository::class, \App\Repositories\DocumentsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\WidgetRepository::class, \App\Repositories\WidgetRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\IndicationRepository::class, \App\Repositories\IndicationRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\IndicationStatusRepository::class, \App\Repositories\IndicationStatusRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\QuizRepository::class, \App\Repositories\QuizRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\QuizResultRepository::class, \App\Repositories\QuizResultRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CampaignRepository::class, \App\Repositories\CampaignRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CategoryRepository::class, \App\Repositories\CategoryRepositoryEloquent::class);
        //:end-bindings:
    }
}
