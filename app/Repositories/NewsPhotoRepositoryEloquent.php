<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\news_photoRepository;
use App\Models\NewsPhoto;
use App\Validators\NewsPhotoValidator;

/**
 * Class NewsPhotoRepositoryEloquent
 * @package namespace App\Repositories;
 */
class NewsPhotoRepositoryEloquent extends BaseRepository implements NewsPhotoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return NewsPhoto::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return NewsPhotoValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
