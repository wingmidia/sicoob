<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DocumentsRepository
 * @package namespace App\Repositories;
 */
interface DocumentsRepository extends RepositoryInterface
{
    //
}
