<?php
/**
 * Created by PhpStorm.
 * User: Wing Mídia
 * Date: 10/04/2018
 * Time: 15:56
 */

namespace App\Repositories;


use Prettus\Repository\Contracts\RepositoryInterface;

interface QuizResultRepository extends RepositoryInterface {

}