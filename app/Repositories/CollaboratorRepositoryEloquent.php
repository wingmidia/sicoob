<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\Collaborator;
use App\Validators\CollaboratorValidator;

/**
 * Class CollaboratorRepositoryEloquent
 * @package namespace App\Repositories;
 */
class CollaboratorRepositoryEloquent extends BaseRepository implements CollaboratorRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Collaborator::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return CollaboratorValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function pickUpCollaboratorForProspecting($departaments)
    {

        return Collaborator::select(
            'collaborators.*',
            DB::raw('COUNT(DISTINCT collaborator_indication.indication_id) AS qtd_indicacao')
            )
            ->join('users', 'users.id', '=', 'collaborators.user_id')
            ->join('role_user', 'role_user.user_id', '=', 'users.id')
            ->join('permission_role', 'permission_role.role_id', '=', 'role_user.role_id')
            ->join('permissions', 'permissions.id', '=', 'permission_role.permission_id')
            ->join('collaborator_departaments', 'collaborator_departaments.collaborator_id', '=', 'collaborators.id')
            ->leftJoin('collaborator_indication', 'collaborator_indication.collaborator_id', '=', 'collaborators.id')
            ->where('collaborators.status', 1)
            ->where('permissions.name', 'indications-prospection')
            ->whereIn(
                'collaborator_departaments.departament_id', $departaments)
            ->groupBy('collaborators.id')
            ->orderBy('qtd_indicacao')
            ->orderBy('collaborators.code')
            ->first();

    }

    public function ofActive()
    {
        $temp = $this->orderBy('name');
        return $temp->findWhere(['status' => 1]);
    }
}
