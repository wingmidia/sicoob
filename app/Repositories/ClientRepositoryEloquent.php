<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ClientRepository;
use App\Models\Client;
use App\Validators\ClientValidator;
use App\Criteria\ClientsCriteria;

/**
 * Class ClientRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ClientRepositoryEloquent extends BaseRepository implements ClientRepository
{

    protected $fieldSearchable = [
        'name'=>'like',
        'company_name'=>'like',
        'email' => '='
    ];
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Client::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return ClientValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->pushCriteria(ClientsCriteria::class);
    }

    public function getProductsIndicated($id){

        $indications = $this->model
            ->find($id)
            ->indications()
            ->where('concluded', 0)
            ->get();

        $products = collect();

        foreach ($indications as $indication) {
            $products = $products->merge($indication->products()
                ->get()
            );
        }

        return $products;

    }
}
