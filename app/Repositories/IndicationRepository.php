<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface IndicationRepository
 * @package namespace App\Repositories;
 */
interface IndicationRepository extends RepositoryInterface
{
    //
}
