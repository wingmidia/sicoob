<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PaycheckRepository;
use App\Models\Paycheck;
use App\Validators\PaycheckValidator;

/**
 * Class PaycheckRepositoryEloquent
 * @package namespace App\Repositories;
 */
class PaycheckRepositoryEloquent extends BaseRepository implements PaycheckRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Paycheck::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
