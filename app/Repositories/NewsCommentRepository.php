<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface NewsCommentRepository
 * @package namespace App\Repositories;
 */
interface NewsCommentRepository extends RepositoryInterface
{
    //
}
