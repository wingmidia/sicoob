<?php
/**
 * Created by PhpStorm.
 * User: Wing Mídia
 * Date: 06/04/2018
 * Time: 16:33
 */

namespace App\Repositories;


use App\Models\Collaborator;
use App\Models\Quiz;
use App\Models\QuizResult;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Yajra\Datatables\Datatables;

class QuizRepositoryEloquent extends BaseRepository implements QuizRepository {


    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Quiz::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    public function getDataTables($collaboratorId){

        return Datatables::of(
            $this->model
                ->select(
                    'quizzes.id',
                     //DB::raw('collaborators.id as collaboratorId'),
                    'quizzes.name',
                    'quizzes.description',
                    'quizzes.time',
                    'quizzes.start_date',
                    'quizzes.active',
                    'quizzes.end_date',
                    DB::raw('quizzes.end_date as end'),
                    'quizzes.created_at'
                )/*->leftJoin('quiz_results', 'quiz_results.quiz_id', '=', 'quizzes.id')
                ->leftJoin('collaborators', 'collaborators.id', '=', 'quiz_results.collaborator_id')*/
                /*->leftJoin('collaborators', function ($join) use ($collaboratorId){
                    $join->on('quiz_results.collaborator_id', '=', 'collaborators.id')
                        ->where('collaborators.id', '=', $collaboratorId);
                })*/
                ->groupBy('quizzes.id')
            )->editColumn('description', function ($data){
            return empty($data->description)?'':$data->description;
            })->editColumn('created_at', function ($data){
            return $data->created_at->format('d/m/Y');
            })->editColumn('start_date', function ($data){
            return $data->start_date->format('d/m/Y');
            })->editColumn('end_date', function ($data){
            return $data->end_date->format('d/m/Y');
            })->editColumn('terminated', function ($data){

                $quizResult = QuizResult::where([
                    'quiz_id' => $data->id,
                    'collaborator_id' => Auth::user()->collaborator->id
                ])->first();

                return (isset($quizResult->terminated) && $quizResult->terminated == 1) ? true : false;
                /*if(!empty($data->collaboratorId)) {
                    $quizResult = QuizResult::where([
                        'quiz_id' => $data->id,
                        'collaborator_id' => $data->collaboratorId
                    ])->first();
                    return ($quizResult->terminated) ? true : false;
                }else return false;*/
            });
    }

}