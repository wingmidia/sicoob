<?php

namespace App\Repositories;

use App\Models\IndicationStatus;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Yajra\Datatables\Datatables;

/**
 * Class IndicationStatusRepositoryEloquent
 * @package namespace App\Repositories;
 */
class IndicationStatusRepositoryEloquent extends BaseRepository implements IndicationStatusRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return IndicationStatus::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));        
    }

    public function getDatatables()
    {
        return Datatables::of($this->model->select('*')->whereNotIn('id', [9000,9001]));
    }

}
