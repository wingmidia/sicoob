<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface IndicationStatusRepository
 * @package namespace App\Repositories;
 */
interface IndicationStatusRepository extends RepositoryInterface
{
    //
}
