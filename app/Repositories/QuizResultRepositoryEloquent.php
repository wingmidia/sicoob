<?php
/**
 * Created by PhpStorm.
 * User: Wing Mídia
 * Date: 10/04/2018
 * Time: 15:57
 */

namespace App\Repositories;


use App\Models\Quiz;
use App\Models\QuizAsk;
use App\Models\QuizAskAlternative;
use App\Models\QuizResult;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Yajra\Datatables\Datatables;

class QuizResultRepositoryEloquent extends BaseRepository implements QuizResultRepository {

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return QuizResult::class;
    }

    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getDataTables($quizId){

        return Datatables::of(
            $this->model
                ->select(
                    'quiz_results.id',
                    'quiz_results.quiz_id',
                    DB::raw('collaborators.id as collaborator_id'),
                    'collaborators.name',
                    'quiz_results.right_answers',
                    'quiz_results.wrong_answers',
                    'quiz_results.terminated',
                    'quiz_results.started_at',
                    'quiz_results.ended_at',
                    'quiz_results.created_at'
                )->join('collaborators', 'quiz_results.collaborator_id','=','collaborators.id')
                ->where('quiz_results.quiz_id', $quizId)
        )->editColumn('created_at', function ($data){
            return $data->created_at->format('d/m/Y');
        })->editColumn('started_at', function ($data){
            return $data->started_at->format('d/m/Y H:i');
        })->editColumn('ended_at', function ($data){
            if(!empty($data->ended_at))
                return $data->ended_at->format('d/m/Y H:i');
            else return '';
        })->editColumn('end_at', function ($data){
            if(!empty($data->ended_at)) {
                return $data->ended_at->diffInMinutes($data->started_at);
            }else return '';
        })->editColumn('pontuacao', function ($data){
            $pontuation = $data->pontuation();
            return number_format($pontuation['result']*100, 2, ',', '.').'%';
        });
    }
}