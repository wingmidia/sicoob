<?php

namespace App\Repositories;

use App\Models\ProductFields;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class ProductRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ProductFieldsRepositoryEloquent extends BaseRepository implements ProductFieldsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProductFields::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));        
    }

}
