<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DepartamentsRepository
 * @package namespace App\Repositories;
 */
interface DepartamentRepository extends RepositoryInterface
{
    //
}
