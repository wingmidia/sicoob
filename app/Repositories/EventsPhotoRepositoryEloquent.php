<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\EventsPhotoRepository;
use App\Models\EventsPhoto;
use App\Validators\EventsPhotoValidator;

/**
 * Class EventsPhotoRepositoryEloquent
 * @package namespace App\Repositories;
 */
class EventsPhotoRepositoryEloquent extends BaseRepository implements EventsPhotoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return EventsPhoto::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return EventsPhotoValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
