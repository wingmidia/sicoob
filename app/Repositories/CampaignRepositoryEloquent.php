<?php
/**
 * Created by PhpStorm.
 * User: Wing Mídia
 * Date: 17/04/2018
 * Time: 11:18
 */

namespace App\Repositories;


use App\Models\Campaign;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Yajra\Datatables\Datatables;

class CampaignRepositoryEloquent extends BaseRepository implements CampaignRepository {


    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Campaign::class;
    }



    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getDataTables(){
        return Datatables::of(
            $this->model->select(
                'campaigns.created_at',
                'campaigns.id',
                'campaigns.name',
                'campaigns.start_at',
                'campaigns.end_at',
                'campaigns.description',
                'campaigns.active'
            )
        )->editColumn('description', function ($data){
            if(empty($data->description)) return '';
            return $data->description;
        })->editColumn('created_at', function ($data){
            return $data->created_at->format('d/m/Y');
        })->editColumn('start_at', function ($data){
            return $data->start_at->format('d/m/Y');
        })->editColumn('end_at', function ($data){
            return $data->end_at->format('d/m/Y');
        });
    }


}