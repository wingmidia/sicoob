<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\Category;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;

/**
 * Class CategoryRepositoryEloquent
 * @package namespace App\Repositories;
 */
class CategoryRepositoryEloquent extends BaseRepository implements CategoryRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Category::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));        
    }

    public function getDatatables()
    {
        return Datatables::of($this->model->select('*'));
    }

    public function getRankingDataTables($id, $filters){

        $usersRaking = $this->getUserRanking($id, $filters);

        return Datatables::of($usersRaking)
                ->editColumn('total', function ($data){
                    $valor = 0;
                    if(!empty($data->total)) $valor = $data->total;
                    return 'R$ '. number_format($valor, 2, ',', '.');
                });
    }

    /**
     * @param $campaign_id
     * @param $filters
     * @return mixed
     */
    public function getUserRanking($campaign_id, $filters = [])
    {
        $usersRaking = DB::table('users')
            ->select('collaborators.id', 'collaborators.name', DB::raw('count(productions.collaborator_id) as sales'), DB::raw('sum(production_product.value) as total'), 'collaborators.photo')
            ->join('collaborators', 'collaborators.user_id', '=', 'users.id')
            ->leftJoin('productions', 'productions.collaborator_id', '=', 'collaborators.id')
            ->leftJoin('clients', 'clients.id', '=', 'productions.client_id')
            ->leftJoin('production_product', 'production_product.production_id', '=', 'productions.id')
            ->leftJoin('products', 'products.id', '=', 'production_product.product_id')
            // ->leftJoin('categories', 'categories.id', '=', 'products.category_id')
            // ->leftJoin('campaign_product', 'campaign_product.product_id', '=', 'products.id')
            ->leftJoin('campaign_categories', 'campaign_categories.category_id', '=', 'products.category_id')
            ->join('campaigns', function ($join){
                $join->on('campaigns.id', '=', 'campaign_categories.campaign_id')
                    ->whereRaw('production_product.sold_at BETWEEN campaigns.start_at and campaigns.end_at');
            })
            ->whereRaw('production_product.sold_at between campaigns.start_at and campaigns.end_at')
            ->where('campaigns.id', $campaign_id)
            ->where('productions.status', 1);

        if (isset($filters['departaments'])) {
            $departaments = $this->parseInt($filters['departaments']);
            if (count($departaments) > 0)
                $usersRaking = $usersRaking->whereIn('clients.departament_id', $departaments)
                    ->orWhereRaw('clients.departament_id IS NULL');
        }
        if (isset($filters['products'])) {
            $products = $this->parseInt($filters['products']);
            if (count($products) > 0)
                $usersRaking = $usersRaking->whereIn('products.id', $products);
        }
        if (isset($filters['categories'])) {
            $categories = $this->parseInt($filters['categories']);
            if (count($categories) > 0)
                $usersRaking = $usersRaking->whereIn('campaign_categories.id', $categories);
        }


        $usersRaking = $usersRaking->groupBy('collaborators.id')
            ->orderBy('total', 'desc')
            ->orderBy('users.name', 'asc');

            //dd($usersRaking->get());

        return $usersRaking;
    }
}
