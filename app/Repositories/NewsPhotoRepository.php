<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface NewsPhotoRepository
 * @package namespace App\Repositories;
 */
interface NewsPhotoRepository extends RepositoryInterface
{
    //
}
