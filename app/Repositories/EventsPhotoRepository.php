<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EventsPhotoRepository
 * @package namespace App\Repositories;
 */
interface EventsPhotoRepository extends RepositoryInterface
{
    //
}
