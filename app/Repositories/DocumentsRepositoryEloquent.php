<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\DocumentsRepository;
use App\Models\Document;
use App\Validators\DocumentsValidator;

/**
 * Class DocumentsRepositoryEloquent
 * @package namespace App\Repositories;
 */
class DocumentsRepositoryEloquent extends BaseRepository implements DocumentsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Document::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
