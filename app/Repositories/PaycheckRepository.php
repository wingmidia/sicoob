<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PaycheckRepository
 * @package namespace App\Repositories;
 */
interface PaycheckRepository extends RepositoryInterface
{
    //
}
