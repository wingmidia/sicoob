<?php

namespace App\Repositories;

use App\Criteria\IndicationCollaboratorCriteria;
use App\Models\Campaign;
use App\Models\Indication;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Yajra\Datatables\Datatables;


/**
 * Class IndicationRepositoryEloquent
 * @package namespace App\Repositories;
 */
class IndicationRepositoryEloquent extends BaseRepository implements IndicationRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Indication::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }


    /**
     * @return mixed
     * @throws \Exception
     */
    public function getDatatables()
    {

        return Datatables::of(
            $this->model
                ->select(
                    'indications.id',
                    'indications.created_at',
                    'indications.updated_at',
                    'clients.name AS client_name',
                    'clients.cpf',
                    'clients.cnpj'
                )
                ->join('clients', 'indications.client_id', '=', 'clients.id')
                ->where('indications.collaborator_id', Auth::user()->collaborator->id)
                ->orderBy('indications.created_at', 'DESC')

            )
            ->editColumn('created_at', function($data){
                return $data->created_at->format('d/m/Y');
            })
            ->editColumn('updated_at', function($data){
                return $data->updated_at->format('d/m/Y');
            })
            ->addColumn('action_show', function ($data) {
                return route('intranet.indications.show', $data->id);
            });
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getDatatablesProspection()
    {

        return Datatables::of(
            $this->model
                ->select(
                    'indications.id',
                    'indications.created_at',
                    'indications.updated_at',
                    'indications.display_at',
                    'client_id',
                    'clients.name AS client_name',
                    'clients.cpf',
                    'clients.cnpj',
                    'clients.cellphone',
                    'indication_status.name AS status_name',
                    'indication_status.state AS status_state'
                )
                ->join('clients', 'indications.client_id', '=', 'clients.id')
                ->join('collaborator_indication', 'indications.id', '=', 'collaborator_indication.indication_id')
                ->join('indication_product', 'indications.id', '=', 'indication_product.indication_id')
                ->leftJoin('indication_status', 'indications.indication_status_id', '=', 'indication_status.id')
                ->where([
                    ['collaborator_indication.collaborator_id', Auth::user()->collaborator->id],
                    ['indication_product.concluded', false]
                ])
                ->orderBy('indication_status.state')
                ->orderBy('indications.display_at')
                ->groupBy('indications.id')
        )
            ->editColumn('created_at', function($data){
                return $data->created_at->format('d/m/Y H:m');
            })
            ->editColumn('updated_at', function($data){
                return $data->updated_at->format('d/m/Y H:m');
            })
            ->editColumn('display_at', function($data){
                return $data->display_at->format('d/m/Y H:m');
            })
            ->addColumn('action_show', function ($data) {
                return route('intranet.indications.show', $data->id);
            });
    }

    /**
     * @param $date_init
     * @param null $date_final
     * @param null $status_effective
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function getByPeriod($date_init, $date_final = null, $status_effective = null, $concluded = null){

        if($date_final === null) {
            $date_final = Carbon::createFromFormat('Y-m-d', $date_init)
                ->lastOfMonth()
                ->toDateString();
        }

        $model = $this->model
            ->join('indication_product', 'indications.id', '=', 'indication_product.indication_id')
            ->where('indications.collaborator_id', Auth::user()->collaborator->id)
            ->whereBetween('indications.created_at', [$date_init, $date_final]);

        if($status_effective !== null){

            $model = $model->leftJoin('indication_status', 'indications.indication_status_id', '=', 'indication_status.id')
                ->whereRaw("indication_status.status = $status_effective");

        }

        if($concluded !== null){

            $model = $model->where('indication_product.concluded', $concluded);

        }

        return $model;

    }

    /*public function setConcluded($concluded = 1, $id = null, $client_id= null, $product_id = null)
    {

        $indication = Indication::join('indication_product', 'indications.id', '=', 'indication_product.indication_id');

        if($id)
            $indication->where('indications.id', $id);

        if($client_id)
            $indication->where('indications.client_id', $client_id);

        if($product_id)
            $indication->where('indication_product.product_id', $product_id);


        return $indication->update(['concluded' => $concluded]);

    }*/

    public function getIndicationByParameters($client_id, $product_id){

        $indication = Indication::join('indication_product', 'indications.id', '=', 'indication_product.indication_id')
            ->where([
                ['indications.client_id', $client_id],
                ['indication_product.product_id', $product_id],
                //['indications.concluded', 0],
            ])
            ->first();

        if($indication) {

            $indication->collaborator;
            $indication->created_at_format = $indication->created_at->format('d/m/Y');

        }

        return $indication;

    }

    public function getRankingDataTables($id, $filters){

        $usersRaking = $this->getUserRanking($id, $filters);

        return Datatables::of($usersRaking)
            ->editColumn('total', function ($data){
                $valor = 0;
                if(!empty($data->total)) $valor = $data->total;
                return 'R$ '. number_format($valor, 2, ',', '.');
            });
    }

    protected function parseInt($arr){
        if(isset($arr)){
            $parseArray = [];
            foreach ($arr as $val){
                $parseArray[] = intval($val);
            }
            return $parseArray;
        }
        return [];
    }

    /**
     * @param $campaign_id
     * @param $filters
     * @return mixed
     */
    public function getUserRanking($campaign_id, $filters = [])
    {
        $usersRaking = DB::table('indications')
            ->select('collaborators.id', 'collaborators.name', DB::raw('count(distinct(indications.id)) as indications'), 'collaborators.photo')
            ->leftJoin('collaborators', 'collaborators.id', '=', 'indications.collaborator_id')
            ->leftJoin('users', 'users.id', '=', 'collaborators.user_id')
            ->leftJoin('production_product', 'production_product.indication_id', '=', 'indications.id')
            ->leftJoin('products', 'products.id', '=', 'production_product.product_id')
            ->leftJoin('campaign_categories', 'campaign_categories.category_id', '=', 'products.category_id')
            ->where('indications.indication_status_id', '9001')
            ->where('campaign_categories.campaign_id', $campaign_id)
            ->whereNotNull('production_product.indication_id')
            ;

        if (isset($filters['departaments'])) {
            $departaments = $this->parseInt($filters['departaments']);
            if (count($departaments) > 0)
                $usersRaking->leftJoin('collaborator_departaments', 'collaborator_departaments.collaborator_id', '=', 'collaborators.id')
                ->whereIn('collaborator_departaments.departament_id', $departaments);
                //$usersRaking = $usersRaking->whereIn('clients.departament_id', $departaments);
        }

        if (isset($filters['categories'])) {
            $categories = $this->parseInt($filters['categories']);
            if (count($categories) > 0)
                $usersRaking->whereIn('campaign_categories.category_id', $categories);
        }

        $usersRaking->groupBy('indications.collaborator_id')
            ->orderBy('indications', 'desc')
            ->orderBy('collaborators.name', 'asc');
        return $usersRaking;
    }

}
